﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

//Script sur la camera principale qui créé un pipeline de rendu pour effectuer des effets spéciaux !
//highLight material a utiliser avec OccludeOverlay shader
//DrawReverbMaterial à utiliser avec le shader DepthRenderGlow
//ajouter BlurOptimized à la camera avec ce script (images effects) et le déactiver

public class GlowManager : MonoBehaviour {

	private CommandBuffer m_renderBuffer = null;
	public  List<GameObject> highLightObjects;//objets auxquels on souhaite ajouter du glow
    public List<GameObject> hightObjectsAdditive;//objets supplémentaires clients pour le glow ajoutés dans l'inspecteur directement
	public List<GameObject>  hightObjectsPlane;//les objets plane du IGN concerné
    public List<Material> m_matAdditive;//Matériaux pour les objets supplémentaires//à synchroniser avec les la liste des mat additive
    private List<Material> m_drawMaterial;//liste de matériaux de glow avec le shader DepthRenderClassique (glow produit grace à la map de depthTexure).
    public List<Material> m_drawReverbMaterial;//liste des reverbZone matériaux de glow avec le shader DepthRenderClassique
    public Material m_drawMaterialDefault;//Mat par défaut pour faire du glow ou des contours (shader "glow" pour contour et shader "DepthRenderGlow" pour glow général)
	private List<float> defaultFadeDistance;
	private List<float> defaultIntensity;
	private List<Color> defaultColor;
	public float globalIntensity;//a définir dans l'inspecteur pour augmenter l'effet global
    public Material m_highLitghtMaterial;//matérial avec le shader Occlude/overlay pour rendre le glow lors du rendu final
	public Shader DepthRenderGlow;
    public BlurOptimized m_blur;

	private void Awake(){
		CreateBuffers();
		m_blur = this.GetComponent<BlurOptimized>();
		m_blur.enabled = false;//il faut le déactiver sinon il va flouter toute l'image
		DepthRenderGlow = Shader.Find("Custom/DephRenderGlow");
        highLightObjects = new List<GameObject>();//objets auxquels on souhaite ajouter du glow
        hightObjectsPlane = new List<GameObject>();//les objets plane du IGN concerné
        m_drawMaterial = new List<Material>();//liste de matériaux de glow avec le shader DepthRenderClassique (glow produit grace à la map de depthTexure).
}
    //fonction qui crée du glow sur l'ensemble des reverbZone et des objets additifs ajoutés dans l'inspecteur
    public void ClearAndResetGlow(List<GameObject> reverbGo){//fonction activée au start sur zoneManager en cas de changement de zone et sur IGN_AudioGUI au moment de OpenIventory
		if(highLightObjects.Count > 0)
        {
			highLightObjects.Clear();
			m_drawMaterial.Clear();
		}
		foreach( GameObject reverb in GameManager.instance.reverbZoneGO){
			foreach(Transform SphereSound in reverb.transform){
				AddHighLightObjects(SphereSound.gameObject, null, 1.0f, 1.0f,RandomColor());//ajout aussi d'un matériau par défaut pirs au hasard dans une liste
			}
		}
        foreach(GameObject additif in hightObjectsAdditive)//remet les objets additifs dans le tableau des objets à traiter pour le glow
        {
            highLightObjects.Add(additif);
            m_drawMaterial.Add(m_drawMaterialDefault);
        }
	}

    //fonction qui fait du glow uniquement sur la reverbzone d'influence et sur les objets additifs
    public void UpdateGlow(GameObject reverbGo)
    {//fonction activée au start et sur zoneManager en cas de changement de zone et sur IGN_AudioGUI au moment de Quitinventory
        if (highLightObjects.Count > 0)
        {
            highLightObjects.Clear();
            m_drawMaterial.Clear();
        }
        foreach (Transform SphereSound in reverbGo.transform)
        {
            AddHighLightObjects(SphereSound.gameObject, null, 1.0f, 1.0f, RandomColor());//ajout aussi d'un matériau par défaut pris au hasard dans une liste
        }
        if (hightObjectsAdditive.Count > 0)
        {
            for (int i = 0; i< hightObjectsAdditive.Count;i++)
            {
                highLightObjects.Add(hightObjectsAdditive[i]);
                m_drawMaterial.Add(m_matAdditive[i]);
            }
        }

	}

	private void AddHighLightPlane(GameObject addit){//permet de mettre à jour le tableau des objets supplémentaire dont on souhaite le glow runtime (déclenché sur le script surfDeform)
		if(hightObjectsPlane.Count>0){
			RemouveHighLightObjects(hightObjectsPlane[0]);
			hightObjectsPlane.Clear();
		}
		hightObjectsPlane.Add(addit);
		AddHighLightObjects(addit, m_drawMaterialDefault, m_drawMaterialDefault .GetFloat("_FadeDistance"), m_drawMaterialDefault.GetFloat("_Intensity"), m_drawMaterialDefault.GetColor("_Color"));
	}

	private Color RandomColor(){
		int rand = Random.Range(0,6);
		Material mat = m_drawReverbMaterial[rand];
		return mat.color;
	}

	private void OnEnable(){
        defaultFadeDistance = new List<float>();
        defaultIntensity = new List<float>();
        defaultColor = new List<Color>();
        if (hightObjectsAdditive != null){
			for (int uu=0;uu<hightObjectsAdditive.Count;uu++){
				defaultFadeDistance.Add( m_matAdditive[uu].GetFloat("_FadeDistance"));
				defaultIntensity.Add(m_matAdditive[uu].GetFloat("_Intensity"));
				defaultColor.Add(m_matAdditive[uu].GetColor("_Color"));
			}
		}
	}

	private void OnDisable(){//enregistrement des valeurs par défault
		if(hightObjectsAdditive != null){
			for (int u=0;u<hightObjectsAdditive.Count;u++){
				m_matAdditive[u].SetFloat("_FadeDistance",defaultFadeDistance[u]);
				m_matAdditive[u].SetFloat("_Intensity",defaultIntensity[u]);
				m_matAdditive[u].SetColor("_Color",defaultColor[u]);
			}
		}
	}

	private void CreateBuffers(){
		m_renderBuffer = new CommandBuffer();
	}

	//fonction à utiliser pour modifier les propriétés du Glow en temps réel
	private Material ChangeMatProperties(Material m_drawMat, float m_highLitghtFadeDistance, float m_highLitghtIntensity, Color m_highLitghtColor){
		m_drawMat.SetFloat("_FadeDistance",m_highLitghtFadeDistance);
		m_drawMat.SetFloat("_Intensity",m_highLitghtIntensity);
		m_drawMat.SetColor("_Color",m_highLitghtColor);
		return m_drawMat;
	}

	public void AddHighLightObjects(GameObject highLightObj, Material mat, float fadeDistance, float intensity, Color col){
		highLightObjects.Add(highLightObj);
		if(mat == null){//mettre le matériaux mat à null pour ajouter automatiquement un matériau par défaut au nouvel object avec du glow
			Material newMat = new Material(DepthRenderGlow);
			ChangeMatProperties(newMat,30.0f, 0.5f, RandomColor());//propriétés par défaut
			m_drawMaterial.Add(newMat);//matérial par défaut
            highLightObj.GetComponent<MeshRenderer>().sharedMaterial = newMat;
		}
		else{
			ChangeMatProperties(mat,fadeDistance, intensity, col);//défini les paramètres du matériau
			m_drawMaterial.Add(mat);//l'ajoute au tableau à cet index

		}
	}

	private void RemouveHighLightObjects(GameObject highLightObj){//permet de retirer un objet de la liste et de synchroniser la liste de matériaux
		int indexx = new int();
		for(int i=0;i<highLightObjects.Count;i++){
			if (highLightObj == highLightObjects[i]){
				indexx = i;
			}
		}
		highLightObjects.RemoveAt(indexx);
		m_drawMaterial.RemoveAt(indexx);//synchro des deux listes
	}

	private void RenderHighLight( RenderTexture rt){//crée l'image floutée du contour des objects concernés en utilisant une renderTexture que l'on passe en argument
		RenderTargetIdentifier m_targetID = new RenderTargetIdentifier(rt);//définition d'une target
		m_renderBuffer.SetRenderTarget(m_targetID);//Attribution de la cible
		for(int i=0;i<highLightObjects.Count;i++){//pour tout les objets dont on souhaite afficher un contour
			Renderer renderer = highLightObjects[i].GetComponent<Renderer>();//on attrape leur renderer
			m_renderBuffer.DrawRenderer(renderer,m_drawMaterial[i],0,0);//et on dessine sur cette renderTexture les objets selectionnés avec une couleur de matériaux solide définie dans un shader
		}
		RenderTexture.active = rt;//On active la renderTexture qui va bien
		Graphics.ExecuteCommandBuffer(m_renderBuffer);//et on exécute sur le champs la commande buffer dans le pipeline de rendu
        CleanCommandBuffer();//on efface le commandeBuffer
        RenderTexture blurred = rt;//assignation d'une nouvelle renderTexture égale à celle qu'on vient de produire afin de pouvoir avoir un input et output sur le script de blur
		m_blur.OnRenderImage(rt, blurred);//on fait appel au OnRenderImage du script blur qui se charge de flouter l'image produite (soit l'object concerné en couleur solide. On fait ainsi déborder la couleur ce qui va créer un contour
        RenderTexture.active = null;//maintenant on peu déactiver cette renderTexture
	}

	private void OnRenderImage(RenderTexture source, RenderTexture destination){
		if(highLightObjects.Count > 0){//si il y à des objets dont on souhaite glowing
			//PASSE 1
			RenderTexture rt0;//defini une texture de rendu
            rt0 = RenderTexture.GetTemporary(Screen.width, Screen.height, 0, RenderTextureFormat.Default);//lui donne un format correspondant à l'écran
            RenderTexture.active = rt0;
			rt0.wrapMode = TextureWrapMode.Clamp;
			GL.Clear(true, true, Color.clear);			
			RenderHighLight(rt0);//rendu fond flouté
            
           // RenderTexture.active = null;//maintenant on peu déactiver cette renderTexture
                                        //PASSE 2
            RenderTexture rt2;//defini une texture de rendu
			rt2 = RenderTexture.GetTemporary(Screen.width, Screen.height, 0, RenderTextureFormat.Default);
            RenderTexture.active = rt2;
            rt2.wrapMode = TextureWrapMode.Clamp;
			GL.Clear(true, true, Color.clear);
			m_highLitghtMaterial.SetTexture("_OccludeMap",rt0);
			m_highLitghtMaterial.SetFloat("_Intensity",globalIntensity);//a définir dans l'inspecteur pour augmenter l'effet global
			Graphics.Blit(source, destination, m_highLitghtMaterial,1);///////////////// 1: glow général   0: contour
            RenderTexture.ReleaseTemporary(rt0);
            RenderTexture.ReleaseTemporary(rt2);
            RenderTexture.active = null;//maintenant on peu déactiver cette renderTexture
        }
	}

	private void CleanCommandBuffer(){
		m_renderBuffer.Clear();//néttoyage remise à zéro
	}
}


