using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

//script attaché à la cameraFPS pour gérer l'avancée de la camera vers le bip afin qu'elle ne traverse pas les murs + mise en route de la navigation
//la camera est un enfant de CameraMagnitude qui permet ainsi de la recentrer en cas de décalage dans l'espace lié à une collision. Pas de maniuplations directes de son transform.position

public class CameraMove : MonoBehaviour
{


    public float camUpLook;//inclinaison vers le haut de la camera//début
    public float camDownLook;//inclinaison vers le bas de la camera//fin
    private float currentDistance;
    private float startNavigation;
    private float speed;
    private bool mouveOnCurveIsRunning;
    private bool waitForGround;
    private bool checkMouseMouvement;//est-on en train de vérifier si le joeur bouge
    private float xtimesHit;
    private float ytimesHit;
    private float ztimesHit;
    public Material dissolveStartScreen;//le materiaux sur l'objet DissolveScreen enfant de la cameraFPS qui permet de dissoudre l'écran au début du niveau
    //private bool  collidingStay;
    private Vector3 magnitude;
    private string currentAnimation;
    private AnimationCurve cameraBlur;//courbe de tremblement de la camera
    private AnimationCurve acceleration;//vitesse de rapprochement du bip
    public bool aperture;//commande le floutage de la camera
    public bool motionBlur;
    public bool eyeFish;
    public bool fieldOfViewCamera;
    public bool contrast;
    public bool noise;
    public bool blurOptimizedGo;
    public bool bloomEffect;
    public bool tonemappingEffect;
    public bool vignetting;
    public bool edgeDetection;
    private bool blind;
    private float targetPosDistance;//position idéale dans l'absolue
    private float mini;
    private float distToPlayer;
    private float radius;
    private float timer;
    private float playerDistance;
    private float securityZone;//la distance à partir de laquelle on calcul toutes les frame une adaptation de la camera pour ne pas collider avec les murs
    private float emergencyZone;//on entre dans le collider de la cam. Adaptation de sa position rapide
    private float camGlobalSpeed;//vitesse de déplacement de la camera
    private float idealPosOffset;//offset position idéale de la camera derrière le joueur.Pour mathilde default = 20
    private float securityZoneValueOffset;//Adaptation de securityZone en cas de contact avec le joeur
    private float timeRate;//Time.deltaTime ou Time.FixedDeltaTime
    private Vector3 oldCamPos;//positionCam lors du check (invokeRepeating)
    private Vector3 delta;
    private Vector3 adapt;//ensemble des adaptation de la position de la camera sur 6 axes
    private Vector3 adaptSecurty;
    private float timerRot;

    private static ScreenOverlay screenOverlay;
    private MotionBlur motionBlurEffect;
    private BlurOptimized blurOptimized;
    private Fisheye fisheye;
    private NoiseAndGrain noiseAndGrain;
    private GameObject magnitudeCamera;
    private DepthOfField depthOfField;
    private ContrastStretch contrastEnhance;
    private BloomAndFlares bloomAndFlares;
    private TonemappingPerso tonemappingPerso;
    private VignetteAndChromaticAberration vignette;
    private EdgeDetection edge;
    private PlayerControler playerControler;
    private MagnetudeCameraLookAt magnetudeCameraLookAt;
    private ParticuleManager particuleManager;
    private SplineManager splineManager;
    private SkyboxManager skyboxManager;
    private GameObject merCollide;//le collider du niveau de la mer

    private Ray[] camRay;//les 6 ray de la camera up, down, right, left, fornt, back
    private Dictionary<int, float> camDistanceList;//list des ray tirés depuis la camera afin de l'ajuster et de ne pas traverser les murs et la distance de la cam au murs
    private Dictionary<int, float> oldCamDistanceList;//stock les anciennes valeurs de distance avant de vider le tableau
    private Vector3[] axes;//axes de déplacement
    private Dictionary<int, float> adaptSecurityZone;//les axes à traiter avec les valeurs de speedRate pour la zone de sécurité
    private Dictionary<int, float> adaptEmergencyZone;//les axes à traiter avec les valeurs de speedRate pour la zone emergency

    private Collider[] hitColliders;
    private int i;
    private int increment;
    private Collision collidingCol;//le go qui collide avec la camera à l'instant T
    private GameObject magnetudeCamera;//le go du player sur lequel on peut aligner la camera.
    private VehiculeManager vehiculeManager;//roue droite du véhicule
    private GameObject player;//mis à jour grace à event system sur gameManager
    public Rigidbody playerRB;
    private float prog;

    public delegate void BoiteAVitesse();
    public BoiteAVitesse boiteAVitesse;

    private void Awake()
    {
        magnetudeCamera = GameObject.Find("MagnitudeCamera");//empty GO pour connaitre l'alignement de départ de la camera. Reste placé idéalement derrière le biped
        playerControler = GameManager.gameControl.GetComponent("PlayerControler") as PlayerControler;
        magnetudeCameraLookAt = magnetudeCamera.GetComponent<MagnetudeCameraLookAt>();
        skyboxManager = GameManager.gameControl.GetComponent<SkyboxManager>();
        particuleManager = GameManager.gameControl.GetComponent<ParticuleManager>();
        splineManager = GameObject.Find("SplineManager").GetComponent<SplineManager>();
        merCollide = GameObject.Find("MerCollide");
        vehiculeManager = GameObject.Find("VehiculesRoot").GetComponent<VehiculeManager>();
    }

    private void PlayerListener(GameObject e)
    {
        player = e;
        playerRB = e.GetComponent<Rigidbody>();
    }

    private void Listener(string e)//reçoit de l'éventSystem le string qui définit l'état du moteur. Permet de choisir la bonne méthode pour le delegate boite à vitesse
    {
        if (e == "Neutral")
        {
                boiteAVitesse = Neutral;
        }
        if (e == "MarcheAvant")
        {
                boiteAVitesse = Neutral;
        }
        if (e == "MarcheArriere")
        {
            boiteAVitesse = Neutral;
        }
        if (e == "Jump")
        {
            boiteAVitesse = Jump;
        }
        if (e == "Transit")
        {
            boiteAVitesse = Transit;
            StartCoroutine(MotionBlurEffect());
        }
        if (e == "Inventory")
        {
            boiteAVitesse = Transit;
        }
        if (e == "EnVoiture")
        {
            boiteAVitesse = EnVoiture;
        }
        if (e == "Train")
        {
            boiteAVitesse = EnVoiture;
        }
        if (e == "Wagon")
        {
            boiteAVitesse = Wagon;
            Debug.Log("Wagon1");
        }
        if (e == "Bird")
        {
            boiteAVitesse = Bird;
        }
    }

    public void Neutral()//speedMarche est compris entre 0 et 0
    {
        timeRate = Time.fixedDeltaTime;
        //ROTATION
        Vector3 magnetudeCameraPos = magnetudeCamera.transform.position;
        Quaternion rotWheels = Quaternion.Euler(0f, vehiculeManager.steering, 0f);
        Vector3 lookAtPlayer = Vector3.Normalize(magnetudeCameraPos - transform.position);
        SpeedPlusMoins(3f);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotWheels * Quaternion.LookRotation(lookAtPlayer) * Quaternion.Euler(xtimesHit, ytimesHit, ztimesHit), Time.fixedDeltaTime * speed);//speed est défini dans start puis dans mouveClassique
        //POSITION
        if (GameManager.instance.navigating)
        {
            timeRate = Time.fixedDeltaTime;
            Vector3 forwardVector = Vector3.Normalize(magnetudeCamera.transform.forward) * idealPosOffset;//forward vector
            Vector3 idealPos = magnetudeCamera.transform.position - forwardVector;
            Vector3 playerPos = magnetudeCamera.transform.position;
            playerDistance = Vector3.Distance(playerPos, transform.position);//distance entre le joueur et la camera active
            idealPos = idealPos + delta;//delta est calculé par la fonction Blind et permet de pousser la camera sur le coté lorsque le joueur est caché par un collider
            idealPos += adaptSecurty * camGlobalSpeed;
            transform.position = Vector3.Lerp(transform.position, idealPos, Time.fixedDeltaTime * speed);
        }
    }

    public void Jump()//speedMarche est compris entre 0 et 0
    {
        timeRate = Time.fixedDeltaTime;
        //ROTATION
        Vector3 magnetudeCameraPos = magnetudeCamera.transform.position;
        Vector3 raydirectionPointer = GameManager.instance.raydirectionpointer;//direction de la sourie à l'écran
        Vector3 lookAtPlayer = Vector3.Normalize(player.transform.position - transform.position);
        Vector3 direction = new Vector3(raydirectionPointer.x, lookAtPlayer.y, raydirectionPointer.z);//reduit cette direction pour ne pas qu'elle bouge sur l'axe y
        if (direction != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction) * Quaternion.Euler(xtimesHit, ytimesHit, ztimesHit), Time.fixedDeltaTime);//speed est défini dans start puis dans mouveClassique
        }
        //POSITION
        if (GameManager.instance.navigating)
        {
            timeRate = Time.fixedDeltaTime;
            Vector3 forwardVector = Vector3.Normalize(magnetudeCamera.transform.forward) * idealPosOffset;//forward vector
            Vector3 idealPos = magnetudeCamera.transform.position - forwardVector;
            Vector3 playerPos = magnetudeCamera.transform.position;
            playerDistance = Vector3.Distance(playerPos, transform.position);//distance entre le joueur et la camera active
            idealPos = idealPos + delta;//delta est calculé par la fonction Blind et permet de pousser la camera sur le coté lorsque le joueur est caché par un collider
            idealPos += adaptSecurty * camGlobalSpeed;
            transform.position = Vector3.Lerp(transform.position, idealPos, Time.fixedDeltaTime * speed);
        }
    }

    public void Transit()
    {
        timeRate = Time.deltaTime;
        //ROTATION
        Vector3 magnetudeCameraPos = magnetudeCamera.transform.position;
        Vector3 lookAtPlayer = magnetudeCameraPos - transform.position;
        SpeedPlusMoins(3f);//augmente la valeur de speed
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(lookAtPlayer) * Quaternion.Euler(xtimesHit, ytimesHit, ztimesHit), Time.deltaTime * speed);//speed est défini dans start puis dans mouveClassique
        //POSITION                                                                                                                                                                           //POSITION
        if (GameManager.instance.navigating)
        {
            timeRate = Time.deltaTime;
            Vector3 forwardVector = Vector3.Normalize(magnetudeCamera.transform.forward) * idealPosOffset;//forward vector
            Vector3 idealPos = magnetudeCamera.transform.position - forwardVector;
            Vector3 playerPos = magnetudeCamera.transform.position;
            playerDistance = Vector3.Distance(playerPos, transform.position);//distance entre le joueur et la camera active
            idealPos = idealPos + delta;//delta est calculé par la fonction Blind et permet de pousser la camera sur le coté lorsque le joueur est caché par un collider
            idealPos += adaptSecurty * camGlobalSpeed;
            transform.position = Vector3.Lerp(transform.position, idealPos, Time.deltaTime * speed);
        }
    }

    public void EnVoiture()//speedMarche est compris entre 0 et 0
    {
        timeRate = Time.deltaTime;
        //ROTATION
        Vector3 magnetudeCameraPos = magnetudeCamera.transform.position;
        Quaternion rotWheels = Quaternion.Euler(0f, vehiculeManager.steering, 0f);
        Vector3 lookAtPlayer = Vector3.Normalize(magnetudeCameraPos - transform.position);
        SpeedPlusMoins(3f);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotWheels * Quaternion.LookRotation(lookAtPlayer) * Quaternion.Euler(xtimesHit, ytimesHit, ztimesHit), Time.deltaTime * speed);//speed est défini dans start puis dans mouveClassique
        //POSITION
        if (GameManager.instance.navigating)
        {
            timeRate = Time.deltaTime;
            Vector3 forwardVector = Vector3.Normalize(magnetudeCamera.transform.forward) * idealPosOffset;//forward vector
            Vector3 idealPos = magnetudeCamera.transform.position - forwardVector;
            Vector3 playerPos = magnetudeCamera.transform.position;
            playerDistance = Vector3.Distance(playerPos, transform.position);//distance entre le joueur et la camera active
            idealPos = idealPos + delta;//delta est calculé par la fonction Blind et permet de pousser la camera sur le coté lorsque le joueur est caché par un collider
            idealPos += adaptSecurty * camGlobalSpeed;
            transform.position = Vector3.Lerp(transform.position, idealPos, Time.deltaTime * speed);
        }
    }

    public void Wagon()
    {
        timeRate = Time.deltaTime;
        //ROTATION
        Vector3 magnetudeCameraPos = magnetudeCamera.transform.position;
        Vector3 raydirectionPointer = GameManager.instance.raydirectionpointer;//direction de la sourie à l'écran
        if (raydirectionPointer != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(raydirectionPointer) * Quaternion.Euler(xtimesHit, ytimesHit, ztimesHit), timeRate);//speed est défini dans start puis dans mouveClassique
        }
        //POSITION
        if (GameManager.instance.navigating)
        {
            timeRate = Time.deltaTime;
            Vector3 forwardVector = Vector3.Normalize(magnetudeCamera.transform.forward) * idealPosOffset;//forward vector
            Vector3 idealPos = magnetudeCamera.transform.position - forwardVector;
            Vector3 playerPos = magnetudeCamera.transform.position;
            playerDistance = Vector3.Distance(playerPos, transform.position);//distance entre le joueur et la camera active
            idealPos = idealPos + delta;//delta est calculé par la fonction Blind et permet de pousser la camera sur le coté lorsque le joueur est caché par un collider
            idealPos += adaptSecurty * camGlobalSpeed;
            transform.position = Vector3.Lerp(transform.position, idealPos, timeRate * speed);
        }
    }

    private void Bird()
    {
        timeRate = Time.deltaTime;
        //ROTATION
        Vector3 magnetudeCameraPos = magnetudeCamera.transform.position;
        Vector3 lookAtPlayer = magnetudeCameraPos - transform.position;
        SpeedPlusMoins(4f);
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(lookAtPlayer) * Quaternion.Euler(xtimesHit, ytimesHit, ztimesHit), Time.deltaTime * speed);//speed est défini dans start puis dans mouveClassique
                                                                                                                                                                                     //POSITION
        if (GameManager.instance.navigating)
        {
            Vector3 forwardVector = Vector3.Normalize(magnetudeCamera.transform.forward) * idealPosOffset;//forward vector
            Vector3 idealPos = magnetudeCamera.transform.position - forwardVector;
            Vector3 playerPos = magnetudeCamera.transform.position;
            playerDistance = Vector3.Distance(playerPos, transform.position);//distance entre le joueur et la camera active
            idealPos = idealPos + delta;//delta est calculé par la fonction Blind et permet de pousser la camera sur le coté lorsque le joueur est caché par un collider
            idealPos += adaptSecurty * camGlobalSpeed;
            transform.position = Vector3.Lerp(transform.position, idealPos, Time.deltaTime * speed);
        }
    }

    public IEnumerator VignetteIntensity(float intense, float fast)//permet de descendre l'intensité du vignetting
    {
        while (vignette.intensity > intense && intense != 1f)
        {
            vignette.intensity -= Time.deltaTime * fast;
            yield return null;
        }
        while (vignette.intensity < intense && intense == 1f)
        {
            vignette.intensity += Time.deltaTime * fast;
            yield return null;
        }
        vignette.intensity = intense;

    }

    public IEnumerator ContrastGo(bool wait)
    {
        if (!contrast)
        {
            contrast = true;
            contrastEnhance.enabled = true;//active le composant
            contrastEnhance.limitMinimum = 0f;
            while (contrastEnhance.limitMinimum < 0.5f)
            {
                contrastEnhance.limitMinimum += Time.deltaTime / 3f;
                yield return null;
            }
            while (contrast && wait)
            {
                yield return null;
            }
            while (contrastEnhance.limitMinimum > 0f)
            {
                contrastEnhance.limitMinimum -= Time.deltaTime / 3f;
                yield return null;
            }
            contrastEnhance.enabled = false;//active le composant
            contrast = false;
        }
    }

    public IEnumerator EdgeDetection(bool wait)
    {
        if (!edgeDetection)
        {
            edgeDetection = true;
            edge.enabled = true;//active le composant
            edge.edgesOnly = 0f;
            while (edge.edgesOnly < 0.8f)
            {
                edge.edgesOnly += Time.deltaTime / 3f;
                yield return null;
            }
            while (edgeDetection && wait)
            {
                yield return null;
            }
            while (edge.edgesOnly > 0f)
            {
                edge.edgesOnly -= Time.deltaTime / 3f;
                yield return null;
            }
            edge.enabled = false;//active le composant
            edgeDetection = false;
        }
    }

    public IEnumerator NoiseAndGrainGo()
    {
        if (!noise)
        {
            noise = true;
            noiseAndGrain.enabled = true;//active le composant
            while (noiseAndGrain.softness < 0.97f)
            {
                noiseAndGrain.softness += Time.deltaTime;
                noiseAndGrain.intensityMultiplier += Time.deltaTime * 10f;
                yield return null;
            }
            while (noiseAndGrain.softness > 0f)
            {
                noiseAndGrain.softness -= Time.deltaTime;
                noiseAndGrain.intensityMultiplier -= Time.deltaTime * 10f;
                yield return null;
            }
            noiseAndGrain.softness = 0f;
            noiseAndGrain.intensityMultiplier = 0.25f;
            noiseAndGrain.enabled = false;//Deactive le composant
            noise = false;
        }
    }

    public IEnumerator FishEyeCam()
    {
        fisheye.enabled = true;//active le composant
        while (fisheye.strengthX < 1)
        {
            fisheye.strengthX += Time.deltaTime * 2;
            fisheye.strengthY = fisheye.strengthX;
            yield return null;
        }
        while (fisheye.strengthX > 0.4)
        {
            fisheye.strengthX -= Time.deltaTime;
            fisheye.strengthY = fisheye.strengthX;
            yield return null;
        }
    }

    public IEnumerator CameraBlur()
    {
        depthOfField.enabled = true;//active le composant
        while (depthOfField.aperture < 10)
        {
            depthOfField.aperture += Time.deltaTime * 5;
            yield return null;
        }
        while (depthOfField.aperture > 0)
        {
            depthOfField.aperture -= Time.deltaTime * 5;
            yield return null;
        }
        depthOfField.enabled = false;//Deactive le composant
    }

    public IEnumerator DepthOfField(bool go)
    {
        depthOfField.enabled = true;//active le composant
        if (go)
        {
            depthOfField.aperture = 0.4f;//valeur par default (DOF lointain)
        }
        while (depthOfField.aperture < 0.66f && go)
        {
            depthOfField.aperture += Time.deltaTime * 5f;
            yield return null;
        }
        while (depthOfField.aperture > 0.0f && !go)
        {
            depthOfField.aperture -= Time.deltaTime * 5f;
            yield return null;
        }
        if (depthOfField.aperture < 0.01f)
        {
            depthOfField.enabled = false;//active le composant
        }
    }

    public IEnumerator MotionBlurEffect()
    {
        motionBlurEffect.enabled = true;//active le composant
        motionBlurEffect.blurAmount = 0f;//reset
        while (motionBlurEffect.blurAmount < 0.9f)
        {
            motionBlurEffect.blurAmount += Time.deltaTime / 7f;
            yield return null;
        }
        while (motionBlurEffect.blurAmount > 0f)
        {
            motionBlurEffect.blurAmount -= Time.deltaTime / 10f;
            yield return null;
            motionBlurEffect.enabled = false;//Deactive le composant
        }
    }

    public IEnumerator BlurOptimzedEffect(bool wait)
    {
        blurOptimized.enabled = true;//active le composant
        blurOptimized.blurSize = 0f;
        if (!blurOptimizedGo)
        {
            blurOptimizedGo = true;
            while (blurOptimized.blurSize < 2.8f && blurOptimizedGo)
            {
                blurOptimized.blurSize += Time.deltaTime * 5;
                yield return null;
            }
            while (blurOptimizedGo && wait)
            {
                yield return null;
            }
            while (blurOptimized.blurSize > 0f)
            {
                blurOptimized.blurSize -= Time.deltaTime * 5f;
                yield return null;
            }
            if (blurOptimized.blurSize < 0f)
            {
                blurOptimized.enabled = false;//active le composant
            }
        }
    }

    public IEnumerator BloomEffect(bool wait)
    {
        bloomAndFlares.enabled = true;//active le composant
        bloomAndFlares.bloomIntensity = 0f;
        if (!bloomEffect)
        {
            bloomEffect = true;
            while (bloomAndFlares.bloomIntensity < 19f && bloomEffect)
            {
                bloomAndFlares.bloomIntensity += Time.deltaTime;
                yield return null;
            }
            while (bloomEffect && wait)
            {
                yield return null;
            }
            while (bloomAndFlares.bloomIntensity > 0f)
            {
                bloomAndFlares.bloomIntensity -= Time.deltaTime;
                yield return null;
            }
            if (bloomAndFlares.bloomIntensity < 0f)
            {
                bloomAndFlares.enabled = false;//active le composant
            }
        }
    }


    public IEnumerator TonnemappingEffect(bool wait)
    {
        tonemappingPerso.enabled = true;//active le composant
        tonemappingPerso.timer = 0f;
        if (!tonemappingEffect)
        {
            tonemappingEffect = true;
            while (tonemappingPerso.timer < 4f && tonemappingEffect)
            {
                tonemappingPerso.timer += Time.deltaTime / 3f;
                tonemappingPerso.UpdateTonemapping();
                yield return null;
            }
            while (tonemappingEffect && wait)
            {
                yield return null;
            }
            while (tonemappingPerso.timer > 0f)
            {
                tonemappingPerso.timer -= Time.deltaTime / 3f;
                tonemappingPerso.UpdateTonemapping();
                yield return null;
            }
            if (tonemappingPerso.timer < 0f)
            {
                tonemappingPerso.enabled = false;//active le composant
            }
        }
    }

    public IEnumerator Vignetting(bool wait)
    {
        if (!vignetting)
        {
            vignetting = true;
            vignette.enabled = true;//active le composant
            while (vignette.intensity < 0.35f)
            {
                vignette.intensity += Time.deltaTime / 3f;
                yield return null;
            }
            while (vignetting && wait)
            {
                yield return null;
            }
            while (vignette.intensity > 0f)
            {
                vignette.intensity -= Time.deltaTime / 3f;
                yield return null;
            }
            vignette.intensity = 0f;
            vignette.enabled = false;//active le composant
            vignetting = false;
        }
    }

    public void StopEffects()
    {
        bloomEffect = false;
        blurOptimizedGo = false;
        tonemappingEffect = false;
    }


    public IEnumerator CastRayAroundCam()//fonction qui tire 6 raycast depuis la camera afin de déterminer la distance de cette dernière avec les colliders environnant afin d'adapter sa position
    {
        Vector3 camPosition = transform.position;
        if (oldCamDistanceList.Count > 0)
        {
            oldCamDistanceList.Clear();
        }
        if (adaptSecurityZone.Count > 0)
        {
            adaptSecurityZone.Clear();
        }
        if (adaptEmergencyZone.Count > 0)
        {
            adaptEmergencyZone.Clear();//vide le tableau maintenant que la couroutine d'adaptation est lancée
        }
        camGlobalSpeed = Vector3.Distance(oldCamPos, camPosition) / 10f;
        camGlobalSpeed = 1f + Mathf.Clamp(camGlobalSpeed, 0f, 2f);
        oldCamPos = camPosition;
        foreach (KeyValuePair<int, float> keyss in camDistanceList)//stock les anciennes valeurs de distance avant de vider le tableau  
        {
            oldCamDistanceList.Add(keyss.Key, keyss.Value);//on l'ajoute au tableau
        }
        if (camDistanceList.Count > 0)
        {
            camDistanceList.Clear();//vide le tableau 
        }
        axes[0] = transform.right;
        Ray newRayRight = new Ray(camPosition, axes[0]);//0 -> droite
        camRay[0] = (newRayRight);
        axes[1] = -transform.right;
        Ray newRayLeft = new Ray(camPosition, axes[1]);//1 -> gauche
        camRay[1] = (newRayLeft);
        axes[2] = transform.up;
        Ray newRayUp = new Ray(camPosition, axes[2]);//2 -> up
        camRay[2] = (newRayUp);
        axes[3] = -transform.up;
        Ray newRayDown = new Ray(camPosition, axes[3]);//3 -> down
        camRay[3] = (newRayDown);
        axes[4] = -transform.forward;
        Ray newRayBack = new Ray(camPosition, axes[4]);//4 -> arrière
        camRay[4] = (newRayBack);
        axes[5] = magnetudeCamera.transform.position - camPosition;
        Ray newRayFront = new Ray(camPosition, axes[5]);//5 -> devant/ray orienté vers le player
        camRay[5] = (newRayFront);
        LayerMask colliderLayer = 1 << 8;
        if (GameManager.instance.chapitre == 1)
        {
            for (int i = 0; i < camRay.Length; i++)
            {
                RaycastHit hit;
                if (Physics.Raycast(camRay[i], out hit, securityZone * 2f, colliderLayer))//si le raycast entre en collision
                { //raycast contre le BL
                    if (hit.collider.tag == "Player" || hit.collider.tag == "Door" || hit.collider.tag == "ReverbZone" || hit.collider.tag == "Trigger")//exclusion
                    {
                        if (hit.collider.tag == "Player")
                        {
                            // playerDistance = hit.distance;//distance du joueur
                        }
                    }
                    else//si on touche un collider
                    {
                        if (hit.distance > 0f)//ray en contact avec collider
                        {
                            if (!camDistanceList.ContainsKey(i))
                            {
                                camDistanceList.Add(i, hit.distance);//ajoute la nouvelle distance trouvée
                            }
                            CheckForCamDistance(i, hit.distance);//pour cet index compare avec la vieille valeur pour voir si le mur approche
                        }
                    }
                }
            }
        }
        yield return new WaitForSeconds(0.02f);
        StartCoroutine(CastRayAroundCam());
    }

    private void CheckForCamDistance(int thisIndex, float hitDist)//regarde sur quel axe on se rapproche d'un collider
    {
        float collisionSpeed = new float();
        float collisionSpeedRate = new float();
        if (oldCamDistanceList.ContainsKey(thisIndex))//si l'index de la clefs est identique à cet index on a une ancienne valeur stockée
        {
            collisionSpeed = oldCamDistanceList[thisIndex] - hitDist; //calcul la vitesse de rapprochement du mur
            collisionSpeedRate = Mathf.Abs(collisionSpeed);//met à jour les valeurs de speedRate (vitesse de rapprochement du mur)
            collisionSpeedRate = Mathf.Clamp(collisionSpeedRate, 0f, 3f);
            if (thisIndex != 5 && thisIndex != 3)
            {
                collisionSpeedRate = Mathf.Clamp(collisionSpeedRate, 0.5f, 2f);
            }
            if (hitDist < securityZone)//si la distance de la camera au collider est inférieur à la zone de sécuritée
            {
                if (!adaptSecurityZone.ContainsKey(thisIndex))//si cet index n'est pas déja présent dans le dictionnaire
                {
                    adaptSecurityZone.Add(thisIndex, collisionSpeedRate);//on l'ajoute au dictionnaire adaptSecurityZone
                }
                if (hitDist < emergencyZone)
                {
                    if (!adaptEmergencyZone.ContainsKey(thisIndex))//si cet index n'est pas déja présent dans le dictionnaire
                    {
                        adaptEmergencyZone.Add(thisIndex, collisionSpeedRate);//on l'ajoute au dictionnaire adaptEmergencyZone
                    }
                }
            }
        }
    }

    public void OutSecurityZone()//adaptation de la camera effective// avancée de la cam sur axe 5 (securityZone) puis poussée sur axe de collision si trop près (emergencyZone)
    {
        if (playerDistance > securityZone)//tant que le player n'entre pas dans la zone de sécurité
        {
            //targetPosOffset = targetPosDistance;//reduit la position idéal de la camera afin de la rapprocher du joueur
            //idealPosOffset = Mathf.Lerp(idealPosOffset, targetPosOffset, Time.deltaTime);//recule la position idéale de la camera pour l'éloigner du player mais juste assez pour sortir de la zone de sécurité          
            if (adaptSecurityZone.Count > 0)//securityZone real thing !
            {
                //Debug.Log("security  "+ adaptSecurityZone.Count);
                float speedRate = 0f;
                List<float> valuesDico = new List<float>(adaptSecurityZone.Values);
                for (int i = 0; i < valuesDico.Count; i++)
                {
                    if (speedRate < valuesDico[i])
                    {
                        speedRate = valuesDico[i];//cherche la plus grande valeur
                    }
                }
                if (adaptSecurityZone.ContainsKey(5))//axe forward // Si il est là c'est que le raycast à rencontré autre chose que le player. Il Y a donc quelque chose qui cache le player
                {
                    Blind();//calcul du delta de poussé de la camera à droite ou à gauche
                }
                else delta = Vector3.zero;
                adaptSecurty = Vector3.Normalize(axes[5]) * (speedRate);//__________fait avancer la camera en fonction du nombre d'endroits en collision
                idealPosOffset = targetPosDistance / 3f;
                if (adaptEmergencyZone.Count > 0)//emergencyZone real thing !
                {
                    List<int> emergencylistKeys = new List<int>(adaptEmergencyZone.Keys);
                    for (int i = 0; i < emergencylistKeys.Count; i++)
                    {
                        if (emergencylistKeys[i] != 5)//! axe forward 
                        {
                            adaptSecurty = Vector3.Normalize(-axes[emergencylistKeys[i]] * speedRate);//______________
                            idealPosOffset = targetPosDistance / 4f;
                        }
                    }
                }
            }
            else//remise à zero de security et emergency car on est très proche du joueur
            {
                adaptSecurty = Vector3.zero;//________________________
            }
            //securityZoneValueOffset = Mathf.Lerp(securityZoneValueOffset, 0f, Time.deltaTime * 2f);//reduit le champs de répérage de la zone de sécurité (lié à la vitesse de déplacement de la camera)
        }
        else//continu la remise à zero et ajout d'un offset en hauteur pour dégager la camera de la zon de sécuritée
        {
            adaptSecurty = Vector3.Normalize(axes[2]) / 3f;//on touche le joueur donc on fait monter la camera
            adaptSecurty.y = Mathf.Clamp(adaptSecurty.y, 0f, 1f);
            idealPosOffset = targetPosDistance / 4f;
            // idealPosOffset = Mathf.Lerp(idealPosOffset, 0f, Time.deltaTime);//recule la position idéale de la camera pour l'éloigner du player mais juste assez pour sortir de la zone de sécurité          
        }
        if (adaptSecurty == Vector3.zero)//si plus de collisions alors on ramène doucement la position idéal à l'arrière de la camera à sa distance par défaut
        {
            idealPosOffset = Mathf.Lerp(idealPosOffset, targetPosDistance, timeRate * 0.3f);
        }
    } 

    private void Blind()
    {//calcul du delta de poussée de camera lorsque le joueur n'est plus visible
        Rigidbody rigidbody = new Rigidbody();
        if (player == GameManager.instance.genericPlayer)
        {
            rigidbody = vehiculeManager.rigidbodyVehicule;
        }
        else rigidbody = player.GetComponent<Rigidbody>();
        Vector3 angle = transform.InverseTransformDirection(rigidbody.velocity);
        float angleX = angle.x / 2f;
        if (angleX != 0.0f)
        {
            if (angleX > 0.0f)
            {
                delta = new Vector3(angleX, 0f, 0f);
            }
            if (angleX < 0.0f)
            {
                delta = new Vector3(0f, 0f, angleX);
            }
        }
    }

    private void SpeedPlusMoins(float maxi)
    {//function d'accélération
        speed = Mathf.Lerp(speed, maxi, timeRate * maxi);
        magnetudeCameraLookAt.speed = speed;//syncronisation
    }

    private IEnumerator CheckMouseMouvement()
    {//vérifie en sortant de l'execution des fonctions de déviation de la camera suite à une collision si la sourie bouge à l'écran
        checkMouseMouvement = true;
        Vector3 mousePosition = Input.mousePosition;
        Vector3 oldPosition = mousePosition;
        yield return new WaitForSeconds(0.2f);
        mousePosition = Input.mousePosition;
        while (mousePosition == oldPosition)
        {//le joeur est inactif
            oldPosition = Input.mousePosition;
            if (increment < 5)
            {
               // mouve = MouveAvant;
            }
            //else
            //mouve = MouveColliding;
            yield return null;
        }
        increment = 0;
        checkMouseMouvement = false;
    }

    private void CreateRT()//crée une texteure blanche qui prends tout l'écran ua démarrage du niveau
    {
        dissolveStartScreen.SetFloat("_Width", Screen.width);
        dissolveStartScreen.SetFloat("_Height", Screen.height);
        dissolveStartScreen.SetFloat("_StepX", 0.0f);
        Color col = new Color(0.0f, 0.2f, 0.6f, 1.0f);
        dissolveStartScreen.SetColor("_Color", col);
    }


    private IEnumerator DissolveStartScreen()//dissout l'écran blanc petit à petit
    {
        yield return new WaitForSeconds(2f);
        float stepxStart = 0.0f;
        while (stepxStart < 1f)
        {
            stepxStart = Mathf.Lerp(stepxStart, 1.5f, Time.deltaTime / 5f);
            dissolveStartScreen.SetFloat("_StepX", stepxStart);
            yield return null;
        }
        GameObject cameraOnTop = GameObject.Find("CameraOnTop");
        Destroy(cameraOnTop);
    }

    private void OnEnable()
    {
        EventManagerPlayer.NewGear += Listener;//souscrit a l'évent system qui permet de déterminer grace à un string dans quel état est le moteur du vehicule
        GameManager.NewPlayer += PlayerListener;//souscrit à l'évent system qui défini le bon joueur
        PlayerListener(GameManager.instance.player);//malgrés qu'on est souscrit au event system il faut définir le player de manière classique la premère fois car l'evenement à lieu en tout premier sur le script GameManager
        if (this.gameObject.name == "CameraFPS")
        {
            screenOverlay = GetComponent<ScreenOverlay>();
            motionBlurEffect = GetComponent<MotionBlur>();
            noiseAndGrain = GetComponent<NoiseAndGrain>();
            contrastEnhance = GetComponent<ContrastStretch>();
            blurOptimized = GetComponent<BlurOptimized>();
            depthOfField = GetComponent<DepthOfField>();
            bloomAndFlares = GetComponent<BloomAndFlares>();
            fisheye = GetComponent<Fisheye>();
            tonemappingPerso = GetComponent<TonemappingPerso>();
            vignette = GetComponent<VignetteAndChromaticAberration>();
            edge = GetComponent<EdgeDetection>();
        }
        motionBlur = false;
        startNavigation = 1;
    }

    public void OnDesable()
    {
        EventManagerPlayer.NewGear -= Listener;//de-inscription à l'envent system
        GameManager.NewPlayer -= PlayerListener;
    }

    private void Start()
    {
        xtimesHit = 0f;
        ytimesHit = 0f;
        ztimesHit = 0f;
        camDistanceList = new Dictionary<int, float>();
        oldCamDistanceList = new Dictionary<int, float>();
        adaptSecurityZone = new Dictionary<int, float>();
        adaptEmergencyZone = new Dictionary<int, float>();
        camRay = new Ray[6];
        axes = new Vector3[6];
        targetPosDistance = 5f;//default distance derrière le joueur pour la camera (sphere avatar)
        mini = 2.5f;
        securityZone = 1f;
        emergencyZone = 0.2f;
        increment = 0;
        blind = false;
        checkMouseMouvement = false;
        contrast = false;
        timer = 0f;
        prog = 0f;
        speed = 1f;
        blurOptimizedGo = false;
        bloomEffect = false;
        aperture = false;
        noise = false;
        radius = 2;
        fieldOfViewCamera = false;
        waitForGround = false;
        screenOverlay.enabled = false;
        fisheye.enabled = false;
        Rigidbody rigid = GetComponent<Rigidbody>();
        rigid.centerOfMass = new Vector3(0 - 1f, 0);
        StartCoroutine(BlurOptimzedEffect(false));
        StartCoroutine(CastRayAroundCam());//cast 6 ray toutes les 0.1 secondes 
    }

    private void Update()
    {
        if (boiteAVitesse == null)
        {
            Vector3 lookAtPlayer = transform.position - player.transform.position;
            SpeedPlusMoins(3f);//augmente la valeur de speed
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(lookAtPlayer), Time.deltaTime * 0.2f);
        }
        if (boiteAVitesse == Bird || boiteAVitesse == Transit || boiteAVitesse == EnVoiture || boiteAVitesse == Wagon)
        {
            //camY = Mathf.Lerp(camY, camYTarget, Time.fixedDeltaTime * speed);//mise à jour toutes les frames de la hauteur de camera
            boiteAVitesse();
            OutSecurityZone();//adapt la camera
            if (Input.GetKey(KeyCode.RightArrow))
            {
                ytimesHit++;
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                ytimesHit--;
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                xtimesHit++;
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                xtimesHit--;
            }
            xtimesHit = Mathf.Lerp(xtimesHit, 0f, Time.deltaTime / 2f);
            ytimesHit = Mathf.Lerp(ytimesHit, 0f, Time.deltaTime / 2f);
            ztimesHit = Mathf.Lerp(ztimesHit, 0f, Time.deltaTime / 2f);
        }
    }

    private void FixedUpdate()
    {
        if (boiteAVitesse == Jump || boiteAVitesse == Neutral)
        {
            //camY = Mathf.Lerp(camY, camYTarget, Time.fixedDeltaTime * speed);//mise à jour toutes les frames de la hauteur de camera
            boiteAVitesse();
            OutSecurityZone();//adapt la camera
            if (Input.GetKey(KeyCode.RightArrow))
            {
                ytimesHit++;
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                ytimesHit--;
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                xtimesHit++;
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                xtimesHit--;
            }
            xtimesHit = Mathf.Lerp(xtimesHit, 0f, Time.deltaTime / 2f);
            ytimesHit = Mathf.Lerp(ytimesHit, 0f, Time.deltaTime / 2f);
            ztimesHit = Mathf.Lerp(ztimesHit, 0f, Time.deltaTime / 2f);
        }
    }
}