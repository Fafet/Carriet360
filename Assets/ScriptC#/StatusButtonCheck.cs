﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StatusButtonCheck : MonoBehaviour,  IPointerDownHandler, IPointerExitHandler, IPointerEnterHandler, IPointerUpHandler {

	//script placé sur des sprites dont on veut récupérer les différents états lors du passage de la sourie dessus (Over, exit, click)

	public GUISystemNavig  naviSystem;
	public PlayerControler dumyMouvementControler;
	public GameObject statusText;
    private AudioManager audioManager;
    private GameObject sliderVolume;
    private bool infoBullesGo;//doit t'on afficher ou non les infos bulles ?
    private bool dark;

    public void Awake(){
		naviSystem = GameObject.Find ("SystemNavig").GetComponent<GUISystemNavig> ();
		dumyMouvementControler = GameManager.gameControl.GetComponent<PlayerControler> ();
        audioManager = GameObject.Find("AudioContainer").GetComponent<AudioManager>();
        statusText = GameObject.Find ("StatusText");
        sliderVolume = GameObject.Find("SliderVolume");
    }

    public void OnPointerDown(PointerEventData eventData)
    {///on recupère l'event system pour avoir la main lors du click.//event déclenché par un bouton UI
        GameManager.instance.NavigatingSetUp(true, GameManager.instance.player);//crée l'event qui passe navigating sur true
                                                                                //dumyMouvementControler.DefineRigidbody();
                                                                                // dumyMouvementControler.SetUpMouveChoise(2);
        if (this.gameObject.name == "Status")
        {
            Text textStatus = statusText.GetComponent<Text>();
            naviSystem.GetComponent<Animator>().SetBool("onOff", true);
            GameObject.Find("VehiculesRoot").GetComponent<VehiculeManager>().GoVehicule();
            textStatus.fontSize = 25;
        }
        if (this.gameObject.name == "Sound")
        {
            StartCoroutine(StartTimer(2, this.gameObject));
            GestionnaireSons gestionnaireSon = GameObject.Find("AudioContainer").GetComponent<GestionnaireSons>();
            AudioSourceEnvironnements audioSourceEnvironnements = GameObject.Find("AudioContainerEnvir").GetComponent<AudioSourceEnvironnements>();//les zones de sons Juliette 
            sliderVolume.GetComponent<Animator>().SetBool("onOff", true);
    }
        if (this.gameObject.name == "Air")
        {
            StartCoroutine(naviSystem.SouriGo());
        }
        if (this.gameObject.name == "Txt")
        {
            Animator anim = GameObject.Find("UIPrompteur").GetComponent<Animator>();
            bool boolOnOff = anim.GetBool("onOff");
            anim.SetBool("onOff", !boolOnOff);
        }
        if (this.gameObject.name == "Exit")
        {
            Application.Quit();
        }
        if (this.gameObject.name == "Start")
        {
            GameObject.Find("StartEnd").GetComponent<Animator>().SetBool("start", true);
        }
        if (this.gameObject.name == "StartEnd")
        {
            CameraMove cameraMouve = GameManager.instance.cameraFPS.GetComponent<CameraMove>();
            Animator anim = this.GetComponent<Animator>();
            anim.SetBool("start", false);
            if (dark)
            {
                StartCoroutine(cameraMouve.VignetteIntensity(0.05f, 0.2f));
            }
            else StartCoroutine(cameraMouve.VignetteIntensity(1f, 0.2f));
            dark = !dark;
        }
        if (this.gameObject.name == "HautCarriet")
        {
            naviSystem.GareSetUp(1);//Haut
        }
        if (this.gameObject.name == "BasCarriet")
        {
            naviSystem.GareSetUp(2);//Bas
        }
        if (this.gameObject.name == "Bordeaux")
        {
            naviSystem.GareSetUp(3);//Bordeaux
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {//idem pour le mouseOver//event déclenché par un bouton UI
        if (this.gameObject.name == "SliderVolume")
        {
            sliderVolume.GetComponent<Animator>().SetBool("onOff", false);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {//event déclenché par un bouton UI
     // dumyMouvementControler.SetUpMouveChoise(5); //met la velocity du rigidbody à zéro
        if (GameManager.instance.navigating)
        {
            if (this.gameObject.name == "Status")
            {
                Text textStatus = statusText.GetComponent<Text>();
                textStatus.fontSize = 30;
                sliderVolume.GetComponent<Animator>().SetBool("onOff", false);
            }
            if (this.gameObject.name == "Air")
            {
                StartCoroutine(StartTimer(0, this.gameObject));
                sliderVolume.GetComponent<Animator>().SetBool("onOff", false);
            }
            if (this.gameObject.name == "Txt")
            {
                StartCoroutine(StartTimer(1, this.gameObject));
                sliderVolume.GetComponent<Animator>().SetBool("onOff", false);
            }
            if (this.gameObject.name == "Sound")
            {
                StartCoroutine(StartTimer(2, this.gameObject));
            }
            if (this.gameObject.name == "Exit")
            {
                StartCoroutine(StartTimer(3, this.gameObject));
                sliderVolume.GetComponent<Animator>().SetBool("onOff", false);
            }
            if (this.gameObject.name == "Start")
            {
                StartCoroutine(StartTimer(4, this.gameObject));
                sliderVolume.GetComponent<Animator>().SetBool("onOff", false);
            }
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {//event déclenché par un bouton UI  
        if (this.gameObject.name == "SliderVolume")//son
        {
            audioManager.SetMaxLevel(sliderVolume.GetComponent<Slider>().value);
            Debug.Log(audioManager.MaxLevel + "  audioManager.MaxLevel");
        }
    }

    public void OnScrollbarOver()
    {//fonction définie dans l'inspecteur avec un event system placé sur le sprite dépourvu de bouton

    }

    public void OnScrollbarExit()
    {//fonction définie dans l'inspecteur avec un event system placé sur le sprite dépourvu de bouton

    }

    private IEnumerator StartTimer(int textIndex, GameObject destiGo)
    {
        float timer = Time.time;
        while (Time.time < timer + 0.5f && !infoBullesGo)
        {
            yield return null;
        }
        if (!infoBullesGo)
        {
            infoBullesGo = true;
            naviSystem.InfoBulleProcess(textIndex, destiGo);
            StartCoroutine(ResetTimer());
        }
    }

    private IEnumerator ResetTimer()
    {
        yield return new WaitForSeconds(3f);
        if (infoBullesGo)
        {
            naviSystem.InfoBulleReset();
            infoBullesGo = false;
        }
    }

    private void Start()
    {
        infoBullesGo = false;//défault
        dark = true;
    }
}