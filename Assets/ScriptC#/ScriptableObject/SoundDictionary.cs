﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SoundDictionary
{
    public List<AudioClip> beatBox;
    public List<AudioClip> ambiance;
    public List<AudioClip> conversation;
    public List<AudioClip> fx;
    public List<AudioClip> r2d2;
    public List<AudioClip> rap;
    public List<AudioClip> expression;
    public List<AudioClip> rythme;
    public List<AudioClip> maria;
    public List<AudioClip> activation;
    public List<AudioClip> bird;
    public List<AudioClip> video;

    public SoundDictionary()
    {
        //pictoVert = new List<Sprite>(4);
        //familyName = "Empty";
    }
}
