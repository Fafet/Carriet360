﻿using UnityEngine;
using System.Collections.Generic;
//Holder de mon scriptable object qui contient la liste des pictos

public class PictoDictionaryHolder :  ScriptableObject {
	
	public List<PictoDictionary> content;
	
	public PictoDictionaryHolder(List<PictoDictionary> content){
		this.content = content;
	}
}
