﻿using UnityEngine;
using System.Collections.Generic;
//Holder de mon scriptable object qui contient la liste des pictos

public class SoundDictionaryHolder : ScriptableObject
{

    public SoundDictionary content;

    public SoundDictionaryHolder(SoundDictionary content)
    {
        this.content = content;
    }
}


