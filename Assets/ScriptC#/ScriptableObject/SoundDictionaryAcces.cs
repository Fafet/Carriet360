﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundDictionaryAcces : MonoBehaviour
{

    public SoundDictionary soundDico = new SoundDictionary();
    // Use this for initialization

    void Start()
    {
        Object o = Resources.Load("SoundDictionaryDatabase", typeof(SoundDictionaryHolder));
        SoundDictionaryHolder soundDictionaryDB = (SoundDictionaryHolder)o;
        soundDico = soundDictionaryDB.content;
    }
}


