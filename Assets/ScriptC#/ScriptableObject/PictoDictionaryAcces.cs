﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PictoDictionaryAcces : MonoBehaviour {

	public List<PictoDictionary> pictoDico = new List<PictoDictionary>();
	// Use this for initialization

	void Start () {
		Object o = Resources.Load("PictoDictionaryDatabase", typeof(PictoDictionaryHolder));
		PictoDictionaryHolder pictoDictionaryDB = (PictoDictionaryHolder)o;
		pictoDico = pictoDictionaryDB.content;	
	}
}
