﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HouseKeeper : MonoBehaviour {

    public static PictoDictionaryAcces pictoDicoAccess;
    private PictoDictionary pictoDico;
    private SkyboxManager skyboxManager;
    private ParticuleManager particuleManager;
    private AudioManager audioManager;
    private CameraMove cameraMove;
    private List<GameObject> intInstances;//liste des interieurs instantiés runtime
    private List<GameObject> sculptrisInstances;//liste des objets sculptris instantiés runtime
    private List<string> intDone;
    private List<string> sculptrisDone;
    private GameObject immeubleConcern;//l'immeuble ou la maison à partir de laquelle l'évent à été déclenché
    private Vector3 immeubleStartPos;//position initile de l'immeuble afin de pouvoir le remettre à sa place
    private GameObject gabarit;
    private MeshRenderer gabaritMeshRenderer;
    public bool intOn;//as t'on déja créé les interieur ?
    public bool sculptrisOn;//as t'on déja créé des objets sculptris

    private void IntListener(GameObject e)
    {
        if (e != null)//c'est le cas lorqu'on sort d'un collider de maison par exemple. Alors un nouvel event est émis afin d'autoriser à nouveau de planter des arbres
        {
            SetIntOn(e);//active/deactive les mesh render de l'ancien et du nouveau intérieur//mets aussi intOn sur false
            if (intOn)
            {
                SetUpInterieur();//mets en place les instance d'int prises au hasard
                if (!audioManager.mariaIsPlaying)//si Maria n'est pas déja en train de parler
                {
                    audioManager.AddAudio("maria", false, false, 0f, 0);
                }
            }
            if (sculptrisOn)
            {
                SetUpSculptris(e);
                audioManager.AddAudio("activation", false, false, 0f, 0);
            }
            StartCoroutine(RaiseUp(immeubleConcern));
        }
        else
        {
            if(this.gameObject.GetComponent<AudioReverbFilter>() != null)//on est sorti des maison donc plus de reverb
            {
                Destroy(this.gameObject.GetComponent<AudioReverbFilter>());
            }
        }
    }

    private void SetIntOn(GameObject immeubConcern)
    {
        if (intOn)//si on est deja passé par un collider qui à mis en route la création et l'affichage des intérieurs
        {
            for(int i=0;i<intInstances.Count;i++)
            {
                StartCoroutine(DestroyForce(i, intInstances));//detruit les int récement créés
            }
            if (gabaritMeshRenderer != null)
            {
                gabaritMeshRenderer.enabled = true;//remise en route du meshRender
            }
            StartCoroutine(RaiseDown(immeubleConcern,immeubleStartPos));
            intOn = false;
        }
        if (sculptrisOn)//si on est deja passé par un collider qui à mis en route la création et l'affichage des intérieurs
        {
            for (int i = 0; i < sculptrisInstances.Count; i++)
            {
                StartCoroutine(DestroyForce(i, sculptrisInstances));//detruit les int récement créés
            }
            StartCoroutine(RaiseDown(immeubleConcern,immeubleStartPos));
            sculptrisOn = false;
        }
        immeubleConcern = immeubConcern;//immeuble dans lequel on viens de pénétrer
        foreach (Transform interieur in immeubConcern.transform)
        {
            if (interieur.gameObject.name.Contains("Int_"))//on récupère le gabarit
            {
                gabarit = interieur.gameObject;//trouve et prends le gabarit
                gabaritMeshRenderer = gabarit.GetComponent<MeshRenderer>();
                gabaritMeshRenderer.enabled = false;//deactive le meshRender
                intOn = true;
            }
        }
        if (!intOn)
        {
            sculptrisOn = true;
        }
    }

    private void SetUpInterieur()
    {
        Vector3 gabaritSize = gabarit.GetComponent<MeshRenderer>().bounds.size;
        // Debug.Log(gabaritSize + "   gabarit size");
        float gabaritSmallest = GetSmallestSide(gabaritSize);
        float howManyInt = Mathf.FloorToInt(gabaritSize.x / gabaritSmallest);//recupère les plus petite valeur avant la virgule en longueur et en hauteur
        LayerMask colliderMask = 1 << 8;
        if (howManyInt > 0f)
        {
            for (int i = 0; i < howManyInt; i++)
            {
                GameObject intConcernX = RandomInt(pictoDico.prefabInt);//prend un Int au hasard
                intConcernX.transform.rotation = gabarit.transform.rotation;
                float resteX = (gabaritSize.x - (gabaritSmallest * howManyInt)) / howManyInt;
                Vector3 offsetX = -gabarit.transform.right * (gabaritSize.x / 2f);//place le point d'insertion à la limite gauche du gabarit puis le décale d'un demi en fonction du nombre d'instances de Int
                Vector3 decalageX = gabarit.transform.right * ((gabaritSmallest / 2f)) + gabarit.transform.right * (gabaritSmallest * i) + gabarit.transform.right * resteX;
                Vector3 pos = gabarit.transform.position + offsetX + decalageX + new Vector3(0f,8f,0f);//on decale le tir vers le haut
                Ray newRay = new Ray(pos, -transform.up);
                RaycastHit hit;
                if (Physics.Raycast(newRay, out hit, 80f, colliderMask))//si on touche un collider
                {
                    intConcernX.transform.position = hit.point;
                }
                else intConcernX.transform.position = gabarit.transform.position + offsetX + decalageX;               
            }
        }
    }

    private void SetUpSculptris(GameObject maisonConcern)
    {
        GameObject sculptrisConcern = RandomInt(pictoDico.prefabSculptris);//prend un objetSculptris au hasard dans le SO
        sculptrisConcern.transform.position = maisonConcern.transform.position;
    }

    private IEnumerator RaiseUp(GameObject objToRaise)//fonction commune aux immeubles et aux maisons
    {
        int rand = Random.Range(1, skyboxManager.skyboxList.Count-1);//prends une skybox au hasard
        particuleManager.ParticuleOn(objToRaise.transform.position, 8, 5f);
        skyboxManager.SetUpNuageChange(rand, 1f, 0.0f,false);//blackSkybox, fog 0
        immeubleStartPos = objToRaise.transform.position;
        float targetHauteur = objToRaise.GetComponent<BoxCollider>().size.y + immeubleStartPos.y;
        while (objToRaise.transform.position.y < targetHauteur)
        {
            objToRaise.transform.position += new Vector3(0f, Time.deltaTime*2f, 0f);
            yield return null;
        }
    }

    private IEnumerator RaiseDown(GameObject objToRaise, Vector3 targetHauteur)
    {
        float firstTime = objToRaise.transform.position.y;
        yield return new WaitForSeconds(0.01f);
        float secondTime = objToRaise.transform.position.y;
        while(firstTime != secondTime)//si ces deux temps sont différents cela signifie que l'immeuble est encore dans la coroutine qui le fait s'élever
        {
            firstTime = objToRaise.transform.position.y;
            yield return new WaitForEndOfFrame();
            secondTime = objToRaise.transform.position.y;
            yield return null;
        }
        while (objToRaise.transform.position.y > targetHauteur.y)
        {
            objToRaise.transform.position -= new Vector3(0f, Time.deltaTime, 0f);
            yield return null;
        }
    }

    private IEnumerator DestroyForce(int index, List<GameObject> toBedestroy)
    {
        GameObject objToDestroy = new GameObject();
        if(toBedestroy[index] != null)
        {
            objToDestroy = toBedestroy[index];
            particuleManager.ParticuleOn(toBedestroy[index].transform.position, 2, 2f);
        }
        float val = 1f;
        while (val > 0.1f)
        {
            val -= Time.deltaTime / 4f;
            if (objToDestroy != null)
            {
                objToDestroy.transform.localScale = new Vector3(val, val, val);
                yield return null;
            }

        }
        if(objToDestroy != null)
        {
            toBedestroy.Remove(objToDestroy);///////////////////////::
            Destroy(objToDestroy);
        }
    }

    private GameObject RandomInt(List<GameObject> ListConcern)
    {

        List<GameObject> objNotDone = new List<GameObject>();
        if (ListConcern == pictoDico.prefabInt)
        {
            if (intDone.Count == ListConcern.Count)
            {
                intDone.Clear();
            }
            foreach (GameObject obj in ListConcern)
            {
                if (!intDone.Contains(obj.gameObject.name))
                {
                    objNotDone.Add(obj);
                }
            }
        }
        if (ListConcern == pictoDico.prefabSculptris)
        {
            if (sculptrisDone.Count == ListConcern.Count)
            {
                sculptrisDone.Clear();
            }
            foreach (GameObject obj in ListConcern)
            {
                if (!sculptrisDone.Contains(obj.gameObject.name))
                {
                    objNotDone.Add(obj);
                }
            }
        }
        int rand = Random.Range(0, objNotDone.Count);
        Debug.Log(rand);
        GameObject objConcern = (GameObject)Instantiate(objNotDone[rand]);
        objConcern.name = objNotDone[rand].name;
        objConcern.transform.localScale = new Vector3(1f, 1f, 1f);
        if(ListConcern == pictoDico.prefabInt)
        {
            intDone.Add(objConcern.gameObject.name);
            intInstances.Add(objConcern);
        }
        if (ListConcern == pictoDico.prefabSculptris)
        {
            sculptrisDone.Add(objConcern.gameObject.name);
            sculptrisInstances.Add(objConcern);
        }
        return objConcern;
    }

    private float GetSmallestSide(Vector3 reference)//prends sur les trois côté le plus petit
    {
        float sideSize = new float();
        if (reference.x < reference.y)
        {
            sideSize = reference.x;
        }
        else sideSize = reference.y;
        if (sideSize > reference.z)
        {
            sideSize = reference.z;
        }
        return sideSize;
    }

    private void Start()
    {
        pictoDicoAccess = GetComponent<PictoDictionaryAcces>();//prends la ref au so de ce GO
        pictoDico = pictoDicoAccess.pictoDico[0];//get the good SO and it's variables in HomeDictionnary script
        skyboxManager = GameManager.instance.GetComponent<SkyboxManager>();
        particuleManager = GameManager.gameControl.GetComponent<ParticuleManager>();
        audioManager = GameObject.Find("AudioContainer").GetComponent<AudioManager>();
        cameraMove = GameManager.instance.cameraFPS.GetComponent<CameraMove>();
        intInstances = new List<GameObject>();
        sculptrisInstances = new List<GameObject>();
        intDone = new List<string>();
        sculptrisDone = new List<string>();
        intOn = false;
        sculptrisOn = false;
    }

    private void OnEnable()
    {
        GameManager.NewInt += IntListener;
    }

    private void OnDesable()
    {
        GameManager.NewInt -= IntListener;
    }

}
