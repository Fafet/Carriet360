﻿using UnityEngine;
using System.Collections;

public class MouseManager : MonoBehaviour {

    // script sur GameControl
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    private Vector3 mousePosition;
	public GestionnaireSons gestionnaireSons;
    public static float yPoint;
    private float yMousePoint;
    public static float xMousePoint;
    private bool navigating;
    public Material matLineRenderer;
    private LineRenderer lineRenderer;

    private delegate void BoiteAVitesse();
    private BoiteAVitesse boiteAVitesse;


    private void Listener(string e)//reçoit de l'éventSystem le string qui définit l'état du moteur. Permet de choisir la bonne méthode pour le delegate boite à vitesse
    {
        if (e == "Neutral")
        {
            boiteAVitesse = Neutral;
        }
        if (e == "MarcheAvant")
        {
            boiteAVitesse = MarcheAvant;
        }
        if (e == "MarcheArriere")
        {
            boiteAVitesse = MarcheAvant;
        }
        if (e == "Jump")
        {
            boiteAVitesse = MarcheAvant;
        }
        if (e == "Transit")
        {
            boiteAVitesse = Neutral;
        }
        if (e == "Visite")
        {
            boiteAVitesse = Neutral;
        }
        if (e == "EnVoiture")
        {
            boiteAVitesse = Neutral;
        }
        if (e == "Wagon")
        {
            boiteAVitesse = Neutral;
        }
        if (e == "Bird")
        {
            boiteAVitesse = Neutral;
        }
    }

    public void NavigatingListener(bool e)
    {
        navigating = e;
    }

   private void Neutral()
    {
        mousePosition = Vector3.zero;
    }

    private void MarcheAvant()
    {
        mousePosition = new Vector3 (GameManager.instance.raydirection.x*yMousePoint,0f, GameManager.instance.raydirection.z);
    }


    void OnMouseEnter()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }
    void OnMouseExit()
    {
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }

    private void DrawLineRender()
    {//déssine à l'écran le trajet de destination
        lineRenderer = this.gameObject.AddComponent<LineRenderer>();
        //lineRenderer.useWorldSpace = true;
        lineRenderer.material = matLineRenderer;
        lineRenderer.SetWidth(0.5f, 0.5f);
        lineRenderer.SetVertexCount(2);
        lineRenderer.SetPosition(0, GameManager.instance.player.transform.position);
        lineRenderer.SetPosition(1, GameManager.instance.player.transform.position);
    }

    private void OnEnable()
    {
        GameManager.NewNavigation += NavigatingListener;
        EventManagerPlayer.NewGear += Listener;//inscription à l'envent system
        boiteAVitesse = Neutral;
    }

    private void OnDesable()
    {
        GameManager.NewNavigation -= NavigatingListener;
        EventManagerPlayer.NewGear -= Listener;//de-inscription à l'envent system
    }

    public void Start()
    {
        yPoint = 0.1f;//va de 0 en bas de l'écran à 1 en haut de l'écran
        yMousePoint = Screen.height / 2;//affecte le milieu de l'écran à la valeur de la sourie pour commencer
        xMousePoint = Screen.width / 2;//affecte le milieu de l'écran à la valeur de la sourie pour commencer
       ////////////////////////////:: DrawLineRender();//ajoute un "élastique" au joueur indiquant dans quelle direction il est tiré par la souris
    }

    void Update () {
        if (navigating)
        {
            yMousePoint = Input.mousePosition.y;//récupére la position y de la sourie	à l'écran sur un axe vertical
            xMousePoint = (Input.mousePosition.x - (Screen.width / 2)) / Screen.width;
            yPoint = yMousePoint / Screen.height;
            boiteAVitesse();//execution du délégat
            if (lineRenderer != null)
            {
                lineRenderer.SetPosition(0, GameManager.instance.player.transform.position);
                lineRenderer.SetPosition(1, GameManager.instance.player.transform.position + mousePosition);//GameManager.instance.cameraFPS.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f)));
            }
            if (Input.GetMouseButtonDown(1))
            {//si on clique droit et que l'on est pas en train de se rendre à una nouvel endroit par les airs		
                GameObject.Find("AudioContainer").GetComponent<AudioGUI>().OpenInventory();
            }
		}
        else
        {
            yMousePoint = 0f;//récupére la position y de la sourie	à l'écran sur un axe vertical
            xMousePoint = 0f;
            yPoint = 0f;
        }
	}
}
