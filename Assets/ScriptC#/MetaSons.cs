﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//script attaché à chaque GO de audioReverbZone ainsi que sur l'audioContainer qui contient les metadonnées de chaque reverbZone

public class MetaSons : MonoBehaviour {

    public AudioClip[] audioChapitres;//tableau de base son
    public string[] description;//tableau de base son/description
	public AudioSource audioSource;//l'audioSource de ce GO
    public AudioClip clip;
    public float clipLength;//longueur du clip
	public int indexActuel;//index de ce clip
    public float maxDistanceActuel;//distance maxi de ce clip
    public float lectTimeActuel;//ou la lecture c'est arretée dans ce clip
	private bool  go;
	private bool  go2;
	public float progressionPlus;
	public float progressionMoins;
	public TextAsset textAsset;//le text lié à cette sphereSound
	public string[] textAssetToString;//le contenu du texte à afficher sur le prompteur en fonction de l'index current position (GO GUIPrompteur)
    public int currentPosition;//la position de l'index dans le textAsset Char (GO GUIPrompteur)
	private float texteScreenOffset;//la position du texte à l'écran (GO GUIPrompteur)
	private AudioGUI audioGUI;
	private GestionnaireSons gestionnaireSons;
    private GUISystemNavig systemNavig;//

    void  Awake (){
		audioSource = GetComponent<AudioSource>();	
		gestionnaireSons = GameObject.Find("AudioContainer").GetComponent<GestionnaireSons>();
		audioGUI = GameObject.Find("AudioContainer").GetComponent<AudioGUI>();
        systemNavig = GameObject.Find("SystemNavig").GetComponent<GUISystemNavig>();

    }
	void OnEnable(){
		textAssetToString = ConvertStringToCharArray (textAsset.text);
	}

    public void SetUpAudioInfo (int indexThisReverbGo){
		clip = audioSource.clip;//récupére l'info sur le composant audioSource de ce GO
		clipLength = clip.length;//récupére l'info sur le composant audioSource de ce GO
		audioSource.time = lectTimeActuel;
		maxDistanceActuel = audioSource.maxDistance;
		indexActuel = indexThisReverbGo;//index de ce GO dans le liste de ReverbGo concerné par la lecture sur le script MétaSons
	}
	
	private static string[] ConvertStringToCharArray (string aText){ //fonction qui convertis un texteString en tableau de char
		string[] textToString = aText.Split();//convertis le texte du prompteur en Array de string
		return textToString;
	}

    public IEnumerator VolumePlus ( float volMini ,   float volMaxi ,   float seconds ,   bool kill  ){ ///////////////////temp
		if(progressionPlus < 1){
			audioSource.Play();
		}
        audioGUI.FadeColorAllButIndex(indexActuel);
        GUISystemNavig nav = systemNavig.GetComponent<GUISystemNavig>();
        nav.textAsset = textAsset;//le bon texte pour le prompteur du systemNavig
		while (audioSource.volume<volMaxi && GameManager.instance.soundPermission){
			audioSource.volume += Time.deltaTime;
			yield return null;
		}
		yield return new WaitForSeconds(seconds);
		if(seconds != 0.0f){
			StartCoroutine(VolumeMoins(kill));
		}	
	}

    public IEnumerator  VolumeMoins (bool kill  ){ ///////////////////temp
		while (audioSource.volume>0.0f){
			audioSource.volume -= Time.deltaTime;
			yield return null;
		}
		if(kill){//si Kill est vrai on détruit l'audioSource
			if(audioSource != null){
				Destroy(audioSource,0.0f);//détruit le composant AudioSource	
			}
		}
		else{
			audioSource.Stop();
			gestionnaireSons.testing = false;
		}
	}

	void  Start (){
		currentPosition = 0;
		texteScreenOffset = -302.91f;
		description = new string[3];//defini longueur tableau
		description[0] = "chapitre 1:Préliminaires";
		description[1] = "Plan BL Intérieur";
		description[2] = "chapitre 1:Préliminaires (suite)";
		//initialisation
		go = true;
		go2 = true;
		lectTimeActuel = 0.0f;
	}
}