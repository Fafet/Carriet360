﻿using UnityEngine;
using System.Collections;

public class RorchatTexture : MonoBehaviour
{

    public RenderTexture renderTexture; // renderTextuer that you will be rendering stuff on
    public Material matRender; // renderer in which you will apply changed texture
    private Camera cam;
    Texture2D texture;
    int width;
    int height;



    void Start()
    {
        cam = GameManager.instance.cameraFPS.GetComponent<Camera>();
    }

    int at = 0;
    // Update is called once per frame
    void OnPostRender()
    {

        //cam.Render();
        // RenderTexture saveActive = RenderTexture.active;
        //renderTexture = new RenderTexture(128, 128, 16);
       RenderTexture.active = renderTexture;

        renderTexture = RenderTexture.GetTemporary(128, 128, 16);

        int width = renderTexture.width;
        int height = renderTexture.height;
        Texture2D texture = new Texture2D(width, height);
        texture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        texture.Apply();
        Material matObjet = new Material(Shader.Find("Custom/RorchatCarriet"));
        matObjet.SetTexture("_MainTex", texture);
       RenderTexture.ReleaseTemporary(renderTexture);
        RenderTexture.active = null; //don't forget to set it back to null once you finished playing with it. 
    }
}
