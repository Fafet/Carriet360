//script placé sur AudioContainerEnvirnnement qui gére les sons environnement

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioSourceEnvironnements : MonoBehaviour {
	

	public AudioClip sonWind;
	public AudioClip sonPlouf;
	public AudioClip sonCameraEau;
	public AudioClip sonFrot;
	public AudioClip sonMoteurEau;
	public AudioClip sonExplosion;
	public AudioClip sonPloufEtPluie;
	public AudioClip sonMorph;
	public AudioClip sonImpact;
	public AudioClip sonOnDirait;
	public AudioClip sonMerAmbiance;
    public AudioClip sonMerBiarritz;
    public AudioClip sonInterferences;
	public AudioClip sonFlipBoard;//sons du flipBoard
	public AudioClip sonVentSynt;
	public AudioClip zoneActivation;
	public AudioClip ouverturePorte;
	public AudioClip fermeturePorte;
	public AudioClip ambianceCave;
	public AudioClip activationZone;//Noise and grain
    public AudioClip cabaneMorph;
    public AudioClip typeWriter;
    public List<AudioClip> wheatherClipList;//liste à remplir dans l'inspecteur des clips audios liées au changement de temps lors de la deuxième partie de newtopie

    public SoundCategorie soundCategorie;

    public List<AudioClip> coupsList;//ensemble des bruits de coups
    public int multipleMax;//accumule les audioSources créés en grand nombre pour les gérer (cas lors d'un éboulement qui génére un grand nombre de buits de chocs


    public AudioSource newAudio;
	public Vector3 pos;
    private List<AudioSource> audioSourceList;

    public enum SoundCategorie
    {
        coups, mer
    }

 

    public IEnumerator  Wait (){
		yield return new WaitForSeconds (0.5f);
	}
	
	public IEnumerator VolumePlus (AudioSource composant ,   float volMini ,   float volMaxi ,   float seconds ,   bool kill  ){ ///////////////////A remplacer par fonction dans GestionnaireSons
		while (composant != null && composant.volume < volMaxi )
        {
			composant.volume += Time.deltaTime/10f;
			yield return null;
		}
		yield return new WaitForSeconds(seconds);
		if(seconds > 0 && composant != null)
        {
			while (composant != null && composant.volume>volMini)
            {
				composant.volume -= Time.deltaTime/10f;
				yield return null;
			}
			if(kill){//si Kill est vrai on détruit l'audioSource
				if(composant != null){
					Destroy(composant,0);//détruit le composant AudioSource	
                   // audioSourceList.Remove(composant);
                }
			}
		}	
	}
	
	public IEnumerator  VolumeMoins ( AudioSource composant ,   float volMini ,   bool kill  ){ ///////////////////A remplacer par fonction dans GestionnaireSons
		while (composant != null && composant.volume>volMini){
            if (composant != null)
            {
                composant.volume -= Time.deltaTime;
            }
			yield return null;
		}
		if(kill){//si Kill est vrai on détruit l'audioSource
			if(composant != null){
                Destroy(composant,0);//détruit le composant AudioSource	
               // audioSourceList.Remove(composant);
            }
		}
	}
	
	public AudioSource AddAudio (AudioClip clip ,   bool loop ,    bool playAwake ,    float vol ,   int prior, bool killOldAudioS  ){
        if (killOldAudioS)
        {
            DestroyAudioSource();//detruit les précédentes audios sources après avoir baissé le son
        }
		AudioSource composant = this.gameObject.AddComponent<AudioSource>();
		composant.clip = clip;
		composant.loop = loop;
		composant.playOnAwake = playAwake;
		composant.volume = vol;
		composant.priority = prior;
		composant.Play();
       // audioSourceList.Add(composant);
		return composant;
	}

    public void AddAudioMultipleManagement(SoundCategorie categorieSon, int maxAudioSourceCount)
    {
        AudioClip choiseClip = coupsList[0];//par défaut le premier clip de la list
        if (categorieSon == SoundCategorie.coups)
        {
            choiseClip = coupsList[Random.Range(0, coupsList.Count)];
        }
        if(multipleMax < maxAudioSourceCount)
        {
            AudioSource composant = this.gameObject.AddComponent<AudioSource>();
            composant.clip = choiseClip;
            composant.loop = false;
            composant.playOnAwake = true;
            composant.volume = 0.8f;
            composant.Play();
            StartCoroutine(KillMultipleAudioSource(composant));
            multipleMax += 1;
        }
    }

    private IEnumerator KillMultipleAudioSource(AudioSource actueAudioS)
    {
        yield return new WaitForSeconds(actueAudioS.clip.length);
        Destroy(actueAudioS);
        multipleMax -= 1;
    }

	public AudioSource GetAudioSource (AudioClip clip){
		AudioSource[] audioS;
		AudioSource aud = new AudioSource();
		audioS = GetComponents<AudioSource>();//attrape tout les composants de type audioSource sur le container audioSourceEnvironnement
		foreach(AudioSource audioSou in audioS){
			if(audioSou.clip == clip){//si l'audio source à comme clip audio ce clip alors c'est l'audioSource que l'on cherche
				aud = audioSou;
			}
			else aud = null;
		}
		return aud;//on retourne donc cette source audio
	}

    public void DestroyAudioSource()
    {
        AudioSource[] audioS;
        audioS = GetComponents<AudioSource>();//attrape tout les composants de type audioSource sur le container audioSourceEnvironnement
        foreach (AudioSource audioSou in audioS)
        {
            if (audioSou != null)
            {
                StartCoroutine(VolumeMoins(audioSou, 0.0f, true));//on baisse le son
            }
        }
    }

    public void SetAmbiance(){
        AudioSource newAudioCurrent = new AudioSource();
        if(newAudioCurrent != null)
        {
            StartCoroutine(VolumePlus(newAudioCurrent, 0.13f, 0.13f, 0.1f, false));//une fois le son joué (gestion volume+ et volume-) le composant est détruit	
        }
	}

	public void  Start (){
        audioSourceList = new List<AudioSource>();
        multipleMax = 0;
    }
}