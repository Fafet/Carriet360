﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
//script attaché au Go ObjetScene qui gére l'acivation des Go en fonction des Zones

public class ZoneManager : MonoBehaviour {

	//script attaché au Go ObjetScene qui gére l'acivation des Go en fonction des Zones
	private GameObject zoneJuliette;

	public GameObject reverbZoneJuliette;
    //varirables pour connaitre la position des GO par rapport aux points de la spline
    private int goIndex;//l'index de la spline dont le go est le plus proche
    private GameObject player;
	private GameObject textPerspective;//le GO parent des prompteurs
	private GameObject environnement;
	private GameObject systemNavig;
    private GameObject guiPrompteur;
    private List<GameObject> zoneJulietteList;
    private List<GameObject> doorGO;//liste des portes à ouvrir et fermer
    private List<bool> openDoors;//liste des portes à ouvrir et fermer
    public List<Sprite> transitionScreenImg;//image à afficher au changement de niveau
    public List<string> transitionScreenTxt;// le texte à afficher au moment de la transition
    public List<Color> transitionTxtColor;// le texte à afficher au moment de la transition
    public List<Color> transitionScreenColor;// le texte à afficher au moment de la transition
    public List<TextAsset> infoToDisplayList;//infos system à afficher dans la bulle infos
    private List<GameObject> textPerspectiveList;//la liste des prompteurs dans le jeu
    private SplineManager splineManager;
	private GestionnaireSons gestionnaireSons;
    private SpriteManager1 spriteManager;
    private bool zoneChange;
	private string text;
	private GUIStyle guiStylePerso;
	private TextAsset infoToDisplay;
	private TextAsset activationToDisplay;//infos d'activation à afficher dans la bulle infos

	private PlayerControler playerControler;
	private AudioGUI audioGUI;
	private AudioSourceEnvironnements audioSourceEnvironnements;
	private SkyboxManager skyboxManager;
    private SplineData splineData;//les datas de la spline du musoir

    private delegate void ZoneActive();
	private ZoneActive zoneActive;

	private void  Awake (){
		gestionnaireSons = GameObject.Find("AudioContainer").GetComponent<GestionnaireSons>();
		playerControler = GameManager.gameControl.GetComponent<PlayerControler>();
		audioGUI = GameObject.Find("AudioContainer").GetComponent<AudioGUI>();
		systemNavig = GameObject.Find("SystemNavig");
		zoneJuliette = GameObject.Find("ZoneJuliette");
		environnement = GameObject.Find("Environnement");
		reverbZoneJuliette = GameObject.Find("ReverbZoneJuliette");
		textPerspective = GameObject.Find("CanvasPerspective");
        audioSourceEnvironnements = GameObject.Find("AudioContainerEnvir").GetComponent<AudioSourceEnvironnements>();
		skyboxManager = GameManager.instance.GetComponent<SkyboxManager> ();
        splineManager = GameObject.Find("SplineManager").GetComponent<SplineManager>();
        zoneJulietteList = new List<GameObject>();
        spriteManager = GameObject.Find("ObjetSprite").GetComponent<SpriteManager1>();
        doorGO = new List<GameObject>();//liste des portes à ouvrir et fermer
        openDoors = new List<bool>();//liste des portes à ouvrir et fermer
        textPerspectiveList = new List<GameObject>();//la liste des prompteurs dans le jeu
    }

    private void PlayerListener(GameObject e)
    {
        player = e;
    }

    private void JulietteZoneActive()
    {
        CancelInvoke();//stop toutes les fonction lancée par invokeRepeating sur ce script
        gestionnaireSons.reverbGO = gestionnaireSons.ResetSoundContext(gestionnaireSons.reverbZoneJulietteList);//met à jour le contexte des lectures
        GameManager.instance.reverbZoneGO = gestionnaireSons.reverbZoneJulietteList;
        if (GameManager.instance.chapitre == 1)
        {
            GUISystemNavig guiSystem = systemNavig.GetComponent<GUISystemNavig>();//text dans status pour le level2
            StartCoroutine(guiSystem.StatusProcess(12));//lance l'affichage au milieu de l'écran de l'info status
        }
        infoToDisplay = infoToDisplayList[0];//infos de la zone musoir
        float sunForce = 1f;
        float fogForce = 0.5f;
        float sunBiais = 72f;
        if (GameManager.instance.chapitre == 1)
        {
            sunForce = 0.2f;
            fogForce = 0.0f;
            sunBiais = 60f;
            skyboxManager.SetUpNuageChange(0, sunForce, fogForce,false);
        }
    }

    private void MusoirDZoneActive(){
		gestionnaireSons.reverbGO = gestionnaireSons.ResetSoundContext(gestionnaireSons.reverbzoneMusoirDList);//met à jour le contexte des lectures
		GameManager.instance.reverbZoneGO = gestionnaireSons.reverbzoneMusoirDList;
        GUISystemNavig guiSystem = systemNavig.GetComponent<GUISystemNavig>();//text dans status pour le level2
        infoToDisplay = infoToDisplayList[1];//infos de la zone musoir
        if (GameManager.instance.chapitre == 2)
        {
            //StartCoroutine(guiSystem.StatusProcess(4));
            skyboxManager.SetUpNuageChange(2, 1f, 0.0f,false);//blackSkybox, fog 0
        }
    }
	
	private IEnumerator ActivateGOZone(List<GameObject> zoneGO, GameObject reverbZone, string text2,List<GameObject> zoneGOToDeactivate,GameObject reverbZoneToDeactivate){//activation des GO de la zone temps que ZoneActivation est vrai
		int pas = 0;
		float timeBetweenPas = 0.02f;
		float wait = Time.time + timeBetweenPas;
		GUISystemNavig navigInfo =systemNavig.GetComponent<GUISystemNavig>();//récupère le GO Info sur le script GUISystemNavig
		navigInfo.totalPas = timeBetweenPas * zoneGO.Count;//temps total pour acitver tout les GO de la list
		while(pas<zoneGO.Count){
			if(!zoneGO[pas].activeInHierarchy ){		
				zoneGO[pas].SetActive(true);
			}
			if(Time.time>wait){
				text2 = zoneGO[pas].name;
				text = "\n"+text2;//nom du GO qu'on affecte à la variable txt/"/n" permet de faire un retour à la ligne
				pas += 1;
				wait = Time.time;
			}
			yield return null;
		}
		reverbZone.SetActive(true);
		Animator anim = navigInfo.info.GetComponent<Animator> ();//prends son animator controler
		SpriteManager.animator = anim;//puis l'affecte à la variable statique anim sur le script SpriteManager
		SpriteManager.SetOff();//afin de retirer les infos
		zoneChange = false;//on a fini d'afficher la liste des GO en train de s'activer à l'écran
    }

	private void SetUpZone(){//défini les grandes zones générales dans laquelle le joueur se trouve
		if(zoneActive != null){
			if(GameManager.instance.zoneJuliette){
				zoneActive = MusoirDZoneActive;//ajoute la nouvelle méthode
			}
			GameManager.instance.zoneJuliette = SwapBool(GameManager.instance.zoneJuliette);//inverse le flag
		}
		if(zoneActive == null){
			zoneActive = JulietteZoneActive;//méthode par défaut au demarage du jeu
			GameManager.instance.zoneJuliette = true;
		}
        if(GameManager.instance.chapitre == 1)
        {
           // playerControler.aditionalGravity = 1.0f;
        }
       //met le son d'ambiance concerné par la zone	
                                                //Physics.gravity = new Vector3(0,-200,0);
    }

	private bool SwapBool(bool boolToSwap){
		boolToSwap = !boolToSwap;
		return boolToSwap;
	}

    public void ChangeChapterActions(){//ensemble des actions à effectuer lors d'un changement de chapitre (level interne)
		if(!zoneChange){
			zoneChange = true;
            GameManager.instance.observation = true;//permet de centrer le regard de la camera au centre en changean de delegete setUpRayDirection sur DummyMouvementController
			SetUpZone();				
			zoneActive();
			StartCoroutine(ZoneChangeActions());
		}
	}


    public static IEnumerator WriteOn(Text textUI, string texToDis, bool addString, float rythme)//le composant UI text et le string à afficher en plus se celui éventuellement déja affiché
    {//écrit à la volée lettre aprés lettre
        string[] characters = new string[texToDis.Length];//crée un tableau de chaque lettre du string
        string firstString = "";//empty declaration
        if (addString)
        {
            firstString = textUI.text;
        }
        for (int i = 0; i < texToDis.Length; i++)
        {
            characters[i] = texToDis[i].ToString();
        }
        float timer = Time.time;
        int increment = 0;
        textUI.text = firstString;
        while (increment < texToDis.Length)
        {
            if (Time.time > timer + rythme)
            {
                textUI.text += characters[increment];
                increment += 1;
                timer = Time.time;
            }
            yield return null;
        }
    }

    public IEnumerator OpenPorte(GameObject door) {//fonction qui géré les listes de portes et les ouvre
        int indexDoor = 0;
        if (doorGO.Contains(door))//si la liste des porte contient déja cette porte on ne l'ajoute pas à la liste
            for (int i = 0; i < doorGO.Count; i++)
            {
                if (doorGO[i] == door)
                {
                    indexDoor = i;
                }
            }
        else {//sinon o nl'ajoute à la liste
            openDoors.Add(false);//ajoute le status de la porte qui par défaut est false
            doorGO.Add(door);//ajoute la nouvelle porte au tableau
            indexDoor = doorGO.Count-1;
            
        }
        if (!openDoors[indexDoor]) { }//si la bool openDoor de cette porte est sur false la porte n'est pas encore ouverte
        {
             Vector3 targetDoorPos = new Vector3();
            float zTargetPos = 90f;
            if (doorGO[indexDoor].name == "porteMusoirD")//si il s'agit de la porte de garage du musoir on ne l'ouvre que de 60 degrés
            {
                zTargetPos = 60f;
            }
            if (!openDoors[indexDoor])
            {
                openDoors[indexDoor] = true;//on peu ouvriri la porte à présent
                targetDoorPos = door.transform.localEulerAngles + new Vector3(0f, 0.0f, zTargetPos);
                AudioSource audioCurrent = audioSourceEnvironnements.GetAudioSource(audioSourceEnvironnements.ouverturePorte);//attrape l'audioSource qui contient le clip audio de l'ouverture de porte si il existe déja
                if (audioCurrent == null)//si il est null alors on l'ajoute
                {
                    audioCurrent = audioSourceEnvironnements.AddAudio(audioSourceEnvironnements.ouverturePorte, false, false, 0f, 1,false);//ajoute l'audioSource sur 1 frame et joue le son de ZoneActivation
                    StartCoroutine(audioSourceEnvironnements.VolumePlus(audioCurrent, 0.0f, 0.1f, 1.0f, true));//une fois le son joué (gestion volume+ et volume-) le composant est détruit	
                }
                float distance = 10f;
                while (distance > 0.2f)//ouverture progressive
                {
                    distance = Mathf.Abs(Vector3.Distance(targetDoorPos, door.transform.localEulerAngles));
                    door.transform.localEulerAngles = Vector3.Lerp(door.transform.localEulerAngles, targetDoorPos, Time.deltaTime);
                    yield return null;
                }                
            }
        }
    }

    public IEnumerator ClosePorte(GameObject door) {
        int indexDoor = 0;
        for (int i = 0; i < doorGO.Count; i++)
        {
            if (doorGO[i] == door)
            {
                indexDoor = i;
            }
        }
        if (openDoors[indexDoor])
        {
            StopCoroutine(OpenPorte(door));
            Vector3 targetDoorPos = new Vector3();
            float zTargetPos = -90f;
            if (doorGO[indexDoor].name == "porteMusoirD")//si il s'agit de la porte de garage du musoir on ne l'ouvre que de 60 degrés
            {
                zTargetPos = -60f;
            }
            if (openDoors[indexDoor])
            {
                openDoors[indexDoor] = false;//la porte est ouverte à présent
                targetDoorPos = door.transform.localEulerAngles + new Vector3(0f, 0.0f, zTargetPos);
                AudioSource audioCurrent = audioSourceEnvironnements.GetAudioSource(audioSourceEnvironnements.fermeturePorte);//attrape l'audioSource qui contient le clip audio de l'ouverture de porte si il existe déja
                if (audioCurrent == null)//si il est null alors on l'ajoute
                {
                    audioCurrent = audioSourceEnvironnements.AddAudio(audioSourceEnvironnements.fermeturePorte, false, false, 0f, 1,false);//ajoute l'audioSource sur 1 frame et joue le son de ZoneActivation
                    StartCoroutine(audioSourceEnvironnements.VolumePlus(audioCurrent, 0.0f, 0.1f, 1.0f, true));//une fois le son joué (gestion volume+ et volume-) le composant est détruit	
                }
                float distance = 10f;
                while (distance > 0.2f)
                {
                    distance = Mathf.Abs(Vector3.Distance(targetDoorPos, door.transform.localEulerAngles));
                    door.transform.localEulerAngles = Vector3.Lerp(door.transform.localEulerAngles, targetDoorPos, Time.deltaTime);
                    yield return null;
                }
            }
        }
    }

    private IEnumerator ZoneChangeActions()
    {//ensemble des actions à executer en mode zoneChange
        GUISystemNavig systemNavi = systemNavig.GetComponent<GUISystemNavig>();
        if (GameManager.instance.chapitre == 1)
        {
            //systemNavi.InfoProcess(0, text);//permet de vider la zone texte en mémoire à chaque passage dans le trigger de changement de zone		
            systemNavi.activationTx.text = null;
            StartCoroutine(systemNavi.ScrollBarProcess(0.01f));//lance la couroutine qui permet de faire défiler le texte
            AudioSource audioCurrent = audioSourceEnvironnements.AddAudio(audioSourceEnvironnements.zoneActivation, true, false, 0f, 1, true);//ajoute l'audioSource sur 1 frame et joue le son de ZoneActivation
            StartCoroutine(audioSourceEnvironnements.VolumePlus(audioCurrent, 0f, 0.1f, 2f, true));//une fois le son jouÃ© (gestion volume+ et volume-) le composant est dÃ©truit	
        }
        CameraMove cameraMove = GameManager.instance.cameraActive.GetComponent<CameraMove>();
        while (zoneChange)
        {
           
            if (GameManager.instance.zoneJuliette)
            {
                //systemNavi.InfoProcess(0, text);
            }
            yield return null;
        }
        GameManager.instance.firstVisit = true;
    }

    private void OnEnable() {
        GameManager.NewPlayer += PlayerListener;//souscrit à l'évent system qui défini le bon joueur    
    }


	private void OnDisable(){
        GameManager.NewPlayer -= PlayerListener;//souscrit à l'évent system qui défini le bon joueur
        zoneActive = null;
	}

    private void Start()
    {
        SetUpZone();//choisi quelle fonction utiliser avec le délégate pour activer/deactiver les bons GO. Mise à jour
        zoneChange = true;
        zoneActive();//lance l'action d'activation/deactivation
    } 
}

