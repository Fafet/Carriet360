﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//script général des metaSons placé sur l'audiocontainer

public class MetaSonsGlobal : MonoBehaviour {

	public Material matSphereVert;
	
	public List<float> progressLectureListJuliette;
	public List<float> progressLectureListTraining;
	public List<float> progressLectureList;//générique
	

	public List<string> titreChapitre; //tableau chapitres
	
	public List<List<AudioClip>> clipSubChapitre;
	//A remplir dans l'inspecteur
	public List<List<GameObject>> ReverbZoneGO;//2D array des reverbZone Go contenant mes clip audio des sousChapitres Juliette
	public List<GameObject> totalreverbZoneGOJuliette;
	
	void  Awake (){
		ReverbZoneGO = new List<List<GameObject>>(totalreverbZoneGOJuliette.Count);
		clipSubChapitre = new List<List<AudioClip>>(totalreverbZoneGOJuliette.Count);
		for(int t=0;t<totalreverbZoneGOJuliette.Count;t++){
			List<GameObject> ReverbGoJu = new List<GameObject>(1);//crée une liste temporaire
			List<AudioClip> ReverbAudioJu = new List<AudioClip>(1);//crée une liste temporaire
			ReverbZoneGO.Add(ReverbGoJu);//qu'on ajoute à la liste de liste
			clipSubChapitre.Add(ReverbAudioJu);
			ReverbGoJu.Add(totalreverbZoneGOJuliette[t]);//et dans laquelle on met à l'index 0 (il n'y a pas de sous chapitres) le reverbeGO à l'index t contenu dans la liste total de reverbe zone que l'on défini dans l'inspecteur
			AudioSource tempAudioSource = totalreverbZoneGOJuliette[t].GetComponent<AudioSource>();
			ReverbAudioJu.Add(tempAudioSource.clip);
            string titre = ReverbAudioJu[0].name ?? "Non définie";
			titreChapitre.Add(titre);
		}
	}
		 
	public void  FillProgressLectureList (){//fonction qui permet de remplir un tableau contenant la progression de la lecture par chapitre sur un total de 1 (1=100%)
		progressLectureListJuliette.Clear();
		foreach(GameObject lectureGO1 in totalreverbZoneGOJuliette){
			float lectTime1 = lectureGO1.GetComponent<MetaSons>().progressionPlus;
			progressLectureListJuliette.Add(lectTime1);
		}
	}
	
	void  Start (){ 
		FillProgressLectureList();
	}
	
	
	void  Update (){
		if(GameManager.instance.chapitre == 1)
        {
				for(int r=0;r<totalreverbZoneGOJuliette.Count;r++){//met à jour le tableau chaque frame
					float lectT=totalreverbZoneGOJuliette[r].GetComponent<MetaSons>().progressionPlus;//stock les infos de progression de lecture de la zone
					progressLectureListJuliette[r] = lectT;
					if(lectT>0.95f){//si on a fini ce chapitre alors la sphèreSound devient verte
						totalreverbZoneGOJuliette[r].GetComponentInChildren<MeshRenderer>().GetComponent<Renderer>().material = matSphereVert;;
					}
				}
            for (int t = 0; t < progressLectureListJuliette.Count; t++)
            { //si lecture de tout les chapitre on affiche le message 6 de trainingInstructions
                if (progressLectureListJuliette[t] > 0.95f)
                {
                    GameManager.instance.toutLuJuliette = true;
                }
                else
                    GameManager.instance.toutLuJuliette = false;
            }
		}
	}
}