﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HydroManager : MonoBehaviour {
    //script sur le GO Hydro
    public List<GameObject> cellsList;
    private List<Vector3> cellsElevationList;//élévation idéale des cellules
    public List<int> elevationLevelList;
    public List<Vector3> cellsPosList;//position de départ des cellules
    private List<Dictionary<int,int>> dicoList;
    private int longueur;//colonne totales du tableau
    private GameObject player;


    private void PlayerListener(GameObject e)
    {
        player = e;
    }

    private int GetClosestCell()
    {
        float distCell = 1000000f;
        int indexClosest = 1000;
        for(int i=0; i< cellsList.Count;i++)
        {
            float distActeulle =Mathf.Abs(Vector3.Distance(cellsList[i].transform.position, player.transform.position));
            if (distActeulle < distCell)
            {
                distCell = distActeulle;
                indexClosest = i;
            }
        }
        return indexClosest;
    }

    private void DefineElevation()
    {
        int indexCenter = GetClosestCell();//récupére le centre de calcul soit la cellule la plus proche du joueur
        int colonneCenter = Mathf.RoundToInt(indexCenter / longueur);
        int moduloCenter = new int();//la valeur du modulo pour le centre
        cellsElevationList = new List<Vector3>();
        elevationLevelList = new List<int>();
        foreach (KeyValuePair<int,int> clefs in dicoList[colonneCenter])//recherche dans la colonne qui va bien
        {
             if (clefs.Value == indexCenter)
            {
                moduloCenter = clefs.Key;//la valeur du modulo pour le centre
            }
        }
        for(int i=0;i< dicoList.Count; i++)
        {
            int elevationLevel = new int();
            foreach (KeyValuePair<int, int> keyss in dicoList[i])
            {
                int deltaColonne = Mathf.Abs(i - colonneCenter);
                deltaColonne = Mathf.Clamp(deltaColonne, 0, longueur);
                int deltaRow = Mathf.Abs(moduloCenter -keyss.Key);
                deltaRow = Mathf.Clamp(deltaRow, 0, longueur);
                if(deltaColonne == 0)
                if (deltaColonne >= 0 && deltaRow <= deltaColonne)
                {
                    elevationLevel = deltaColonne;
                  }
                if(deltaRow>=0 && deltaRow <= longueur)
                {
                    if (deltaColonne < deltaRow)
                    {
                        elevationLevel = deltaRow;
                    }
                    else elevationLevel = deltaColonne; 
                }
                if (elevationLevel >= 1)
                {
                    elevationLevel = elevationLevel - 1;
                }
                cellsElevationList.Add(cellsPosList[keyss.Value]+(new Vector3(0f, 90f * elevationLevel, 0f)));
                elevationLevelList.Add(elevationLevel);
            }
        }
       StartCoroutine(UpdateCellsPos(indexCenter));
    }

    private IEnumerator UpdateCellsPos(int indexCentre)
    {
        Vector3 oldElelvation = cellsElevationList[indexCentre];
        float oldTime = Time.time;
        while (Time.time < oldTime + 5f && cellsElevationList[indexCentre] == oldElelvation)//si l'élévation n'a pas changée et qe moins de 5 secondes sont passés
        {
            for (int i = 0; i < cellsList.Count; i++)
            {
                cellsList[i].transform.position = Vector3.Lerp(cellsList[i].transform.position, cellsElevationList[i], Time.deltaTime);//élévation effective
            }
            yield return null;
        }
    }

    private void OnEnable()
    {
        GameManager.NewPlayer += PlayerListener;
    }

    private void OnDesable()
    {
        GameManager.NewPlayer -= PlayerListener;
    }

    private void Start()
    {     
        cellsList = new List<GameObject>();
        dicoList = new List<Dictionary<int, int>>();
        cellsElevationList = new List<Vector3>();
        cellsPosList = new List<Vector3>();
        elevationLevelList = new List<int>();
        foreach (Transform cell in this.transform)//construit la liste de cellules dans l'ordre
        {
            cellsList.Add(cell.gameObject);
        }
        longueur = 20;
        int increment = 0;
        Dictionary<int, int> tempDico = new Dictionary<int, int>();
        for (int i=0;i<cellsList.Count;i++)
        {
            cellsPosList.Add(cellsList[i].transform.position);//stock les position de base des cellules
            elevationLevelList.Add(0);
            if (increment >= longueur)
            {
                dicoList.Add(tempDico);//ajoute le nouveau dictionnaire à la liste de dico
                tempDico = new Dictionary<int, int>();
                increment = 0;//remise à zero pour le nouveau dico
            }
            if (increment < longueur)
            {
                int modulo = (i + 1) % longueur;
                if(modulo == 0)//permet de faire une suite consistante de chiffres: 1,2,3....6 au lieu de ,2,3....0
                {
                    modulo = longueur;
                }
                tempDico.Add(modulo, i);//crée un dico avec d'un côté le modulo en clefs et de l'autre en valeur l'index de la liste principale de cellules
                //Debug.Log(modulo + " modulo");
                increment += 1;
            }
            if(i == cellsList.Count - 1)
            {
                dicoList.Add(tempDico);//ajoute le dernier dictionnaire à la liste de dico avant de sortir de la boucle
            }
        }
        InvokeRepeating("DefineElevation", 3f, 3f);
        DefineElevation();//cherche de quelle céllule le joueur est le plus proche
    }
}
