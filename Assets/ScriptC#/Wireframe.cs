﻿using UnityEngine;
using System.Collections;

public class Wireframe : MonoBehaviour {

	public void OnPreRender() {
		
		GL.wireframe = true;
	}
	public void OnPostRender() {
		
		GL.wireframe = false;
	}

}
