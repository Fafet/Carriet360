﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrainController : MonoBehaviour {
    //script sur le GO Train

    private SplineManager splineManager;
    private VehiculeManager vehiculeManager;
    public SplineData splineData;//les infos de la spline du train
    public BezierSpline spline;
    private GameObject trainRoot;
    private GameObject wagon;
    public List<GameObject> wagonList;
    public List<float> wagonProgression;//la position en temps sur la spline
    public List<GameObject> gare;//objet qui sybolise la gare d'arrivée
    private GameObject player;
    private GameObject trainRover;
    private GameObject concernGO;//generique soit player soit gare ou autre chose si nécéssaire
    private GameObject trainSubstitute;//substitut du train en cas de pilotage direct du train afin de garder les wagons alignés
    private float locoVelocityZ;//vitessse de déplacement du train. Négatif si marche arrière
    public float progression;//de 0 à 1. représente la distance totale de la ligne de chemin de fer
    private float lineRenderprogression;
    public float wagonOffset;//décalcage de chaque wagon pour les aligner les uns derrière les autres
    private float wagonIdealDist;//la longueur d'un wagon
    public bool trafficOn;//le train peut-il circuler
    public bool inTrain;//le joueur est-il dans le train
    private bool calcLocoSpeed;//calcul de la vitesse tant que vrai
    public float speed;//speed est calculé pour adapter la vitesse du train en fonction de sa vitesse de déplacement effectif
    private int index;

    private Vector3 p0;
    private Vector3 p1;
    private Vector3 p2;
    private Vector3 p3;

    public delegate void BoiteAVitesse();
    public BoiteAVitesse boiteAVitesse;

    private void Awake()
    {
        vehiculeManager = GameObject.Find("VehiculesRoot").GetComponent<VehiculeManager>();
        splineManager = GameObject.Find("SplineManager").GetComponent<SplineManager>();
        wagon = GameObject.Find("Wagon");
        trainRoot = GameObject.Find("TrainRoot");
    }

    private void PlayerListener(GameObject e)
    {
        player = e;
    }

    private void Listener(string e)//reçoit de l'éventSystem le string qui définit l'état du moteur. Permet de choisir la bonne méthode pour le delegate boite à vitesse
    {
        if (e == "Train")//train
        {
            if (boiteAVitesse == SetLocoPosition)//si par hasard on avait pas fini de déplacer le reste de la position de la loco il faut terminer le travail
            {
                lineRenderprogression = 0f;
                if (player.GetComponent<LineRenderer>() != null)
                {
                    Destroy(player.GetComponent<LineRenderer>());//detruit la lineRender
                }
            }
            trainSubstitute.transform.position = wagonList[0].transform.position;//position le substitut à l'endroit du train
            trainSubstitute.transform.rotation = wagonList[0].transform.rotation;//position le substitut à l'endroit du train
            wagonList[0] = trainSubstitute;//remplace le train par son substitut afin de garder les wagons alignés et en déplacement constant en faonction de la position du player
            List<GameObject> tempList = wagonList;
            wagonList = new List<GameObject>();
            foreach (GameObject list in tempList)
            {
                wagonList.Add(list);
            }
            boiteAVitesse = TrainRoverPower;
        }
        if (e == "Jump")
        {
            if (boiteAVitesse == TrainRoverPower)//si le train est piloté par le mode TrainRoverPower alors on vient de quitter le vehicule Train
            {
                if (lineRenderprogression == 0f || lineRenderprogression == 1f)//si la valeur de lineRenderProgression est 0 ou 1 cela signifie que le processus PointCurve est terminé ou pas commencé
                {
                    wagonList[0] = this.gameObject;//remplace le train par son substitut afin de garder les wagons alignés et en déplacement constant en faonction de la position du player
                    PointCurve(trainSubstitute);//on peut donc lancer PointCurve
                    boiteAVitesse = SetLocoPosition;//remet le loco en place avant de relance TrainAI
                }
            }
            if (boiteAVitesse != SetLocoPosition)
            {
                boiteAVitesse = TrainAI;
                NewTrainPosition(false, player);//le train cherche le joueur
            }
        }
        if (e == "Transit")
        {
            if (boiteAVitesse == TrainRoverPower)//si le train est piloté par le mode TrainRoverPower alors on vient de quitter le vehicule Train
            {
                if (lineRenderprogression == 0f || lineRenderprogression == 1f)//si la valeur de lineRenderProgression est 0 ou 1 cela signifie que le processus PointCurve est terminé ou pas commencé
                {
                    wagonList[0] = this.gameObject;//remplace le train par son substitut afin de garder les wagons alignés et en déplacement constant en faonction de la position du player
                    PointCurve(trainSubstitute);//on peut donc lancer PointCurve
                    boiteAVitesse = SetLocoPosition;//remet le loco en place avant de relance TrainAI
                }
            }
            if (boiteAVitesse != SetLocoPosition)
            {
                boiteAVitesse = TrainAI;
                NewTrainPosition(true, GameManager.instance.destination);//le train cherche le joueur
            }
        }
        if (e == "Wagon")
        {
            if (boiteAVitesse == TrainRoverPower)//si le train est piloté par le mode TrainRoverPower alors on vient de quitter le vehicule Train
            {
                if (lineRenderprogression == 0f || lineRenderprogression == 1f)//si la valeur de lineRenderProgression est 0 ou 1 cela signifie que le processus PointCurve est terminé ou pas commencé
                {
                    wagonList[0] = this.gameObject;//remplace le train par son substitut afin de garder les wagons alignés et en déplacement constant en faonction de la position du player
                    PointCurve(trainSubstitute);//on peut donc lancer PointCurve
                    boiteAVitesse = SetLocoPosition;//remet le loco en place avant de relance TrainAI
                }
            }
            if (boiteAVitesse != SetLocoPosition)
            {
                boiteAVitesse = TrainAI;
                NewTrainPosition(false, player);//le train cherche le joueur
            }
        }
        if (e == "Bird")
        {
            if (boiteAVitesse == TrainRoverPower)//si le train est piloté par le mode TrainRoverPower alors on vient de quitter le vehicule Train
            {
                if (lineRenderprogression == 0f || lineRenderprogression == 1f)//si la valeur de lineRenderProgression est 0 ou 1 cela signifie que le processus PointCurve est terminé ou pas commencé
                {
                    wagonList[0] = this.gameObject;//remplace le train par son substitut afin de garder les wagons alignés et en déplacement constant en faonction de la position du player
                    PointCurve(trainSubstitute);//on peut donc lancer PointCurve
                    boiteAVitesse = SetLocoPosition;//remet le loco en place avant de relance TrainAI
                }
            }
            if (boiteAVitesse != SetLocoPosition)
            {
                boiteAVitesse = TrainAI;
                NewTrainPosition(false, player);//le train cherche le joueur
            }
        }
    }

    private void SetUpTrainPosition()//mise ne place de la loco sur une frame
    {
        speed = 0.03f;
        BezierSpline spline = splineData.bezierSpline;
        int indexPlayer = splineManager.DistObjectSplinePoints(player.transform.position, splineData);//permet de savoir de quel point de la spline le player est le plus proche
        index = indexPlayer;//index générique 
        float indexToTimePlayer = splineData.indexToTime[index];
        Vector3 position = spline.GetPoint(indexToTimePlayer); //appel au calcul de courbe de bézier dans le temps (permet un déplacement le long d'une spline frame après frame)
        wagonList.Add(this.gameObject);//la première entrée du tableau est la locomotive
        wagonList.Add(wagon);//ajoute le premier wagon prééxistant
        wagonProgression.Add(indexToTimePlayer);
        wagonProgression.Add(indexToTimePlayer-wagonOffset);
        for (int i = 1; i < 10; i++)
        {
            GameObject wagonTemp = (GameObject)Instantiate(wagon, trainRoot.transform);//instantiation
            wagonTemp.transform.position = wagonList[0].transform.position;//alignement sur le train
            wagonList.Add(wagonTemp);//ajoute au tableau
            float prog = indexToTimePlayer - (wagonOffset * i);
            wagonProgression.Add(prog);//décalage vers l'arrière
        }
        wagonIdealDist = wagonList[1].GetComponent<BoxCollider>().size.z+1;
        concernGO = player;
        NewTrainPosition(false, concernGO);//mode de magnétisme indexplayer
    }

    public void NewTrainPosition(bool flashChange, GameObject newConcernGO)//change la position du train de manière brutale ou progressif (flashChange)//pour un mode magnétisme gare par exemeple si on déplace la gare
    {
        concernGO = newConcernGO;
        if (flashChange)//change de manière brutale la position du train
        {
            BezierSpline spline = splineData.bezierSpline;
            index = splineManager.DistObjectSplinePoints(concernGO.transform.position, splineData);
            float indexToTimeConcern = splineData.indexToTime[index];
            progression = indexToTimeConcern;
            for (int i = 0; i < 10; i++)
            {
                float prog = indexToTimeConcern - (wagonOffset * i);
                wagonProgression[i] = prog;
                wagonList[i].transform.position = spline.GetPoint(prog);
            }
        }
        else index = splineManager.DistObjectSplinePoints(concernGO.transform.position, splineData);//permet de changer le mode de calcul de progression et déplacer tranquillement le train à ce nouvel emplacement
    }

    private void SetLocoPosition()//reset loco position après une ballade du player avec la loco avant de repasser en mode TrainAI
    {
        float oldProgession = progression;
        BezierSpline spline = splineData.bezierSpline;
        index = splineManager.DistObjectSplinePoints(concernGO.transform.position, splineData);//mise à jour en cas de changement de position de player ou gare
        float indexToTimeConcern = splineData.indexToTime[index];
        {
            progression = Mathf.Lerp(progression, indexToTimeConcern, Time.deltaTime * speed);
        }
        progression = Mathf.Clamp(progression, 0f, 1f);
        lineRenderprogression += Time.deltaTime*0.3f;
        lineRenderprogression = Mathf.Clamp(lineRenderprogression,0f,1f);
        Vector3 velocityRay = Bezier.GetPoint(p0, p1, p2, p3, lineRenderprogression);
        if (lineRenderprogression == 1f)
        {
            Destroy(player.GetComponent<LineRenderer>());//detruit la lineRender 
            boiteAVitesse = TrainAI;//reviens au déplacement de type AI
        }
        wagonList[0].transform.rotation = Quaternion.Slerp(wagonList[0].transform.rotation, trainSubstitute.transform.rotation, Time.deltaTime);
        wagonList[0].transform.position = Vector3.Lerp(wagonList[0].transform.position, spline.GetPoint(wagonProgression[0]), Time.deltaTime);
        TrainRoverPos();
    }

    private void DrawLineRender()
    {//déssine à l'écran le trajet de destination
        float t;
        LineRenderer lineRenderer = new LineRenderer();
        if(player.GetComponent<LineRenderer>() == null)
        {
            lineRenderer = player.AddComponent<LineRenderer>();
        }
        if(lineRenderer != null)
        {
            lineRenderer.useWorldSpace = true;
            lineRenderer.sharedMaterial = GameManager.instance.matLineRenderer;
            lineRenderer.SetWidth(0.2f, 0.05f);
            lineRenderer.SetVertexCount(50);
            for (int li = 0; li < 50; li++)
            {
                t = li / (50f - 1f);
                Vector3 position = Bezier.GetPoint(p0, p1, p2, p3, t);
                lineRenderer.SetPosition(li, position);
            }
        }
    }

    public void PointCurve(GameObject destinationObject)
    {//fonction qui permet de calculer les 4 points nécéssaires à la création d'une spline. Déclenché par QuitInventory sur AudioGUI
        Vector3 posArrivee = destinationObject.transform.position;//position de la sphere sound d'arrivée lors de transitOK
        lineRenderprogression = 0f;
        p0 = player.transform.position;// + new Vector3(0.0f,-2.0f,0.0f);
         p3 = posArrivee;//affecte au dernier point de la spline la destination
        Vector3 direction = p3 - p0;//calcul du vecteur de direction du player à la destination en ligne droite
         p1 = p0 + new Vector3(direction.x / 3f, 10f, direction.z / 3f);
         p2 = p0 + new Vector3(direction.x / 1.5f, 10f, direction.z / 1.5f);
        //calculs supplémentaires pour P0 afin d'arriver jusqu'au sol
        Ray down = new Ray(posArrivee, Vector3.down);
        float radiusArrivee = 30f;
        RaycastHit hit;
        Physics.Raycast(down, out hit, radiusArrivee * 2);
        float toTheFlor = hit.distance;
        p3 = posArrivee + new Vector3(0.0f, -toTheFlor, 0.0f);//on ajoute cette distance a p3
        DrawLineRender();
    }

    private void TrainAI()
    {
        float oldProgession = progression;
        BezierSpline spline = splineData.bezierSpline;
        index = splineManager.DistObjectSplinePoints(concernGO.transform.position, splineData);//mise à jour en cas de changement de position de player ou gare
        float indexToTimeConcern = splineData.indexToTime[index];
        if (locoVelocityZ < 0.01F)//si la vitesse n'éxéde pas ce seuil on peut faire avancer la loco
        {
            progression = Mathf.Lerp(progression, indexToTimeConcern, (Time.deltaTime / 500f));
        }
        progression = Mathf.Clamp(progression, 0f, 1f);
        TrainPositionControler();
        TrainRoverPos();
    }

    private void TrainRoverPower()
    {
        float oldProgession = progression;
        //speed = 0.06f;//augmente la vitesse d'adaptation
        BezierSpline spline = splineData.bezierSpline;
        index = splineManager.DistObjectSplinePoints(concernGO.transform.position, splineData);//mise à jour en cas de changement de position de player ou gare
        float indexToTimeConcern = splineData.indexToTime[index];
        {
            {
                progression = Mathf.Lerp(progression, indexToTimeConcern, Time.deltaTime * speed);
            }
        }
        progression = Mathf.Clamp(progression, 0f, 1f);
        TrainPositionControler();
    }

    private void TrainPositionControler()
    {
        Vector3 trainDirection = new Vector3();
        float distance = new float();
        for (int i = 0; i < wagonList.Count; i++)
        {
            if (i > 0)//pour les wagons uniquement
            {
                distance = Vector3.Distance(wagonList[i - 1].transform.position, wagonList[i].transform.position);
                float multiplicateur = Mathf.Abs((Mathf.Abs(distance) - wagonIdealDist)) / Time.deltaTime;//on retire la distance idéale entre deux wagon du calcul afin que le multiplicateur tende vers zéro quand la distance est idéale
                multiplicateur = multiplicateur / 1000000f;
                multiplicateur = Mathf.Clamp(multiplicateur, 0f, 0.01f);//multiplicateur exprime une fraction de la distance entre deux wagons

                //position des wagons

                if (distance != wagonIdealDist)
                {
                    float deltaProg = wagonProgression[i-1] - wagonProgression[i];
                    if (deltaProg < 0)//si deltaProg est négatif il faut vite mettre en place les wagons car il avancent par rapport à la loco !
                    {
                        wagonProgression[i] -= (multiplicateur + (locoVelocityZ * 2f)) * speed;
                        wagonProgression[i] = Mathf.Clamp(wagonProgression[i], 0f, 1f);
                    }
                    else
                    {
                        if (deltaProg > 0)//cas normal. Positif
                        {
                            if (distance > wagonIdealDist+1.2f)
                            {
                                wagonProgression[i] += (multiplicateur + (locoVelocityZ*1.3f)) * speed;
                                wagonProgression[i] = Mathf.Clamp(wagonProgression[i], 0f, 1f);
                            }
                            if (distance > wagonIdealDist && distance < wagonIdealDist+1.2f)
                            {
                                wagonProgression[i] -= (multiplicateur + (locoVelocityZ*0.6f)) * speed;
                                wagonProgression[i] = Mathf.Clamp(wagonProgression[i], 0f, 1f);
                            }
                            if (distance < wagonIdealDist)
                            {
                                wagonProgression[i] -= (multiplicateur + (locoVelocityZ * 1.3f)) * speed;
                                wagonProgression[i] = Mathf.Clamp(wagonProgression[i], 0f, 1f);
                            }
                        }
                    }                   
                }
            }
            if (i == 0)//si c'est la loco
            {
                wagonProgression[0] = progression;
            }
            //vecteur directeur de la rotation des wagons
            float offsetPlus = wagonProgression[i] + (wagonOffset*0.5f);
            offsetPlus = Mathf.Clamp(offsetPlus, 0f, 1f);
            float offsetMoins = wagonProgression[i] - (wagonOffset*0.5f);
            offsetMoins = Mathf.Clamp(offsetMoins, 0f, 1f);
            if (offsetMoins == 0f)
            {
                trainDirection = spline.GetPoint(wagonProgression[i]) - spline.GetPoint(offsetPlus);
            }
            if (offsetPlus == 1f)
            {
                trainDirection = spline.GetPoint(offsetMoins) - spline.GetPoint(wagonProgression[i]);
            }
            if (offsetMoins > 0f && offsetPlus < 1f)
            {
                trainDirection = spline.GetPoint(offsetPlus) - spline.GetPoint(offsetMoins);//classique
            }
            wagonList[i].transform.rotation = Quaternion.Lerp(wagonList[i].transform.rotation, Quaternion.LookRotation(trainDirection) * Quaternion.Euler(-0f, 0f, 0f), Time.deltaTime);
            wagonList[i].transform.position = Vector3.Lerp(wagonList[i].transform.position, spline.GetPoint(wagonProgression[i]), Time.deltaTime*3f);//positionne les wagon avec un offset pour répartir les wagons sur la spline
        }
    }

    private void TrainRoverPos()
    {
        trainRover.transform.rotation = wagonList[0].transform.rotation;//aligne le rover sur la loco
        trainRover.transform.position = wagonList[0].transform.position;//aligne le rover sur la loco
    }

    private bool CheckForTrainCollision(GameObject wagonConcern)
    {
        bool carembolage = false;
        BoxCollider[] wagonsColliders = wagonConcern.GetComponents<BoxCollider>();
        foreach(BoxCollider coll in wagonsColliders)
        {
            Collider hitCollider = coll.GetComponent<Collider>();
            if (hitCollider != null && wagonConcern.layer == 10 )//layer vehicule
            {
                carembolage = true;
            }
        }
        return carembolage;
    }

   public IEnumerator CalcLocoVelocity()
    {
        Vector3 oldPos = new Vector3();
        while (calcLocoSpeed)
        {
            oldPos = wagonList[0].transform.position;

            yield return new WaitForEndOfFrame();

            locoVelocityZ = Vector3.Distance(oldPos , wagonList[0].transform.position);
            locoVelocityZ = Mathf.Abs(locoVelocityZ);//permet de savoir la vitesse du train. Négatif si marche arrière
            locoVelocityZ = locoVelocityZ / 100f;
            speed = 0.02f - locoVelocityZ;
            speed = Mathf.Clamp(speed, 0f, 0.02f);
        }
    }

    private void Start()
    {
        wagonList = new List<GameObject>();
        wagonProgression = new List<float>();
        inTrain = false;
        progression = 0f;
        trainSubstitute = new GameObject();
        splineData = splineManager.GetTheGoodSpline("TrainLine");//crée les tableaux de configuration globale de la spline de ligne de chemin de fer
        spline = splineData.bezierSpline;
        vehiculeManager.spline = spline;
        vehiculeManager.trainController = this.GetComponent<TrainController>();
        trafficOn = true;
        wagonOffset = 0.007f;//taille d'une wagon + espace entre deux wagons
        //speed = 1f;
        progression = 0.5f;
        SetUpTrainPosition();
        calcLocoSpeed = true;
        StartCoroutine(CalcLocoVelocity());
    }

    private void OnEnable()
    {
        GameManager.NewPlayer += PlayerListener;//souscrit à l'évent system qui défini le bon joueur
        EventManagerPlayer.NewGear += Listener;//souscrit a l'évent system qui permet de déterminer grace à un string dans quel état est le moteur du vehicule
        trainRover = vehiculeManager.vehiculeList[3];
        boiteAVitesse = TrainAI;
    }

    private void OnDesable()
    {
        GameManager.NewPlayer -= PlayerListener;//souscrit à l'évent system qui défini le bon joueur
        EventManagerPlayer.NewGear -= Listener;//souscrit a l'évent system qui permet de déterminer grace à un string dans quel état est le moteur du vehicule
    }

    private void Update()
    {
        if (trafficOn)
        {
            boiteAVitesse();
        }
    }

}
