using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using System.Collections;
using System.Collections.Generic;

public class AudioGUI : MonoBehaviour
{
    //script sur l'audioContainer qui gére l'affichage de l'inventiare des sons de lecture Juliette

    private GameObject GOActuel;

    private List<AudioClip> totalItem;//l'ensemble des audiosClips du tableau 2D
    private Material matLineRender;//la couleur de la line render losqu'on passe dessus avec la sourie
    private SpriteRenderer matSpritePlanloc;
    public string flipBoardString;//le titre de la barreVie sur laquelle on a cliqué pour changer de position sur la map
                                  //Sprite 
    private GameObject titreAudio;//objet de base à instancier
    private GameObject canvasBarresVies;//le parent canvas des barres vies
    private GameObject planLocationSprite;//
    private GameObject enventSystem;//
                                    //Sprite tableaux
    public List<Vector3> target;//position du GO empty à suivre pour afficher la fleche sur le tableau des sons
    public List<GameObject> planLocation;//GO empty à suivre pour afficher la fleche sur le tableau des sons
    public List<GameObject> avatarsList;
    private List<SpriteRenderer> planLocationSpriteList;//GO empty à suivre pour afficher la fleche sur le tableau des sons

    private List<Vector2> pointList;

    private GameObject background;//le sprite
    private GameObject planSommaire;//le sprite du plan du BL à l'écran
    private GameObject planSommaireJuliette;//le sprite du plan du BL à l'écran
    private GameObject planLocationZone;//le GO parent (soit planLocationJuliette soit PlanLocationMusoirD)
    private GameObject planLocationJuliette;//planLocation représente soit planLocationJuliette soit planLocationMusoirD
    private GameObject avatarsSprites;
    private GameObject vousEtesIci;
    //Scripts
    public GestionnaireSons gestionnaireSons;
    private MetaSonsGlobal metaSonsGlobal;//métadonnées générales à prendre sur le GO AudioContainer
    private MetaSons metaSonsThisGO;
    private PlayerControler playerControler;
    private GameObject systemNavig;

    //var
    private float offset;//décalage à droite de la barreVie
    private float step;//écartement entre deux barresVie
    public float progression;//en cas de transit la progression sur le chemin du voyage
    private int z;//incrément
    public int indexDestination;//l'index du reverbZone sur laquelle on a cliqué

    public Sprite planLocationSpriteOver;//the image of the sprite when mouse over it
    public Sprite planLocationSpriteDefault;//the image of the sprite when mouse do not over it

    private void Awake()
    {
        gestionnaireSons = GetComponent<GestionnaireSons>();
        metaSonsGlobal = GetComponent<MetaSonsGlobal>();
        background = GameObject.Find("Background");
        planSommaireJuliette = GameObject.Find("PlanJuliette");
        planLocationJuliette = GameObject.Find("PlanLocationJuliette");
        planLocationSprite = GameObject.Find("PlanLocationSprite");
        vousEtesIci = GameObject.Find("VousEtesIci");
        playerControler = GameManager.gameControl.GetComponent<PlayerControler>();
        titreAudio = GameObject.Find("TitreAudio");
        canvasBarresVies = GameObject.Find("CanvasBarresVies");
        systemNavig = GameObject.Find("SystemNavig");
        avatarsSprites = GameObject.Find("AvatarsSprites");
        totalItem = new List<AudioClip>();
    }

    private void Listener(string e)
    {
        if (e == "Inventory")
        {
            planSommaireJuliette.GetComponent<Animator>().SetBool("onOff", true);//affiche le plan
            vousEtesIci.GetComponent<SpriteRenderer>().enabled = true;//affiche le sprite de localisation sur la carte
            SolidColorAll();
            GUISystemNavig nav = systemNavig.GetComponent<GUISystemNavig>();
            nav.RunStatus2Process(17,false);
            nav.RunStatus2Process(18,true);
            nav.RunStatus2Process(19,true);
            nav.RunStatus2Process(20,true);
            foreach (Transform plan in planLocationJuliette.transform)
            {//
                plan.GetComponent<Button>().interactable = true;//rends le bouton inactif
            }
        }
        if (e == "Transit")
        {
                GUISystemNavig nav = systemNavig.GetComponent<GUISystemNavig>();
                nav.RunStatus2Process(15,false);
                nav.RunStatus2Process(16,true);
        }
        if (e == "Bird")
        {
            GUISystemNavig nav = systemNavig.GetComponent<GUISystemNavig>();
            nav.RunStatus2Process(14,false);
            nav.RunStatus2Process(20, true);
        }
        if (e == "Jump")
        {
            GUISystemNavig nav = systemNavig.GetComponent<GUISystemNavig>();
            if (!nav.GetComponent<Animator>().GetBool("statusGo") && !nav.writeOn)
            {
                nav.RunStatus2Process(21, true);
            }
        }
    }

    private List<string> CreateList(List<string> listToFill, List<string> fromList)
    {//fonction de création des tableaux de titres de chapitre pour ce script en prenant les valeurs de MetaSonsGlobal
     //listToFill.Clear();
        foreach (string tot in fromList)
        {
            listToFill.Add(tot);
        }
        return listToFill;
    }

    private List<List<AudioClip>> CreateList2(List<List<AudioClip>> listToFill2, List<List<AudioClip>> fromList2)
    {//fonction de création des tableaux de sous chapitre inspirés de MetaSonsGlobal
     //listToFill2.Clear();
        foreach (List<AudioClip> tot2 in fromList2)
        {
            listToFill2.Add(tot2);
        }
        return listToFill2;
    }

    private void CreatePlanLocationList()
    {
        if (GameManager.instance.zoneJuliette)
        {//si on se rends dans le secteur Juliette
            planLocationZone = planLocationJuliette;
            foreach (Transform plan in planLocationJuliette.transform)
            {//
                AlphaColorImage(0.0f, plan.gameObject);//affiche les sprites de plan location
                planLocation.Add(plan.gameObject);//ajoute les emplacements des Juliette sur la carte
                plan.GetComponent<Button>().interactable = false;//rends le bouton inactif
                plan.gameObject.SetActive(true);
                planLocationSpriteList.Add(plan.GetComponent<SpriteRenderer>());
            }
            planSommaire = planSommaireJuliette;
        }
    }


    private IEnumerator ClearBarresVies()
    {//détruit tout les GO instantiés par le processus BarreVie et met à zero les lists associées aux barresVies
        CloseInventory();
        yield return new WaitForSeconds(2f);
        totalItem.Clear();
        planLocation.Clear();
        target.Clear();
        planLocationSpriteList.Clear();
        gestionnaireSons.totalItem.Clear();
        z = new int();
        z = 0;
        step = new float();
        step = 1;
        StopAllCoroutines();//////////////:
    }

    private Vector2 GORecTAnchoredPosition(GameObject GOConcern)
    {//prends le RecT de GOConcern et le convertis en vector2
        Vector2 trans = GetRecT(GOConcern).anchoredPosition;
        return trans;
    }

    private RectTransform GetRecT(GameObject recTConcern)
    {//prends le RectT du GO concerné
        RectTransform recT = (RectTransform)recTConcern.GetComponent<RectTransform>();
        return recT;
    }

    private Vector3 GetPointInRect(GameObject alignOnThis, int adjust)
    {
        RectTransform recT = alignOnThis.GetComponent<RectTransform>();
        Vector2 posCorrected = RectTAdjust(alignOnThis, adjust);
        Vector3 worldPoint = new Vector3();
        RectTransformUtility.ScreenPointToWorldPointInRectangle(recT, posCorrected, null, out worldPoint);
        return worldPoint;
    }

    private Vector2 GetRectSizeDelat(GameObject recTConcern2)
    {// prends le RectT d'un GO et retourne la taille du rectancle dessiné à l'écran
        RectTransform recT = GetRecT(recTConcern2);
        Vector2 rectZise = recT.rect.size;
        return rectZise;
    }
    //fonction qui permet un offset afin d'aligner la lineRender sur les bords interieur ou exterieur du rectangle d'un sprite
    private Vector2 RectTAdjust(GameObject alignOnThis, int adjust)
    {//recTConcern3 est le GO sur lequel aligner les points et recToAdjustFrom est le GO par rapport auquel on doit s'aligner.
        float deltaZiseX = GetRectSizeDelat(alignOnThis).x;
        //float deltaSizeY = GetRectSizeDelat(alignOnThis).y;
        Vector2 idealPos = GORecTAnchoredPosition(alignOnThis);//anchored position du go sur lequel il faut s'aligner.
        if (adjust == 0)
        {//aligné sur le bord gauche du sprite
            idealPos = new Vector2(idealPos.x - deltaZiseX / 2f, idealPos.y);
        }
        if (adjust == 1)
        {// 1 veut dire centré

        }
        if (adjust == 2)
        {// 0 veut dire centré mais sur le bord droit du sprite
            idealPos = new Vector2(idealPos.x + deltaZiseX / 2f, idealPos.y);
        }
        return idealPos;
    }

    public void SolidColorAll()
    {
        //change la transparence des titres et des barres vies pour quils deviennent solide
        for (int iie = 0; iie < planLocation.Count; iie++)
        { //et des barresVie
            AlphaColorImage(1.0f, planLocation[iie]);
        }
    }

    public void FadeColorAllButIndex(int destiIndex)
    {
        //change la transparence des titres et des barres vies pour quils deviennent solide
        for (int iie = 0; iie < planLocation.Count; iie++)
        { //et des barresVie
            AlphaColorImageFade(0.0f, planLocation[iie]);
        }
    }

    private void AlphaColorImage(float colorDigit, GameObject goColor)
    {
        Image image = goColor.GetComponent<Image>();
        image.color = new Color(image.color.r, image.color.g, image.color.b, colorDigit);
        image.enabled = false;
        image.enabled = true;//switch qui permet de réafficher l'image (sinon elle ne s'affiche pas !????)
    }

    private void AlphaColorImageFade(float colorDigit, GameObject goColor)
    {
        float fadeTime = 1.0f;
        Image image = goColor.GetComponent<Image>();
        image.CrossFadeAlpha(colorDigit, fadeTime, true);
    }

    public void Action()
    {
        CreatePlanLocationList();//crée les tableaux nécéssaires à la création des barresVies
                                 //CreateBarresVies();//mise en forme des barres vies (inventaire)
        GameManager.instance.inventoryMap = true;//autorise l'affichage du plan
        GUISystemNavig nav = systemNavig.GetComponent<GUISystemNavig>();
        nav.RunStatus2Process(1,false);//affiche l'info bulle "click droit pour afficher le plan"
        nav.RunStatus2Process(0, true);//affiche l'info bulle "click droit pour afficher le plan"
        FadeColorAllButIndex(0);
    }

    public void OpenInventory()
    {
        if (GameManager.instance.inventoryMap)//si on est en  mode ne pas afficher de plan et qu'on essaie malgrés tout d'ouvrir l'inventaire, on le ferme de force
        {
            if (EventManagerPlayer.engineStateBool[5])//si Inventory est déja ouvert ce second click veut dire que l'on souhaite le fermer
            {
                CloseInventory();
            }
            else
            {
                if (!EventManagerPlayer.engineStateBool[4])//si on est pas en mode transit alors on peut déclencher l'inventory
                {
                    EventManagerPlayer.BooleanSetUp("Inventory");//EVENT/////////////////////déclenche l'event Inventory
                }
            }
        }
    }

    public void CloseInventory()
    {
        planSommaireJuliette.GetComponent<Animator>().SetBool("onOff", false);//affiche le plan                                                
        vousEtesIci.GetComponent<SpriteRenderer>().enabled = false;//masque le sprite de localisation sur la carte 
        foreach (Transform plan in planLocationJuliette.transform)
        {//
            plan.GetComponent<Button>().interactable = true;//rends le bouton inactif
        }
        GUISystemNavig nav = systemNavig.GetComponent<GUISystemNavig>();
        GameManager.instance.cameraActive.GetComponent<CameraMove>().StopEffects();
        if (gestionnaireSons.reverbGO[indexDestination] != gestionnaireSons.reverbGO[GameManager.instance.indexlectureActuelle])
        {
            FadeColorAllButIndex(indexDestination);
        }
        else FadeColorAllButIndex(GameManager.instance.indexlectureActuelle);
        EventManagerPlayer.engineStateBool[5] = false;//!Inventory 
    }

    public IEnumerator ActionGo()//lancement au démarrage du niveau afin de créer les barresVies au bout de 30 secondes si le joueur ne l'a pas déja fait en entrant dans une sphéreSound
    {
        yield return new WaitForSeconds(3f);
        Action();
    }


    public void MouseOnPointerEnter(Button GOConcern, int mouseOverI)
    {//check déclenché par la bouton barres vies sur son script MyNameIs
        Image planLocationImage = planLocation[mouseOverI].GetComponent<Image>();
        planLocationImage.sprite = planLocationSpriteOver;
        //[mouseOverI].sprite = planLocationSpriteOver;
    }

    public void MouseOnPointerExit(Button GOConcern, int mouseOverI)
    {//check déclenché par la bouton barres vies sur son script MyNameIs
        Image planLocationImage = planLocation[mouseOverI].GetComponent<Image>();
        planLocationImage.sprite = planLocationSpriteDefault;
        //planLocationSpriteList[mouseOverI].sprite = planLocationSpriteDefault;
    }

    private void OnEnable()
    {
        foreach (Transform locati in planLocationJuliette.transform)
        {
            locati.gameObject.SetActive(true);
            AlphaColorImage(0.0f, locati.gameObject);
        }
        EventManagerPlayer.NewGear += Listener;//souscrit a l'évent system qui permet de déterminer grace à un string dans quel état est le moteur du vehicule
    }

    public void OnDesable()
    {
        EventManagerPlayer.NewGear -= Listener;//de-inscription à l'envent system
    }

    private void Start()
    {
        planLocationSpriteList = new List<SpriteRenderer>();
        avatarsList = new List<GameObject>();
        foreach (Transform avatar in avatarsSprites.transform)
        {//
            avatarsList.Add(avatar.gameObject);//remplis le tableau
        }
        VehiculeManager vehiculeManager = GameObject.Find("VehiculesRoot").GetComponent<VehiculeManager>();
        for(int i=0; i< avatarsList.Count; i++)
        {
            if (i == 8)
            {
                GameManager.instance.playerList.Add(new GameObject());
            }
            if(i == 9)
            {
                GameManager.instance.playerList.Add(vehiculeManager.vehiculeCarosserie[1]);
            }
            if (i == 10)
            {
                GameManager.instance.playerList.Add(vehiculeManager.vehiculeCarosserie[2]);
            }
            if (i == 11)
            {
                GameManager.instance.playerList.Add(vehiculeManager.vehiculeCarosserie[3]);
            }
            if (i == 12)
            {
                GameManager.instance.playerList.Add(vehiculeManager.vehiculeCarosserie[6]);
            }
            if (i == 13)
            {
                GameManager.instance.playerList.Add(vehiculeManager.vehiculeCarosserie[7]);
            }
        }
        z = 0;
        step = 0;
        StartCoroutine(ActionGo());
    }
}