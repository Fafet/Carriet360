﻿using UnityEngine;
using System.Collections;

public class SpriteRotation : MonoBehaviour {

    private GameObject player;
    private GameObject thisGO;


    private void PlayerListener(GameObject e)
    {
        player = e;
    }

    private void OnEnable()
    {
        GameManager.NewPlayer += PlayerListener;
        player = GameManager.instance.player;
        thisGO = this.gameObject;
        thisGO.transform.rotation = Quaternion.identity;
    }

    private void OnDesable()
    {
        GameManager.NewPlayer -= PlayerListener;
    }

    // Update is called once per frame
    void Update () {
        Vector3 direction = (player.transform.position - this.transform.position);
        thisGO.transform.rotation = Quaternion.Slerp(thisGO.transform.rotation, Quaternion.LookRotation(-direction), Time.deltaTime);
	}
}
