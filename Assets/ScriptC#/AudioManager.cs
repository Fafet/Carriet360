﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {

    public static SoundDictionaryAcces soundDicoAccess;
    public SoundDictionary soundDico;//le SO qui stock tout les sons
    private Dictionary<AudioClip, string> playedClip;
    public Dictionary<AudioSource, string> ActifAudioSource;
    public bool mariaIsPlaying;//Maria est-elle ne train de parler ?
    private int totalTypeOfSound;//combien de type de son différents? 11
    public bool videoGo;//la vidéo est-elle en train de tourner ?
    private bool ambianceCheck;
    public float MaxLevel;//volume maximum décidé par le slider de volume (fraction su slider sur 10)

    public void SetMaxLevel(float valMaxi)
    {
        MaxLevel = valMaxi;
        foreach (KeyValuePair<AudioSource, string> source in ActifAudioSource)
        {
            if (source.Key.volume> valMaxi)
            {
                source.Key.volume = source.Key.volume * MaxLevel;
            }
            else source.Key.volume = MaxLevel;
        }
    }

    private AudioClip RandomAudioClip(string thisType)
    {
        List<AudioClip> clipPlayedForThisString = new List<AudioClip>();//crée un tableau qui regroupe les audioClip déja joué pour ce type de son en se basant sur le nom du type de son
        List<AudioClip> goodSoList = new List<AudioClip>();
        goodSoList = GetGoodSOList(thisType);//va chercher la bonne liste dans le SO par son nom en string
        if (playedClip.Count > 0)
        {
            foreach (KeyValuePair<AudioClip, string> keys in playedClip)//regarde dans le tableau de son déja joués (audioClip en clefs et type en value)
            {
                if (keys.Value == thisType)//si la value équivaut au type de son qu'on cherche à créer
                {
                    clipPlayedForThisString.Add(keys.Key);//on ajoute ce son déja joué au tableau
                }
            }
        }
        List<AudioClip> clipNotPlayedForThisString = new List<AudioClip>();
        foreach (AudioClip sound in goodSoList)
        {
            if (!clipPlayedForThisString.Contains(sound))//si on trouve des sons qui n'ont pas déja été joués on les ajoute à cette nouvelle liste
            {
                clipNotPlayedForThisString.Add(sound);
            }
        }
        int randomVal = new int();
        if (thisType != "video")//pour tout les sons sauf le son des videos prends un valeur au hasard
        {
            randomVal = Random.Range(0, clipNotPlayedForThisString.Count);//prends un index au hasard dans la liste des clip non joués
        }
        if (clipNotPlayedForThisString.Count == 0)//si il ne reste pas de sons de ce type à jouer
        {
            foreach (AudioClip keys in clipPlayedForThisString)//enleve de la liste des des clip déja joués ceux de ce type pour permettre de reprendre un lecture aléatoire
            {
                playedClip.Remove(keys);
            }
            clipPlayedForThisString.Clear();//reset  
            foreach (AudioClip sound in goodSoList)//ajoute tout les sons à nouvaue
            {
                if (!clipPlayedForThisString.Contains(sound))//si on trouve des sons qui n'ont pas déja été joués on les ajoute à cette nouvelle liste
                {
                    clipNotPlayedForThisString.Add(sound);
                }
            }
        }
        if (thisType == "video")
        {
            randomVal = 0;//on prends le premier son de la liste dans le SO
        }
        playedClip.Add(clipNotPlayedForThisString[randomVal], thisType);//ajoute au dictionnaire ce nouveau clip pour ce type de string
        return clipNotPlayedForThisString[randomVal];//retourne ce clip
    } 

    public IEnumerator Volume(AudioSource audioS, float vol)//fonction qui fait varier le volume en haut et en bas
    {
        float timer = Time.time;
        string tempAudioSName = audioS.clip.name;
        while(audioS != null && Time.time< timer + 3f)
        {
            audioS.volume = Mathf.Lerp(audioS.volume, vol* MaxLevel, Time.deltaTime / 2f);//baisse ou monte le son
            if (!audioS.isPlaying)//si ça ne joue plus on détruit l'audioSource
            {
                if (ActifAudioSource.ContainsKey(audioS))
                {
                    {
                        ActifAudioSource.Remove(audioS);//enleve cet audioSource de la liste des sons actifs
                    }
                }
                Destroy(audioS);
            }
            yield return null;
        }
        if (tempAudioSName.Contains("ambiance"))//si le temps est écoulé et que l'audioSource n'a pas été détrutite en sortant de cette coroutine
        {
            StartCoroutine(AmbianceCheck(audioS));//si il s'agit d'un son d'ambiance on doit s'assurer que lors de la fin de la lecture un nouveau son d'ambaince soit lancé
        }
        if (tempAudioSName.Contains("Maria"))//si le temps est écoulé et que l'audioSource n'a pas été détrutite en sortant de cette coroutine
        {
            StartCoroutine(MariaCheck(audioS));//si il s'agit d'un son d'ambiance on doit s'assurer que lors de la fin de la lecture un nouveau son d'ambaince soit lancé
        }
    }

    private IEnumerator AmbianceCheck(AudioSource source)
    {
        if (source != null && !ambianceCheck)
        {
            while (source.isPlaying)
            {
                ambianceCheck = true;
                yield return null;
            }
            if (source != null)
            {
                if (ActifAudioSource.ContainsKey(source))
                {
                    {
                        ActifAudioSource.Remove(source);//enleve cet audioSource de la liste des sons actifs
                    }
                }
                Destroy(source);
                ambianceCheck = false;
            }
            AddAudio("ambiance", false, false, 0f, 0);
         }
    }

    private IEnumerator MariaCheck(AudioSource source)
    {
        if (source != null && !mariaIsPlaying)
        {
            while (source.isPlaying)
            {
                mariaIsPlaying = true;
                yield return null;
            }
            if (source != null)
            {
                if (ActifAudioSource.ContainsKey(source))
                {
                    {
                        ActifAudioSource.Remove(source);//enleve cet audioSource de la liste des sons actifs
                    }
                }
                Destroy(source);
                mariaIsPlaying = false;
            }  
        }
    }

    private void CheckForSoundPLaying()//regarde uniquement dans les audioSources actives pour prendre des décisions (monter/baisser le son ou interdire une lecture
    {      
        List<List<AudioSource>> listOfList = new List<List<AudioSource>>();//pour chaque type de son une liste d'audioSource va dans cette liste temporaire
        for(int i = 0; i < totalTypeOfSound; i++)//initialise la liste avec le nombre d'entrée (type de sons différents)
        {
            listOfList.Add(new List<AudioSource>());
        }
        foreach (KeyValuePair<AudioSource, string> source in ActifAudioSource)
        {
            if (source.Value == "maria")
            {
                listOfList[0].Add(source.Key);
            }
            if (source.Value == "rythme")
            {
                listOfList[1].Add(source.Key);
            }
            if (source.Value == "rap")
            {
                listOfList[2].Add(source.Key);
            }
            if (source.Value == "conversation")
            {
                listOfList[3].Add(source.Key);
            }
            if (source.Value == "expression")
            {
                listOfList[4].Add(source.Key);
            }
            if (source.Value == "beatBox")
            {
                listOfList[5].Add(source.Key);
            }
            if (source.Value == "fx")
            {
                listOfList[6].Add(source.Key);
            }
            if (source.Value == "r2d2")
            {
                listOfList[7].Add(source.Key);
            }
            if (source.Value == "activation")
            {
                listOfList[8].Add(source.Key);
            }
            if (source.Value == "bird")
            {
                listOfList[9].Add(source.Key);
            }
            if (source.Value == "ambiance")
            {
                listOfList[10].Add(source.Key);
            }
            if (source.Value == "video")
            {
                listOfList[11].Add(source.Key);
            }
        }
        int score = 0;
        for (int i = 0; i < listOfList.Count - 2; i++)//pour tout les sons sauf les deux derniers ("ambiance" et "video")
        {
            if (listOfList[i].Count > 0)// si pour ce type de son on à une audioSources
            {
                score += 1;
                float volume = 0f;//on défini le volume en fonction du score obtenu (petit score haut volume)
                if (score == 1)
                {
                    volume = 0.8f;
                }
                if (score == 2)
                {
                    volume = 0.4f;
                }
                if (score == 3)
                {
                    volume = 0.1f;
                }
                if (score > 4)
                {
                    volume = 0f;                            //prends le dernier élément de la liste
                }
                if (listOfList[i].Count > 0)// si pour ce type de son on à plusieurs audioSources qui jouent on ne l'autorise pas et on reduit le volume des plus anciennes avant de les détruire afin qu'un seul son de ce type ne joue
                {
                    for (int u = 0; u < listOfList[i].Count; u++)//on prends toute les audioSources 
                    {
                        if (u < listOfList[i].Count - 1)//à part la dernière
                        {
                           StartCoroutine(Volume(listOfList[i][u], 0f));//diminue le son
                        }
                        else
                        {
                            if (videoGo)//si on est dans le collider vidéo
                            {
                                StartCoroutine(Volume(listOfList[i][u], 0));//adapte le volume
                            }
                            if (!videoGo)//si on est dans le collider vidéo
                            {
                                StartCoroutine(Volume(listOfList[i][u], volume));//adapte le volume
                            }

                        }
                    }
                }
            }
        }
        //VIDEO
        if (listOfList[11].Count > 0)//si on est en train de jouer la vidéo  
        {
            if (videoGo)//si on est dans le collider vidéo
            {
                StartCoroutine(Volume(listOfList[11][listOfList[11].Count - 1], 0.8f));//met le volume de la video à fond
            }
            else
            {                
                StartCoroutine(Volume(listOfList[11][listOfList[11].Count - 1], 0.01f));//met le volume de la video à zéro
            }
        }
        //AMBIANCE
        if (listOfList[10].Count > 0)//si on a bien un son d'ambiance il faut empêcher sa mise à zéro du volume pour qu'on l'entende en arrière plan
        {
            if (videoGo)//si on est dans le collider vidéo
            {
                StartCoroutine(Volume(listOfList[10][listOfList[10].Count - 1], 0.1f));//valeur intermédiare
            }
            else
            {
                StartCoroutine(Volume(listOfList[10][listOfList[10].Count - 1], 0.4f));//valeur intermédiare
            }
        }
    }



    public AudioSource AddAudio(string typeOfClip, bool loop, bool playAwake, float vol, int prior)
    {
        AudioSource composant = new AudioSource();
        AudioClip newAudio = RandomAudioClip(typeOfClip);//crée le clip au hasard en tenant compte des clips déja joués
        composant = this.gameObject.AddComponent<AudioSource>();
        ActifAudioSource.Add(composant, typeOfClip);//garde une trace de cet audioSource dans la liste
        composant.clip = newAudio;//affecte le clip à l'audio source
        composant.clip = newAudio;
        //Debug.Log(composant.clip.name+"   clipName");
        composant.loop = loop;
        composant.playOnAwake = playAwake;
        composant.volume = vol;
        composant.priority = prior;
        composant.Play();
        CheckForSoundPLaying();
        return composant;
    }

    private List<AudioClip> GetGoodSOList(string attribute)//permet de convertir un string en list provenant du SO des audioClips
    {
        List<AudioClip> newList = new List<AudioClip>();
        switch (attribute)
        {
            case "ambiance": newList = soundDico.ambiance; return newList;
            case "beatBox": newList = soundDico.beatBox; return newList;
            case "conversation": newList = soundDico.conversation; return newList;
            case "rythme": newList = soundDico.rythme; return newList;
            case "fx": newList = soundDico.fx; return newList;
            case "rap": newList = soundDico.rap; return newList;
            case "r2d2": newList = soundDico.r2d2; return newList;
            case "maria": newList = soundDico.maria; return newList;
            case "expression": newList = soundDico.expression; return newList;
            case "activation": newList = soundDico.activation; return newList;
            case "bird": newList = soundDico.bird; return newList;
            case "video": newList = soundDico.video; return newList;
        }
        return newList;
    }

    private void Start()
    {
        MaxLevel = 0.8f;
        mariaIsPlaying = false;
        videoGo = false;
        ambianceCheck = false;
        totalTypeOfSound = 12;//keep it sync
        soundDicoAccess = GetComponent<SoundDictionaryAcces>();
        soundDico = soundDicoAccess.soundDico;//le bon SO
        playedClip = new Dictionary<AudioClip,string>();
        ActifAudioSource = new Dictionary<AudioSource, string>();
        AudioSource newAudioS = AddAudio("ambiance", false, false, 0f, 0);//met en route un son d'ambiance
    }
}
