﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GPS : MonoBehaviour {

    private GameObject territoire;
    private GameObject gps;//miniPlan
    private GameObject gpsPoint;//position du player sur le miniPlan
    private GameObject outLimit;
    private GameObject actualGPSObject;
    private List<GameObject> territoireList;
    private int index;//la position du go ou est le player dans la list principale du territoire.
    private float row;
    private float column;
    private bool navigating;

    private void Awake()
    {
        territoire = GameObject.Find("Territoire");
        gps = this.gameObject;
        gpsPoint = GameObject.Find("GPSPoint");
        outLimit = GameObject.Find("OutLimit");
    }

    private void ListenerTerritoire(GameObject e)//Fonction qui change le mat du sol en Rorchat shader
    {
        if(e != null)
        {
            if(e != actualGPSObject)
            {
                for (int i = 0; i < territoireList.Count; i++)
                {
                    if (territoireList[i] == e)
                    {
                        index = i;
                        actualGPSObject = territoireList[i];
                        NewGPSPoint();
                    }
                }
            } 
        }
       else OutMap();
    }

    public void NavigatingListener(bool e)
    {
        navigating = true;
        gps.GetComponent<Animator>().SetBool("onOff", true);
    }

    public void GetGoodGPS(GameObject territ)
    {
        if (territ.name.Contains("LOD0"))
        {
            ListenerTerritoire(territ);
        }
        else
        {
            if (territ.name.Contains("Plan"))
            {
                ListenerTerritoire(null);
            }
        }
    }

    private void NewGPSPoint()
    {
        outLimit.SetActive(false);
        gpsPoint.GetComponent<Image>().enabled = true;
        float result = ((float)(index+1) / (float)row);//on obtient la colonne
        float resultAdapt = ((float)(index + 1) / (float)row) - 0.01f;//on obtient la colonne//On baisse le résultat afin de ne pas avoir un chiffre rond qui nous ferais ainsi passer à la colonne suivante alors qu'il s'agit en fait de la dernière cellule du tableau
        int colonne = (Mathf.FloorToInt(resultAdapt));//prends la partie integer//on obtient la bonne colonne
        float reste = (resultAdapt - Mathf.Floor(resultAdapt)) *10f;//enleve la partie integer pour s'intérésser au résultat après la virgule que l'on divise par le nombre de rangs. On optient la bonne rangée
        reste = Mathf.Abs(reste - 0.01f);
        int rang = Mathf.FloorToInt ((reste / 10f)*(float)row);
        Vector2 globalSize = gps.GetComponent<RectTransform>().sizeDelta;
        float pasX = globalSize.x / (float)column;
        float pasY = globalSize.y / (float)row;
        Vector2 effectivePosition = new Vector2((pasX * colonne)+(pasX/2f), (pasY * rang)+(pasY/2f));
        Vector2 positionZero = gps.GetComponent<RectTransform>().anchorMin - gps.GetComponent<RectTransform>().sizeDelta;
        StartCoroutine(NewPos(positionZero + effectivePosition));
    }

    private IEnumerator NewPos(Vector2 pos)
    {
        float timer = Time.time;
        while (Time.time < timer + 1)
        {
            gpsPoint.transform.localPosition = Vector2.Lerp(gpsPoint.transform.localPosition, pos, Time.deltaTime);
            yield return null;
        }

    }

    private void OutMap()
    {
        outLimit.SetActive (true);
        gpsPoint.GetComponent<Image>().enabled = false;
    }

    private void OnEnable()
    {
      VehiculeControler.NewTerritoire += ListenerTerritoire;
        GameManager.NewNavigation += NavigatingListener;
    }

    private void OnDesable()
    {
        VehiculeControler.NewTerritoire -= ListenerTerritoire;
        GameManager.NewNavigation -= NavigatingListener;
    }

    private void Start()
    {
        outLimit.SetActive(false);
        gpsPoint.GetComponent<Image>().enabled = true;
        row = 7f;
        column = 8f;
        territoireList = new List<GameObject>();
        foreach(Transform terri in territoire.transform)
        {
            if (terri.gameObject.name.Contains("LOD0"))
            {
                territoireList.Add(terri.gameObject);
            }
        }  
    }
}
