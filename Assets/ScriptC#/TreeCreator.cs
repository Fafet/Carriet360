﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TreeCreator : MonoBehaviour {
    //script sur le GO territoire
    public static PictoDictionaryAcces pictoDicoAccess;
    private PictoDictionary pictoDico;
    private SkyboxManager skyboxManager;
    private ParticuleManager particuleManager;
    private List<GameObject> treeInstances;//liste des arbres instantiés runtime
    private GameObject player;
    private bool treeCreation;
    private float rayon;//rayon du tir

    private void PlayerListener(GameObject e)
    {
        player = e;
    }

    private void TreeListener(GameObject e)
    {
        if (e == null)//si null cela signifie qu'on vient de sortir de la fonction onTriggerExit du script OnCollisionPlayer. On sort donc d'une maison ou d'un intérieur et on peut donc planter des arbres
        {
            treeCreation = true;
            SetIntOn();//active/deactive les mesh render de l'ancien et du nouveau intérieur//mets aussi intOn sur false
        }
        else treeCreation = false;
    }

    private void SetIntOn()
    {
        if(treeInstances.Count != 0)
        {
            if(treeInstances.Count > 5)//si on a déja créé plus de 5 arbres
            {
                for(int i = 0; i< treeInstances.Count - 4; i++)//on detruit les premiers abres créés pour qu'il n'en reste que 5
                {
                    GameObject thisTree = treeInstances[i];
                    treeInstances.Remove(treeInstances[i]);//on l'enlève du tableau avant de la détruire
                    StartCoroutine(DestroyForce(thisTree));//detruit les int récement créés
                }
            }
        }
        SetUpTree();
    }

    private void SetUpTree()//crée 3 raycast devant le player pour définir si il faut créer des arbres
    {
        Vector3 offsetHauteur = new Vector3(0f, 3f, 0f);
        Vector3 playerPos = player.transform.position+ offsetHauteur;
        Ray[] treeRay = new Ray[3];
        Vector3[] rayDirect = new Vector3[3];
        rayDirect[0] = player.transform.forward - player.transform.right;
        Ray newRay0 = new Ray(playerPos, rayDirect[0]);//45° à gauche
        treeRay[0] = newRay0;
        rayDirect[1] = player.transform.forward + player.transform.right;
        Ray newRay1 = new Ray(playerPos, rayDirect[1]);//droit devant
        treeRay[1] = newRay1;
        rayDirect[2] = player.transform.forward + player.transform.right;//45° à droite
        Ray newRay2 = new Ray(playerPos, rayDirect[2]);
        treeRay[2] = newRay2;
        LayerMask colliderMask = 1 << 8;
        for (int i = 0; i < treeRay.Length; i++)//pour chacun de ces trois rayons
        {
            RaycastHit hit;
            Vector3 downRayPos = new Vector3(0f,rayon,0f);//offset vers le haut
            Physics.Raycast(treeRay[i], out hit, rayon);
            if (hit.distance > 0f)
            {
                downRayPos += hit.point - (player.transform.forward);//recule le point de création un peu de l'objet de contact
            }
            else downRayPos += (playerPos + (rayDirect[i]* rayon));
            if (hit.distance > rayon - 5f || hit.distance == 0f)//on ne tire ce second raycast que si on à touché un point assez loin du joueur
            {
                Ray rayDown = new Ray(downRayPos, Vector3.down);//on tire un autre rayon vers le bas pour déterminer la position du sol
                RaycastHit hit2;
                if (Physics.Raycast(rayDown, out hit2, rayon * 10f))
                {
                    if (hit2.collider.tag == "Territoire")
                    {
                        CheckToCreateInstance(hit2.point);//lance la fonction qui créé effectivement l'arbre si les conditions sont réunis
                    }
                }
            }
        }
        StartCoroutine(RepeatTreeCreation());
    }

    private void CheckToCreateInstance(Vector3 treePosition)
    {
        float treeDistance = 100f;
        if (treeInstances.Count>0)//si il existe déja un arbre créé
        {
            for(int i=0; i< treeInstances.Count-1;i++)//on compare la position de ce nouvel arbre que l'on souhaite créé par rapport à tout ceux existants
            {
                float tempDistance = Mathf.Abs(Vector3.Distance(treeInstances[i].transform.position, treePosition));
                if(tempDistance< treeDistance)// pour chaque distance plus petite on la stock
                {
                    treeDistance = tempDistance;
                }
            }
        }
        if (treeDistance > 5f)//si la distance entre la plus petite distance de tout les arbres créés est suffisante on crée enfin l'arbre
        {
            GameObject newTree = RandomInt(pictoDico.prefabTree);//prends un prefab d'arbre au hasard
            newTree.transform.position = treePosition;
            newTree.transform.localScale = Vector3.zero;
            StartCoroutine(RaiseUp(newTree));
        }
    }

    private IEnumerator RepeatTreeCreation()
    {
        float timer = Time.time;
        while (treeCreation && Time.time < timer + 3f)
        {
            yield return null;
        }
        if (treeCreation)
        {
            SetIntOn();
        }
    }

    private IEnumerator RaiseUp(GameObject objToRaise)//fonction commune aux immeubles et aux maisons
    {
        float delay = Random.Range(0f, 1.5f);
        yield return new WaitForSeconds(delay);//plante les arbres en décaler pour ne pas planter deux arbres en même temps
        particuleManager.ParticuleOn(objToRaise.transform.position, 5, 5f);
        while (objToRaise != null && objToRaise.transform.localScale.y < 0.98f)
        {
            objToRaise.transform.localScale += new Vector3(Time.deltaTime, Time.deltaTime, Time.deltaTime);
            yield return null;
        }
    }

    private IEnumerator DestroyForce(GameObject toBedestroy)
    {
        float val = 1f;
        while (val > 0.1f)
        {
            val -= Time.deltaTime / 4;
            if (toBedestroy != null)
            {
                toBedestroy.transform.localScale = new Vector3(val, val, val);
                yield return null;
            }

        }
        if (toBedestroy != null)
        {
            particuleManager.ParticuleOn(toBedestroy.transform.position, 5, 2f);
        }
        Destroy(toBedestroy);
    }

    private GameObject RandomInt(List<GameObject> ListConcern)
    {
        int rand = Random.Range(0, ListConcern.Count);
        GameObject objConcern = (GameObject)Instantiate(ListConcern[rand]);
        objConcern.transform.localScale = new Vector3(1f, 1f, 1f);
        if (ListConcern == pictoDico.prefabTree)
        {
            treeInstances.Add(objConcern);
        }
        return objConcern;
    }

    private float GetSmallestSide(Vector3 reference)//prends sur les trois côté le plus petit
    {
        float sideSize = new float();
        if (reference.x < reference.y)
        {
            sideSize = reference.x;
        }
        else sideSize = reference.y;
        if (sideSize > reference.z)
        {
            sideSize = reference.z;
        }
        return sideSize;
    }

    private void Start()
    {
        rayon = 25f;
        skyboxManager = GameManager.instance.GetComponent<SkyboxManager>();
        particuleManager = GameManager.gameControl.GetComponent<ParticuleManager>();
        treeInstances = new List<GameObject>();
        treeCreation = false;
        pictoDicoAccess = HouseKeeper.pictoDicoAccess;//prends la ref au so de ce GO
        pictoDico = pictoDicoAccess.pictoDico[0];//get the good SO and it's variables in HomeDictionnary script
    }

    private void OnEnable()
    {
        GameManager.NewInt += TreeListener;
        GameManager.NewPlayer += PlayerListener;
    }

    private void OnDesable()
    {
        GameManager.NewInt -= TreeListener;
        GameManager.NewPlayer -= PlayerListener;
    }

    private void Update()
    {
        if (treeCreation)
        {
            //SetIntOn();//active/deactive les mesh render de l'ancien et du nouveau intérieur//mets aussi intOn sur false
        }

    }

}
