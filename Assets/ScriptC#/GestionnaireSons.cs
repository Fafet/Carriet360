﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//script sur l'audioContainer qui gére les GO reverbZone

public class GestionnaireSons : MonoBehaviour {	
	private MetaSonsGlobal metaSonsGlobal;//script métadonnés sons
	private MetaSons metaSonsThisGO;
	private AudioGUI audioGUI;
	private GUISystemNavig navigInfo;
	private AudioSourceEnvironnements audioSourceEnvironnements;
	private ParticuleManager particuleManager;
	private ZoneManager zoneManager;
	//GO et Audio
	public AudioClip clip;//clip
	public AudioSource audioSource;
	public AudioSource tempAudioS;
	public GameObject audioContainer;
	private GameObject reverbZoneJuliette;
	private GameObject soundBulles;//les bulles qui éclatent dans les musoirs
		//Lists des GO sons avec lesquels le bip est entré en collision
	public List<GameObject> reverbGO;//list des reberbGO
    public List<GameObject> oldReverbGOList;//on crée ube liste temporaire pour contenir les Go en cours de lecture en cas de changement de underinfluenceaudio afin de d'avoir le temps de les arreter tout en lançant le nouveau son
    public List<GameObject> reverbZoneJulietteList;//list des reberbGO Juliette
    public List<GameObject> reverbzoneMusoirDList;//list des reberbGO zoneMusoirD
    public List<GameObject> reverbZoneTrainingList;//list des reberbGO Training
    public List<GameObject> soundBullesList;//la liste des bulles qui explosent au passage du joueur dans les musoirs
	public List<AudioClip> totalItem;//l'ensemble des audiosClips du tableau 2D
	//int, float, private bool
	public int index;//index tableau clip
	public float clipLength;//longueur du clip
	public float clipSegment;//le temps total du clip divisé par 10.
	public bool testing;
	private bool  go;
	private float timer2;
	public int indexGlow;
	private int oldIndex;
	private float minDistanceActuel;
	public float maxDistanceActuel;
	public float lectTimeActuel;
	public float volumeActuel;
	private Vector3 positionActuel;
	private float distance;// distance du centre du GO qui emet le son actuellement
	public bool  influence;//dans la zone d'influence d'un reverbZone ou pas
	//vector3
	private Vector3 posPlayer;//la position actuelle du bip
	private Vector3 posUnderInfluenceAudio;//la position du GO qui emet le son actuellement


	
	private void  Awake (){
		metaSonsGlobal = GetComponent<MetaSonsGlobal>();
		audioSourceEnvironnements = GameObject.Find("AudioContainerEnvir").GetComponent<AudioSourceEnvironnements>();//les zones de sons Juliette 
		reverbZoneJuliette = GameObject.Find("ReverbZoneJuliette");//les zones de sons Juliette
		audioGUI = GameObject.Find("AudioContainer").GetComponent<AudioGUI>();
		navigInfo = GameObject.Find("SystemNavig").GetComponent<GUISystemNavig>();//Info sur le script GUISystemNavig
		particuleManager = GameManager.gameControl.GetComponent<ParticuleManager>();
		zoneManager = GameObject.Find("ObjetsScene").GetComponent<ZoneManager>();
		soundBulles = GameObject.Find("SoundBulles");

    }

    private IEnumerator  WaitAndInstructGo ( float second  ){
		yield return new WaitForSeconds(second);
		GameManager.instance.zoneActivation = true;
	}

	public void OnOffCollider(List<GameObject> listConcern, bool onOff){//en fonction de la forme du collider de la reverbZone, active déactive ce collider
		//if(onOff != go){
			foreach(GameObject tot in listConcern){
				if(tot.GetComponent<SphereCollider>() != null){
					foreach(SphereCollider c in tot.GetComponents<SphereCollider>()){
						c.enabled = onOff;//activation par défaut des colliders des sphéres au cas ou lors d'une précédentes visites un collider soit resté ouvert afin de laisser passer la camera sans détecter la collision
					}
				}
            if (GameManager.instance.zoneJuliette && GameManager.instance.chapitre == 1)
            {
                tot.GetComponentInChildren<MeshRenderer>().enabled = true;
            }
            if (tot.GetComponent<BoxCollider>() != null){
					foreach(BoxCollider c in tot.GetComponents<BoxCollider>()){
						c.enabled = onOff;//activation par défaut des colliders des sphéres au cas ou lors d'une précédentes visites un collider soit resté ouvert afin de laisser passer la camera sans détecter la collision
					}
				}
			}
		//}
	}
	
	private List<GameObject> CreateList(List<GameObject> fromList){//fonction de création de tableaux génériques
		List<GameObject>  listToFill = new List<GameObject>() ;
		foreach(GameObject tot in fromList){
			listToFill.Add(tot);
		}
		OnOffCollider(listToFill, true);//active les collider
		return listToFill;
	}

	private void SoundBullesManager(GameObject soundBullesConcern){
		for (int i=0;i<soundBullesList.Count;i++){
			if(soundBullesList[i].name == soundBullesConcern.name){
				AudioSource audioCurrent = audioSourceEnvironnements.AddAudio(audioSourceEnvironnements.sonInterferences , false, false,0f,1, false);//ajoute l'audioSource sur 1 frame
				StartCoroutine(audioSourceEnvironnements.VolumePlus(audioCurrent,0.0f,0.4f,0.5f,true));
				StartCoroutine(FlickeringRenderer(soundBullesList[i]));
				Vector3 bullePos = soundBullesList[i].transform.position;
				particuleManager.CollisionParticle(bullePos,4,0.2f);//implosion index 5 = fireball
				particuleManager.CollisionParticle(bullePos,5,0.2f);//implosion index 6 = shockwave
			}
		}
	}

	private IEnumerator FlickeringRenderer(GameObject flickeringRenderer){
		Renderer rend = flickeringRenderer.GetComponent<Renderer>();
		rend.enabled = false;//déactive le renderer
		yield return new WaitForSeconds(0.2f);
		rend.enabled = true;
		yield return new WaitForSeconds(0.2f);
		rend.enabled = false;//déactive le renderer
		yield return new WaitForSeconds(0.2f);
		rend.enabled = true;
	}

	private void SetUpNextSphere(GameObject reverbGOInfluence){//affiche ou elève le glow et le mesh render de la sphereSound concernée
		for(int i=0;i<GameManager.instance.reverbZoneGO.Count;i++){
			if(GameManager.instance.reverbZoneGO[i] == reverbGOInfluence){
				indexGlow = i;
				GameManager.instance.reverbZoneGO[i].GetComponentInChildren<MeshRenderer>().enabled = true;
				GameManager.instance.reverbZoneGO[i].GetComponent<SphereCollider>().enabled = true;
				GlowManager glowManager = GameManager.instance.cameraFPS.GetComponent<GlowManager>();
				glowManager.UpdateGlow(GameManager.instance.reverbZoneGO[i]);
			}
			else{
				//GameManager.instance.reverbZoneGO[i].GetComponentInChildren<MeshRenderer>().enabled = false;
				//GameManager.instance.reverbZoneGO[i].GetComponent<SphereCollider>().enabled = false;
			}		
		}
	}
	
	private void NextReverbSphere(){//permet de mettre l'index sur le texte suivant à la fin de la lecture du texte actuel
		for(int u=reverbGO.Count-1;u>=0;u--){//loop à l'envers pour prendre la reverbSound la plus proche du début du récit (lecture dans l'ordre le plus possible)
			MetaSons meta = reverbGO[u].GetComponent<MetaSons>();//metaSonsThisGO est le script métaSons qui est sur chaque GO AudioReverbZone
			if(meta.progressionPlus < 0.85f){				
				GameManager.instance.underInfluenceAudio = reverbGO[u];
				indexGlow = u;
				SetUpNextSphere(reverbGO[u]);
			}
		}
	}

	public IEnumerator GestionSon(){
		for (int u =0; u<reverbGO.Count ; u++){//boucle qui permet de récupérer l'index actuel du GO qui joue le son
			if(GameManager.instance.underInfluenceAudio == reverbGO[u]){
				oldIndex = GameManager.instance.indexlectureActuelle;//old index devient l'index précedent le nouvel index
                GameManager.instance.indexlectureActuelle = u;//ainsi que l'index 
			}
		}
		metaSonsThisGO = reverbGO[GameManager.instance.indexlectureActuelle].GetComponent<MetaSons>();//metaSonsThisGO est le script métaSons qui est sur chaque GO AudioReverbZone
		if(oldIndex == GameManager.instance.indexlectureActuelle)
        {
			if(metaSonsThisGO.audioSource.isPlaying){//il n'y a rien à faire
				testing = false;//on sort immédiatement de la fonction GestioSon
;			}
			else testing = true;//sinon on lance la mise en route du son
		}
		else{
			if(metaSonsThisGO.progressionPlus < 1){//si la lecture n'est pas encore effectuée entièrement et que isPlaying est false ( le clip audio n'est donc pas en mode play)
                testing = true;
                GameManager.PlugNewSound(reverbGO[GameManager.instance.indexlectureActuelle]);//passe Event avec le nom de la reverb en train de lire
            }
			else testing = false;//lance la fonction
		}
        if (testing){//met à jour les donnée afin de gérer le son concerné
			metaSonsThisGO.SetUpAudioInfo(GameManager.instance.indexlectureActuelle);	
			volumeActuel = metaSonsThisGO.audioSource.volume;
			positionActuel = metaSonsThisGO.transform.position;
			maxDistanceActuel = metaSonsThisGO.maxDistanceActuel;
			go = true;
		}
		while(testing){//est-on encore dans une reverbeZone ?
			posPlayer = GameManager.instance.player.transform.position;//mise à jour toutes les frames de la position du player
            if(GameManager.instance.chapitre == 1 & GameManager.instance.zoneJuliette)
            {
                posPlayer = GameManager.instance.cameraActive.transform.position;
            }
			distance = Vector3.Distance(positionActuel,posPlayer);//distance qui sépare le player du centre de diffusion du son soit le GO ReverbZone
			if (distance <= maxDistanceActuel){ //on compare cette distance avec celle enregistrée dans le parametre maxDistance du composant audioReverbZone de la zone de son concernée
				if(go){
					StartCoroutine(metaSonsThisGO.VolumePlus (0,1,0,false));//lance la fonction de lecture du son sur le script Metason. Lecture depuis là ou la lecture s'est arretée (si la lecture de calligramme est finie)			
					oldReverbGOList.Add(reverbGO[GameManager.instance.indexlectureActuelle]);
					go = false;
				}
			}
			if(oldReverbGOList.Count>1){//mécanisme qui permet de vérifier si une seule lecture à lieu. Dans le cas contraire arrete les lectures des autres ReverbZone
				for(int i=0;i<oldReverbGOList.Count-1;i++){
					MetaSons oldReverbMetaSons = oldReverbGOList[i].GetComponent<MetaSons>();
					if(oldReverbGOList[i] != reverbGO[GameManager.instance.indexlectureActuelle]){
						if(metaSonsThisGO.progressionPlus != 1){//on baisse le son de cette précedente zone de son que si la nouvelle zone n'est pas entièrement lue
							StartCoroutine(oldReverbMetaSons.VolumeMoins (false)) ;//baisse le son
						}
					}
					oldReverbGOList.RemoveAt(i);//on l'enlève du tableau
				}
			}
            if (GameManager.instance.zoneJuliette)
            {
                if (distance > maxDistanceActuel)//si on sort de la zone d'influence de ce son défini par maxDistance de la sphèreSound on arrête l
                { //idem dans l'autre sens
                    StartCoroutine(metaSonsThisGO.VolumeMoins(false));//baisse le son
                    testing = false;//sort de la couroutine
                }
            }
            yield return null;
		}
	}

	public List<GameObject>  ResetSoundContext(List<GameObject> listReverbConcern){
		if (metaSonsThisGO != null) {
			StartCoroutine(metaSonsThisGO.VolumeMoins(false));
		}
		GameManager.instance.underInfluenceAudio = null;//remise à zéro
        GameManager.instance.indexlectureActuelle = 0;
		if(reverbGO != null){
			OnOffCollider(reverbGO, true);//deactive tout les colliders des sphères sound de la zone de départ avant d'éffacer le tableau
		}
 		reverbGO = CreateList(listReverbConcern);//création du tableau des reverbGO de la zone
		//GameManager.instance.destination = reverbGO [0];//destination par défaut au cas ou
		OnOffCollider(reverbGO, true);//active tout les colliders des sphères sound de la zone de départ avant d'éffacer le tableau
		return listReverbConcern;
	}

	public void  ResetSoundLevelChange(){
		if (metaSonsThisGO != null) {
			StartCoroutine (metaSonsThisGO.VolumeMoins (false));
		}
		GameManager.instance.underInfluenceAudio = null;//remise à zéro
        GameManager.instance.indexlectureActuelle = 0;
		if (reverbGO != null) {
			OnOffCollider (reverbGO, false);//deactive tout les colliders des sphères sound de la zone de départ avant d'éffacer le tableau
		}
	}

	public void EnterSoundZoneProcess () { //en cas de non collision Ã  l'entrÃ©e du collider on dÃ©sire quand meme lancer la lecture de la zone de lecture et faire les actions nÃ©cÃ©ssaires
		GameManager.instance.underInfluenceAudio = GameManager.instance.destination;
		influence = true;//le son est audible
		StartCoroutine(GestionSon());//lance la gestion du son
	}

     private void Start()
    {
        GameManager.instance.indexlectureActuelle = 0;//0 afin que le condition première de la fonction GestionSon soit toujours vrai pour commencer le jeu
        index = 0;
        testing = true;
        go = true;
        timer2 = 0;
        foreach (Transform reverb in reverbZoneJuliette.transform)
        { //cherche dans reverbZone les enfants et mets a jour les metas données
            reverbZoneJulietteList.Add(reverb.transform.gameObject);//incrémente et met à jour des tableaux de paramètres du son
        }
    }
	
	private void  Update (){
		timer2 += Time.deltaTime;//mets en route le timer à partir du moment ou l'on dit ok pour le son
		Vector3 temp3 = GameManager.instance.cameraActive.transform.position;
		audioContainer.transform.position = temp3;// changement d'audiolistener. l'audiosource s'aligne dans l'espace
	}	
}