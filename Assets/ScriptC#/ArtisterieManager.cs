﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArtisterieManager : MonoBehaviour {
    //script sur le GO Artisterie qui gère l'artisterie
    private GlowManager glowManager;
    private GameObject solArtisterie;//reseau de dalles du sol de l'artisterie
    private GameObject player;
    private Vector3 positionSol;//postion de l'artisterie au niveau du sol
    private Vector3 positionAir;//position idéal au plus haut dans les airs
    private float idealPositionY;//
    private float offset;
    public List<GameObject> cellsList;
    private GameObject spriteRoot;//ensemble des GO Sprite
    private GameObject babyFoot;
    private List<GameObject> spriteGOList;
    public List<Sprite> spriteList;//liste de sprite à faire changer
    public Material defaultMaterial;
    private List<int> elevationLevelList;//élévation idéale des cellules
    private List<Vector3> cellsPosList;//position de départ des cellules
    private List<Dictionary<int, int>> dicoList;
    private List<Material> matList;//list des mat de cellules
    private int longueur;//colonne totales du tableau
    private bool solDance;//faut'il mettre en route le sol ?


    private void Awake()
    {
        foreach(Transform child in this.transform)//assigne les GO
        {
            if(child.gameObject.name == "ArtisterieSol")
            {
                solArtisterie = child.gameObject;
            }
            if (child.gameObject.name == "SpriteRoot")
            {
                spriteRoot = child.gameObject;
            }
            if (child.gameObject.name == "BabyFoot")
            {
                babyFoot = child.gameObject;
            }
        }
        SetActiveGO(false);//déactive tout le monde
    }

    private void PlayerListener(GameObject e)
    {
        player = e;
    }

    private void SetActiveGO(bool onOff)
    {
        babyFoot.SetActive(onOff);
        solArtisterie.SetActive(onOff);
        spriteRoot.SetActive(onOff);
    }

    private void GetPlayerDist()//appelé par InvokeRepeating
    {
        float distActeulle = Mathf.Abs(Vector3.Distance(positionSol, player.transform.position));//distance entre le player et l'artisterie au niveau du sol
        idealPositionY = 10000f;
        if (distActeulle < 1000f)
        {
            idealPositionY = ((positionAir.y - positionSol.y) / 80f) * distActeulle;
        }
        if (distActeulle < 20f)
        {
            idealPositionY = 0f;
            solDance = true;
            GetComponent<Animator>().SetBool("onOff", true);
            SetActiveGO(true);//active l'intérieur de l'artisterie
        }
        else {
            solDance = false;
            GetComponent<Animator>().SetBool("onOff", false);
            SetActiveGO(false);
        }
        // StartCoroutine(RaiseArtisterie());
        if (distActeulle >= 200f)
        {
            solDance = false;
        }
        if (solDance)
        {
            ChangeSprite();
        }
    }

    private void SetUpSolArtisterie()
    {
        cellsList = new List<GameObject>();
        dicoList = new List<Dictionary<int, int>>();
        elevationLevelList = new List<int>();
        cellsPosList = new List<Vector3>();
        matList = new List<Material>();
        spriteGOList = new List<GameObject>();
        foreach (Transform cell in solArtisterie.transform)//construit la liste de cellules dans l'ordre
        {
            cellsList.Add(cell.gameObject);
        }
        longueur = 10;
        int increment = 0;
        Dictionary<int, int> tempDico = new Dictionary<int, int>();
        for (int i = 0; i < cellsList.Count; i++)
        {
            cellsPosList.Add(cellsList[i].transform.position);//stock les position de base des cellules
            if (increment >= longueur)
            {
                dicoList.Add(tempDico);//ajoute le nouveau dictionnaire à la liste de dico
                tempDico = new Dictionary<int, int>();
                increment = 0;//remise à zero pour le nouveau dico
            }
            if (increment < longueur)
            {
                int modulo = (i + 1) % longueur;
                if (modulo == 0)//permet de faire une suite consistante de chiffres: 1,2,3....6 au lieu de ,2,3....0
                {
                    modulo = longueur;
                }
                tempDico.Add(modulo, i);//crée un dico avec d'un côté le modulo en clefs et de l'autre en valeur l'index de la liste principale de cellules
                //Debug.Log(modulo + " modulo");
                increment += 1;
            }
            if (i == cellsList.Count - 1)
            {
                dicoList.Add(tempDico);//ajoute le dernier dictionnaire à la liste de dico avant de sortir de la boucle
            }
            if (i < longueur)
            {
                matList.Add(new Material(defaultMaterial));//crée 20 materiaux de couleur différente
                matList[i].color = RandomColor();//defini une couleu au hasard

            }
            foreach(Transform sprit in spriteRoot.transform)
            {
                spriteGOList.Add(sprit.gameObject);
            }
        }
    }

    private int GetClosestCell()//cherche la cellule du sol la plus proche du joueur
    {
        float distCell = 1000000f;
        int indexClosest = 1000;
        for (int i = 0; i < cellsList.Count; i++)
        {
            float distActeulle = Mathf.Abs(Vector3.Distance(cellsList[i].transform.position, player.transform.position));
            if (distActeulle < distCell)
            {
                distCell = distActeulle;
                indexClosest = i;
            }
        }
        return indexClosest;
    }

    private void DefineElevation()
    {
        int indexCenter = GetClosestCell();//récupére le centre de calcul soit la cellule la plus proche du joueur
        int colonneCenter = Mathf.RoundToInt(indexCenter / longueur);
        int moduloCenter = new int();//la valeur du modulo pour le centre
        elevationLevelList.Clear();
        foreach (KeyValuePair<int, int> clefs in dicoList[colonneCenter])//recherche dans la colonne qui va bien
        {
            if (clefs.Value == indexCenter)
            {
                moduloCenter = clefs.Key;//la valeur du modulo pour le centre
            }
        }
        for (int i = 0; i < dicoList.Count; i++)
        {
            int elevationLevel = new int();
            foreach (KeyValuePair<int, int> keyss in dicoList[i])
            {
                int deltaColonne = Mathf.Abs(i - colonneCenter);
                deltaColonne = Mathf.Clamp(deltaColonne, 0, longueur);
                int deltaRow = Mathf.Abs(moduloCenter - keyss.Key);
                deltaRow = Mathf.Clamp(deltaRow, 0, longueur);
                if (deltaColonne == 0)
                    if (deltaColonne >= 0 && deltaRow <= deltaColonne)
                    {
                        elevationLevel = deltaColonne;
                    }
                if (deltaRow >= 0 && deltaRow <= longueur)
                {
                    if (deltaColonne < deltaRow)
                    {
                        elevationLevel = deltaRow;
                    }
                    else elevationLevel = deltaColonne;
                }
                if (elevationLevel >= 1)
                {
                    elevationLevel = elevationLevel - 1;
                }
                elevationLevelList.Add(elevationLevel);
            }
        }
        UpdateCellsPos(indexCenter);
    }

    private void UpdateCellsPos(int indexCentre)
    {
        for (int i = 0; i < cellsList.Count; i++)
        {
            cellsList[i].transform.position = Vector3.Lerp(cellsList[i].transform.position, cellsPosList[i] + (new Vector3(0f, 0.2f * elevationLevelList[i], 0f)), Time.deltaTime*2f);//élévation effective
            cellsList[i].GetComponent<MeshRenderer>().material = matList[elevationLevelList[i]];
        }
        float emission = Mathf.PingPong(Time.time, 0.5f);
        Color finalColor = matList[0].GetColor("_Color") * Mathf.LinearToGammaSpace(emission);
        matList[0].SetColor("_EmissionColor", finalColor);
    }

    private void ChangeSprite()
    {
        foreach( GameObject sprit in spriteGOList)
        {
            sprit.GetComponent<SpriteRenderer>().sprite = spriteList[Random.Range(0, spriteList.Count)];//prends un sprite dans la liste au hasard
        }
    }

    private Color RandomColor()
    {
        Color newColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        return newColor;
    }

    private void Start()
    {
        glowManager = GameManager.instance.cameraFPS.GetComponent<GlowManager>();
        longueur = 10;
        offset = 30f;
        positionSol = this.transform.position;//enregistre la position de départ de l'artisterie
        positionAir = positionSol + new Vector3(0f,offset,0f);
        InvokeRepeating("GetPlayerDist", 1f, 0.3f);
        solDance = false;
        foreach(Transform sol in this.gameObject.transform)
        {
            if(sol.gameObject.name == "ArtisterieSol")//trouve le GO sol de l'artisterie
            {
                solArtisterie = sol.gameObject;
            }
        }
        SetUpSolArtisterie();
    }

    private void OnEnable()
    {
        GameManager.NewPlayer += PlayerListener;
    }

    private void OnDesable()
    {
        GameManager.NewPlayer -= PlayerListener;
    }

    private void Update()
    {
        if (solDance)
        {
            DefineElevation();
        }
        Vector3 posAritsterie = positionSol + new Vector3(0f, idealPositionY, 0f);
        this.transform.position = Vector3.Lerp(this.transform.position, posAritsterie, Time.deltaTime);
    }
}
