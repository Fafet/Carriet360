﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class GUIPrompteur : MonoBehaviour {
	
	//script attaché à chaque PrompteurPerspective qui gére l'affichage du texte dans chaque prompteur

	private GameObject textPerspective;//le Go qui contient le composant text qui s'affiche dans le jeu en perspective
	private TextAsset texteAsset;//le texte concerné dans les metadonnées de MetaSons
	private GameObject goActuel;//le GameManager.instance underinfluenceaudio concerné
	public string[] texteToDisplay;//le texte convertis en char pour l'affichage
	public int currentPosition;//la position de l'index dans le tableau des char du texte
	public static bool prompteurGo;//si le flag est vrai alors on affiche le texte sur le prompteurPerspective
	private Text textPromp;//le texte qui s'affiche effectivement dans le GO TextPerspective
	private bool nextWord;//substring a fini son affichage donc on peu passer au suivant
	private int increment;//correspond au nombre de mot à afficher dans le prompteur
    public List<Font> fontList;//liste de polices de caractères
	private int randomFontsize;
	private int randomFont;
	private float randomColor;

	private void OnEnable()
    {
        prompteurGo = false;
		textPromp = GetComponentInChildren<Text>();
    }

	private void OnDisable(){
		StopAllCoroutines();
	}

	private void RandomFunction(){
		randomFontsize = Random.Range(3,5);
		//randomColor =  Random.Range(0f,1f);
		randomFont =  Random.Range(0,fontList.Count);
		textPromp.fontSize = 1;
		//Color col = textPromp.color;
		//textPromp.color = new Color(randomColor,col.g,col.b,col.a);
		textPromp.font = fontList[randomFont];
	}

    public void TexteToDisplay(){//set up du texte actuel (ne gére pas l'affichage) 
		if(!prompteurGo){//verifie si le prompteur n'est pas déja en train de faire son boulot pour ne pas le relancer une seconde fois
			increment = 0;
            currentPosition = 0;
			prompteurGo = true;
			nextWord = true;
			StartCoroutine(DrawText());
		}
	}
	
	private IEnumerator DrawText(){
		float pas = 0.2f;//donne le rytme d'affichage
		float timer = Time.time;
        textPromp.text = " ";//en sortant de la couroutine efface le texte affiché
        while (prompteurGo)
        {
            if (Time.time > timer + pas)
            {//toutes les 0.05f secondes on affiche un nouveau mot grace à la fonction WriteText
                WriteText();
                timer = Time.time;
            }
            yield return null;
        }
	}
	
	private void  WriteText (){//fonction de traitement du string mot à mot du texte
		if(currentPosition < texteToDisplay.Length){
			string prompteurWordTemp = textPromp.text + " "+ texteToDisplay[currentPosition];
            textPromp.text = " ";
            textPromp.text = prompteurWordTemp;
            currentPosition += 1;			
		}
	}
	
    private static string[] ConvertStringToCharArray(string aText)
    { //fonction qui convertis un texteString en tableau de char
        string[] textToString = aText.Split();//convertis le texte du prompteur en Array de string
        return textToString;
    }

    public void  PrompteurDatas (TextAsset textConcern){//fonction de récupération des metadatas sur le script MetaSons
		texteToDisplay = ConvertStringToCharArray(textConcern.text);
        currentPosition = 0;
	}
}