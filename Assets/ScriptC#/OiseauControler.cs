﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OiseauControler : MonoBehaviour {
    //script présent sur chaque oiseau qui enclenche les modes de vol de l'oiseau
    private OiseauxManager oiseauxManager;
    private OiseauData thisOiseauData;
    private VehiculeManager vehiculeManager;
    private GameObject player;

    private GameObject birdRover;

    public int indexBird;
    private Animator anim;
    private Rigidbody birdRB;
    private Vector3 groundPoint;
    private float progression;
    private float distance;

    private Vector3 p0;
    private Vector3 p1;
    private Vector3 p2;
    private Vector3 p3;
    private LineRenderer lineRenderer;
    private float speed;
    public float takeOffTimer;
    private int random;

    private void Awake()
    {
        oiseauxManager = GameObject.Find("OiseauxRoot").GetComponent<OiseauxManager>();
        birdRover = GameObject.Find("Bird_Rover");
        vehiculeManager = GameObject.Find("VehiculesRoot").GetComponent<VehiculeManager>();
    }

    private void PlayerListener(GameObject e)
    {
        player = e;
  
    }

    private void BirdModeListener(OiseauData e)
    {
        if (e == oiseauxManager.datatPlayerIsOn)//si l'oiseau sur lequel est le player qui a déclenché l'event est cet oiseau avec ce script
        {
            if (e == thisOiseauData)//si c'est cet oiseau sur lequel est monté le player (defini par l'event)
            {
                thisOiseauData.birdMode = BirdFly;//relance le vol
                thisOiseauData.playerOnThatBird = true;
                GameManager.instance.destination = thisOiseauData.bird;
            }
        }
        else thisOiseauData.playerOnThatBird = false;
    }

    public void FloorRayCast()
    {
        if (takeOffTimer > 5f)//On fait le raycast uniquement si on à pris de l'atitude
        {
            LayerMask colliderLayer = 1 << 8;
            Ray ray = new Ray(this.transform.position, -transform.up);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 5000f, colliderLayer))
            {
                groundPoint = hit.point;
                float toGround = Vector3.Distance(groundPoint, this.transform.position);
                if (toGround < 10f)
                {
                    if (thisOiseauData.birdMode != Landing)
                    {
                        takeOffTimer = 0f;//reset
                        birdRB.isKinematic = false;
                        thisOiseauData.birdMode = Landing;
                    }
                }
            }
        }
    }

    public void BirdFly()//mode vol
    {
        takeOffTimer += Time.deltaTime;
        anim.SetBool("land", false);
        anim.SetBool("fly", true);

        FlyingControler();
        if (progression >= 1f)
        {
            progression = 0f;//reset
            PointCurve();
        }
    } 

    public void Landing()
    {
        birdRB.isKinematic = false;
        progression = 0;
        speed = Mathf.Lerp(speed, 0f,Time.fixedDeltaTime*0.05f);
        float distance = Mathf.Abs(this.transform.position.y - groundPoint.y);
        if (distance < 5f)//l'oiseau n'avance plus
        {
            anim.SetBool("fly", false);
            anim.SetBool("land", true);
        }
        else {
            birdRB.AddForce(transform.forward * speed * 100f, ForceMode.VelocityChange);
        }
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.AngleAxis(0f, transform.up), Time.fixedDeltaTime);
    }

    private void ChoiseDestination()
    {
        List<GameObject> hydroCellsOk = oiseauxManager.GetGoodHydroCells();//prends uniquesment les cellules Hydro avec une élévation de zero
        int oldRandom = random;
        random = Random.Range(0, hydroCellsOk.Count);//prends dans cette nouvelle liste une cellule au hasard
        for(int i = 0; i < 10; i++)
        {
            if (oldRandom == random)//Au cas où la nouvelle destination soit la même que l'ancienne on continue à itérer
            {
                random = Random.Range(0, hydroCellsOk.Count);//prends dans cette nouvelle liste une cellule au hasard
            }
            else
            {
                thisOiseauData.destination = hydroCellsOk[random];//on prends un nouveau GO de destination pour l'oiseau dans la liste des hydro
                thisOiseauData.altitude = thisOiseauData.destination.transform.position + oiseauxManager.defaultAltitude + oiseauxManager.RandomVector3() + new Vector3(0f, (distance / 100f), 0f);
                return;
            }
        }
    }

    private void FlyingControler()
    {
        birdRB.isKinematic = true;
        distance = Mathf.Abs(Vector3.Distance(this.transform.position, thisOiseauData.altitude));//distance du centre magnetique
        float random = Random.Range(0.03f, 0.08f);
        speed = Mathf.Lerp(speed, random, Time.deltaTime * (random / 2f));
        speed = Mathf.Clamp(speed, 0.02f, 1f);
        anim.SetFloat("birdSpeed", speed*100f);//adapte le batement d'aile à la vitesse de l'oiseau
        progression += Time.deltaTime * speed;
        progression = Mathf.Clamp(progression, 0f, 1f);
        //position
        transform.position = Bezier.GetPoint(p0, p1, p2, p3, progression);
        if (thisOiseauData.playerOnThatBird && oiseauxManager.datatPlayerIsOn != null)
        {
            Vector3 correctPos = vehiculeManager.ConvertOffsetPlayerPos(vehiculeManager.indexVehicule, vehiculeManager.offsetDestinationPos[vehiculeManager.indexVehicule]);
            player.transform.position = Bezier.GetPoint(p0, p1, p2, p3, progression)+correctPos;
        }
        //rotation
        float progPlus = progression + 0.1f;
        progPlus = Mathf.Clamp(progPlus, 0f, 1f);//maxi 1
        Vector3 posInFront = Bezier.GetPoint(p0, p1, p2, p3, progPlus);
        Vector3 toAngle = posInFront - this.transform.position;//To - from
        float angle = (Vector3.Angle(transform.forward, toAngle)) / 2f;//permet de connaitre l'angle d'ou l'oiseau est jusqu'a devant l'oiseau
        angle = angle * Mathf.Sign(Vector3.Cross(transform.position, toAngle).y);//permet de savoir si l'angle est positif ou négatif
        angle = Mathf.Clamp(angle, -80f, 80f);//pas d'angle dépassant 80 ° sinon l'oiseau se retrouve la tête en bas
        Vector3 direction = posInFront - this.transform.position;//destination - position actuelle de l'oiseau
        if(direction != Vector3.zero)
        {
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction) * Quaternion.Euler(0f, 0f, -angle) , Time.deltaTime * 4f);//* Quaternion.FromToRotation(transform.up,Vector3.up)
        }       
        DrawLineRender();
    }

    public void PointCurve()
    {//fonction qui permet de calculer les 4 points nécéssaires à la création d'une spline. Déclenché par QuitInventory sur AudioGUI
        ChoiseDestination();//choisi la destination au hasard avant de lancer PointCurve
        Vector3 direct = -(p2 - p3);
        p0 = this.transform.position;// + new Vector3(0.0f,-2.0f,0.0f);
        p3 = thisOiseauData.altitude;//affecte au dernier point de la spline la destination
        Vector3 direction = p3 - p0;//calcul du vecteur de direction du player à la destination en ligne droite
        Vector3 randomVector = oiseauxManager.RandomVector3();
        p1 = p0 + new Vector3((direct.x) / 3f, 0f, direct.z / 3f);
        p1 += CastRayForPointCurve(p1);
        p2 = p0 + new Vector3((direction.x) / 1.5f, 0f, (direction.z) / 1.5f) + new Vector3(randomVector.x,randomVector.y,randomVector.x);
        p2 += CastRayForPointCurve(p2);
    }

    private Vector3 CastRayForPointCurve(Vector3 p)
    {
        LayerMask colliderLayer = 1 << 8;
        Ray ray = new Ray(p, -transform.up);
        RaycastHit hit;
        float toGround = 1000000f;
        if (Physics.Raycast(ray, out hit, 5000f, colliderLayer))
        {
            groundPoint = hit.point;
            toGround = Mathf.Abs(Vector3.Distance(groundPoint, this.transform.position));
            if (toGround < 5f )//si on est près du sol et que ce n'est pas une cellule Hydro (Si cellule Hydro il faut faire remonter l'oiseau grâce au prochain raycast tiré vers le haut
            {
                p += new Vector3(0f, toGround + 5f, 0f);///ajoute un peu de hauteur
            }
            if (hit.collider.gameObject.name.Contains("Plan"))
            {
                p += new Vector3(0f, 200f, 0f);//remonte drastiquement
            }
        }
        return p;
    }

    private void DrawLineRender()
    {//déssine à l'écran le trajet de destination
        float t;
        int totalPoint = 30;
        lineRenderer.SetVertexCount(totalPoint);
        for (int li = 0; li < totalPoint; li++)
        {
            t = li / (float)totalPoint;
            Vector3 position = Bezier.GetPoint(p0, p1, p2, p3, progression * t);
            lineRenderer.SetPosition(li, position);
        }
    }

    private void OnEnable()
    {
        GameManager.NewPlayer += PlayerListener;
        OiseauxManager.NewBirdMode += BirdModeListener;
    }

    private void OnDesable()
    {
        GameManager.NewPlayer -= PlayerListener;
        OiseauxManager.NewBirdMode -= BirdModeListener;
    }

    public void Start()
    {
        p0 = new Vector3();
        p1 = new Vector3();
        p2 = new Vector3();
        p3 = new Vector3();
        anim = GetComponent<Animator>();
        birdRB = GetComponent<Rigidbody>();
        thisOiseauData = oiseauxManager.oiseauxData[indexBird];//get the good oiseauData
        thisOiseauData.birdMode = Landing;
        lineRenderer = this.gameObject.AddComponent<LineRenderer>();
        lineRenderer.useWorldSpace = true;
        lineRenderer.sharedMaterial = GameManager.instance.matLineRenderer;
        lineRenderer.SetWidth(0.2f, 0.05f);
        progression = 0f;  
        //PointCurve();
        InvokeRepeating("FloorRayCast", 1f, 0.01f);//toutes les secondes vérifie si oui ou non on est loin du sol//si oui passe en mode Landing
        PlayerListener(GameManager.instance.player);
        thisOiseauData.playerOnThatBird = false;
    }

    public void FixedUpdate()
    {
        if (oiseauxManager.birdOn)
        {

            if (thisOiseauData.birdMode == Landing)//moteur physic pour l'attérissage
            {
                thisOiseauData.birdMode();
                if (thisOiseauData.playerOnThatBird && oiseauxManager.datatPlayerIsOn != null)
                {
                    player.transform.position = this.transform.position;
                }
            }
        }
    }

    public void Update()
    {
        if (oiseauxManager.birdOn)
        {
            if (thisOiseauData.birdMode == BirdFly)//déplacement classique pour le vol (pas de caluls pysics)
            {
                thisOiseauData.birdMode();
            }
        }
    }
}
