﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticuleManager : MonoBehaviour {
	
	//script attaché au GameControl
	//le GO particule regroupe l'ensemble des particulesSystem du jeu et est donc le parent des particuleSystem

	public List<GameObject> particuleGOList;
	private GameObject particules;

	private void Awake(){
		particules = GameObject.Find("Particules");
    }
	
	public void  ParticuleOn ( GameObject particuleActif ,   bool off ,    float offTime  ){//met en route le systéme de particule
		particuleActif.SetActive(true);
        ParticleSystem.EmissionModule em = particuleActif.GetComponent<ParticleSystem>().emission;
        em.enabled = true;
		if(off){
			StartCoroutine(WaitAndParticuleOff(particuleActif,offTime));//lance la fonction d'arret du système de particule si on décide de l'arreter rapidement
		} 
	}

    public void ParticuleOn(Vector3 contacts, int paticuleAsGO, float seconds){
		GameObject tempParticule = Instantiate(particuleGOList[paticuleAsGO],contacts,particuleGOList[paticuleAsGO].transform.rotation)as GameObject;
		tempParticule.SetActive(true);
		//tempParticule.GetComponent<ParticleSystem>().enableEmission = true;
		Destroy(tempParticule,seconds);
	}
	
	private IEnumerator  WaitAndParticuleOff ( GameObject particuleGO ,   float second  ){
		yield return new WaitForSeconds(second);
		particuleGO.SetActive(false);
	}

    public void ParticulesOff(GameObject particuleGO)
    {
        particuleGO.SetActive(false);
    }

	public void CollisionParticle(ContactPoint contacts, int paticuleAsGO, float seconds){
		GameObject tempParticule = Instantiate(particuleGOList[paticuleAsGO],contacts.point,particuleGOList[paticuleAsGO].transform.rotation)as GameObject;
		tempParticule.SetActive(true);
        ParticleSystem.EmissionModule em = tempParticule.GetComponent<ParticleSystem>().emission;
       em.enabled = true;
		Destroy(tempParticule,seconds);
		Debug.Log("destroyed in "+seconds);
	}

	public void CollisionParticle(Vector3 contacts, int paticuleAsGO, float seconds){
		GameObject tempParticule = Instantiate(particuleGOList[paticuleAsGO],contacts,particuleGOList[paticuleAsGO].transform.rotation)as GameObject;
		tempParticule.SetActive(true);
        ParticleSystem.EmissionModule em = tempParticule.GetComponent<ParticleSystem>().emission;
        em.enabled = true;
		Destroy(tempParticule,seconds);
	}

	private void OnEnable(){
        particuleGOList = new List<GameObject>();
        foreach (Transform part in particules.transform){
			particuleGOList.Add(part.gameObject);//crée la liste des particules dans l'ordre de la hierarchie
            part.gameObject.SetActive(false);
		}
	}
}
