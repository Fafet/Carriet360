﻿//script de character controler perso

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerControler : MonoBehaviour {
    public GameObject sphre1;
    public GameObject sphre12;
    public GameObject sphre13;
    public GameObject sphre14;

    private ZoneManager zoneManager;
	private Ray ray;
	private bool  go;
    private bool navigating;
    private bool mouveOnCurveIsRunning;//déplacement du joueur en transit
    public bool mouveSpline;
    private bool localIsMagnetique;
    private bool gravityIsRunning;
    private bool jumpAndFalseIsRunning;
    public bool magnetismeIsOn;
    private bool landingCheck;//est-on dans les airs en train de vérifier si le joueur touche ou non le sol

    private Vector3 p0;
    private Vector3 p1;
    private Vector3 p2;
    private Vector3 p3;
    private GameObject player;
    public GameObject collidingGO;//le go qui collide sur le calque 23 du script OnCollisionPlayer
    private GameObject gps;
    public VehiculeManager vehiculeManager;
	private GestionnaireSons gestionnaireSons;/// <summary>
	private AudioGUI audioGUI;
	private SplineManager splineManager;//le script qui gére les splines
	private GUISystemNavig gUISystemNavig;//
    private OiseauxManager oiseauxManager;//
    private AudioManager audioManager;
    /// 
    /// </summary>
	private Vector3 rayDirection;
    public Vector3 velocity;//rigidbody player velocity
	private Vector3 velocityForce;
    private Vector3 magnetisme;//magnétisme si nécéssaire à ajouter aux forces appliquée au joueur
    private Vector3 angleChange;
    public Vector3 offsetPlayer;// décalage de la position du player pour aligner les pieds sur le véhicule
    private Vector3 neutralRot;//rotation du RB du player au moment du neutral
    public Vector3 directionRB;//la direction de déplacement du RB du joueur
    public Rigidbody playerRB;//le rigidbody du player actif
	private Animator animator;
    public RuntimeAnimatorController animControlInactif;
    public RuntimeAnimatorController animControlActif;

    private Material playerMat;
    public Material playerMatAlpha;//le shader de remplacement si l'on veut faire disparaitre le player momentanément en utilsant la couche alpha d'un shader
    //deplacement click droit dans la zone musoirD
    public int indexIdealPlayer;//l'index de la spline le plus proche du joueur (valeur du tableau de correspondance Time/position sur SplineManager)
    public int indexIdealDesti;//l'index de la spline le plus proche de la destination (valeur du tableau de correspondance Time/position sur SplineManager)
    public int indexIdealCamera;//idem pour la camera
    private float floorDistance;//distance du joueur au sol en cas de saut ou transit
    public float distance;//distance entre la destination et le joueur
    public float progression;//la position le long d'une spline exprimé en temps (de 0 à 1)
    public float distanceSplinePoint;//distance du player au point de la spline sur lequel on à choisi de s'aligner avant de se déplacer
    public float speedMarche;
    private float oldSpeed;
    public float aditionalGravity;//set a donner de la vitesse en cas de descente du bip
    public float pasProgression;//à quelle vitesse se déplacer le long de la courbe
    public float jumpGravity;//gravitée en plus au moment du Jump
    private float force;//force applicable au RB du joueur
    private float addForce;//force supplémentaire avec up arrow
    public float speed;//vitesse pour amener le joueur sur la spline
    public float pasProgressionMin;//minimum rythme de progression le long d'une courve (mouveOnSpline)
    public float pasProgressionMax;//maximum rythme de progression le long d'une courve (mouveOnSpline)
    public float pasProgressionIdeal;//vitesse de visite idéale
    private float scrollForce;

    public delegate void SetUpRayDirection();//delegate de calcul de la postion de la souris à l'écran
    public SetUpRayDirection setUpRayDirection;

    public delegate void SetUpMouve();
    public SetUpMouve setUpMouve;

    public delegate void BoiteAVitesse();
    public BoiteAVitesse boiteAVitesse;

	private void  Awake (){
		gestionnaireSons = GameObject.Find("AudioContainer").GetComponent<GestionnaireSons>();
		audioGUI = GameObject.Find("AudioContainer").GetComponent<AudioGUI>();
		gUISystemNavig = GameObject.Find("SystemNavig").GetComponent<GUISystemNavig>();
		zoneManager = GameObject.Find("ObjetsScene").GetComponent<ZoneManager>();
        splineManager = GameObject.Find("SplineManager").GetComponent<SplineManager>();
        vehiculeManager = GameObject.Find("VehiculesRoot").GetComponent<VehiculeManager>();
        oiseauxManager = GameObject.Find("OiseauxRoot").GetComponent<OiseauxManager>();
        audioManager = GameObject.Find("AudioContainer").GetComponent<AudioManager>();
        gps = GameObject.Find("GPS");
    }

    private void PlayerListener(GameObject e)
    {
        player = e;
    }

    public void NavigatingListener(bool e)
    {
        navigating = true;
    }

    private void Listener(string e)//permet de sauter lorsque l'évent sur EventManagerPlayer est déclenché. EventManagerPlayer est un composant du GO eventSystem
    {
        if (e == "Neutral")
        {
            boiteAVitesse = Neutral;
        }
        if (e == "MarcheAvant")
        {
            boiteAVitesse = MarcheAvant;
        }
        if (e == "MarcheArriere")
        {
            boiteAVitesse = MarcheArriere;
        }
        if (e == "Jump")
        {//si on frappe la barre d'espace pour sauter
            addForce = 0f;
            force = 0.3f * Mathf.Clamp(MouseManager.yPoint, 0.5f, 1f);//adapter la vitesse en fonction de la position de la sourie à l'écran;
            jumpGravity = 0.3f;//force maxi en début de saut
            gUISystemNavig.barresEspace += 1;//ajoute 1 au systeme d'affichage des infos bulles
            if (!EventManagerPlayer.engineStateBool[4])//pas en Transit{
            {
                animator.SetTrigger("jump");
            }
            boiteAVitesse = Jump;
            if (!landingCheck)//si on a pas déja lancé la fonction
            {
                StartCoroutine(Landing());//on la lance afin de vérifier quand le joueur touche le sol et le dire à l'eventSystemManager
            }
        }
        if (e == "Transit")
        {
            GameManager.instance.isGrounded = false;
            force = 0f;
            PointCurveReverb(gestionnaireSons.reverbGO[audioGUI.indexDestination]);//crée la courbe de déplacement idéal que doit suivre le player 
            if (!landingCheck)//si on a pas déja lancé la fonction
            {
                StartCoroutine(Landing());//on la lance afin de vérifier quand le joueur touche le sol et le dire à l'eventSystemManager
            }
            boiteAVitesse = Transit;
        }
        if (e == "EnVoiture")
        {
            force = 0f;
            if (GameManager.instance.zoneJuliette)
            {              
                PointCurve(GameManager.instance.destination);//crée la courbe de déplacement idéal que doit suivre le player 
            }
            boiteAVitesse = EnVoiture;
        }
        if (e == "Train")
        {
            force = 0f;
            if (GameManager.instance.zoneJuliette)
            {
                PointCurve(GameManager.instance.destination);//crée la courbe de déplacement idéal que doit suivre le player 
            }
            boiteAVitesse = EnTrain;
        }

        if (e == "Wagon")
        {
            force = 0f;
            boiteAVitesse = Wagon; 
        }
        if (e == "Bird")
        {
            force = 0f;
            if (GameManager.instance.zoneJuliette)
            {
                PointCurve(GameManager.instance.destination);//crée la courbe de déplacement idéal que doit suivre le player  
            }
            boiteAVitesse = Bird;
        }
    }

    public void Neutral()//speedMarche est compris entre 0 et 0
    {
        addForce = 0f;
        directionRB = neutralRot;
        speedMarche = Mathf.Clamp(speedMarche, -0.3f, 1f);//clamp la valeur de speedMarche à moins de 1 au cas ou juste avant sa valeur avait dépassé 1 (en cas de Transit par exemple)
        if (speedMarche > 0.1f || speedMarche < -0.1f)
        {
            speedMarche = Mathf.Lerp(speedMarche, 0f, Time.fixedDeltaTime * 8f);//amene speedMarche à zéro
        }
        else speedMarche = 0f;
        //Position
        neutralRot = Vector3.Normalize(playerRB.velocity);
        Vector3 velocityRay = new Vector3();
        velocityRay = Vector3.zero;
        playerRB.AddForce(-playerRB.velocity, ForceMode.VelocityChange);//on applique la force au rigidbody du playerer
        gUISystemNavig.MarcheCurseur();
    }

     public void MarcheAvant()//speedMarche est compris entre -0.3 et 1
    {
        speedMarche = Mathf.Clamp(speedMarche, -0.3f, 1f);//clamp la valeur de speedMarche à moins de 1 au cas ou juste avant sa valeur avait dépassé 1 (en cas de Transit par exemple)
        float speedTarget = MouseManager.yPoint-0.3f;//adapter la vitesse en fonction de la position de la sourie à l'écran
        speedMarche = Mathf.Lerp(speedMarche, speedTarget, Time.fixedDeltaTime * 1.5f);
        if (Mathf.Abs(transform.InverseTransformDirection(playerRB.velocity).z) > 2f)//si le RB prends de la vitesse on aligne magnetudeCamera sur la velocity du RB Sinon on reste sur la position enregistrée lors du passage au Neutral
        {
            directionRB = Vector3.Lerp(directionRB, Vector3.Normalize(playerRB.velocity), Time.fixedDeltaTime * 3f);
            addForce = 0f;
        }
        if (Mathf.Abs(transform.InverseTransformDirection(playerRB.velocity).y) > 1.5f)
        {
            animator.SetBool("fall", true);
        }
        else animator.SetBool("fall", false);
        //Position
        Vector3 offsetVehicule = vehiculeManager.vehiculeList[vehiculeManager.indexVehicule].transform.position + vehiculeManager.ConvertOffsetPlayerPos(vehiculeManager.indexVehicule, vehiculeManager.offsetDestinationPos[vehiculeManager.indexVehicule] + offsetPlayer);
        player.transform.position = Vector3.Lerp(player.transform.position, offsetVehicule, Time.fixedDeltaTime * 10f);
        player.transform.rotation = Quaternion.Lerp(player.transform.rotation, vehiculeManager.vehiculeList[vehiculeManager.indexVehicule].transform.rotation, Time.fixedDeltaTime * 7f);
        gUISystemNavig.MarcheCurseur();
    }

    public void MarcheArriere()//speedMarche est compris entre -0.3 et 1
    {
        speedMarche = Mathf.Clamp(speedMarche, -0.3f, 1f);//clamp la valeur de speedMarche à moins de 1 au cas ou juste avant sa valeur avait dépassé 1 (en cas de Transit par exemple)
        float speedTarget = 0.5f- MouseManager.yPoint;//adapter la vitesse///////////////////////////////
        speedMarche = Mathf.Lerp(speedMarche, -speedTarget, Time.fixedDeltaTime * 1f);
        Vector3 vectorTo = Vector3.Normalize(playerRB.velocity);
        directionRB = Vector3.Lerp(neutralRot, vectorTo, Time.fixedDeltaTime);
        addForce = 0f;
        //Position
        Vector3 offsetVehicule = vehiculeManager.vehiculeList[vehiculeManager.indexVehicule].transform.position + vehiculeManager.ConvertOffsetPlayerPos(vehiculeManager.indexVehicule, vehiculeManager.offsetDestinationPos[vehiculeManager.indexVehicule] + offsetPlayer);
        player.transform.position = Vector3.Lerp(player.transform.position, offsetVehicule, Time.fixedDeltaTime * 10f);
        player.transform.rotation = Quaternion.Lerp(player.transform.rotation, vehiculeManager.vehiculeList[vehiculeManager.indexVehicule].transform.rotation, Time.fixedDeltaTime * 7f);
        gUISystemNavig.MarcheCurseur();
    }

    public void Jump()
    {
        directionRB = Vector3.Lerp(directionRB, Vector3.Normalize(playerRB.velocity), Time.fixedDeltaTime);
        speedMarche = Mathf.Clamp(speedMarche, 0f, 0.1f);//clamp la valeur de speedMarche à moins de 1 au cas ou juste avant sa valeur avait dépassé 1 (en cas de Transit par exemple)
        force = Mathf.Lerp(force, 0f, Time.fixedDeltaTime * 1f);
        float speedTarget = Mathf.Clamp( MouseManager.yPoint - 0.3f, 0f,1f);//adapter la vitesse en fonction de la position de la sourie à l'écran
        speedMarche = Mathf.Lerp(speedMarche, speedTarget, Time.fixedDeltaTime * 1.5f);
        //Position
        Vector3 directionForce = (rayDirection + rayDirection*scrollForce) * (speedMarche * 0.5f);
        directionForce = new Vector3(directionForce.x, 0f, directionForce.z);
        Vector3 velocityRay = new Vector3(directionForce.x,force, directionForce.z);//force ascenstionnelle
        playerRB.AddForce(velocityRay, ForceMode.VelocityChange);//on applique la force au rigidbody du play
        if(directionForce != Vector3.zero)
        {
            player.transform.rotation = Quaternion.Lerp(player.transform.rotation, Quaternion.LookRotation(directionForce), Time.fixedDeltaTime);
        }
        gUISystemNavig.MarcheCurseur();
    }

    public void Transit()
    {
        addForce = 0f;
        force = 0f;
        velocityForce = Vector3.zero;
        playerRB.AddForce(velocityForce, ForceMode.VelocityChange);//on applique la force au rigidbody du player	
        Vector3 destinationPos = GameManager.instance.destination.transform.position - player.transform.position;
        destinationPos = new Vector3(destinationPos.x, 0f, destinationPos.z);
        player.transform.position = GoNewDestination();
        player.transform.rotation = Quaternion.Slerp(player.transform.rotation, Quaternion.LookRotation(destinationPos), Time.deltaTime);//oriente le joueur	

    }

    public void Inventory()
    {
        addForce = 0f;
        if (speedMarche > 0.1f)
        {
            speedMarche = Mathf.Lerp(speedMarche, 0f, Time.deltaTime * 2f);
        }
        else speedMarche = 0f;
        //POSITION
        Vector3 velocityRay = new Vector3();
        velocityRay = Vector3.zero;
        playerRB.AddForce(velocityRay, ForceMode.VelocityChange);//on applique la force au rigidbody du playerer
        Vector3 offsetVehicule = vehiculeManager.vehiculeList[vehiculeManager.indexVehicule].transform.position + vehiculeManager.ConvertOffsetPlayerPos(vehiculeManager.indexVehicule, vehiculeManager.offsetDestinationPos[vehiculeManager.indexVehicule] + offsetPlayer);
        player.transform.position = Vector3.Lerp(player.transform.position, offsetVehicule, Time.fixedDeltaTime * 10f);
        player.transform.rotation = Quaternion.Lerp(player.transform.rotation, vehiculeManager.vehiculeList[vehiculeManager.indexVehicule].transform.rotation, Time.fixedDeltaTime * 7f);
    }

    public void EnVoiture()
    {
        speedMarche = Mathf.Clamp(speedMarche, -0.3f, 1f);//clamp la valeur de speedMarche à moins de 1 au cas ou juste avant sa valeur avait dépassé 1 (en cas de Transit par exemple)
        float speedTarget = MouseManager.yPoint - 0.3f;//adapter la vitesse en fonction de la position de la sourie à l'écran
        speedMarche = Mathf.Lerp(speedMarche, speedTarget, Time.deltaTime * 1.5f);
        if (Mathf.Abs(transform.InverseTransformDirection(playerRB.velocity).z) > 2f)//si le RB prends de la vitesse on aligne magnetudeCamera sur la velocity du RB Sinon on reste sur la position enregistrée lors du passage au Neutral
        {
            directionRB = Vector3.Lerp(directionRB, Vector3.Normalize(playerRB.velocity), Time.deltaTime * 3f);
            addForce = 0f;
        }
        force = Mathf.Lerp(force, (0.94f - progression), Time.deltaTime / 2f);
        force = Mathf.Clamp(force, 0f, 1f);
        //POSITION
        Vector3 velocityRay = GoEnTrain();//la force du rigidbody est définie par la fonction GoNewDestination soit une spline qui va du player au point de destination
        Vector3 destinationPos = p3;
        player.transform.position = velocityRay;
        velocityForce = Vector3.zero;
        playerRB.AddForce(velocityForce, ForceMode.VelocityChange);//on applique la force au rigidbody du player		
        destinationPos = new Vector3(destinationPos.x, 0f, destinationPos.z);
        player.transform.rotation = Quaternion.Slerp(player.transform.rotation, Quaternion.LookRotation(-destinationPos), Time.deltaTime);//oriente le joueur
        gUISystemNavig.MarcheCurseur();
    }

    public void EnTrain()
    {
        speedMarche = Mathf.Clamp(speedMarche, -0.3f, 1f);//clamp la valeur de speedMarche à moins de 1 au cas ou juste avant sa valeur avait dépassé 1 (en cas de Transit par exemple)
        float speedTarget = MouseManager.yPoint - 0.3f;//adapter la vitesse en fonction de la position de la sourie à l'écran
        speedMarche = Mathf.Lerp(speedMarche, speedTarget, Time.deltaTime * 1.5f);
        if (Mathf.Abs(transform.InverseTransformDirection(playerRB.velocity).z) > 2f)//si le RB prends de la vitesse on aligne magnetudeCamera sur la velocity du RB Sinon on reste sur la position enregistrée lors du passage au Neutral
        {
            directionRB = Vector3.Lerp(directionRB, Vector3.Normalize(playerRB.velocity), Time.deltaTime * 3f);
            addForce = 0f;
        }
        force = Mathf.Lerp(force, (0.94f - progression), Time.deltaTime / 2f);
        force = Mathf.Clamp(force, 0f, 1f);
        //POSITION
        Vector3 velocityRay = GoEnTrain();//la force du rigidbody est définie par la fonction GoNewDestination soit une spline qui va du player au point de destination
        Vector3 destinationPos = p3;
        player.transform.position = velocityRay;
        velocityForce = Vector3.zero;
        playerRB.AddForce(velocityForce, ForceMode.VelocityChange);//on applique la force au rigidbody du player		
        destinationPos = new Vector3(destinationPos.x, 0f, destinationPos.z);
        player.transform.rotation = Quaternion.Slerp(player.transform.rotation, Quaternion.LookRotation(-destinationPos), Time.deltaTime);//oriente le joueur
        gUISystemNavig.MarcheCurseur();
    }

    public void Wagon()
    {
        speedMarche = Mathf.Clamp(speedMarche, -0.3f, 1f);//clamp la valeur de speedMarche à moins de 1 au cas ou juste avant sa valeur avait dépassé 1 (en cas de Transit par exemple)
        float speedTarget = MouseManager.yPoint - 0.3f;//adapter la vitesse en fonction de la position de la sourie à l'écran
        speedMarche = Mathf.Lerp(speedMarche, speedTarget, Time.deltaTime * 1.5f);
        addForce = 0f;
        //Position
        Vector3 offsetVehicule = vehiculeManager.vehiculeList[5].transform.position + vehiculeManager.ConvertOffsetPlayerPos(5, vehiculeManager.offsetDestinationPos[5] + offsetPlayer);
        player.transform.position = Vector3.Lerp(player.transform.position, offsetVehicule, Time.deltaTime * 15f);
        player.transform.rotation = Quaternion.Lerp(player.transform.rotation, vehiculeManager.vehiculeList[5].transform.rotation, Time.deltaTime * 7f);
        gUISystemNavig.MarcheCurseur();
    }

    public void Bird()
    {
        speedMarche = Mathf.Clamp(speedMarche, -0.3f, 1f);//clamp la valeur de speedMarche à moins de 1 au cas ou juste avant sa valeur avait dépassé 1 (en cas de Transit par exemple)
        float speedTarget = MouseManager.yPoint - 0.3f;//adapter la vitesse en fonction de la position de la sourie à l'écran
        speedMarche = Mathf.Lerp(speedMarche, speedTarget, Time.deltaTime * 1.5f);
        addForce = 0f;
        //Position
        if (progression < 1f)
        {
            Vector3 velocityRay = GoEnTrain();//la force du rigidbody est définie par la fonction GoNewDestination soit une spline qui va du player au point de destination
            player.transform.position = velocityRay;
        }
        else
        {
            oiseauxManager.ChangeMode();//relance le mode fly de l'oiseau pour qu'il redécolle//l'index de l'oiseau sur lequel est le player est l'argument de la fonction
           // La position du player est calculé sur le script OiseauControler de l'oiseau qui porte ce dernier. player.transform.position = vehiculeManager.vehiculeList[4].transform.position;
            gUISystemNavig.MarcheCurseur();
        }
        player.transform.rotation = GameManager.instance.destination.transform.rotation;
    }

    public IEnumerator Landing()//attends l'atterissage sur le sol pour remetre en route le mode de déplacement classique
    {
        landingCheck = true;
        yield return new WaitForSeconds(0.5f);
        int layerMask = 1 << 8;//collider
        while (EventManagerPlayer.engineStateBool[3] || EventManagerPlayer.engineStateBool[4] || EventManagerPlayer.engineStateBool[6])//Jump ou Transit
        {
            Vector3 playerPos = player.transform.position;
            Vector3 direction = (playerPos + Vector3.down * 100f) - playerPos;
            Ray down = new Ray(playerPos, direction);
            float radius = 500f;
            RaycastHit hit;
            Physics.Raycast(down, out hit, radius, layerMask);
            Debug.DrawRay(playerPos, direction, Color.red);
            floorDistance = hit.distance;
            if (floorDistance == 0f)//on a dépassé le rayon au dela duquel le raycast ne se fait plus et floordistance deviens donc 0 faussant les calculs
            {
                floorDistance = 99.9f;
            }
            floorDistance = Mathf.Clamp(floorDistance, 0f, 100f);
            if (floorDistance > 1f && floorDistance <95f)
            {
                animator.SetBool("land", false);
                if(hit.collider != null)
                {
                    if (hit.collider.gameObject.tag == "Territoire")
                    {
                        gps.GetComponent<GPS>().GetGoodGPS(hit.collider.gameObject);
                    }
                    if (hit.collider.gameObject.tag == "Hydro")
                    {
                        gps.GetComponent<GPS>().GetGoodGPS(hit.collider.gameObject);
                    }
                }
            }
            if (floorDistance < 1f && !hit.collider.isTrigger)//si on est près du sol et que le collider touché n'est pas juste un collider en mode trigger
            {
                animator.SetBool("land", true);//remise à zéro
                EventManagerPlayer.engineStateBool[3] = false;//remet le flag de jump sur faux et sort de la couroutine
                EventManagerPlayer.engineStateBool[4] = false;//remet le flag de Transit sur faux et sort de la couroutine   
            }
            if (GameManager.instance.isGrounded)
            {
                EventManagerPlayer.engineStateBool[3] = false;//remet le flag de jump sur faux et sort de la couroutine
                EventManagerPlayer.engineStateBool[4] = false;//remet le flag de Transit sur faux et sort de la couroutine
                EventManagerPlayer.engineStateBool[0] = true;//force le passage au neutral
                GameManager.instance.destination = vehiculeManager.vehiculeList[0];
            }
            yield return null;
        }
        GameManager.instance.destination = vehiculeManager.vehiculeList[0];
        animator.SetBool("jump", false);
        animator.SetBool("transit", false);
        audioManager.AddAudio("r2d2", false, false, 0f, 0);
        landingCheck = false;
    }

    public GameObject ChangePlayer(GameObject oldPlayer, GameObject newPlayer)
    {
        int playerLayer = 9;
        int avatarLayer = 11;
		OnCollisionPlayer oldCollision = oldPlayer.GetComponent<OnCollisionPlayer>();
        MathildeControler mathildeControler = oldPlayer.GetComponent<MathildeControler>();
		Destroy(oldCollision);//detruit le script de détection de collision du vieux player
        Destroy(mathildeControler);//détruit le scrit
		//newPlayer.SetActive(true);
        GameManager.instance.PlayerSetUp(newPlayer);//déclenche l'event du changement de player sur GameManager
        GetPlayerMaterial();
        GetPlayerAnimator();
		OnCollisionPlayer onColPlayer = newPlayer.AddComponent<OnCollisionPlayer>();//active le script de détection de collision du vieux player
		onColPlayer.enabled = true;
        newPlayer.AddComponent<MathildeControler>();
        oldPlayer.layer = avatarLayer;//Layer Avatar
        newPlayer.layer = playerLayer;//Layer Player
        Vector3 direct = (oldPlayer.transform.position - newPlayer.transform.position);
        Vector3 newPos = -direct + newPlayer.transform.position + new Vector3(0f, 0.2f, 0f);
        vehiculeManager.vehiculeList[vehiculeManager.indexVehicule].transform.position = newPos;
        SetAnimatorPlayers(oldPlayer, newPlayer);//swap les animator controler et replace oldPlayer au sol
        GameManager.instance.swapAvatar = false;
        GameManager.PlugNewPlayer(newPlayer);//////Event nouvel avatar
        GameManager.instance.NavigatingSetUp(true, player);
        EventManagerPlayer.BooleanSetUp("MarcheAvant");//réenclenche la marche avant
        StartCoroutine(DropOldPlayer(oldPlayer));// fait descendre et redresse l'ancien joueur pour qu'il touche le sol
        DefineRigidbody();
        vehiculeManager.playerAnim = player.GetComponent<Animator>();
        return newPlayer;	
	}

    private void SetAnimatorPlayers(GameObject old, GameObject nouveau)
    {
        Animator oldAnim = old.GetComponent<Animator>();
        bool skat = oldAnim.GetBool("skate");
        bool driv = oldAnim.GetBool("drive");
        bool march = oldAnim.GetBool("marche");
        Animator newAnim = nouveau.GetComponent<Animator>();
        oldAnim.runtimeAnimatorController = animControlInactif;
        newAnim.runtimeAnimatorController = animControlActif;
        newAnim.SetBool("drive", driv);
        newAnim.SetBool("skate", skat);
        newAnim.SetBool("marche", march);
        newAnim.SetBool("swap", true);
        foreach (Transform child in old.transform)
        {
            if(child.gameObject.name == "Fantome")//active ou deactive le fantome
            {
                child.gameObject.SetActive(true);
            }
        }
        foreach (Transform child in nouveau.transform)
        {
            if (child.gameObject.name == "Fantome")
            {
                child.gameObject.SetActive(false);
            }
        }
    }

    private IEnumerator DropOldPlayer(GameObject old)
    {
        CapsuleCollider oldCollider = old.GetComponent<CapsuleCollider>();
        Ray newRay = new Ray( old.transform.position+new Vector3(0f,5f,0f),-Vector3.up);//decalage vers le haut 
        RaycastHit hit;
        oldCollider.Raycast(newRay, out hit, 100f);
        float posIdealY = hit.point.y;
        float posOldY = old.transform.position.y;
        float timer = Time.time;
        while (Time.time < timer+5f)
        {
            Vector3 correctPos = new Vector3(old.transform.position.x, posIdealY - offsetPlayer.y-0.35f, old.transform.position.z);//pose l'ancien joueur au sol
            old.transform.position = Vector3.Lerp(old.transform.position, correctPos, Time.deltaTime);
            old.transform.rotation = Quaternion.Slerp(old.transform.rotation, Quaternion.identity, Time.deltaTime);
            yield return null;
        }
    }

	private void  DrawLineRender (Vector3 p000,Vector3 p111,Vector3 p222,Vector3 p333)
    {//déssine à l'écran le trajet de destination
		float t; 
		LineRenderer lineRenderer = new LineRenderer();
        if (player.GetComponent<LineRenderer>() == null)
        {
            lineRenderer = player.AddComponent<LineRenderer>();
        }
        else lineRenderer = player.GetComponent<LineRenderer>();
        //lineRenderer.useWorldSpace = true;
        lineRenderer.sharedMaterial = GameManager.instance.matLineRenderer;
		lineRenderer.SetWidth(0.2f,0.05f);
		lineRenderer.SetVertexCount(50);
		for(int li=0;li< 50; li++){
			t = li /(50f-1f);
			Vector3 position = Bezier.GetPoint(p000,p111,p222,p333,t);      	
			lineRenderer.SetPosition(li, position); 
		}
	}

    public void PointCurve (GameObject destinationObject)
    {//fonction qui permet de calculer les 4 points nécéssaires à la création d'une spline. Déclenché par QuitInventory sur AudioGUI
        Vector3 correctPos = vehiculeManager.ConvertOffsetPlayerPos(vehiculeManager.indexVehicule, vehiculeManager.offsetDestinationPos[vehiculeManager.indexVehicule]);//adapte le offset à la position dans l'espace du vehicule
        Vector3 posArrivee = destinationObject.transform.position + correctPos;//position de la sphere sound d'arrivée lors de transitOK
        progression = 0f;
        p0 = player.transform.position;// + new Vector3(0.0f,-2.0f,0.0f);
		p3 = posArrivee;//affecte au dernier point de la spline la destination
		Vector3 direction = p3 - p0;//calcul du vecteur de direction du player à la destination en ligne droite
		p1 = p0 + new Vector3(direction.x/3f,3f,direction.z/3f);
		p2 = p0 + new Vector3(direction.x/1.5f,3f,direction.z/1.5f);
        DrawLineRender(p0,p1,p2,p3);
    }

    public void PointCurveReverb(GameObject destinationObject)
    {//fonction qui permet de calculer les 4 points nécéssaires à la création d'une spline. Déclenché par QuitInventory sur AudioGUI
         Vector3 posArrivee = destinationObject.transform.position;//position de la sphere sound d'arrivée lors de transitOK
        progression = 0f;
        p0 = player.transform.position;// + new Vector3(0.0f,-2.0f,0.0f);
        p3 = posArrivee;//affecte au dernier point de la spline la destination
        Vector3 direction = p3 - p0;//calcul du vecteur de direction du player à la destination en ligne droite
        p1 = p0 + new Vector3(direction.x / 3f,150f, direction.z / 3f);
        p2 = p0 + new Vector3(direction.x / 1.5f, 150f, direction.z / 1.5f);
        DrawLineRender(p0, p1, p2, p3);
    }

    private Vector3 GoEnVoiture()
    {//fonction qui gére le déplacement vers la nouvelle destination
        MagnetismeCheck(false);
        distance = DistanceDestinationPlayer();
        progression += Time.deltaTime;
        progression = Mathf.Clamp(progression, 0f, 1f);  
        Vector3 velocityRay = Bezier.GetPoint(p0, p1, p2, p3,progression);
        if(progression == 1f)
        {
            Destroy(player.GetComponent<LineRenderer>());//detruit la lineRender 
        }
        return velocityRay;
    }

    private Vector3 GoEnTrain()
    {//fonction qui gére le déplacement vers la nouvelle destination
        MagnetismeCheck(false);
        distance = DistanceDestinationPlayer();
        progression += Time.deltaTime;
        progression = Mathf.Clamp(progression, 0f, 1f);
        Vector3 velocityRay = Bezier.GetPoint(p0, p1, p2, p3, progression);
        if (progression == 1f)
        {
            Destroy(player.GetComponent<LineRenderer>());//detruit la lineRender 
            GameManager.instance.destination = vehiculeManager.vehiculeList[vehiculeManager.indexVehicule];
        }
        return velocityRay;
    }

    private Vector3 GoNewDestination ()
    {//fonction qui gére le déplacement vers la nouvelle destination
        MagnetismeCheck(false);
        distance = DistanceDestinationPlayer();
        progression = Mathf.Lerp(progression,1.1f, Time.deltaTime/10f);
        progression = Mathf.Clamp(progression, 0f, 1f);
        Vector3 velocityRay = Bezier.GetPoint(p0, p1, p2, p3, progression);
        if (progression == 1f)
        {
            Vector3 posParticule = p3;
            vehiculeManager.particuleManager.ParticuleOn(posParticule, 9, 2f);//dustContourRouge
            Destroy(player.GetComponent<LineRenderer>());//detruit la lineRender 
            GameManager.instance.destination = vehiculeManager.vehiculeList[0];//retour au rover (important pour magnetudeCamera qui calcul la rotation en fonction de la destination
            EventManagerPlayer.engineStateBool[4] = false;//!Transit 
        }
		return velocityRay;
	}

    private Vector3 GoNewZone()
    {//fonction qui pousse le joueur d'un côté ou l'autre du collider qui vient de déclencher le changement de zone
        Vector3 velocityRay = new Vector3();
        if (GameManager.instance.chapitre == 1)//poussée du joueur
        {
            Vector3 collidingGOForward = Vector3.Normalize(collidingGO.transform.forward);
            CameraMove cameraMove = GameManager.instance.cameraActive.GetComponent<CameraMove>();
            velocityRay = collidingGOForward * 5;
        }
		return velocityRay;
	}

    private IEnumerator MatPlayerAlpha()//fonction qui fait disparaitre la player puis le fait réaparaitre dès que mouvePlayer est faux
    {
        Material newMat = SetPlayerMaterial(playerMatAlpha);//change de shader pour un shader qui gère la transparence
        newMat.SetFloat("_AlphaBlend", 1f);
        float val = 1f;
        while (val > 0f)
        {
            val -= Time.deltaTime / 3f;
            newMat.SetFloat("_AlphaBlend", val);
            yield return null;
        }
        while (mouveSpline)
        {
            yield return null;
        }
        while (val < 1f)
        {
            val += Time.deltaTime;
            newMat.SetFloat("_AlphaBlend", val);
            yield return null;
        }
        SetPlayerMaterial(playerMat);//Remet le mat par défaut au joueur
    }


    private void MagnetismeCheck(bool magnOn)
    {
        if (!magnOn)
        {
            localIsMagnetique = magnetismeIsOn;
            magnetismeIsOn = false;
        }

        if (magnOn)
        {
            if (localIsMagnetique)
            {
                magnetismeIsOn = true;
            }
            else magnetismeIsOn = false;
        }
    }

	private float DistanceDestinationPlayer()
    {
		GameObject destinationGO = GameManager.instance.destination;
		Vector3 posAarrivee = destinationGO.transform.position;//sphere sound d'arrivée lors de transitOK
		float distance = Vector3.Distance (posAarrivee, player.transform.position);
		return distance;
	}

    public void GetPlayerAnimator()
    {
       animator = player.GetComponent<Animator>();
    }

    public void GetPlayerMaterial()
    {
        if(GameManager.instance.player == GameManager.instance.genericPlayer)
        {
           playerMat = GameManager.instance.player.GetComponentInChildren<SkinnedMeshRenderer>().sharedMaterial;
        }
        if(GameManager.instance.player == GameManager.instance.sphereAvatar)
        {
            playerMat = GameManager.instance.player.GetComponentInChildren<MeshRenderer>().sharedMaterial;
        }

    }

    public Material SetPlayerMaterial(Material newMat)
    {
        if (GameManager.instance.player == GameManager.instance.genericPlayer)
        {
            GameManager.instance.player.GetComponentInChildren<SkinnedMeshRenderer>().sharedMaterial = newMat;
        }
        if (GameManager.instance.player == GameManager.instance.sphereAvatar)
        {
            GameManager.instance.player.GetComponentInChildren<MeshRenderer>().sharedMaterial = newMat;
        }
        return newMat;
    }

        public void  DefineRigidbody ()
    {//configuration du rigidbody du player
		playerRB = player.GetComponent<Rigidbody>();
        if (player != GameManager.instance.sphereAvatar)
        {
            vehiculeManager.RBNormal();//setUp du RB du vehicule et de Mathild   
        }
	}

    public void MagnetismeOn(Vector3 destination)//ajoute du magnetisme au joeur. Le point magnétique est défini sur le script Fleche avec la variable destiantion.
    {
        if (magnetismeIsOn)
        {
            Vector3 playerToDesti = destination - player.transform.position;
            float sQrPlayeurToDis = playerToDesti.sqrMagnitude;
            float force = sQrPlayeurToDis / 10000f;
            magnetisme = Vector3.Normalize(playerToDesti) * force;
        }
        else magnetisme = Vector3.zero;
    }

    public void SpeedUP(bool upDown)
    {
        Vector3 rayDirectionAdapt = new Vector3(rayDirection.x, 0f, rayDirection.z);
        if (upDown)//up
        {
            directionRB += rayDirectionAdapt * speedMarche ;//on ajoute la force au rigidbody du player );
            if (player != GameManager.instance.sphereAvatar)
            {
                vehiculeManager.maxiSpeed = 450f;//si chapitre 4 on augmente le seuil de la vitesse du rover
            }
        }
        if (!upDown)
        {
            rayDirectionAdapt = new Vector3(0f,-1f , 0f);
            directionRB += rayDirectionAdapt * speedMarche;////on ajoute la force au rigidbody du player );
            if (player == GameManager.instance.genericPlayer)
            {
                vehiculeManager.maxiSpeed = 150f;//si chapitre 4 on diminu le seuil de la vitesse du rover
            }

        }
    }

  private void OnEnable()
    {
        mouveSpline = false;
        EventManagerPlayer.NewGear += Listener;//souscrit a l'évent system qui permet de déterminer grace à un string dans quel état est le moteur du vehicule
        GameManager.NewPlayer += PlayerListener;//souscrit
        GameManager.NewNavigation += NavigatingListener;
    }

    public void OnDesable()
    {
        EventManagerPlayer.NewGear -= Listener;//de-inscription à l'envent system
        GameManager.NewPlayer -= PlayerListener;//souscrit
        GameManager.NewNavigation -= NavigatingListener;
    }

    private void  Start (){
        navigating = false;
        pasProgression = 0.001f;//default
        pasProgressionMax = 0.001f;
        pasProgressionMin = 0.0001f;
        force = 0f;
        player.GetComponent<Rigidbody>().isKinematic = true;
        //DefineRigidbody();
        GameManager.instance.worldLimit = false;
        GameManager.instance.indexProgression = 1;
        boiteAVitesse = Neutral;//point mort
        gravityIsRunning = false;
        magnetisme = Vector3.zero;
        localIsMagnetique = false;
        jumpAndFalseIsRunning = false;
        GetPlayerAnimator();//met a jour l'animator component du player en l'affectant à la variable animator
        GetPlayerMaterial();
        animator = player.GetComponent<Animator>();
        playerRB = player.GetComponent<Rigidbody>();
    }
	
	private void  FixedUpdate (){
        if (navigating){
			//setUpRayDirection();//calcul de la position de la sourie à l'écran (appel au delegate)
            if(boiteAVitesse == Jump || boiteAVitesse == MarcheArriere || boiteAVitesse == MarcheAvant || boiteAVitesse == Neutral)
            {
                rayDirection = GameManager.instance.raydirectionpointer;
                boiteAVitesse();//calcul de la vitesse en fonction de la position de la sourie à l'écran et de la vitesse enclenchée
            }
		}
	}
	
	private void  Update (){
		if(navigating){
            if (boiteAVitesse == Bird || boiteAVitesse == Transit || boiteAVitesse == EnVoiture || boiteAVitesse == EnTrain || boiteAVitesse == Wagon)
            {
                rayDirection = GameManager.instance.raydirectionpointer;
                boiteAVitesse();//calcul de la vitesse en fonction de la position de la sourie à l'écran et de la vitesse enclenchée
            }
            if (Input.GetAxis("Mouse ScrollWheel") > 0) // back
            {
                scrollForce += 0.4f;
            }
            if (Input.GetAxis("Mouse ScrollWheel") < 0) // back
            {
                scrollForce -= 0.4f;
            }
            scrollForce = Mathf.Lerp(scrollForce, 0f, Time.deltaTime / 4f);
        }
	}	
}