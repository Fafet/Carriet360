﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

// EventManager pour le déplacement du player et surtout de Mathilde.
//ce Script doit être déposé sur un gameObject de préférence le GO EventSystem

public class EventManagerPlayer : MonoBehaviour {

    public float vehiculeYPoint;
    private bool navigating;
    private bool reverseIsPossible;
    private bool CheckForReverseIsRunning;
    public static List<string> engineState;//liste de string qui décrit dans quel état est le moteur (marche avant, neutral etc.)
    public static List<bool> engineStateBool;//liste des bool associées au passage d'un état à l'autre. Permet d'activer l'évent sur une frame seulement

    public delegate void EventHandler(string e);//création delegate
    public static event EventHandler NewGear;//NewGear est le déclencheur d'event de type EventHandler/ doit etre static pour ajouter facilement un Listener depuis un autre script

    public static void PlugNewGear(string e)//Methode pour definir le contenu du delegate et appeler les différent gestionnaires ajoutés à l'event newgear. Cette méthode est appelée à chaque évent et permet de retourner le bon string au listener
    {
        if (NewGear != null)
        {
            NewGear(e);
        }
        else Debug.Log("PlugNewGear empty");
    }

    public void NavigatingListener(bool e)
    {
        navigating = e;
    }

    public static void BooleanSetUp(string actualEngineState)//mets toutes les autres boolean d'état sur false à part celle concernée/ Si inverseBoole st vrai alors c'est le contraire
    {
        for (int i = 0; i < engineState.Count; i++)
        {
            if (engineState[i] == actualEngineState)
            {
                engineStateBool[i] = true;
                PlugNewGear(engineState[i]);//déclenche l'event avec comme argument le String correspondant à l'état du moteur (neutral, marche avant etc;)
              // Debug.Log(i);

            }
            else
            {
                engineStateBool[i] = false;
            }
        }
    }

    private IEnumerator CheckForReverse()//il faut que le RB du player soit à l'arrêt et d'abord passer par le mode neutral avant de déclencher la marche arrière
    {
        CheckForReverseIsRunning = true;
        while (vehiculeYPoint < 0.25f)
        {
            if (vehiculeYPoint >= 0.15f)
            {
                reverseIsPossible = true;
            }
            if (reverseIsPossible && vehiculeYPoint < 0.15f)
            {
                BooleanSetUp("MarcheArriere");//lance la fonction qui détermine le bon index et le bon string de l'état du moteur et déclenche l'évènement
            }
            yield return null;
        }
        reverseIsPossible = false;
        CheckForReverseIsRunning = false;
    }

    private void OnEnable()
    {
        GameManager.NewNavigation += NavigatingListener;
    }

    private void OnDesable()
    {
        GameManager.NewNavigation -= NavigatingListener;
    }

    // Use this for initialization
    void Start() {
        vehiculeYPoint = MouseManager.yPoint;
        navigating = true;
        engineState = new List<string>();
        engineStateBool = new List<bool>();
        engineState.Add("Neutral");//0
        engineStateBool.Add(false);
        engineState.Add("MarcheAvant");//1
        engineStateBool.Add(false);
        engineState.Add("MarcheArriere");//2
        engineStateBool.Add(false);
        engineState.Add("Jump");//3
        engineStateBool.Add(false);
        engineState.Add("Transit");//4//declenché sur le script audioGUI dans la fonction quiInventory
        engineStateBool.Add(false);
        engineState.Add("Inventory");//5
        engineStateBool.Add(false);
        engineState.Add("Visite");//6
        engineStateBool.Add(false);
        engineState.Add("EnVoiture");//7
        engineStateBool.Add(false);
        engineState.Add("Train");//8
        engineStateBool.Add(false);
        engineState.Add("Wagon");//9
        engineStateBool.Add(false);
        engineState.Add("Bird");//10
        engineStateBool.Add(false);
    }

    // Update is called once per frame
    void Update()
    {
        vehiculeYPoint = MouseManager.yPoint;
        if (navigating)
        {
            if (!engineStateBool[4])//si Tranist n'est pas en vrai
            {
                if (!engineStateBool[6])//si Visite n'est pas vrai
                {
                    if (!engineStateBool[5])//si Inventory n'est pas en vrai
                    {
                        if (!engineStateBool[3])//si Jump n'est pas en vrai, on ne saute pas
                        {
                            if (!engineStateBool[7])//si EnVoiture n'est pas en vrai
                            {
                                if (!engineStateBool[8])//si Train n'est pas en vrai
                                {
                                    if (!engineStateBool[9])//si Wagon in'est pas en vrai
                                    {
                                        if (!engineStateBool[10])//si Bird n'est pas en vrai
                                        {
                                            if (vehiculeYPoint >= 0.15f && vehiculeYPoint <= 0.25f)
                                            {
                                                if (!engineStateBool[0])
                                                {
                                                    BooleanSetUp("Neutral");//lance la fonction qui détermine le bon index et le bon string de l'état du moteur et déclenche l'évènement
                                                }
                                            }
                                            if (vehiculeYPoint >= 0.25f)
                                            {
                                                if (!engineStateBool[1])
                                                {
                                                    BooleanSetUp("MarcheAvant");//lance la fonction qui détermine le bon index et le bon string de l'état du moteur et déclenche l'évènement
                                                }
                                            }
                                            if (vehiculeYPoint < 0.15f)// on attends que le RB du player n'ai plus de vitesse pour tenter de mettre la marche arrière
                                            {
                                                if (!engineStateBool[2] && !CheckForReverseIsRunning)
                                                {
                                                    StartCoroutine(CheckForReverse());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (Input.GetKeyDown("space") && ! engineStateBool[6])//si on frappe la barre d'espace et qu'on est pas en mode Visite guidée
        {

            BooleanSetUp("Jump");//lance la fonction qui détermine le bon index et le bon string de l'état du moteur et déclenche l'évènement
        }
    }
}

