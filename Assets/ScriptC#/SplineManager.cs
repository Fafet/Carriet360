﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SplineManager : MonoBehaviour {

    //script sur le GO SplineManager qui gére les spline du niveau

    public List<SplineData> splineData;//remplis automatiquement à l'appel de CreateSplineData
    public int index;
    public List<BezierSpline> splineList;//remplis automatiquement au démarrage. Prends les enfants du GO splineManager et les ajoute à la liste

    private void Awake()
    {
        foreach(Transform bez in this.transform)
        {
            BezierSpline splineConcern = bez.gameObject.GetComponent<BezierSpline>();
            splineList.Add(splineConcern);
            CreateSplineData(splineConcern);//met à jour les variable de la spline concernée dans SplineData
        }
    }

    public void CreateSplineData(BezierSpline splineConcern)//Crée la bonne spline en fonction de son nom et crée son contexte data si il n'éxiste pas
    {
        SplineData newSpline = new SplineData();
        splineData.Add(newSpline);//ajoute le nouveau splineData
        BezierSpline oldBezierSpline = newSpline.bezierSpline;
        newSpline.bezierSpline = splineConcern;
        FillSplineData(newSpline);//met à jour les variable de la spline concernée dans SplineData
    }

    public SplineData GetTheGoodSpline(string stringToGet2)//defini le bon contexte data de la spline en fonction de son nom
    {
        for (int i = 0; i < splineData.Count; i++)
        {
            if (splineData[i].splineName.Contains(stringToGet2))//Si il est null ou qu'il ne contient pas le nom qu'on tente de lui donner
            {
                index = i;
                return splineData[i];
            }
        }
        return null;
    }

    private void FillSplineData(SplineData dataSpline)//crée le contexte (infos)
    {
        dataSpline.splineGO = dataSpline.bezierSpline.gameObject;
        dataSpline.splineName = dataSpline.bezierSpline.gameObject.name;//defini le nom de la spline
        dataSpline.position = dataSpline.splineGO.transform.position;//la position nouvelle suite à l'instantiation à l'origine du monde
        WorldSpacePoints(dataSpline);//met à jour le tableau concerné
    }

	public int DistObjectSplinePoints ( Vector3 goPosition, SplineData goWithSpline){//permet de savoir de quel point de la spline l'objet est le plus proche
		List<float> dist = new List<float>();
		foreach(Vector3 pp in goWithSpline.timePos)
        {	
			float howFar = Vector3.Distance(pp,goPosition); 
			dist.Add(howFar);//crée un tableau avec les distance trouvée
		}
		int objectSplineIndex = new int();
		objectSplineIndex =	GetMinimumValue(dist);//récupère l'index du plus petit élément du tableau (le plus proche de zéro). C'est l'index dont l'objet est le plus proche dans l'espace
		return objectSplineIndex;
	}
	
	public void WorldSpacePoints(SplineData splineConcern){//crée un tableau de tout les points de la spline en worldSpace (à utiliser pour les manipulation runtime)
		Vector3[] points = splineConcern.bezierSpline.points;
        splineConcern.worldSpaceSpline = new Vector3[points.Length];//initialisation du tableau
        for (int i = 0; i< points.Length;i++){
			Vector3 worldP = transform.TransformPoint(points[i]);//convertis la position de ppp de local à world space	
            splineConcern.worldSpaceSpline[i] = splineConcern.position + worldP;//remplis le tableau des points de la spline en worldSpace
		}
		ConvertPointInTime(splineConcern);
	}

	private int GetMinimumValue(List<float> listToCheck){
		int min = new int();
		min = 0;//index du tableau
		float actu = new float();
		actu = listToCheck[0];//affecte la première valeur du tableau a actu
		for (int p=0;p<listToCheck.Count;p++){
			if(listToCheck[p]<actu){
				min = p;
				actu = listToCheck[p];
			}
		}
		return min;	
	}
	private void ConvertPointInTime(SplineData goWithSpline)
    {
		int arrayLength = goWithSpline.worldSpaceSpline.Length*3;
        goWithSpline.indexToTime = new float[arrayLength];//initialisation du tableau
        goWithSpline.timePos = new Vector3[arrayLength];//initialisation du tableau
        goWithSpline.timeToIndex = new int[arrayLength];//initialisation du tableau
		float fraction = 1f/arrayLength;
		float temp = 0;
		for(int u = 0; u<arrayLength;u++){
			temp = fraction*u;
            goWithSpline.indexToTime[u] = temp;//on a un tableau des temps de la spline à intervals réguliers
			Vector3 posTemp = goWithSpline.bezierSpline.GetPoint(temp);
            goWithSpline.timePos[u] = posTemp;//et un tableau de positions dans l'espace correspondantes
            goWithSpline.timeToIndex[u] = DistObjectSplinePoints(goWithSpline.timePos[u], goWithSpline);//permet de savoir de quel point de la spline timePos est le plus proche

		}
	}
}



[System.Serializable]
public class SplineData
    //class qui permet de stocker les différentes splines. Le set up est automatique
{
    public string splineName;
    public BezierSpline bezierSpline;//A Ajouter dans l'inspecteur
    public GameObject splineGO;//le GO sur lequel est la spline
    public Vector3 position;
    public Vector3[] worldSpaceSpline;//les points de la spline en worldCoordonate
    public int influencePlayerIndex;//l'index le plus proche du player.
    public float[] indexToTime;
    public int[] timeToIndex;
    public Vector3[] timePos;
    //public Vector3 offsetSpline;//de combien décaler la spline(décalage equivalent au transform de l'objet centré sur 0,0,0 position
}
