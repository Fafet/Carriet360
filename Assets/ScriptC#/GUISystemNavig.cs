﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class GUISystemNavig : MonoBehaviour {

    //script attaché a SystemNavig (cameraSprite) pour afficher le texte et gérer les sprites de navigation et d'information


    private GameObject systemNavig;
    private GameObject prompteur;
    public GameObject info;
    private GameObject status;//le bouton de status qui s'affiche au milieu de l'écran
    private GameObject status2;//le bouton qui affiche des infos du style "barre d'espace pour sauter"
    public GameObject carrietExpress;//le UI général qui à pour enfants les boutons de destination
    private GameObject infoBulle;//le go qui affiche les infos bulles
    private GameObject texInfo;
    private GameObject texActivation;
    private GameObject vitesse;
    private GameObject marcheAvant;
    private GameObject marcheArriere;
    private GameObject neutral;
    private GameObject marcheCurseur;
    public List<string> infoBulleTexList;//la liste des textes à afficher dans les infos bulles en fonction du bouton concerné
    private List<int> statusPool;//liste des textes à afficher mis en file d'attente
    private Text vitesseChiffre;//le curseur qui indique la vitesse
    private GameObject infosImportantes;
    public Text activationTx;
    public TextAsset textAsset;//le text de la sphereSound de la zone d'influence dans laquelle est le player
    public float totalPas;//le temps total pour activer les GO de la liste au moment du changement de zone pour le telex(valeur donnée par le script ZoneManager
    private float duration;//le temps d'affichage des status
    private PlayerControler playerControler;
    public List<string> textToDisplayList;
    public int barresEspace;
    private bool firstVisit;
    public bool writeOn;
    private int increment;

    private GameObject infoRoot;
    private GameObject barreEspaceJaune;
    private GameObject sourisRoot;
    public List<GameObject> barreEspaceList;
    public List<GameObject> sourisRootList;
    public List<string> caractere;

    private bool sourisGo;


    private void Awake(){	
		playerControler = GameManager.gameControl.GetComponent<PlayerControler>();
        systemNavig = GameObject.Find("SystemNavig");
        info = GameObject.Find("Info");
        texInfo = GameObject.Find("TextInfo");
        infoBulle = GameObject.Find("InfoBulle");
        texActivation = GameObject.Find("TextActivation");
        status = GameObject.Find("Status");
        activationTx = texActivation.GetComponent<Text>();//récupère la liste des GO qu'on est en train d'activer
		prompteur =  GameObject.Find("Prompteur");
        vitesse = GameObject.Find("Vitesse");
		vitesseChiffre = GameObject.Find("VitesseChiffre").GetComponent<Text>();
        status2 = GameObject.Find("Status2");
        infosImportantes = GameObject.Find("InfosImportantes");
        statusPool = new List<int>();
        infoRoot = GameObject.Find("InfoRoot");
        barreEspaceJaune = GameObject.Find("BarreEspaceJaune");
        sourisRoot = GameObject.Find("SourisRoot");
        carrietExpress = GameObject.Find("CarrietExpress");
    }

    private void Listener(string e)//permet de sauter lorsque l'évent sur EventManagerPlayer est déclenché. EventManagerPlayer est un composant du GO eventSystem
    {
        if (e == "Neutral")
        {
            NewGearSprite(neutral);
        }
        if (e == "MarcheAvant")
        {
            NewGearSprite(marcheAvant);
        }
        if (e == "MarcheArriere")
        {
            NewGearSprite(marcheArriere);
        }
        if(e == "Jump")
        {
            carrietExpress.GetComponent<Animator>().SetBool("statusGo", false);//enclenche l'affichage du choix de la gare
        }
        if (e == "Wagon")
        {
            carrietExpress.GetComponent<Animator>().SetBool("statusGo", true);//enclenche l'affichage du choix de la gare
        }
        if (e == "Bird")
        {
            carrietExpress.GetComponent<Animator>().SetBool("statusGo", false);//enclenche l'affichage du choix de la gare
        }
    }

    public void  Exit (){
		//Application.LoadLevel(0);
		Application.Quit ();
	}

    public void Air(){
		Animator anim = systemNavig.GetComponent<Animator> ();
		SpriteManager.animator = anim;
		//SpriteManager.SetOff();

	}

    public void SetTextAsset()
    {
        Text textPrompteur = prompteur.GetComponentInChildren<Text>();
        textPrompteur.text = "";//vide la zone de texte
        textPrompteur.text = textAsset.text;//donne le texte asset en argument au prompteur afin qu'il affiche le bon texte
    }

    public void MarcheCurseur(){//affiche le vitesse sur le coté gauche de l'écran sous forme de vue mètre
		Vector2 recTPos = marcheCurseur.GetComponent<RectTransform>().localPosition;
        float pos = new float();
        if (MouseManager.yPoint > 0.5f)
        {
            pos = (MouseManager.yPoint - 0.5f) * 700f;
        }
        else {
            pos = -(0.5f - MouseManager.yPoint) * 700f;
        }
        recTPos.y = pos;
		marcheCurseur.GetComponent<RectTransform>().localPosition = recTPos;
		float figure = Mathf.Round(playerControler.speedMarche*100f);
		vitesseChiffre.text = figure.ToString();
	}

    public static void Selectable(List<GameObject> GOWithButtonList)
    {//Active deactive les boutons pour les rendres actifs ou non en fonction si on est ou pas en mode inventaire 
        for (int h = 0; h < GOWithButtonList.Count; h++)
        {
            GOWithButtonList[h].GetComponent<Button>().interactable = true;
        }
    }

    public static void NotSelectable(List<GameObject> GOWithButtonList)
    {//idem
        for (int h = 0; h < GOWithButtonList.Count; h++)
        {
            GOWithButtonList[h].GetComponent<Button>().interactable = false;
        }
    }

    public void InfoBulleProcess(int indexText, GameObject destination)
    {
        infoBulle.SetActive(true);
        Text textInfoBulle = infoBulle.GetComponentInChildren<Text>();
        textInfoBulle.fontSize = 11;
        textInfoBulle.text = infoBulleTexList[indexText];
        float addSizeX = infoBulleTexList[indexText].ToCharArray().Length * 8f;
        float addSizeY = 20f;
        AdaptRecSize(infoBulle, addSizeX, addSizeY);//Calcul la taille de la boite à afficher
        MoveRecT(infoBulle, destination);
    }


    public void InfoProcess(TextAsset infoToDisplay, string activationToDisplay)
    {//fonction qui gère les infos à afficher dans le prompteur infos qui s'affiche en bas à droite de l'écran
        Animator anim = info.GetComponent<Animator>();
        SpriteManager.animator = anim;
        SpriteManager.SetOn();//affiche les infos
        Text infoTx = texInfo.GetComponent<Text>();//récupère le texte à afficher pour indiquer la zone dans laquelle on rentre
        infoTx.text = infoToDisplay.text;
        activationTx.text += activationToDisplay;
    }

    public IEnumerator StatusProcess(int stringInList)//sprite qui s'affiche au milieu de l'écran sur lequel on doit cliquer pour lancer la navigation
    {//affiche le texte dans l'info bulle
        Text txtToDisplay = status.GetComponentInChildren<Text>();//choisi le bon texte à afficher
        txtToDisplay.text = textToDisplayList[stringInList];//affiche le bon texte
        Animator anim = status.GetComponent<Animator>();
        anim.SetBool("statusGo", true);
        yield return new WaitForSeconds(0.5f);
        while (anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.95f)
        {
            yield return null;
        }
        anim.SetBool("statusGo", false);//le changement de state peu intervenir à la fin de l'animation
    }

    public void RunStatus2Process(int addString, bool newStatus2)//fonction générique qui déclenche à son tour la mise en pool des infos à afficher puis qui gére l'affichage des différentes infos
    {
        float rythme = 0.03f;
        Animator anim = status2.GetComponent<Animator>();
        anim.SetBool("statusGo", true);
        if (firstVisit)
        {
            StartCoroutine(InfosImportanteReset());//deactive InfoImportance dans 5 secondes
            firstVisit = false;
        }
        Text txtToDisplay = status2.GetComponentInChildren<Text>();//choisi le bon texte à afficher 
        if (!newStatus2)//nouveau texte
        {
            anim.SetBool("statusGo", false);
            txtToDisplay.text = "";
            caractere.Clear();
            duration = 0f;//reset
            statusPool.Clear();
            increment = 0;
        }
        foreach (char letter in textToDisplayList[addString].Substring(0, textToDisplayList[addString].Length))
        {
            caractere.Add(letter.ToString());
        }
        caractere.Add("\n");
        statusPool.Add(addString);//ajoute à la liste des infos à afficher tour à tour
                                  //string tempString = txtToDisplay.text + "\n"+ textToDisplayList[addString];
        duration += (textToDisplayList[addString].Length * rythme) + 1f;//ajoute du temps à l'affichage
        float addSizeX = 60f + (11f * textToDisplayList[addString].Length);
        float addSizeY = 35f + (16f * statusPool.Count);
        AdaptRecSize(status2, addSizeX, addSizeY);//Calcul la taille de la boite à afficher         
        StartCoroutine(WriteText(txtToDisplay, rythme, newStatus2));//ajoute du contenue en écriture prompteur aux infos status2
    }

    private IEnumerator WriteText(Text textUI, float pas, bool newText)//le composant UI text , rythme d'affichage
    {//écrit à la volée lettre aprés lettre
        Animator anim = status2.GetComponent<Animator>();
        if (!writeOn)
        {
            anim.SetBool("statusGo", true);
            float timer = Time.time;
            writeOn = true;
            while (writeOn == true && duration > 0f)
            {
                // Debug.Log(writeOn + "  2222");
                if (duration <= 0f)
                {
                    writeOn = false;
                }
                if (increment < caractere.Count && Time.time > timer + pas)
                {
                    string workingString = textUI.text;
                    workingString += caractere[increment];
                    textUI.text = workingString;
                }
                if (Time.time > timer + pas)
                {
                    increment += 1;
                    duration -= Time.deltaTime;
                    timer = Time.time;
                }

                yield return null;
            }
            anim.SetBool("statusGo", false);
            textUI.text = "";
            caractere.Clear();
            increment = 0;
            duration = 0f;//reset
            statusPool.Clear();
            writeOn = false;
        }    
    }

    public IEnumerator ScrollBarProcess(float val)
    {//permet de faire défiler le texte du telex en utilisant la scrillbar
        Scrollbar scrollbarActivation = texActivation.GetComponentInChildren<Scrollbar>();
        float oldTime = Time.time + 1;//attends 2.5 secondes avant de lancer le mécanisme que le prompteur soit remplis
        scrollbarActivation.value = 1.2f;
        while (scrollbarActivation.value > 0.0f)
        {
            float pas = (1 / totalPas) / (totalPas / 2) / 35;
            if (Time.time > oldTime + pas)
            {
                scrollbarActivation.value -= pas;//rythme de défilement
                oldTime = Time.time;
            }
            yield return null;
        }
    }

    public void GareSetUp(int whereToPutGare)//si animGo est faux on retire le choix des destinations de l'écran
    {
        TrainController trainControler = playerControler.vehiculeManager.trainController;
        trainControler.NewTrainPosition(false, trainControler.gare[whereToPutGare - 1]);//defini une nouvelle destination pour le train
        carrietExpress.GetComponent<Animator>().SetBool("statusGo", false);
    }

    private void NewGearSprite(GameObject spriteGear)
    {
        NewGear newGear = spriteGear.GetComponent<NewGear>();
        newGear.plusGearSprite();
    }

    private void AdaptRecSize(GameObject objRectToAdapt, float sizeToAddX, float sizeToAddY)
    {
        RectTransform recT = objRectToAdapt.GetComponent<RectTransform>();
        recT.sizeDelta = new Vector2(sizeToAddX, sizeToAddY);//ajoute la hauteur nécéssaire pour l'affichage du texte
    }

    public void MoveRecT(GameObject thisGO, GameObject toDestination)
    {
        RectTransform thisGORecT = thisGO.GetComponent<RectTransform>();
        RectTransform toDestinationRecT = toDestination.GetComponent<RectTransform>();
        float offsetX = thisGORecT.sizeDelta.x / 2f;
        float offsetY = thisGORecT.sizeDelta.y;
        thisGORecT.anchoredPosition = new Vector2(toDestinationRecT.anchoredPosition.x + offsetX + 50f, toDestinationRecT.anchoredPosition.y + offsetY + 22f);
    }

    public IEnumerator SouriGo()
    {
        if (!sourisGo)
        {
            sourisGo = true;
            if (sourisGo)
            {
                yield return new WaitForSeconds(0.3f);
            }
            infoRoot.GetComponent<Animator>().SetBool("onOff", true);//active la mise en place à l'écran de l'ensemble souris+clavier
            if (sourisGo)
            {
                yield return new WaitForSeconds(0.1f);
            }
            Animator anim = sourisRoot.GetComponent<Animator>();
            float timeClip = 0f;
            foreach (GameObject souri in sourisRootList)//active déplacer la souris
            {
                if ((souri.name.Contains("0_")))
                {
                    souri.SetActive(true);
                }
            }
            yield return new WaitForSeconds(0.05f);
            anim.SetBool("onOff", true);
            yield return new WaitForSeconds(0.05f);
            while (timeClip < 0.99f && sourisGo)//tant que l'anim de la souris qui se déplace n'est pas fini on attend
            {
                AnimatorStateInfo sourisState = anim.GetCurrentAnimatorStateInfo(0);
                timeClip = sourisState.normalizedTime % 1;
                anim.SetBool("onOff", true);//lance le mouvement de la souris
                yield return null;
            }
            anim.SetBool("onOff", false);//lance le mouvement de la souris
            foreach (GameObject souri in sourisRootList)//active le click droit
            {
                ;
                if ((souri.name.Contains("0_SourisFlechesTxt")))
                {
                    souri.SetActive(false);
                }
            }
            timeClip = 0f;
            if (sourisGo)
            {
                yield return new WaitForSeconds(0.3f);
            }
            foreach (GameObject souri in sourisRootList)//active le click droit
            {
                if ((souri.name.Contains("1_")))
                {
                    souri.SetActive(true);
                }
            }
            while (timeClip < 0.98f && sourisGo)//tant que l'anim de la souris qui se déplace n'est pas fini on attend
            {
                timeClip += Time.deltaTime * 0.5f;
                yield return null;
            }
            timeClip = 0f;
            foreach (GameObject souri in sourisRootList)//deactive le click droit
            {
                if ((souri.name.Contains("1_")))
                {
                    souri.SetActive(false);
                }
            }


            if (sourisGo)
            {
                yield return new WaitForSeconds(0.3f);
            }
            foreach (GameObject souri in sourisRootList)//active le click droit
            {
                if ((souri.name.Contains("3_")))
                {
                    souri.SetActive(true);
                }
            }
            while (timeClip < 0.98f && sourisGo)//tant que l'anim de la souris qui se déplace n'est pas fini on attend
            {
                timeClip += Time.deltaTime * 0.5f;
                yield return null;
            }
            timeClip = 0f;
            foreach (GameObject souri in sourisRootList)//deactive le click droit
            {
                if ((souri.name.Contains("3_")))
                {
                    souri.SetActive(false);
                }
            }
            /*foreach (GameObject souri in sourisRootList)//active le click gauche
            {
                if ((souri.name.Contains("2_")))
                {
                    souri.SetActive(true);
                }
            }*/
            if (sourisGo)
            {
                yield return new WaitForSeconds(0.1f);
            }
            foreach (GameObject souri in sourisRootList)//déactive la souris
            {
                if ((souri.name.Contains("0_")))
                {
                    souri.SetActive(false);
                }
            }
            timeClip = 0f;
            barreEspaceJaune.SetActive(true);
            while (timeClip < 0.98f && sourisGo)//tant que l'anim de la souris qui se déplace n'est pas fini on attend
            {
                timeClip += Time.deltaTime * 0.5f;
                yield return null;
            }
            barreEspaceJaune.SetActive(false);//deactive le clavier
            infoRoot.GetComponent<Animator>().SetBool("onOff", false);//déactive la mise en place à l'écran de l'ensemble souris+clavier
        }
        sourisGo = false;
    }

    private IEnumerator InfosImportanteReset()
    {
        yield return new WaitForSeconds(8f);
        infosImportantes.SetActive(false);
    }

    public void InfoBulleReset()
    {
        infoBulle.SetActive(false);
    }

    private void ScrollReset(GameObject scrollbar)
    {
        Scrollbar scroll = scrollbar.GetComponent<Scrollbar>();
        scroll.value = 1.0f;//reset de la position de la scrollbar du prompteur
    }

    private void OnEnable()
    {
        foreach (Transform goTransform in vitesse.transform)
        {
            if (goTransform.gameObject.name == "MarcheCurseur")
            {
                marcheCurseur = goTransform.gameObject;
            }
            if (goTransform.gameObject.name == "MarcheAvant")
            {
                marcheAvant = goTransform.gameObject;
            }
            if (goTransform.gameObject.name == "MarcheArriere")
            {
                marcheArriere = goTransform.gameObject;
            }
            if (goTransform.gameObject.name == "Neutral")
            {
                neutral = goTransform.gameObject;
            }
            if (goTransform.gameObject.name == "PlateForme")
            {
                neutral = goTransform.gameObject;
            }
            if (goTransform.gameObject.name == "EnVoiture")
            {
                neutral = goTransform.gameObject;
            }
            if (goTransform.gameObject.name == "EnTrain")
            {
                neutral = goTransform.gameObject;
            }
            if (goTransform.gameObject.name == "Wagon")
            {
                neutral = goTransform.gameObject;
            }
            if (goTransform.gameObject.name == "Bird")
            {
                neutral = goTransform.gameObject;
            }
        }
        EventManagerPlayer.NewGear += Listener;//souscrit a l'évent system qui permet de déterminer grace à un string dans quel état est le moteur du vehicule
        foreach (Transform thisGo in barreEspaceJaune.transform)
        {
            barreEspaceList.Add(thisGo.gameObject);
        }
        foreach (Transform thisGo in sourisRoot.transform)
        {
            sourisRootList.Add(thisGo.gameObject);
            thisGo.gameObject.SetActive(false);
        }
        barreEspaceJaune.SetActive(false);
    }

    public void OnDesable()
    {
        EventManagerPlayer.NewGear -= Listener;//de-inscription à l'envent system
    }

    private void Start()
    {
        increment = 0;
        sourisGo = false;
        writeOn = false;
        barresEspace = 0;
        duration = 5f;
        firstVisit = true;
        InfoBulleReset();//déactive infoBulles par défault
    }
}