﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VehiculeControler : MonoBehaviour
{///scrip attaché au Rover Vehicule qui gère les paramétres d'un véhicule
    public GameObject vehiculeRoot;
    public VehiculeManager vehiculeManager;
    private TrainController trainControler;
    private AudioManager audioManager;
    private CameraMove cameraMove;
    private GameObject newTerritoire;
    private GameObject oldTerritoire;
    private GameObject territoire;
    private GameObject gps;//script de localisation pour le miniPlan
    public int wagonIndex;//index du wagon dans lequel se trouve le joueur
    private Skidmarks skidmarks;//traces de pneus au sol
    public int lastSkid;

    private Material oldMat;
    private Material newMat;

    public delegate void EventHandlerTerritoire(GameObject e);
    public static event EventHandlerTerritoire NewTerritoire;

    private void Awake()
    {
        territoire = GameObject.Find("Territoire");
    }

    private void ListenerTerritoire(GameObject e)//Fonction qui change le mat du sol en Rorchat shader
    {
        if(e != null)
        {
            oldTerritoire = newTerritoire;//l'ancien newTerritoire deviens le vieux avant de lui affecter une nouvelle valeur suite à l'event
            if (oldTerritoire != null)
            {
                string name = oldTerritoire.gameObject.name.Substring(0, 2);
                foreach (Transform lieu in territoire.transform)
                {
                    if (lieu.gameObject.name.Contains(name) && lieu.gameObject.GetComponent<MeshRenderer>() != null)
                    {
                        lieu.GetComponent<Renderer>().material = oldMat;//remets le mat d'origine au renderer de l'objet Territoire que l'on vient de quitter
                    }
                }
            }
            Material mat = e.GetComponent<Renderer>().material;
            oldMat = mat;//à présent on lui affecte celui du nouvel objet Territoire
            Texture texture = oldMat.GetTexture("_MainTex");
            newMat.SetTexture("_MainTex", texture);
            newMat.SetTexture("_MainTex2", texture);
            newMat.SetTexture("_BumpMap", oldMat.GetTexture("_BumpMap"));
            newMat.SetFloat("_BumpScale", 10f);
            newMat.SetFloat("_Blend", 0.005f);
            newMat.SetColor("_TintColor", vehiculeManager.col);
            newTerritoire = e;
            string name2 = newTerritoire.gameObject.name.Substring(0, 2);
            foreach (Transform lieu in territoire.transform)
            {
                if (lieu.gameObject.name.Contains(name2) && lieu.gameObject.GetComponent<MeshRenderer>() != null)
                {
                    lieu.GetComponent<Renderer>().material = newMat;//remets le mat d'origine au renderer de l'objet Territoire que l'on vient de quitter
                }
            }
        }      
    }


private void OnCollisionEnter(Collision vehiculeEnter)
    {
        if (!GameManager.instance.swapAvatar)
        {
            if (vehiculeEnter.gameObject.layer == 10)
            {
                for (int i = 0; i < vehiculeManager.vehiculeList.Count; i++)//si collision avec un autre véhicule on cherche dans la liste le nouveau véhicule
                {
                    if (vehiculeManager.vehiculeList[i] == vehiculeEnter.gameObject)
                    {
                        if (vehiculeEnter.gameObject.tag != "Train" && vehiculeEnter.gameObject.tag != "Oiseau")//Vehicule mais pas le train
                        {
                            if (!EventManagerPlayer.engineStateBool[7] && !vehiculeManager.delay)//si on est pas déja en mode EnVoiture et que le temps de latence est écoulé
                            {
                                GameManager.instance.destination = vehiculeManager.vehiculeList[i];
                                vehiculeManager.indexVehicule = i;//nouveau vehicule à cet index de la liste VehiculeList
                                EventManagerPlayer.BooleanSetUp("EnVoiture");//passe en mode en Voiture et déclenche l'Event///EVENT IN
                                Vector3 posParticule = vehiculeManager.vehiculeList[vehiculeManager.indexVehicule].transform.position + vehiculeManager.ConvertOffsetPlayerPos(vehiculeManager.indexVehicule, vehiculeManager.offsetDestinationPos[vehiculeManager.indexVehicule]);
                                vehiculeManager.particuleManager.ParticuleOn(posParticule, 9, 2f);//dustContourRouge
                            }
                        }
                        if (vehiculeEnter.gameObject.tag == "Train")//Train
                        {
                            if (!EventManagerPlayer.engineStateBool[8] && !vehiculeManager.delay)//si on est pas déja en mode EnVoiture et que le temps de latence est écoulé
                            {
                                GameManager.instance.destination = vehiculeManager.vehiculeList[3];//Train
                                vehiculeManager.indexVehicule = i;//nouveau vehicule à cet index de la liste VehiculeList
                                EventManagerPlayer.BooleanSetUp("Train");//passe en mode en Voiture et déclenche l'Event///EVENT IN
                                Vector3 posParticule = vehiculeManager.vehiculeList[vehiculeManager.indexVehicule].transform.position + vehiculeManager.ConvertOffsetPlayerPos(vehiculeManager.indexVehicule, vehiculeManager.offsetDestinationPos[vehiculeManager.indexVehicule]);
                                vehiculeManager.particuleManager.ParticuleOn(posParticule, 9, 2f);//dustContourRouge
                            }
                        }
                    }
                }
            }
            if (vehiculeEnter.gameObject.layer == 11)//layer Player
            {
                GameManager.instance.player.GetComponent<OnCollisionPlayer>().PlayerCollide(vehiculeEnter);
            }

            if (vehiculeEnter.gameObject.layer == 8)//layer Collide
            {
                if (vehiculeEnter.gameObject.tag == "Sculptris")
                {
                    audioManager.AddAudio("r2d2", false, false, 0f, 0);
                    StartCoroutine(cameraMove.EdgeDetection(false));//lance le contrast sur la camera
                    audioManager.AddAudio("expression", false, false, 0f, 0);
                }
                if (vehiculeEnter.gameObject.tag == "Arbre")
                {
                    audioManager.AddAudio("conversation", false, false, 0f, 0);
                    StartCoroutine(cameraMove.NoiseAndGrainGo());//lance le contrast sur la camera
                }
            }
        }
    }

    private void OnCollisionStay(Collision vehiculeEnter)
    {
        transform.Rotate(0f, Time.deltaTime*vehiculeManager.steering, 0);//fait tourner le joueur si ce dernier est coincé contre un mur
    }

    private void OnCollisionExit(Collision vehiculeEnter)
    {
        if (vehiculeEnter.gameObject.layer == 8)//layer Collide
        {
            vehiculeManager.playerAnim.SetBool("isGrounded", false);
            GameManager.instance.isGrounded = false;
        }
    }

    private void OnWheelCollision()//détecte toutes les frames (Update) si le rover touche une cellule afin de compenser sa position en hauteur lors de l'élévation ( FixedUpdate pour les cellules !)
    {
        if (vehiculeManager.axleInfos != null)//c'est le cas pour l'oiseau (pas de roues) donc il faut vérifier si il existe bien des weelCollider
        {
            if (vehiculeManager.axleInfos[2].isGrounded)
            {
                vehiculeManager.playerAnim.SetBool("isGrounded", true);

                GameManager.instance.isGrounded = true;
            }
            WheelHit wheelHitInfo;
            foreach (WheelCollider axel in vehiculeManager.axleInfos)///construit les skid mark au sol
            {
                if (axel.GetGroundHit(out wheelHitInfo))
                {
                    // Check sideways speed
                    // Gives velocity with +Z being our forward axis
                    Vector3 localVelocity = transform.InverseTransformDirection(vehiculeManager.rigidbodyVehicule.velocity);
                    float skidSpeed = Mathf.Abs(localVelocity.x);
                    if (skidSpeed <= 1f)
                    {
                        // MAX_SKID_INTENSITY as a constant, m/s where skids are at full intensity
                        float intensity = Mathf.Clamp01(skidSpeed / 1f);
                        Vector3 skidPoint = wheelHitInfo.point + (transform.right*0.1f) + (vehiculeManager.rigidbodyVehicule.velocity * Time.fixedDeltaTime);
                        lastSkid = skidmarks.AddSkidMark(skidPoint, wheelHitInfo.normal, intensity, lastSkid);
                    }
                    else {
                        lastSkid = -1;
                    }
                }
            }
            if (vehiculeManager.axleInfos[0].GetGroundHit(out wheelHitInfo))//verifie sur une seule roue si on est dans le wagon
            {
                if (wheelHitInfo.collider.gameObject.tag == "Wagon")//Si c'est un wagon
                {
                    GameObject parentWagon = wheelHitInfo.collider.gameObject.transform.parent.gameObject;
                    int indexInWagon = new int();
                    for (int i = 0; i < trainControler.wagonList.Count; i++)
                    {
                        if (trainControler.wagonList[i] == parentWagon)
                        {
                            indexInWagon = i;
                        }
                    }
                     if (!EventManagerPlayer.engineStateBool[9])
                    {
                        vehiculeManager.indexVehicule = 5;//Wagon
                        GameManager.instance.destination = wheelHitInfo.collider.gameObject;
                        EventManagerPlayer.BooleanSetUp("Wagon");//passe en mode en Wagon et déclenche l'Event///EVENT IN
                        Vector3 posParticule = vehiculeManager.vehiculeList[vehiculeManager.indexVehicule].transform.position +vehiculeManager.ConvertOffsetPlayerPos(vehiculeManager.indexVehicule, vehiculeManager.offsetDestinationPos[vehiculeManager.indexVehicule]);
                        vehiculeManager.particuleManager.ParticuleOn(posParticule, 7, 2f);//dustContourOrange
                    }
                }
                if (EventManagerPlayer.engineStateBool[9] && wheelHitInfo.collider.gameObject.tag != "Wagon")//Si c'est un wagon
                {
                    EventManagerPlayer.engineStateBool[9] = false;//remet le flag de wagon sur faux
                }
                if(wheelHitInfo.collider.gameObject.tag == "Territoire" && wheelHitInfo.collider.gameObject != newTerritoire || wheelHitInfo.collider.gameObject.tag == "Maison" && wheelHitInfo.collider.gameObject != newTerritoire)
                {
                    NewTerritoire(wheelHitInfo.collider.gameObject);//EVENT territoire
                }
                if (wheelHitInfo.collider.gameObject.tag == "Hydro")
                {
                    NewTerritoire(null);//EVENT territoire
                }
            }
        }
    }

    private void Start()
    {
        newMat = new Material(Shader.Find("Custom/RorchatCarriet"));
        trainControler = vehiculeManager.trainController;
        cameraMove = GameManager.instance.cameraFPS.GetComponent<CameraMove>();
    }

    private void OnEnable()
    {
        NewTerritoire += ListenerTerritoire;
        vehiculeRoot = GameObject.Find("VehiculesRoot");
        skidmarks = vehiculeRoot.GetComponent<Skidmarks>();
        vehiculeManager = vehiculeRoot.GetComponent<VehiculeManager>();
        audioManager = GameObject.Find("AudioContainer").GetComponent<AudioManager>();
        InvokeRepeating("OnWheelCollision", 1f, 0.1f);
        skidmarks.skidmarksMaterial = vehiculeManager.skidMarkMatList[vehiculeManager.indexMat];
    }

    private void OnDesable()
    {
        NewTerritoire -= ListenerTerritoire;
        vehiculeRoot = GameObject.Find("VehiculesRoot");
        vehiculeManager = vehiculeRoot.GetComponent<VehiculeManager>();
        InvokeRepeating("OnWheelCollision", 1f, 0.1f);
    }
}





