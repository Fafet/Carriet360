﻿using UnityEngine;
using System.Collections;

public class MagnetudeCameraLookAt : MonoBehaviour {

    //script attaché à un empty qui permet d'utiliser son foward vector pour aligner la camera à l'arrière du player

    private Quaternion rot;
    public float speed;
    private GameObject player;
    public GameObject lookAtMe;//objet généique qui permet à la camera de regarder ailleurs en cas de non navigation
    private Vector3 offsetCamMathilde;//offset vers le haut si le joueur est mathilde
    private Vector3 playerPos;//position du player à l'écran
    private PlayerControler playerControler;
    private bool navigating;


    public delegate void BoiteAVitesse();
    public BoiteAVitesse boiteAVitesse;

    private void Awake()
    {
        playerControler = GameManager.gameControl.GetComponent<PlayerControler>();
    }

    private void PlayerListener(GameObject e)
    {
        player = e;
        CheckPlayerOffset();
    }

    public void NavigatingListener(bool e)
    {
        navigating = e;
    }

    private void Listener(string e)//reçoit de l'éventSystem le string qui définit l'état du moteur. Permet de choisir la bonne méthode pour le delegate boite à vitesse
    {
        if (e == "Neutral")
        {
            boiteAVitesse = FixedUpdateCalculation;
        }
        if (e == "MarcheAvant")
        {
            boiteAVitesse = FixedUpdateCalculation;
        }
        if (e == "MarcheArriere")
        {
            boiteAVitesse = FixedUpdateCalculation;
        }
        if (e == "Jump")
        {
            boiteAVitesse = FixedUpdateCalculation;
        }
        if (e == "Transit")
        {
            boiteAVitesse = UpdateCalculation;
        }
        if (e == "Inventory")
        {
            boiteAVitesse = UpdateCalculation;
        }
        if (e == "EnVoiture")
        {
            boiteAVitesse = UpdateCalculation;
        }
        if (e == "Train")
        {
            boiteAVitesse = UpdateCalculation;
        }
        if (e == "Wagon")
        {
            boiteAVitesse = UpdateCalculation;
        }
        if (e == "Bird")
        {
            boiteAVitesse = UpdateCalculation;
        }
    }

    private void UpdateCalculation()
    {
        Quaternion direction = new Quaternion();
        if (navigating)
        {
            if (player != GameManager.instance.sphereAvatar)
            {
                direction = GameManager.instance.destination.transform.rotation;
            }
            else
            {
                direction = Quaternion.LookRotation(playerControler.directionRB);
                direction.y = 0f;
            }
            playerPos = player.transform.position + offsetCamMathilde;
            transform.position = playerPos;
        }
        else
        {
            lookAtMe = GameManager.instance.lookAtMe;
            transform.position = lookAtMe.transform.position;
            direction = Quaternion.LookRotation(lookAtMe.transform.position);
            direction.y = 0f;
        }
        transform.rotation = direction;// Quaternion.Slerp(transform.rotation, direction, Time.deltaTime * speed * 2f);//s'aligne sur le joueur
    }

    private void FixedUpdateCalculation()
    {
        Quaternion direction = new Quaternion();
        if (navigating)
        {
            if (player != GameManager.instance.sphereAvatar)
            {
                direction = GameManager.instance.destination.transform.rotation;
            }
            else
            {
                direction = Quaternion.LookRotation(playerControler.directionRB);
                direction.y = 0f;
            }
            playerPos = player.transform.position + offsetCamMathilde;
            transform.position = playerPos;
        }
        else
        {
            lookAtMe = GameManager.instance.lookAtMe;
            transform.position = lookAtMe.transform.position;
            direction = Quaternion.LookRotation(lookAtMe.transform.position);
            direction.y = 0f;
        }
        transform.rotation = direction;// Quaternion.Slerp(transform.rotation, direction, Time.deltaTime * speed * 2f);//s'aligne sur le joueur
    }

    private void CheckPlayerOffset()
    {
        if (player != GameManager.instance.sphereAvatar)
        {
            offsetCamMathilde = new Vector3(0f, 1f, 0f);//décalage vers le haut pour Mathilde
        }
    }

    private void OnEnable()
    {
        EventManagerPlayer.NewGear += Listener;//de-inscription à l'envent system
        GameManager.NewPlayer += PlayerListener;
        GameManager.NewNavigation += NavigatingListener;
    }

    private void OnDesable()
    {
        EventManagerPlayer.NewGear -= Listener;//de-inscription à l'envent system
        GameManager.NewNavigation -= NavigatingListener;
        GameManager.NewPlayer -= PlayerListener;
    }

    public void Start()
    {
        CheckPlayerOffset();
        speed = 1f;//default
        lookAtMe = player;
    }

    public void Update()
    {
        if (boiteAVitesse == UpdateCalculation)
        {
            boiteAVitesse();
        }
    }

    public void FixedUpdate()
    {
        if (boiteAVitesse == FixedUpdateCalculation)
        {
            boiteAVitesse();
        }
    }
}