﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpriteManager : MonoBehaviour {

	// Script sur le GameManager qui gére l'affichage des sprites de menus 

	public static Animator animator;

	public static void SetOn(){
		animator.SetBool("onOff",true);
	}
	public static void SetOff(){
		animator.SetBool("onOff",false);
	}
	public static void ScaleSetOn(){
		animator.SetBool("scaleOnOff",true);
	}
	public static void ScaleSetOff(){
		animator.SetBool("scaleOnOff",false);
	}
	
}
