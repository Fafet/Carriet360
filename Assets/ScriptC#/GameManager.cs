
//Cette oeuvre, création, site ou texte est sous licence Creative Commons  Attribution - Pas de Modification 4.0f International. Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse suivante http://creativecommons.org/licenses/by-nd/4.0f/ ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
//script attaché au sprites Boutons
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameManager : MonoBehaviour {	
	private static GameManager _instance;//singleton	
	public static GameManager instance{
		get{

			if(_instance == null){
				_instance = Object.FindObjectOfType(typeof(GameManager)) as GameManager;				
			}
			return _instance;
		}
	}

    public static GameObject gameControl;
    public static GameObject cameraSprite;

    public AnimationCurve anim;
	public float tempT;
	public float hauteurMer;//hauteur de la mer (diferente d'un chapitre à l'autre
	public int collidingGO;
	public int chapitre;//quel chapitre
    public int indexlectureActuelle;//index de la zone reverbZone de son à l'écoute
	public int indexProgression;
	public int iteration;//le nombre de fois ou le player est venu dans l'editeur Newtopie
    public int gridIncrement;//nombre de grille de la passerelle créés
	
	public bool  worldLimit;
	public bool  navigating;//navigation FPS 
	public bool  observation;//regarder autour de soi sans déplacer la camera
	public bool  collide;
	public bool  createMesh;//flag de début de creation du mesh de sortie vers le BL
	public bool  walkBack;//marcher en arrière le long de la walkLine
	public bool  isGrounded;//flag pour savoir si le bip touche ou pas le sol
	public bool  barresVies;//sprites de progression de lecture
	public bool  inventoryMap;//autorise ou pas l'usage de l'inventaire (plan)
    public bool  zoneJuliette;//defini si je suis dans la zone de texte de Juliette soit le BL
	public bool  zoneActivation;//flag pour activer la zone à afficher
	public bool  displayTexte;//affiche le texte ou pas
	public bool  toutLuJuliette;//flag qui permet de savoir si tout les chapitres du niveau ont été lu ou non
	public bool  toutLuMusoirD;//flag qui permet de savoir si tout les chapitres du niveau ont été lu ou non
	public bool  toutLuTraining;//flag qui permet de savoir si tout les chapitres du niveau ont été lu ou non
	public bool swapAvatar;//changement de joueur
	public bool firstVisit;//premiére fois du niveau que le player collide avec une RéverbZone
    public bool soundPermission;//peut-on jouer du son ?
		//
	public Vector3 raydirectionpointer;//vecteur normalisé
    public Vector3 raydirection;//raydirectionPointer mais pas normalisé

    public Vector3 posStart;//position du bip au départ du chapitre
	public Vector3 posWorldLimits;//lieu de repositionnement du player une fois qu'il à touché la world limit
	public GameObject destination;//la nouvelle destination sur laquelle on a cliqué (dans le sommaire)
    private Vector3 previous;//position du player à cette frame moins 1
		//
	public GameObject[] atterissageList;
	public List<GameObject> lineRenderTarget;//tableau des GO pour la line render dans le BLInt afin d'indiquer le chemin à suivre
	public List<GameObject> reverbZoneGO;//liste des bulles audios actives
		//
	public Ray ray;
    //
    public List<GameObject> playerList;//liste de l'ensemble des players
	public GameObject player;//le joueur à l'instant T
	public GameObject sphereAvatar;//la sphere Avatar
    public GameObject avatarAbstrait;
    public GameObject genericPlayer;//le player Mathilde niveau 4
	public GameObject bipPlayer;//le bip personnage
	public GameObject WorldLimits;
	public GameObject underInfluenceAudio;//reverbGO dans lequel le player est entré.
	public GameObject atterissage;//le lieu d'atterissage du voyager. Le lieu change en fonction de la destination
    public GameObject lookAtMe;//la boiteScenette en cours

    public Material matLineRenderer;


    public GameObject cameraFPS;
	public GameObject cameraActive;

    public delegate void EventHandler(GameObject e);//création delegate
    public static event EventHandler NewPlayer;//NewGear est le déclencheur d'event de type EventHandler/ doit etre static pour ajouter facilement un Listener depuis un autre script
    public delegate void EventHandlerNav(bool e);//création delegate
    public static event EventHandlerNav NewNavigation;//NewGear est le déclencheur d'event de type EventHandler/ doit etre static pour ajouter facilement un Listener depuis un autre script
    public delegate void EventHandlerPrefab(Vector3 positionPrefab);//création delegate qui contient la position où instancier le prefab
    public static event EventHandlerPrefab NewPrefab;//NewGear est le déclencheur d'event de type EventHandler/ doit etre static pour ajouter facilement un Listener depuis un autre script
    public delegate void EventHandlerSon(GameObject objWithSound);
    public static event EventHandlerSon NewSound;
    public delegate void EventHandlerInt(GameObject e);//création delegate
    public static event EventHandlerInt NewInt;//NewGear est le déclencheur d'event de type EventHandler/ doit etre static pour ajouter facilement un Listener depuis un autre script
    private void Awake()
    {
        gameControl = this.gameObject;
        cameraFPS = GameObject.Find("CameraFPS");
        cameraSprite = GameObject.Find("CameraSprite");
        sphereAvatar = GameObject.Find("SphereAvatar");
        genericPlayer = GameObject.Find("LindaWalk");
        WorldLimits = GameObject.Find("WorldLimits");
        underInfluenceAudio = null;
        cameraActive = cameraFPS;
    }

    public static void PlugNewPlayer(GameObject e)//Methode pour definir le contenu du delegate et appeler les différent gestionnaires ajoutés à l'event newgear. Cette méthode est appelée à chaque évent et permet de retourner le bon string au listener
    {
        if (NewPlayer != null)
        {
            NewPlayer(e);
        }
        else Debug.Log("PlugNewPlayer empty");
    }

    public static void PlugNewNavigation(bool e)//Methode pour definir le contenu du delegate et appeler les différent gestionnaires ajoutés à l'event newgear. Cette méthode est appelée à chaque évent et permet de retourner le bon string au listener
    {
        if (NewNavigation != null)
        {
            NewNavigation(e);
        }
        else Debug.Log("NewNavigation empty");
    }

    public static void PlugNewPrefab(Vector3 e)//Methode pour definir le contenu du delegate et appeler les différent gestionnaires ajoutés à l'event newgear. Cette méthode est appelée à chaque évent et permet de retourner le bon string au listener
    {
        if (NewPrefab != null)
        {
            NewPrefab(e);
        }
        else Debug.Log("NewPrefab empty");
    }

    public void PlayerSetUp(GameObject playe)
    {
        PlugNewPlayer(playe);
        player = playe;
        lookAtMe = playe;
    }

    public void NavigatingSetUp(bool navGo, GameObject lookAt)//Nvigation ou non, objet eventuellement à faire observer par la camera qui n'est plus obligée de suivre le player au cas ou la navigation est false
    {
        PlugNewNavigation(navGo);
        navigating = navGo;
        if(lookAt != null)
        {
            lookAtMe = lookAt;//affecte à la variable du gameManager l'objet à observer avec la camera
        }
    }

    public static void PlugNewSound(GameObject e)//Methode pour definir le contenu du delegate et appeler les différent gestionnaires ajoutés à l'event newgear. Cette méthode est appelée à chaque évent et permet de retourner le bon string au listener
    {
        if (NewSound != null)
        {
            NewSound(e);
        }
        else Debug.Log("PlugNewSound empty");
    }

    public static void PlugNewInt(GameObject e)//Methode pour definir le contenu du delegate et appeler les différent gestionnaires ajoutés à l'event newgear. Cette méthode est appelée à chaque évent et permet de retourner le bon string au listener
    {
        if (NewInt != null)
        {
            NewInt(e);
        }
        else Debug.Log("PlugNewInt empty");
    }

    IEnumerator  WaitAndLoad (){//chargement d'un nouveau niveau d'index 2 soit "ilesLycee"
		yield return new WaitForSeconds(2);
        SceneManager.LoadSceneAsync(2);
	}

	public float  PlayerSpeed (){
		float playerSpeed = ((player.transform.position - previous).magnitude) / Time.deltaTime;
		previous = player.transform.position;
		//Debug.Log("vitesse"+playerSpeed);
        return playerSpeed;
    }
	
	public AnimationCurve CreateCurveLinear ( float timeEnd ,   float origineGO ,   float destinationGO  ){
		Keyframe[] ks;//les clefs d'animation dont le nombre est défini par l'argument timene
		ks = new Keyframe[2];
		AnimationCurve anim= new AnimationCurve(ks);//affecte les clefs à une nouvelle courbe d'anim
		anim = AnimationCurve.Linear(0,origineGO,timeEnd,destinationGO);
		return  anim;//retourne l'anim avec la fonction. Pratique pour récupérer la courbe sur le nouveau script
	}
	
	//fonction qui anime un mouvement progressif d'un GO vers un autre. TimeEnd décrit la durée totale de l'animation.Minimum de 4 clefs
	public AnimationCurve CreatCurvFromTo ( int ksLength ,   float timeEnd ,   float offset ,   float origineGO ,   float destinationGO  ){// timeEnd est le temps total de l'anim et offset est la valeur du décalage afin d'ajouter une variation supplémentaire à l'anim
		Keyframe[] ks;//les clefs d'animation dont le nombre est défini par l'argument timene
		ks = new Keyframe[ksLength];//définie le temps et les clefs d'un courbe d'animation pour y
		float timeFraction; 
		timeFraction = new float ();
		timeFraction = timeEnd/ksLength;//divise le temps par le nombre de clefs d'anim
		for (int p=0;p<ksLength;p++){
			if(p==0){
				ks[p] = new Keyframe(timeFraction,origineGO);
			}
			if(p==1){
				ks[p] = new Keyframe(timeFraction*(p+1),Mathf.Abs(origineGO)*offset);  
			}
			if(p==ksLength-3){
				ks[p] = new Keyframe(timeFraction*p,Mathf.Abs(origineGO)*offset);
			}
			if(p==ksLength-2){
				ks[p] = new Keyframe(timeFraction*p,Mathf.Abs(destinationGO)*offset);
			}
			if(p==ksLength-1){
				ks[p] = new Keyframe(timeFraction*p,destinationGO);
			}
		}
		anim = new AnimationCurve(ks);//affecte les clefs à une nouvelle courbe d'anim
		return  anim;//retourne l'anim avec la fonction. Pratique pour récupérer la courbe sur le nouveau script
	}
	
	//fonction qui permet le déplacement le long des courbes x, y et z 
	public void  MouveMorphCurve ( GameObject mouveThis ,   AnimationCurve animCuuX ,    AnimationCurve animCuuY ,   AnimationCurve animCuuZ ,   float timerScript  ){
        mouveThis.transform.position = new Vector3(animCuuX.Evaluate(timerScript), animCuuY.Evaluate(timerScript), animCuuZ.Evaluate(timerScript));
	}

    void OnEnable (){
		firstVisit = true;
		swapAvatar = false;
		if(  WorldLimits != null){
			WorldLimits.SetActive(true);
		}
        inventoryMap= false;//autorise ou pas l'usage de l'inventaire (plan)
        collide = false;
		navigating = true;
		observation = false;
		createMesh = false;
		isGrounded = false;
		zoneActivation = false;
		toutLuJuliette = false;
		displayTexte = false;
        soundPermission = true;
        indexlectureActuelle = 0;
        SetSceneStartSetUp();
    }

    private void SetSceneStartSetUp()
    {
        if (SceneManager.GetActiveScene().name == "Level1")
        {
            chapitre = 1;
            zoneJuliette = true;
            PlayerSetUp(genericPlayer);//linda
            posStart = sphereAvatar.transform.position;
        }
    }

    private void Start()
    {
        SetSceneStartSetUp();
        //localDatas = GlobalControl.Instance.savedGameManagerData;//sous localDatas on retrouve tout les membres de la class SavedSerializedDatas où sont placés les variables que l'on souhaite sauvegardées
    }

    void Update()
    {
        ray = GameManager.instance.cameraActive.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        raydirection = ray.GetPoint(20) - ray.origin;
        raydirectionpointer = Vector3.Normalize(raydirection);
        //Debug.DrawRay (ray.origin,raydirectionpointer);
    }
}	
