﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowSpline : MonoBehaviour {	

	//script sur parent Lucioles "LuciolesMagnet" qui gère le déplacement des lucioles le long de la spline

	public SplineManager splineManager;
	public SplineData splineFosses;
	public GameObject objectWithSpline;
	public float progression;
	public float duration;
	public Vector3 magnetude;
	public Vector3 startPoint;
	public Vector3 endPoint;
	public Vector3 from;
	public float timer;
	public bool  seePlayer;
	public bool  rescueMode;
	public int nouvelIndex;
	public int lucioleIndex;

	public delegate void Mouvement();
	public Mouvement mouvement; //delegate pour action plus ou moins de progression

	public void  Awake (){
		splineManager = GameObject.Find("SplineManager").GetComponent<SplineManager>();
	}
	
	public void ProgressionPlus (){
		progression += Time.deltaTime / duration;
		if (progression > 1) {
			progression = 1;
		}
	}
	
	public void ProgressionMoins (){
		progression -= Time.deltaTime / duration;
		if (progression < 0) {
			progression = 0;
		}
	}

	public void DistanceFromPlayer(){
		Vector3 playerPos = GameManager.instance.player.transform.position;
		float distance = Vector3.Distance (playerPos, this.transform.position);
		float vitesse = 500.0f - (distance*3.0f);
		duration = Mathf.Max(40.0f, vitesse);
	}

	public int InfluencePointPlayer (){//permet de savoir de quel point de la spline le player est le plus proche
		Vector3 posPlayer = GameManager.instance.player.transform.position;
		nouvelIndex = splineManager.DistObjectSplinePoints(posPlayer, splineFosses);//recupere index le plus proche des luciolles
		splineFosses.influencePlayerIndex = nouvelIndex;//met à jour l'index le plus près du joueur sur le script générique SplineManager
		InfluencePointLucioles(posPlayer.y);//vérifie toutes les 3 secondes si le point d'influence à changé et défini la valeur du nouvel index de ce point
		return nouvelIndex;
	}
	
	public int InfluencePointLucioles (float yPlayer){
		Vector3 thisPos = new Vector3(this.transform.position.x,yPlayer,this.transform.position.z);
		lucioleIndex = splineManager.DistObjectSplinePoints(thisPos, splineFosses);//recupere index le plus proche des luciolles
		return lucioleIndex;
	}
	
	public void RescueFirst (){//instruction sur une frame pour définir le point de départ et d'arriver des lucioles pour aller récupérer le player
		Vector3 thisPos = this.transform.position;
		startPoint = thisPos;//stock le point de départ des lucioles
		endPoint = GameManager.instance.player.transform.position;//stock le point d'arrivée
		from = startPoint;//prépare l'inversion du mouvement pour le retour des lucioles à leur point de départ
		StartCoroutine(RescueSecond());
	}
	
	public IEnumerator RescueSecond (){	
		while(rescueMode){
			Vector3 thisPos = this.transform.position;
			this.transform.position = Vector3.Lerp(thisPos, endPoint,Time.deltaTime*2f);//on déplace les luciolles vers le player pour lui indiquer le chemin à prendre	
			float temp= Vector3.Distance(thisPos,endPoint);
			if(temp<3){//la distance des luciolles jusqu'a la destination
				if(from == endPoint){
					this.transform.position = endPoint;
					rescueMode = false;
					timer = 0;//reset
				}
				if(from != endPoint){//marche arrière (retour des lucioles à leur place de départ
					endPoint = startPoint;
					endPoint = from;//inverse le déplacement pour retour au point de départ
				}
			}
			yield return 0;	
		}
		StartCoroutine(InfluencePoint());
	}
	
	public IEnumerator InfluencePoint (){
		while(!rescueMode){
			this.transform.position = splineFosses.bezierSpline.GetPoint(progression);//positione les lucioles sur la spline	
			mouvement();//déplace les lucioles le long de la spline
			if(lucioleIndex>nouvelIndex+2 && !seePlayer){
				mouvement = ProgressionMoins;
			}
			if(lucioleIndex<=nouvelIndex){
				mouvement = ProgressionPlus;
				if(seePlayer){
					timer += Time.deltaTime;
					if(timer>6){
						rescueMode = true;//lance le timer et tout ce qu'il faut pour aller chercher le joueur
					}
				}
				else{
					timer = 0;
				}
			}
			yield return 0;	
		}
		RescueFirst();
	}
	
	public bool CastRay (){
		magnetude = GameManager.instance.player.transform.position - this.transform.position;
		Ray ray = new Ray(this.transform.position,magnetude);
		RaycastHit hit;
		Physics.Raycast(ray, out hit);
		if(hit.transform.gameObject == GameManager.instance.player){
			seePlayer = true;
		}
		else
			seePlayer = false;
		DistanceFromPlayer ();
		return seePlayer;
	}

	public IEnumerator WaitAndLuciolesGo(){
		yield return new WaitForSeconds(5);
		if(this.gameObject.activeInHierarchy){
			StartCoroutine(InfluencePoint());//lance les calculs
			InvokeRepeating("CastRay",2,3);//vérifie si les lucioles voient le player ou non
			InvokeRepeating("InfluencePointPlayer",1,1);//vérifie toutes les 3 secondes si le point d'influence à changé et défini la valeur du nouvel index de ce point
		}
	}
	
	public void  OnEnable (){
		duration =400.0f;
		progression = 0;
		timer = 0;
		seePlayer = false;
		nouvelIndex = 0;
		lucioleIndex = 0;
		mouvement = ProgressionPlus;
		rescueMode = false;
		StartCoroutine(WaitAndLuciolesGo());//lance les calculs

	}
	public void  OnDisable (){
		progression = 0;
	}

    private void Start()
    {
        splineFosses = splineManager.GetTheGoodSpline("Musoir");//récupère la bonne spline
    }
}