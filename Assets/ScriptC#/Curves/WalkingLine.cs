﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WalkingLine : MonoBehaviour {	

	//Script sur le GO parent Avatar

	private List<Vector3> points;
	private List<Vector3> currentPoints;
	public List<Vector3> drawLinePoints;
	private List<GameObject> alphabet;//list des GO alphabet sprites 3D rendus en WireFrame
    //private GameObject alphabetGO;
    private int curveCount;
	private float distance;
	private Vector3 lastPoint;//le dernier point posé dans le décor
	private int index;//le numéro sur quatre de l'index concerné de la sline actuelle

    public Material matLineRenderer;
    public Material alphabetMaterial;
    public LineRenderer lineRenderer;
    public LineRenderer lineRenderer2;
	
	private Vector3 p0;
	private Vector3 p1;
	private Vector3 p2;
	private Vector3 p3;
	private int numberOfPoints;
	
	private void  Awake (){

        points = new List<Vector3>();
        currentPoints = new List<Vector3>();
        alphabet = new List<GameObject>();
}
	
	private void Distance (){//calcul la distance entre le dernier point déposé et le player
		Vector3 playerPos = GameManager.instance.player.transform.position;
		float dis = Vector3.Distance(lastPoint, playerPos);//calcul distance du Player du dernier point de spline posé dans le décor pour marquer sa progression
        distance = dis;
	}
	
	private void  DropAPoint (){
		for (int idx=index;idx>=index && idx<currentPoints.Count;idx++){
			Vector3 playerPos = GameManager.instance.player.transform.position;
			Vector3 offset = new Vector3(0.0f,-0.0f ,0.0f);
			currentPoints[idx] = playerPos  + offset;//pose les points de la spline plus petits que l'index actuel à la position du player
		}
	}
	
	private void  Index (){
		lastPoint = currentPoints[index];//affecte le dernier point déposé à la variable lastPoint
		if(index <3){
			index += 1;	//incrément si on a pas atteind le dernier point de la spline
		}
		if(index >=3){	
			ResetPoint();//sinon on ajoute une nouvelle spline pour continuer à dessiner le chemin parcouru
			curveCount += 1;
			index = 0;
		}
	}
	
	private void  ResetPoint (){
		if(curveCount>0){//transfert les 3 points de la spline dans un tableau ou tout les points du parcour du joueur sont rassemeblés avant de créer une nouvelle courbe
			p0 = points[points.Count-1];//le dernier point crée dans le tableau Point devient le premier point de la nouvelle courbe 
			p1 = currentPoints[0];
            p2 = currentPoints[1];
            p3 = currentPoints[2];
            DrawLineRender();
			foreach(Vector3 oo in currentPoints){			
				points.Add(oo);
			}
		}
		currentPoints = new List<Vector3>();//crée une nouvelle liste
		for(int o=0;o<4;o++){
			Vector3 playerPos = GameManager.instance.player.transform.position;
			currentPoints.Add(playerPos);
		}
	}
	
	public void  DrawLineRender (){
		Vector3 position;
		int t; 
		for(int li=0;li<numberOfPoints;li++){
			t = li / (numberOfPoints - 1);
			float tFloat = (float)t;
			position = Bezier.GetPoint(p0,p1,p2,p3,tFloat);      	
			drawLinePoints.Add(position); 
			lineRenderer.SetPosition(li, position); 
		}
	}
	
	private void  WalkBack (){//affiche les les lettre sur la walkLine
		//lineRenderer2 = new LineRenderer();
		lineRenderer2 = GameManager.instance.player.GetComponent<LineRenderer>();
		lineRenderer2.useWorldSpace = true;
		lineRenderer2.material = matLineRenderer;
		//lineRenderer2.SetColors(Color(1,0,0,1), Color(0,1,0,1));
		lineRenderer2.SetWidth(1.0f,1.0f);
		lineRenderer2.SetVertexCount(drawLinePoints.Count);
		int pas = drawLinePoints.Count/24;
		for (int y=drawLinePoints.Count-1;y>=0;y--){
			lineRenderer2.SetPosition(y, drawLinePoints[y]);
		}
		for (int alpha=0;alpha<alphabet.Count;alpha++){
			if(alpha<25){
				alphabet[alpha].transform.position = drawLinePoints[pas*alpha]; //deplace les lettre de l'alphabet l'une après l'autre avec un pas de 24
				//GameManager.instance.Render(alphabet[alpha]);
			}
		}
		GameManager.instance.walkBack = false;
	}
	
	
	
	private void  ResetWalkBack (){//masque les chiffres sur la walkLine
		lineRenderer2 = new LineRenderer();
		for (int al=0;al<alphabet.Count;al++){
			//GameManager.instance.DontRender(alphabet[al]);//masque les lettre de l'alphabet de la WalkLine
		}
	}
	
	private void  OnEnable ()
    {
		//remise à zero des tableaux
		if (drawLinePoints != null){
			drawLinePoints.Clear();
			currentPoints.Clear();
			points.Clear();
		}
		distance = 0.1f;
		curveCount = 0;
		index = 0;
		numberOfPoints = 5;
		Vector3 playerPos = GameManager.instance.player.transform.position;
		lastPoint = playerPos;
		ResetPoint();
		p0 = playerPos; 
		p1 = playerPos; 
		p2 = playerPos;
		p3 = playerPos;
		points.Add(p0);
		points.Add(p1);
		points.Add(p2);
		points.Add(p3);
		///////////////////GameManager.instance.player.AddComponent<LineRenderer>();//ajoute une nouvelle line render au player (pour la fonction WalkBack)
		/*if(alphabet == null){
			foreach(Transform alph  in alphabetGO.transform){//ajoute les lettre de l'aphabet en 3D au tableau
				alphabet.Add(alph.gameObject);
				alph.gameObject.GetComponent<MeshRenderer>().materials[0] = alphabetMaterial;//affecte matérial aux lettre de l'alphabet
				GameManager.instance.DontRender(alph.gameObject);//masque les lettres de l'alphabet
			}
		}*/
        InvokeRepeating("Distance",0.2f,1.0f);
	}

	private void  OnDisable(){
		LineRenderer lineRenderer;
		lineRenderer = GetComponent<LineRenderer>();
		Destroy(lineRenderer);
	}

    private void Start()
    {        
        lineRenderer = GetComponentInChildren<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
        lineRenderer.SetColors(new Color(1.0f, 0.0f, 0.0f, 0.8f), new Color(0.0f, 1.0f, 0.0f, 0.0f));
        lineRenderer.SetWidth(1.0f, 0.1f);
        lineRenderer.SetVertexCount(numberOfPoints);
    }

	private void  FixedUpdate(){
		if(distance>4.0f){
			DropAPoint();
			Index();
			distance = 0.0f;
		}
		if(GameManager.instance.walkBack){
			WalkBack ();
		}
		
	}
	
}