﻿using UnityEngine;
using System.Collections;

public class AvatarSetUp : MonoBehaviour {

	//script sur la racine Avatar qui gère les animations, activations et collisions des avatar

	private PlayerControler playerContoler;
    private GameObject player;
    private ParticuleManager particuleManager;
    private WalkingLine walkingLine;
	
	private void Awake (){
		playerContoler = GameManager.gameControl.GetComponent<PlayerControler>();
        particuleManager = GameManager.gameControl.GetComponent<ParticuleManager>();
    }

    private void PlayerListener(GameObject e)
    {
        player = e;
    }

    public void MiseEnPlace(GameObject newPlayer, GameObject intermediateGO, string genericName){//intemediate GO est utile pour l'animation du tetra par exemple qui utilise deux GO: 1 pour la construction animée du tétra et l'autre pour l'avatar lui meme que l'on peut déplacer
                                                                                                        ///////////////////////////mettre intermediateGO à Null si il n'y à pas d'animation faite par un GO intermedaire
        GameManager.instance.swapAvatar = true;
        GameManager.instance.NavigatingSetUp(false, null);
//walkingLine.enabled = false;		
		GameObject oldPlayer = player;
        StartCoroutine(SwapFalse(oldPlayer));
        playerContoler.ChangePlayer(oldPlayer, newPlayer);//appel à la fonction qui permet de changer de player	
	}

    private IEnumerator SwapFalse(GameObject oldJoueur)
    {
        yield return new WaitForSeconds(1f);
        Animator oldAnimator = oldJoueur.GetComponent<Animator>() as Animator;
        Animator newAnimator = player.GetComponent<Animator>() as Animator;
        oldAnimator.SetBool("swap", false);
        oldAnimator.SetFloat("playerSpeed", 0f);
        newAnimator.SetBool("isGrounded", true);
        newAnimator.SetBool("swap", false);
    }

	private IEnumerator WaitAndDestroy(GameObject destruct, float seconds){
		yield return new WaitForSeconds(seconds);
		Destroy(destruct);
	}

    private void OnEnable()
    {
        GameManager.NewPlayer += PlayerListener;//souscrit à l'évent system qui défini le bon joueur
    }

    private void OnDesable()
    {
        GameManager.NewPlayer -= PlayerListener;//souscrit à l'évent system qui défini le bon joueur
    }
}
