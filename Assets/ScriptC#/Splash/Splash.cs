﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Splash : MonoBehaviour
{

    //script sur SystemNavig pour le splashScreen

    private bool loading;
    private GameObject jouerAir;
    private GameObject infoRoot;
    private GameObject barreEspaceJaune;
    private GameObject infosLegales;
    private GameObject generique;
    private GameObject sourisRoot;
    private GameObject sliderVolume;
    private GameObject chargement;
    private GameObject instruction;
    public List<GameObject> barreEspaceList;
    public List<GameObject> sourisRootList;
    public AudioSource polza;
    public TextAsset instructionText;
    public TextAsset emptyText;
    private float idealVolume;

    private bool sourisGo;

    private void Awake()
    {
        jouerAir = GameObject.Find("JouerAir");
        infoRoot = GameObject.Find("InfoRoot");
        barreEspaceJaune = GameObject.Find("BarreEspaceJaune");
        sourisRoot = GameObject.Find("SourisRoot");
        infosLegales = GameObject.Find("InfosLegales");
        generique = GameObject.Find("Generique");
        sliderVolume = GameObject.Find("SliderVolume");
        chargement = GameObject.Find("Chargement");
        instruction = GameObject.Find("Instruction");
    }

    public void PolzaVolume()
    {
        polza.volume = sliderVolume.GetComponent<Slider>().value;
    }

    public void PolzaGo(bool slider)
    {
            sliderVolume.GetComponent<Animator>().SetBool("onOff", slider);
    }

    private IEnumerator InstructionGo()
    {
        yield return new WaitForSeconds(3f);
        StartCoroutine(WriteOn(instruction.GetComponent<Text>(), instructionText.text, false, 0.02f));
    }

    public IEnumerator SouriGo()
    {
        Animator anim = sourisRoot.GetComponent<Animator>();
        infosLegales.GetComponent<Animator>().SetBool("onOff", false);
        generique.GetComponent<Animator>().SetBool("onOff", false);
        GameObject.Find("CarrietLow").GetComponent<Animator>().SetBool("onOff", false);
        GameObject.Find("Jouer").GetComponent<Animator>().SetBool("onOff", false);
         if (!sourisGo)
        {
            sourisGo = true;
            if (sourisGo)
            {
                yield return new WaitForSeconds(0.3f);
            }
            infoRoot.GetComponent<Animator>().SetBool("onOff", true);//active la mise en place à l'écran de l'ensemble souris+clavier
            if (sourisGo)
            {
                yield return new WaitForSeconds(0.1f);
            }
            float timeClip = 0f;
            foreach (GameObject souri in sourisRootList)//active déplacer la souris
            {
                if ((souri.name.Contains("0_")))
                {
                    souri.SetActive(true);
                    anim.SetBool("onOff", false);
                }    
            }
            yield return new WaitForSeconds(0.05f);
            anim.SetBool("onOff", true);
            yield return new WaitForSeconds(0.05f);
            while (timeClip < 0.98f && sourisGo)//tant que l'anim de la souris qui se déplace n'est pas fini on attend
            {
                AnimatorStateInfo sourisState = anim.GetCurrentAnimatorStateInfo(0);
                timeClip = sourisState.normalizedTime % 1;
                anim.SetBool("onOff", true);//lance le mouvement de la souris
                yield return null;
            }
            foreach (GameObject souri in sourisRootList)//active le click droit
            {
                ;
                if ((souri.name.Contains("0_SourisFlechesTxt")))
                {
                    souri.SetActive(false);
                }
            }
            timeClip = 0f;
            if (sourisGo)
            {
                yield return new WaitForSeconds(0.3f);
            }
            foreach (GameObject souri in sourisRootList)//active le click droit
            {
                if ((souri.name.Contains("1_")))
                {
                    souri.SetActive(true);
                }
            }
            while (timeClip < 0.98f && sourisGo)//tant que l'anim de la souris qui se déplace n'est pas fini on attend
            {
                timeClip += Time.deltaTime * 0.5f;
                yield return null;
            }
            timeClip = 0f;
            foreach (GameObject souri in sourisRootList)//deactive le click droit
            {
                if ((souri.name.Contains("1_")))
                {
                    souri.SetActive(false);
                }
            }


            if (sourisGo)
            {
                yield return new WaitForSeconds(0.3f);
            }
            foreach (GameObject souri in sourisRootList)//active le click droit
            {
                if ((souri.name.Contains("3_")))
                {
                    souri.SetActive(true);
                }
            }
            while (timeClip < 0.98f && sourisGo)//tant que l'anim de la souris qui se déplace n'est pas fini on attend
            {
                timeClip += Time.deltaTime * 0.5f;
                yield return null;
            }
            timeClip = 0f;
            foreach (GameObject souri in sourisRootList)//deactive le click droit
            {
                if ((souri.name.Contains("3_")))
                {
                    souri.SetActive(false);
                }
            }
            /*foreach (GameObject souri in sourisRootList)//active le click gauche
            {
                if ((souri.name.Contains("2_")))
                {
                    souri.SetActive(true);
                }
            }*/
            if (sourisGo)
            {
                yield return new WaitForSeconds(0.1f);
            }
            foreach (GameObject souri in sourisRootList)//déactive la souris
            {
                if ((souri.name.Contains("0_")))
                {
                    souri.SetActive(false);
                }
            }
            timeClip = 0f;
            barreEspaceJaune.SetActive(true);
            while (timeClip < 0.98f && sourisGo)//tant que l'anim de la souris qui se déplace n'est pas fini on attend
            {
                timeClip += Time.deltaTime * 0.5f;
                yield return null;
            }
            barreEspaceJaune.SetActive(false);//deactive le clavier
            infoRoot.GetComponent<Animator>().SetBool("onOff", false);//déactive la mise en place à l'écran de l'ensemble souris+clavier
        }
        sourisGo = false;

        GameObject.Find("Jouer").GetComponent<Animator>().SetBool("onOff", true);
        GameObject.Find("CarrietLow").GetComponent<Animator>().SetBool("onOff", true);
    }

    public static IEnumerator WriteOn(Text textUI, string texToDis, bool addString, float rythme)//le composant UI text et le string à afficher en plus se celui éventuellement déja affiché
    {//écrit à la volée lettre aprés lettre
        string[] characters = new string[texToDis.Length];//crée un tableau de chaque lettre du string
        string firstString = "";//empty declaration
        if (addString)
        {
            firstString = textUI.text;
        }
        for (int i = 0; i < texToDis.Length; i++)
        {
            characters[i] = texToDis[i].ToString();
        }
        float timer = Time.time;
        int increment = 0;
        textUI.text = firstString;
        while (increment < texToDis.Length)
        {
            if (Time.time > timer + rythme)
            {
                textUI.text += characters[increment];
                increment += 1;
                timer = Time.time;
            }
            yield return null;
        }
    }

    public IEnumerator InfoThenLoadScene()
    {
        sourisGo = false;
        StartCoroutine(WriteOn(chargement.GetComponent<Text>(), "Chargement....", false, 0.02f));
        infosLegales.GetComponent<Animator>().SetBool("onOFf", false);
        generique.GetComponent<Animator>().SetBool("onOff", false);
        sourisGo = false;
        if (!loading)
        {
            loading = true;
            GameObject.Find("Jouer").GetComponent<Animator>().SetBool("onOff", false);
            Animator anim = jouerAir.GetComponent<Animator>();
            anim.SetBool("onOff", true);
            yield return new WaitForSeconds(3f);
            SceneManager.LoadScene(1);
        }

    }

    public void InfosLegales()
    {
        instruction.GetComponent<Text>().text = emptyText.text;
        bool newBool = !infosLegales.GetComponent<Animator>().GetBool("onOff");//permet de passer de true à false et inversement
        sourisGo = false;
        infosLegales.GetComponent<Animator>().SetBool("onOff", newBool);
        generique.GetComponent<Animator>().SetBool("onOff", newBool);
        sourisRoot.GetComponent<Animator>().SetBool("onOff", !newBool);
    }

    private void Start()
    {
        idealVolume = 1f;
        sourisGo = false;
        loading = false;
        foreach (Transform thisGo in barreEspaceJaune.transform)
        {
            barreEspaceList.Add(thisGo.gameObject);
        }
        foreach (Transform thisGo in sourisRoot.transform)
        {
            sourisRootList.Add(thisGo.gameObject);
            thisGo.gameObject.SetActive(false);
        }
        barreEspaceJaune.SetActive(false);
        sliderVolume.GetComponent<Slider>().value = 0.8f;
        StartCoroutine(InstructionGo());
    }
}
