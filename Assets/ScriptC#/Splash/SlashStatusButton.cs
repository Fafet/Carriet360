﻿
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class SlashStatusButton : MonoBehaviour, IPointerDownHandler, IPointerExitHandler, IPointerEnterHandler, IPointerUpHandler
{

    //script placé sur des sprites dont on veut récupérer les différents états lors du passage de la sourie dessus (Over, exit, click)

    private Splash splash;

    private void Awake()
    {
        splash = GameObject.Find("SystemNavig").GetComponent<Splash>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {///on recupère l'event system pour avoir la main lors du click.//event déclenché par un bouton UI
        if (this.gameObject.name == "Air")
        {
            splash.PolzaGo(false);
            StartCoroutine(splash.InfoThenLoadScene());
        }
        if (this.gameObject.name == "Txt")
        {
            splash.PolzaGo(false);
            StartCoroutine(splash.SouriGo());
        }
        if (this.gameObject.name == "Exit")//exit
        {
            splash.PolzaGo(false);
            Application.Quit();
        }
        if (this.gameObject.name == "Echap")//son
        {
            splash.PolzaGo(true);
        }
        if (this.gameObject.name == "Jouer")//son
        {
            splash.PolzaGo(false);
            StartCoroutine(splash.InfoThenLoadScene());
        }
        if (this.gameObject.name == "InfosProjet")//info sur le projet
        {
            splash.PolzaGo(false);
            splash.InfosLegales();
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {//idem pour le mouseOver//event déclenché par un bouton UI

    }

    public void OnPointerEnter(PointerEventData eventData)
    {//event déclenché par un bouton UI
        Vector3 newScale = new Vector3(1f, 1f, 1f);
        if (this.gameObject.name == "Air")
        {
            splash.PolzaGo(false);
            foreach (Transform go in this.transform)
            {

                go.gameObject.GetComponent<RectTransform>().localScale = newScale;
            }
        }
        if (this.gameObject.name == "Txt")
        {
            splash.PolzaGo(false);
            foreach (Transform go in this.transform)
            {

                go.gameObject.GetComponent<RectTransform>().localScale = newScale;
            }
        }
        if (this.gameObject.name == "Exit")
        {
            splash.PolzaGo(false);
            foreach (Transform go in this.transform)
            {

                go.gameObject.GetComponent<RectTransform>().localScale = newScale;
            }
        }
        if (this.gameObject.name == "Echap")//son
        {
            foreach (Transform go in this.transform)
            {
                go.gameObject.GetComponent<RectTransform>().localScale = newScale;
            }
        }
        if (this.gameObject.name == "Jouer")//son
        {
            splash.PolzaGo(false);
            foreach (Transform go in this.transform)
            {
                go.gameObject.GetComponent<RectTransform>().localScale = newScale;
            }
        }
        if (this.gameObject.name == "InfosProjet")//info sur le projet
        {
            splash.PolzaGo(false);
            foreach (Transform go in this.transform)
            {
                Debug.Log(go.gameObject.name);
                go.gameObject.GetComponent<RectTransform>().localScale = newScale;
            }
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {//event déclenché par un bouton UI  
        if (this.gameObject.name == "SliderVolume")//son
        {
            splash.PolzaVolume();
        }
    }
}
