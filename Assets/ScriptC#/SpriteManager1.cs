﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpriteManager1 : MonoBehaviour {
    //sprite manager sur ObjetSprite
    private AudioManager audioManager;
    public List<Sprite> quartierList;
    private List<Sprite> listQuartierDone;
    public List<TextAsset> textList;//liste des textes à afficher
    private List<TextAsset> listTextDone;
    public List<MovieTexture> movieMat;
    public MovieTexture movieMatSanaa;
    private int movieIncrement;
    private GameObject prefab;//généric
    public GameObject quartierPrefab;//sprites de quartier
    public GameObject prompteurPrefab;//prompteur
    private GameObject uiPrompteur;
    private GUIPrompteur uiGUIPrompteur;
    private List<GameObject> actifPrefab;//tout les préfab instancié quelque soit leur type sont stockés ici. 5 maxi
    private Animator anim;
    private GameObject player;
    private GameObject murVideo;//le mur qui explose où sont diffusés les vidéos
    private GameObject murCamion;//le mur qui explose où sont diffusés les vidéos
    private bool videoIsRunning;


    private void PlayerListener(GameObject e)
    {
        player = e;
    }

    public void SetIntOn(string prefabName)
    {
        prefab = GetGoodPrefab(prefabName);//met le bon préfab dans l'objet généric préfab
        if (actifPrefab.Count != 0)
        {
            if (actifPrefab.Count > 5)//si on a déja créé plus de 5 sprites
            {
                for (int i = 0; i < actifPrefab.Count - 4; i++)//on detruit les premiers sprites créés pour qu'il n'en reste que 5
                {
                    GameObject thisSprite = actifPrefab[i];
                    actifPrefab.Remove(actifPrefab[i]);//on l'enlève du tableau avant de la détruire
                    StartCoroutine(DestroyForce(thisSprite));//detruit les int récement créés
                }
            }
        }
        SetUpSpriteQuartier();
    }

    private GameObject GetGoodPrefab(string attribut)
    {
        GameObject prefabConcern = new GameObject();
        switch (attribut)
        {
            case "quartier": prefabConcern = quartierPrefab; return prefabConcern;
            case "prompteur": prefabConcern = prompteurPrefab; return prefabConcern;
        }
        return prefabConcern;

    }

    private void SetUpSpriteQuartier()//crée 1 raycast devant le player pour définir si il faut créer des sprites
    {
        Vector3 offsetHauteur = new Vector3(0f, 15f, 0f);
        Vector3 rayDirect = -player.transform.up;
        Vector3 playerPos = player.transform.position + offsetHauteur + (player.transform.forward * 10f);//tir le rayon devant le joueur avec décalage vers le haut pour être sur de toucher le sol
        Ray newRay = new Ray(playerPos, rayDirect);//
        LayerMask colliderMask = 1 << 8;
        RaycastHit hit;

        Physics.Raycast(newRay, out hit, 100f, colliderMask);
        if (hit.distance > 0f)
        {
            Vector3 creationPoint = hit.point + new Vector3(0f,1.5f,0f);//décalage leger vers le haut
            CheckToCreateInstance(creationPoint);
        }
    }

    private void CheckToCreateInstance(Vector3 spritePosition)
    {
        float treeDistance = 100f;
        if (actifPrefab.Count > 0)//si il existe déja un arbre créé
        {
            for (int i = 0; i < actifPrefab.Count - 1; i++)//on compare la position de ce nouvel arbre que l'on souhaite créé par rapport à tout ceux existants
            {
                float tempDistance = Mathf.Abs(Vector3.Distance(actifPrefab[i].transform.position, spritePosition));
                if (tempDistance < treeDistance)// pour chaque distance plus petite on la stock
                {
                    treeDistance = tempDistance;
                }
            }
        }
        if (treeDistance > 5f)//si la distance entre la plus petite distance de tout les arbres créés est suffisante on crée enfin l'arbre
        {
            GameObject newTree = RandomInt(spritePosition);//prends un prefab d'arbre au hasard
        }
    }

    private IEnumerator DestroyForce(GameObject toBedestroy)
    {
        float val = 1f;
        toBedestroy.GetComponent<Animator>().SetBool("off", true);//reset animation
        while (val > 2f)
        {
            val -= Time.deltaTime;
            if (toBedestroy != null)
            {
                yield return null;
            }
        }
        Destroy(toBedestroy);
    }

    private GameObject RandomInt(Vector3 concernPos)
    {
        GameObject objConcern = (GameObject)Instantiate(prefab);

        if (prefab == quartierPrefab)
        {
            objConcern.transform.SetParent(this.gameObject.transform);//parent de ce GO
            List<Sprite> notDone = new List<Sprite>();// crée une liste qui contiendra les sprites qui n'ont pas encore été montrés 
            if(listQuartierDone.Count == quartierList.Count)//si on a déja tout montré on repart de zero
            {
                listQuartierDone.Clear();
            }
            foreach(Sprite sprit in quartierList)//regarde dans la liste générale
            {
                if (listQuartierDone.Count == 0)
                {
                    notDone.Add(sprit);//on l'ajoute à la liste des éléments pas encore montrés
                }
                else
                {
                    if (!listQuartierDone.Contains(sprit))//si l'élément de la liste générale n'est pas présent dans la liste des sprites déja montrés
                    {
                        notDone.Add(sprit);//on l'ajoute à la liste des éléments pas encore montrés
                    }
                }
            }
            int rand = Random.Range(0, notDone.Count);//prends un chiffre au hasard dans cette liste
            objConcern.GetComponent<SpriteRenderer>().sprite = notDone[rand];//et effecte un sprite qui n'a pas encore été montré
            listQuartierDone.Add(notDone[rand]);//on l'ajoute à la liste des sprites déja montrés
            actifPrefab.Add(objConcern);//ajoute à la liste générale qui suit l'évolution du nombre total de préfab
        }
        if (prefab == prompteurPrefab)
        {
            uiGUIPrompteur.enabled = false;
            GameObject perspectiveCanvas = GameObject.Find("CanvasPerspective");
            objConcern.transform.SetParent(perspectiveCanvas.gameObject.transform);//parent de ce GO
            List<TextAsset> notDone = new List<TextAsset>();// crée une liste qui contiendra les sprites qui n'ont pas encore été montrés 
            if (listTextDone.Count == textList.Count)//si on a déja tout montré on repart de zero
            {
                listTextDone.Clear();
            }
            foreach (TextAsset sprit in textList)//regarde dans la liste générale
            {
                if (listTextDone.Count == 0)
                {
                    notDone.Add(sprit);//on l'ajoute à la liste des éléments pas encore montrés
                }
                else
                {
                    if (!listTextDone.Contains(sprit))//si l'élément de la liste générale n'est pas présent dans la liste des sprites déja montrés
                    {
                        notDone.Add(sprit);//on l'ajoute à la liste des éléments pas encore montrés
                    }
                }
            }
            int rand = Random.Range(0, notDone.Count);
            GUIPrompteur guiPromteur = objConcern.GetComponent<GUIPrompteur>();
            guiPromteur.PrompteurDatas(notDone[rand]);//permet de convertir en string le texte et l'affecte par la même occasion au prompteur
            listTextDone.Add(notDone[rand]);
            actifPrefab.Add(objConcern);
            guiPromteur.TexteToDisplay();//lance la fonction d'affichage du prompteur
            uiGUIPrompteur.enabled = true;
            uiGUIPrompteur.texteToDisplay = guiPromteur.texteToDisplay;//sync entre affichage écran et affichage Perspective
            uiGUIPrompteur.TexteToDisplay();//lance la fonction d'affichage du prompteur
        }
        objConcern.transform.position = concernPos;//positionne le sprite à l'endroit du raycast
        return objConcern;
    }

    public IEnumerator RunVideo()//gestion su flux video et audio du mursVidéo (rap)
    {
        if (!videoIsRunning)//si on ne joue plus un rap
        {
            audioManager.videoGo = true;
            Animation animation = murVideo.GetComponent<Animation>();
            foreach (Transform movieCube in murVideo.transform)//récupére les cubes du mur pour leur affecter une de vidéo en fonction de l'increment (dans l'ordre de la liste)
            {
                movieCube.GetComponent<MeshRenderer>().material.mainTexture = movieMat[movieIncrement];//affecte la vidéo au mat du murVidéo
                ((MovieTexture)movieCube.GetComponent<MeshRenderer>().material.mainTexture).Play();//lance a lecture
            }
            if (movieIncrement == 0)//on lance l'animation d'explosion de l'écran pour le premier clip seulement
            {
                animation["MurVideoExplode"].speed = -1;//animation à l'envers pour que le mur ce reconstitue
                animation["MurVideoExplode"].time = animation["MurVideoExplode"].length;
                animation.Play("MurVideoExplode");
            }
            videoIsRunning = true;
            audioManager.AddAudio("video", false, false, 0f, 0);//ajoute le bon son
            while (videoIsRunning)
            {
                videoIsRunning = movieMat[movieIncrement].isPlaying;
                yield return null;
            }
            movieMat[movieIncrement].Stop();//il faut arrêter la vidéo si on veut avoir une chance de la remettre en route plus tard
            movieIncrement += 1;
            videoIsRunning = false;
            audioManager.videoGo = false;
            if (movieIncrement < movieMat.Count && !videoIsRunning)//tant qu'on à pas parcouru toute le liste de lecture
            {
                StartCoroutine(RunVideo());//relance le processus de lecture
            }
            else//lance l'animation de destruction du mur
            {
                animation["MurVideoExplode"].speed = 1;//lecture de l'animation en sens inverse
                animation["MurVideoExplode"].time = 0f;
                animation.Play("MurVideoExplode");
                movieIncrement = 0;//on a tout lu on repasse au premier clip
                audioManager.videoGo = false;
            }
        }
    }

    public void OpenPorte(GameObject camion)
    {
        Animator anim = camion.GetComponent<Animator>();
        anim.SetBool("open", true);//ouvre la porte du camion
    }

    public IEnumerator RunVideoSanaa(GameObject thisCamion)//gestion su flux video et audio du mursVidéo (rap)
    {

        if (!videoIsRunning)//si on ne joue plus un rap
        {
            audioManager.videoGo = true;
            //thisCamion.GetComponent<MeshRenderer>().materials[0].mainTexture = movieMatSanaa;//affecte la vidéo au mat du murVidéo
            ((MovieTexture)thisCamion.GetComponent<MeshRenderer>().materials[0].mainTexture).Play();//lance a lecture
        }
        videoIsRunning = true;
        audioManager.AddAudio("beatBox", false, false, 0f, 0);//ajoute le bon son
        while (videoIsRunning)
        {
            videoIsRunning = movieMatSanaa.isPlaying;
            yield return null;
        }
        movieMatSanaa.Stop();//il faut arrêter la vidéo si on veut avoir une chance de la remettre en route plus tard
        videoIsRunning = false;
        audioManager.videoGo = false;
    }

    public void CloseCamion(GameObject camion)
    {
        camion.GetComponent<Animator>().SetBool("open", false);
    }

    private void Start()
    {
        uiPrompteur = GameObject.Find("UIPrompteur");
        uiGUIPrompteur = uiPrompteur.GetComponent<GUIPrompteur>();
        audioManager = GameObject.Find("AudioContainer").GetComponent<AudioManager>();
        listTextDone = new List<TextAsset>();
        listQuartierDone = new List<Sprite>();
    }

    private void OnEnable()
    {
        GameManager.NewPlayer += PlayerListener;
        actifPrefab = new List<GameObject>();
        movieIncrement = 0;
        murVideo = GameObject.Find("MurVideo");
    }

    private void OnDesable()
    {
        GameManager.NewPlayer -= PlayerListener;
    }
}
