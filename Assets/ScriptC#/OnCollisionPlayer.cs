using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


public class OnCollisionPlayer : MonoBehaviour {	
	
	//script de character controler perso
	private GestionnaireSons gestionnaireSons;
	private PlayerControler playerControler;
	private AudioSourceEnvironnements audioSourceEnvironnements;
    private VehiculeManager vehiculeManager;
    private ZoneManager zoneManager;
	private CameraMove cameraMove;
	private ParticuleManager particuleManager;
	private AudioGUI audioGUI;//reference au script AudioGUI
    private AudioManager audioManager;
    private AvatarSetUp avatarSetUp;//racine des avatar
    private SpriteManager1 spriteManager;
	private Vector3 contact;//le point de contact du collider lors de la derniÃ¨re collision 
    private GameObject player;
    private GameObject focus;
	private GameObject oiseaux;//le parent de tout les oiseaux
	private GameObject systemNavig;
	private float timer6;
	private int increment;
	private int layerMask;//le calque Collider
	private bool particuleIsRunning;//devient vrai lorsqu'on génére des particules alphaber suite à la collision du joeur avec son environnement
    private bool jumping;//est-on en train de sauter ???
    private Animator playerAnimator;
	
    private void PlayerListener(GameObject e)
    {
        player = e;
    }

    private void Listener(string e)//reçoit de l'éventSystem le string qui définit l'état du moteur. Permet de choisir la bonne méthode pour le delegate boite à vitesse
    {
        if (e == "Jump")
        {
            jumping = true;
        }
        else jumping = false;
    }

    private void  OnEnable (){
        GameManager.NewPlayer += PlayerListener;
        EventManagerPlayer.NewGear += Listener;//souscrit a l'évent system qui permet de déterminer grace à un string dans quel état est le moteur du vehicule
        zoneManager = GameObject.Find("ObjetsScene").GetComponent<ZoneManager>();
		gestionnaireSons = GameObject.Find("AudioContainer").GetComponent<GestionnaireSons>();
		playerControler = GameObject.Find("GameControl").GetComponent<PlayerControler>();
		oiseaux = GameObject.Find("Oiseaux");
		avatarSetUp = GameObject.Find("Avatars").GetComponent<AvatarSetUp>();
		audioGUI = GameObject.Find("AudioContainer").GetComponent<AudioGUI>();
		audioSourceEnvironnements = GameObject.Find("AudioContainerEnvir").GetComponent<AudioSourceEnvironnements>();
		particuleManager = GameManager.gameControl.GetComponent<ParticuleManager>();
		systemNavig = GameObject.Find("SystemNavig");
		cameraMove = GameManager.instance.cameraFPS.GetComponent<CameraMove> ();
        vehiculeManager = GameObject.Find("VehiculesRoot").GetComponent<VehiculeManager>();
        audioManager = GameObject.Find("AudioContainer").GetComponent<AudioManager>();
        spriteManager = GameObject.Find("ObjetSprite").GetComponent<SpriteManager1>();
        player = GameManager.instance.player;
        playerAnimator = player.GetComponent<Animator>();
		increment = 0;
		layerMask = 8;//collider
	}
	
    private void OnDesable()
    {
        GameManager.NewPlayer -= PlayerListener;
        EventManagerPlayer.NewGear -= Listener;//Desouscrit a l'évent system qui permet de déterminer grace à un string dans quel état est le moteur du vehicule
    }

	private IEnumerator  WaitAndNotCollide ( GameObject goColliderOff ,   float seconds  ){
        float timer = Time.time;
		yield return new WaitForSeconds(seconds);
		if(Time.time> timer+3f)//vÃ©rifie le moment ou le player est entrÃ© dans le collider pour s'assurer qu'il n'est pas coincÃ©
        {
			WaitAndCollideAgain(goColliderOff,3);	//si il est coincÃ© alors on lance la fonction qui dÃ©active le collider en question pour libÃ©rer le player	
		}
	}
	
	public IEnumerator  WaitAndCollideAgain ( GameObject goColliderOn,   float seconds   ){//active et deÃ©ctive un collider
        if (goColliderOn != null)
        {
            goColliderOn.GetComponent<BoxCollider>().enabled = false;
            yield return new WaitForSeconds(seconds);
        }
	}

    public void PlayerCollide(Collision other)
    {
        if (!GameManager.instance.swapAvatar)
        {
            GameManager.PlugNewNavigation(false);///////Event Navigation == false
            avatarSetUp.MiseEnPlace(other.gameObject, null, null);
            audioManager.AddAudio("r2d2", false, false, 0f, 0);//permet de relancer la fonction pour ajuster le son
        }
        if (other.gameObject.layer == 11)//Avatar
        {
            ContactPoint contPoint = other.contacts[0];
            float distance = player.transform.position.y - contPoint.point.y;
            float magRigid = playerControler.velocity.magnitude;
            if (distance < 0.1f && distance != 0.0f)
            {
                GUISystemNavig nav = systemNavig.GetComponent<GUISystemNavig>();
                Debug.Log("PlayerCollide");
                nav.RunStatus2Process(0, false);// lance la fonction qui permet d'afficher des infos bulles du style "barre d'espace pour sauter"
            }
            if (magRigid > 5.0f)
            {//explosion de particules au dela d'une certaine vitesse seulement
                Vector3 playerPos = player.transform.position;
                if (!particuleIsRunning)
                {
                    particuleManager.CollisionParticle(playerPos, 2, 1.0f);
                    StartCoroutine(ParticuleIsRunningStop(1.0f));//remet partuculeIsRunning sur false
                }
            }
        }
    }

    private void OnCollisionEnter(Collision other)//surveilles les collisions du bip afin de :
    {
        if (other.gameObject.tag == "Player")
        {
        }
        else playerAnimator.SetBool("isGrounded", true);
        if (other.gameObject.tag == "Sculptris")
        {
            audioManager.AddAudio("r2d2", false, false, 0f, 0);
            StartCoroutine(cameraMove.EdgeDetection(false));//lance le contrast sur la camera
            audioManager.AddAudio("expression", false, false, 0f, 0);
        }
         if (other.gameObject.tag == "Arbre")
        {
            audioManager.AddAudio("conversation", false, false, 0f, 0);
            StartCoroutine(cameraMove.NoiseAndGrainGo());//lance le contrast sur la camera
        }
    }

    private void OnCollisionStay(Collision otherStay)
    {//surveilles les collisions du bip afin de :
        if (otherStay.gameObject.layer == layerMask)
        {
            GameManager.instance.isGrounded = true;
            playerAnimator.SetBool("isGrounded", true);
        }
    }

    private void OnCollisionExit(Collision otherExit)
    {//surveilles les collisions du bip afin de :
        if (otherExit.gameObject.layer == layerMask)
        {
            GameManager.instance.isGrounded = false;
        }
    }

    private void OnTriggerEnter(Collider playerEnter)
    {//vÃ©rifie le nom du game object qui entre en collision
        if (playerEnter.gameObject.layer == 12 && playerEnter.gameObject.tag == "Oiseau")//Oiseau + layer = Vehicule
        {
            if (!EventManagerPlayer.engineStateBool[10] && !vehiculeManager.delay)//si on est pas déja en mode Bird et que le temps de latence est écoulé
            {
                vehiculeManager.indexVehicule = 4;//on conserve le rover
                GameManager.instance.destination = GameObject.Find("OiseauxRoot").GetComponent<OiseauxManager>().birdRide.bird;//il faut se rendre à cet oiseau
                EventManagerPlayer.BooleanSetUp("Bird");//passe en mode en Voiture et déclenche l'Event///EVENT IN
                Vector3 posParticule = vehiculeManager.vehiculeList[vehiculeManager.indexVehicule].transform.position + vehiculeManager.ConvertOffsetPlayerPos(vehiculeManager.indexVehicule, vehiculeManager.offsetDestinationPos[vehiculeManager.indexVehicule]);
                vehiculeManager.particuleManager.ParticuleOn(posParticule, 9, 2f);//dustContourRouge
            }
        }
        if (playerEnter.gameObject.tag == "WorldLimits")
        {//si on touche le fond du monde
            Debug.Log("world limits..............");
            GameManager.instance.worldLimit = true;
        }
        if (playerEnter.gameObject.tag == "Maison")
        {//si on touche le fond du monde
            StartCoroutine(cameraMove.Vignetting(true));//lance le contrast sur la camera
            GameManager.PlugNewInt(playerEnter.gameObject);//déclenche event et transmet le gabarit à HouseKeeper
            audioManager.AddAudio("conversation", false, false, 0f, 0);
        }
        if (playerEnter.gameObject.tag == "BabyFoot")
        {//si on touche le fond du monde
           AudioSource aud = playerEnter.gameObject.GetComponent<AudioSource>();
            aud.time = 0f;
            aud.Play();
        }
        if (playerEnter.gameObject.tag == "ReverbZone")//Si c'est une maison
        {
            audioManager.AddAudio("conversation", false, false, 0f, 0);//permet de relancer la fonction pour ajuster le son
        }
        if (playerEnter.gameObject.tag == "Camion")//Si c'est un camion en lance le reportage de Sanaa
        {
            spriteManager.OpenPorte(playerEnter.gameObject);
            audioManager.AddAudio("conversation", false, false, 0f, 0);//permet de relancer la fonction pour ajuster le son
            audioManager.AddAudio("fx", false, false, 0f, 0);//permet de relancer la fonction pour ajuster le son
        }
        if (playerEnter.gameObject.tag == "Fantome")//Si c'est un camion en lance le reportage de Sanaa
        {
            audioManager.videoGo = true;//monte le volume
            StartCoroutine(spriteManager.RunVideoSanaa(playerEnter.gameObject));
        }
        if (playerEnter.gameObject.layer == 8 && playerEnter.gameObject.tag == "Wagon")//Si c'est un wagon et un collider layer
        {
            vehiculeManager.OpenRoof(playerEnter.gameObject,true);//ouvre le toit des wagon
        }
        if (playerEnter.gameObject.layer == 8 && playerEnter.gameObject.tag == "Video")//Si c'est le collider du murVideo
        {
            audioManager.videoGo = true;//monte le volume
            StartCoroutine(spriteManager.RunVideo());
            audioManager.AddAudio("fx", false, false, 0f, 0);//permet de relancer la fonction pour ajuster le son
        }
        if (playerEnter.gameObject.layer == 8 && playerEnter.gameObject.tag == "Lion")//Si c'est le collider du murVideo
        {
            playerEnter.gameObject.GetComponent<Animator>().SetBool("onOff", true);
            audioManager.AddAudio("rap", false, false, 0f, 0);//permet de relancer la fonction pour ajuster le son
        }
        if (jumping && playerEnter.gameObject.layer == 12)//VehiculeTrigger
        {
            if (playerEnter.gameObject.tag == "Train")//VehiculeTrigger
            {
                if (!EventManagerPlayer.engineStateBool[8] && !GameObject.Find("VehiculesRoot").GetComponent<VehiculeManager>().delay)//si on est pas déja en mode Train et que le temps de latence est écoulé
                {
                    vehiculeManager.indexVehicule = 3;
                    GameManager.instance.destination = vehiculeManager.vehiculeList[3];//Train
                    EventManagerPlayer.BooleanSetUp("Train");//passe en mode en Voiture et déclenche l'Event///EVENT IN
                }
            }
            if (playerEnter.gameObject.tag == "Voiture")//Vehicule mais pas le train
            {
                if (!EventManagerPlayer.engineStateBool[7] && !vehiculeManager.delay)//si on est pas déja en mode EnVoiture et que le temps de latence est écoulé
                {
                    GameManager.instance.destination = playerEnter.gameObject;
                    for(int i = 0; i < vehiculeManager.vehiculeCarosserie.Count; i++)
                    {
                        if(playerEnter.gameObject == vehiculeManager.vehiculeCarosserie[i])
                        {
                            vehiculeManager.indexVehicule = i;
                        }
                    }                   
                    EventManagerPlayer.BooleanSetUp("EnVoiture");//passe en mode en Voiture et déclenche l'Event///EVENT IN
                }
            }
        }      
    }

    private void OnTriggerExit(Collider playerEnter2)//si on sort de la zone de collision
    {
        if (playerEnter2.gameObject.tag == "Camion")//Si c'est un camion en lance le reportage de Sanaa
        {
            spriteManager.CloseCamion(playerEnter2.gameObject);//monte le volume
            audioManager.AddAudio("fx", false, false, 0f, 0);//permet de relancer la fonction pour ajuster le son
        }
        if (playerEnter2.gameObject.layer == 8 && playerEnter2.gameObject.tag == "Video")//Si c'est le collider du murVideo
        {
            audioManager.videoGo = false;//baisse le volume
            audioManager.AddAudio("conversation", false, false, 0f, 0);
        }
        if (playerEnter2.gameObject.tag == "Door")//ouverture classique de porte
        {
            GameObject porte = (GameObject)playerEnter2.gameObject;
            StartCoroutine(zoneManager.OpenPorte(porte));
        }
        if (playerEnter2.gameObject.tag == "ReverbZone")
        {
            audioManager.AddAudio("conversation", false, false, 0f, 0);
            spriteManager.SetIntOn("prompteur");//affiche un prompteur
        }
        if (playerEnter2.gameObject.tag == "Maison")//Si c'est une maison
        {
            cameraMove.vignetting = false;
            GameManager.PlugNewInt(null);//déclenche event et transmet le gabarit à HouseKeeper
            spriteManager.SetIntOn("quartier");//affiche les spritesQuartier
        }
        if (playerEnter2.gameObject.tag == "Wagon")//Si c'est un wagon et un collider layer
        {
            vehiculeManager.OpenRoof(playerEnter2.gameObject,false);//ouvre le toit des wagon
        }
        if (playerEnter2.gameObject.layer == 8 && playerEnter2.gameObject.tag == "Lion")//Si c'est le collider du murVideo
        {
            playerEnter2.gameObject.GetComponent<Animator>().SetBool("onOff", false);
        }
    }

    private IEnumerator ParticuleIsRunningStop( float seconds){
		particuleIsRunning = true;
		yield return new WaitForSeconds (seconds);
		particuleIsRunning = false;
	}
	
	private IEnumerator  ResetWoldLimit (){
		yield return new WaitForSeconds (3);	
		GameManager.instance.worldLimit = false;
	}

	private void OnDestroy(){
		StopAllCoroutines();
	}

	private void  Update (){
		if(GameManager.instance.worldLimit){
			//transform.position = Vector3.Lerp(transform.position, GameManager.instance.posWorldLimits , Time.fixedDeltaTime *15);
			ResetWoldLimit();	
		}
	}
}