﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VehiculeManager : MonoBehaviour
{
    ///scrip attaché au VehiculeRoot qui gère les paramétres d'un véhicule
    public List<GameObject> vehiculeList;//quel véhicule: rover, twingo, peugeot, train ?
    public List<GameObject> vehiculeCarosserie;// meshRender qui s'adapte aux 
    public GameObject destinationTarget;//objet empty qui permet de déplacer le centre du vehicule pour indiquer où positionner le player correctement en fonction du véhicule
    public List<Vector3> offsetDestinationPos;//offset de position pour le train nottement
    public Color col;
    private GameObject oldVehicule;//le vehicule avant que le changement de vehivule n'intervienne
    private PlayerControler playerControler;
    public ParticuleManager particuleManager;
    public TrainController trainController;//le script TrainControler sur le GO Train
    public BezierSpline spline;//les infos de la spline du train
    public GameObject player;
    private GameObject collidingHex;//la cellule en contact
    public List<WheelCollider> axleInfos;//infos de la class AxleInfo defini à la fin du script qui comprends les infos de chaque roue
    public List<GameObject> rouesList;//les roues avec le meshRenderer du véhicule
    private List<GameObject> pareChocList;//liste des éléments de pareChoc
    public float maxMotorTorque; // maximum torque the motor can apply to wheel
    private float maxSteeringAngle; // maximum steer angle the wheel can have
    private float motor;//la vitesse du moteur (dépend de la position y de la sourie à l'écran
    public float vehiculeSpeed;//la vitesse de déplacement du véhicule
    public Vector3 birdCenter;//centre le joueur sur l'oiseau
    private float delatSpeed;//vitesse idéale / différence entre la vitesse du moteur(position de la sourie) et la vitesse réélle du véhicule
    private float forceBreak;//force de freinage
    private float maxiBreakForce;//force de freinage maxi
    public float steering;//angle de braquage des roues
    public float maxiSpeed;
    public float vehiculeMultiplicateur;
    private float speedBird;//la vitesse en fonction de la position de la souris à l'écran
    public Vector3 localVelocityRB;//la vistesse locale de déplacement du RB
    private Vector3 directionRB;//la direction de déplacement du RB du joueur
    public Rigidbody rigidbodyVehicule;//le RB du véhicule
    private MathildeControler mathildeControler;//script qui gére le state machine sur Mathilde
    private OiseauxManager oiseauxManager;
    private AudioManager audioManager;
    public Animator playerAnim;//animator controler du player

    public int indexVehicule;//quel véhicule ? 0 Rover, 1 Twingo

    public bool motorW; // is this wheel attached to motor?
    private bool coroutineIsRunnig;
    private bool navigating;
    private bool resetVehiculeRotation;
    public bool delay;//permet d'attendre un peu avant de redéclencher un event de type EnVoiture
    private bool birdPower;
    //variables pour le mode InWagon

    public List<Material> skidMarkMatList;
    public int indexMat;

    private delegate void BoiteAVitesse();//surveille la position y de la sourie et enclenche soit la marche avant, le point mort ou la marche arrière
    private BoiteAVitesse boiteAVitesse;
    private object vehic;

    public void Awake()
    {
        playerControler = GameManager.gameControl.GetComponent<PlayerControler>();
        particuleManager = GameManager.gameControl.GetComponent<ParticuleManager>();
        oiseauxManager = GameObject.Find("OiseauxRoot").GetComponent<OiseauxManager>();
        audioManager = GameObject.Find("AudioContainer").GetComponent<AudioManager>();
    }

    private void PlayerListener(GameObject e)
    {
        player = e;
    }

    public void NavigatingListener(bool e)
    {
        navigating = true;
    }

    private void Listener(string e)//reçoit de l'éventSystem le string qui définit l'état du moteur. Permet de choisir la bonne méthode pour le delegate boite à vitesse
    {
        if (e == "Neutral")
        {
            RBNormal();
            boiteAVitesse = Neutral;
            rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB
        }
        if (e == "MarcheAvant")
        {
            boiteAVitesse = MarcheAvant;
            RBNormal();
            rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB
            maxiSpeed = 150f * vehiculeMultiplicateur;
        }
        if (e == "MarcheArriere")
        {
            boiteAVitesse = MarcheArriere;
            RBNormal();
            rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB
            maxiSpeed = 5f * vehiculeMultiplicateur;
        }
        if (e == "Jump")
        {
            audioManager.AddAudio("fx", false, false, 0f, 0);
            EventManagerPlayer.engineStateBool[10] = false;//passe le mode Bird sur faux
            EventManagerPlayer.engineStateBool[8] = false;//passe le mode Wagon sur faux           
            indexVehicule = 0;
            RBSwap(true);
            ChangeVehicule();//pour chaque saut on quitte le vehicule actuel pour revenir au rover
            rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB
            boiteAVitesse = Jump;
        }
        if (e == "Transit")
        {
            audioManager.AddAudio("rythme", false, false, 0f, 0);
            EventManagerPlayer.engineStateBool[10] = false;//passe le mode Bird sur faux
            EventManagerPlayer.engineStateBool[8] = false;//passe le mode Wagon sur faux           
            indexVehicule = 0;
            RBSwap(true);
            ChangeVehicule();//pour chaque saut on quitte le vehicule actuel pour revenir au rover
            rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB
            boiteAVitesse = Transit;
        }

        if (e == "Visite")
        {
            RBSwap(true);//defini les RB du vehicule et de Mathilde
            boiteAVitesse = Jump;//déplace le véhicule sous les pattes de Mathilde
        }
        if (e == "EnVoiture")
        {
            audioManager.AddAudio("rythme", false, false, 0f, 0);
            audioManager.AddAudio("r2d2", false, false, 0f, 0);
            StartCoroutine(Delay(3f));//bloque la rééxécution de l'event EnVoiture pendant quelques secondes
            RBSwap(true);
            ChangeVehicule();
            rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB
            boiteAVitesse = EnVoiture;
        }
        if (e == "Train")
        {
            audioManager.AddAudio("rythme", false, false, 0f, 0);
            StartCoroutine(Delay(3f));//bloque la rééxécution de l'event EnVoiture pendant quelques secondes           
            RBSwap(true);
            ChangeVehicule();
            rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB
            Vector3 posParticule = vehiculeList[indexVehicule].transform.position + ConvertOffsetPlayerPos(indexVehicule, offsetDestinationPos[indexVehicule]);
            particuleManager.ParticuleOn(posParticule, 7, 2f);//dustContourOrange         
            boiteAVitesse = Train;
        }

        if (e == "Wagon")
        {
            audioManager.AddAudio("fx", false, false, 0f, 0);
            StartCoroutine(Delay(3f));//bloque la rééxécution de l'event EnVoiture pendant quelques secondes
            RBZero();//deactive les deux RB (Rover et Player)
            ChangeVehicule();
            rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB
            Vector3 posParticule = vehiculeList[indexVehicule].transform.position + ConvertOffsetPlayerPos(indexVehicule, offsetDestinationPos[indexVehicule]);
            particuleManager.ParticuleOn(posParticule, 7, 2f);//dustContourOrange
            vehiculeList[5].transform.position = GameManager.instance.destination.transform.position;//met en place Wagon_Rover avant d'aligner le joueur sur lui          
            boiteAVitesse = Wagon;
        }

        if (e == "Bird")
        {
            audioManager.AddAudio("bird", false, false, 0f, 0);
            audioManager.AddAudio("rap", false, false, 0f, 0);
            StartCoroutine(Delay(3f));//bloque la rééxécution de l'event EnVoiture pendant quelques secondes
            RBZero();//deactive les deux RB (Rover et Player)
            ChangeVehicule();
            rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB
            Vector3 posParticule = vehiculeList[indexVehicule].transform.position + ConvertOffsetPlayerPos(indexVehicule, offsetDestinationPos[indexVehicule]);
            particuleManager.ParticuleOn(posParticule, 7, 2f);//dustContourOrange
            vehiculeList[4].transform.position = GameManager.instance.destination.transform.position;//met en place Bird_Rover avant d'aligner le joueur sur          
            boiteAVitesse = Bird;
        }
    }

    private void Neutral()//Point mort
    {
        motor = 0f;
        forceBreak += Time.fixedDeltaTime * 100f;
        RoverPower();
        CarosseriePosition();
    }

    private void MarcheAvant()
    {
        float IdealSpeed = maxMotorTorque * playerControler.speedMarche;//speedMarche est maxi égal à 1. Donc la cadence la plus rapide équivaut à maxMortorTorque
        delatSpeed = vehiculeSpeed - IdealSpeed;
        if (delatSpeed > 0f)//descend le régime du moteur
        {
            motor = 0f;
            forceBreak = maxMotorTorque * (MouseManager.yPoint - 0.3f);
        }
        if (delatSpeed < 0f)//accélére
        {
            motor = maxMotorTorque * (MouseManager.yPoint-0.3f);
            forceBreak = 0f;
        }
        RoverPower();
        CarosseriePosition();
    }

    private void MarcheArriere()
    {
        float IdealSpeed = maxMotorTorque * playerControler.speedMarche;//speedMarche est maxi égal à 1. Donc la cadence la plus rapide équivaut à maxMortorTorque
        delatSpeed = vehiculeSpeed - IdealSpeed;
        if (delatSpeed < 0f)//descend le régime du moteur
        {
            motor = 0f;
            forceBreak = Mathf.Lerp(motor, IdealSpeed, Time.fixedDeltaTime * Mathf.Abs(delatSpeed));
        }
        if (delatSpeed > 0f)//accélére
        {
            motor = Mathf.Lerp(motor, IdealSpeed, Time.fixedDeltaTime * Mathf.Abs(delatSpeed));
            forceBreak = 0;
        }
        RoverPower();//valeur absolue de deltaSpeed afin de faire les calculs dans la même fonction que pour la marche avant
        CarosseriePosition();
}

    private void Jump()
    {
        vehiculeList[0].transform.position = player.transform.position;//on fait suivre le rover
        vehiculeList[0].transform.rotation = player.transform.rotation;
        CarosseriePosition();
    }

    private void Transit()
    {
        vehiculeList[0].transform.position = player.transform.position;//on fait suivre le rover
        vehiculeList[0].transform.rotation = player.transform.rotation;
        CarosseriePosition();
    }

    public void Inventory()
    {
        motor = Mathf.Lerp(motor, 0f, Time.fixedDeltaTime * Mathf.Abs(delatSpeed));
        forceBreak = 200f;
                    RoverPower();//valeur absolue de deltaSpeed afin de faire les calculs dans la même fonction que pour la marche avant
        CarosseriePosition();
    }

    private void EnVoiture()//fonction qui dure le temps de mettre en place le player dans la voiture
    {
        if (playerControler.progression == 1)//tant que le player n'est pas en place dans la voiture
        {
            RBSwap(false);//deactive le rb de l'ancien vehicule
            oldVehicule = vehiculeList[indexVehicule];//met à jour la variable
            rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB
            Vector3 posParticule = vehiculeList[indexVehicule].transform.position + ConvertOffsetPlayerPos(indexVehicule, offsetDestinationPos[indexVehicule]);
            particuleManager.ParticuleOn(posParticule, 7, 2f);//dustContourOrange
            EventManagerPlayer.engineStateBool[7] = false;//on sort du mode EnVoiture ///EVENT OUT
        }
    }

    private void Train()//fonction qui dure le temps de mettre en place le player sur le train
    {
        if (playerControler.progression == 1f)//tant que le player n'est pas en place dans la loco
        {
            RBSwap(false);//deactive le rb de l'ancien vehicule
            oldVehicule = vehiculeList[indexVehicule];//met à jour la variable
            rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB
            particuleManager.ParticuleOn(vehiculeList[indexVehicule].transform.position, 7, 2f);//dustContourOrange
            EventManagerPlayer.engineStateBool[8] = false;//on sort du mode Train ///EVENT OUT
        }
    }

    private void Wagon()//fonction qui dure le temps de mettre en place le player sur le train
    {
        //genéral
        //RBZero();//deactive le rb de l'ancien vehicule
        oldVehicule = vehiculeList[indexVehicule];//met à jour la variable
        rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB
        vehiculeList[0].transform.position = player.transform.position;//on fait suivre le rover
        vehiculeList[0].transform.rotation = player.transform.rotation;
        CarosseriePosition();
        //particulier
        WagonPower();
    }

    private void Bird()//
    {
        if (playerControler.progression == 1f)//Le player est positionné par le script PlayerControler et la fonction SetUpMouve27. 
        {//donc le rover suis simplement le joueur
            //RBZero();//deactive le rb de l'ancien vehicule
            oldVehicule = vehiculeList[indexVehicule];//met à jour la variable
            rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB
            BirdPower();
        }
    }

    private void ChangeVehicule()
    {
        //RBSwap(true);//ne deactive pas encore le rb du OldVehicule afin de le faire reculer un peu mais active celui du player malgrés tout
        if(boiteAVitesse != MarcheArriere && boiteAVitesse != MarcheAvant && boiteAVitesse != Neutral && boiteAVitesse != Jump && boiteAVitesse != Transit)
        {
            Destroy(oldVehicule.GetComponent<VehiculeControler>());//detruit l'ancien composant afin d'un créé un nouveau pour le nouveau vehicule plus bas
        }
        if (indexVehicule != 0 && vehiculeList[0].activeSelf)//si on utilise plus le rover il faut le deactiver pour ne pas affoler le moteur phisyc
        {
            vehiculeList[0].SetActive(false);//deactive le rover
        }
        else
        {
            vehiculeList[0].SetActive(true);//active le rover
        }
        if (indexVehicule == 0)//rover
        {
            maxMotorTorque = 1000f;
            vehiculeMultiplicateur = 5f;
            indexMat = 0;//pas verts
        }
        if (indexVehicule == 1)//Twingo
        {
            maxMotorTorque = 300000f;
            vehiculeMultiplicateur = 100f;
            indexMat = 1;//pneus
        }
        if (indexVehicule == 2)//Peugeot
        {
            maxMotorTorque = 500000f;
            vehiculeMultiplicateur = 200f;
            indexMat = 1;//pneus
        }
        if (indexVehicule == 3)//Train
        {
            maxMotorTorque = 50000f; ;
            vehiculeMultiplicateur = 200f;
            indexMat = 1;//pneus
        }
        if (indexVehicule != 4)//si le rover n'est pas le wagon ou l'oiseau on ajoute le script VehiculeControler responsable de la détection des colliders sous les roues du rover
        {
            if (indexVehicule != 5)//si le rover n'est pas le wagon ou l'oiseau on ajoute le script VehiculeControler responsable de la détection des colliders sous les roues du rover
            {
                if(vehiculeList[indexVehicule].GetComponent<VehiculeControler>() == null)
                {
                    vehiculeList[indexVehicule].AddComponent<VehiculeControler>();//crée le nouveau composant sur le nouveau vehicule
                }
                axleInfos = new List<WheelCollider>();
                pareChocList.Clear();
                rouesList.Clear();
                foreach (Transform trans in vehiculeList[indexVehicule].transform)
                {
                    if (trans.gameObject.name.Contains("PareChoc"))
                    {
                        pareChocList.Add(trans.gameObject);
                    }
                }
                foreach (Transform child in vehiculeList[indexVehicule].transform)//va chercher les colliders de roues pour les mettre dans le tableau axelInfos
                {
                    if (child.gameObject.name == "Wheels")
                    {
                        foreach (Transform wheel in child)
                        {
                            axleInfos.Add(wheel.gameObject.GetComponent<WheelCollider>());
                        }
                    }
                }
                foreach (Transform roue in vehiculeCarosserie[indexVehicule].transform)
                {
                    if (roue.gameObject.name.Contains("Roue"))
                    {
                        rouesList.Add(roue.gameObject);
                    }
                }
            }
        }
    }

    public Vector3 ConvertOffsetPlayerPos(int indexVehi, Vector3 offset)//convertis au moment de PointCurve les offset afin qu'ils s'adaptent à la position dans l'espace du véhicule
    {
        Vector3 x = vehiculeList[indexVehi].transform.right * offset.x;
        Vector3 y = vehiculeList[indexVehi].transform.up * offset.y;
        Vector3 z = vehiculeList[indexVehi].transform.forward * offset.z;

        Vector3 newDirection = x + y + z;
        return newDirection;
    }

    private IEnumerator Delay(float seconds)
    {
        delay = true;
        yield return new WaitForSeconds(seconds);
        delay = false;

    }

    public void RBSwap(bool onOffVehiculeRB)
    {
        motorW = false;
        if (rigidbodyVehicule == null)
        {
            rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB

        }
        rigidbodyVehicule.isKinematic = onOffVehiculeRB;
        playerControler.playerRB = player.GetComponent<Rigidbody>();
        playerControler.playerRB.isKinematic = false;//active le RB de Mathilde
        GameManager.instance.cameraActive.GetComponent<CameraMove>().playerRB = playerControler.playerRB;//met à jour le RB pour la camera afin que le calcul de la velocity se fasse bien sur le bon RB actif(Mathilde ou Vehicule)
    }

    public void RBNormal()
    {
        player.GetComponent<Rigidbody>().isKinematic = true;//deactive le RB de Mathilde
        rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB
        playerControler.playerRB = rigidbodyVehicule;
        rigidbodyVehicule.centerOfMass = new Vector3(0f, -0.2f, 0f);
        rigidbodyVehicule.isKinematic = false;
        motorW = true;
        GameManager.instance.cameraActive.GetComponent<CameraMove>().playerRB = rigidbodyVehicule;
    }

    public void RBZero()
    {
        motorW = false;
        rigidbodyVehicule.isKinematic = true;
        playerControler.playerRB = player.GetComponent<Rigidbody>();
        playerControler.playerRB.isKinematic = true;//active le RB de Mathilde
        GameManager.instance.cameraActive.GetComponent<CameraMove>().playerRB = playerControler.playerRB;//met à jour le RB pour la camera afin que le calcul de la velocity se fasse bien sur le bon RB actif(Mathilde ou Vehicule)
    }

    private void CheckPareChoc()//Permet de savoir si le rover penche au dela de 90° toutes les 0.5 secondes afin de déactiver les parechocs
    {
        if(boiteAVitesse != Jump && boiteAVitesse != Transit)
        {
            float dotProduct = Vector3.Dot(vehiculeList[indexVehicule].transform.up, Vector3.down);
            if (dotProduct > -0.8f)
            {
                mathildeControler.Fall(true);
                if (!EventManagerPlayer.engineStateBool[4] && !EventManagerPlayer.engineStateBool[3] && !EventManagerPlayer.engineStateBool[6] && !EventManagerPlayer.engineStateBool[7] && !EventManagerPlayer.engineStateBool[8])//!transit
                {
                    if (!resetVehiculeRotation)
                    {
                        StartCoroutine(VehiculeResetRotation());//lance la fonction qui permet de redresser la voiture
                    }
                }
            }
            else mathildeControler.Fall(false);
        }
    }

    private IEnumerator VehiculeResetRotation()//fonction qui permet de faire sauter le vehicule en l'air tout en le remettant sur ces roues
    {
        resetVehiculeRotation = true;
        rigidbodyVehicule.AddForce(Vector3.up * 5f, ForceMode.VelocityChange);
        float oldTime = Time.time;
        while (Time.time < oldTime + 2f)
        {
            vehiculeList[indexVehicule].transform.rotation = Quaternion.Slerp(vehiculeList[indexVehicule].transform.rotation, Quaternion.identity, Time.deltaTime);
            yield return null;
        }
        resetVehiculeRotation = false;
    }

    private void CarosseriePosition()
    {

        for (int ii = 1; ii < vehiculeList.Count; ii++)
        {
            vehiculeCarosserie[ii].transform.position = vehiculeList[ii].transform.position;//aligne la carosserie sur le moteur
            vehiculeCarosserie[ii].transform.rotation = vehiculeList[ii].transform.rotation;//idem en rotation
        }
        if (indexVehicule > 0)
        {
            for (int i = 0; i < rouesList.Count; i++)
            {
                if (rouesList[i].name.Contains("Front"))
                {
                    rouesList[i].transform.rotation = vehiculeCarosserie[indexVehicule].transform.rotation * Quaternion.AngleAxis(steering, Vector3.up);
                }
                rouesList[i].transform.Rotate(Time.deltaTime + vehiculeSpeed * 100f, 0f, 0f);
            }
        }
    }

    public void OpenRoof(GameObject wagonConcern,bool openRoof)//ferme et ouvre le roof
    {
        wagonConcern.GetComponent<Animator>().SetBool("open", openRoof);
    }

    //Paramétrage du moteur et des roues et déplacement éffectif
    public void RoverPower()//le delta (inversé pour marche arrière) et le seuil de vitesse de croisière qui n'est pas le même pour la marche arrière que la marche avant
    {
        rigidbodyVehicule.mass = 600;// (2500f * rigidbodyNormSpeed) + 600f;//600 est le poids minimum du rover. Le poids maxi à pleine vitesse du rover est donc de 600 + 2500 soit 3100 kg
                                     //Rayon de braquage
        float vehiculeNormSpeed = 1f - (Mathf.Clamp(vehiculeSpeed, 0.001f, 20f) / 50f);//de 1 à 0.34
        float targetSteering = new float();
        if (motorW)
        {
            targetSteering = maxSteeringAngle * MouseManager.xMousePoint * vehiculeNormSpeed;//plus on va vite moins le rayon de braquage est grand
            steering = targetSteering;//adouci les coups de volant
        }
        if (!motorW)
        {
            motor = 0f;
            forceBreak = 50000f;
        }
        foreach (WheelCollider axleInfo in axleInfos)
        {
            //Braquage
            axleInfos[0].steerAngle = steering;
            axleInfos[1].steerAngle = steering;
            if (boiteAVitesse == Neutral)//si on est au point mort
            {
                axleInfos[2].steerAngle = -steering;//tourne les roue arrière en sens inverse
                axleInfos[3].steerAngle = -steering;
            }
            float steeringOff = axleInfos[3].steerAngle;//angle de braquage de la roue arrière
            if (vehiculeSpeed > 2f)
            {
                maxSteeringAngle = 60f;//reduit l'angle de braquage maxi
                steeringOff = Mathf.Lerp(steeringOff, 0f, Time.fixedDeltaTime * vehiculeSpeed);//remet progressivement les roue arrière dans l'axe du véhicule
            }
            else maxSteeringAngle = 90f;//augmente l'angle de braque
            axleInfos[2].steerAngle = steeringOff;
            axleInfos[3].steerAngle = steeringOff;
        }
        foreach (WheelCollider axel in axleInfos)
        {
            //Force du moteur
            if (axel == axleInfos[2] || axel == axleInfos[3])//si ce sont les roues arrières, propulsion
            {
                axel.motorTorque = motor;// * softMotorTorque;
            }
            //Force du freinage
            if (indexVehicule == 3)//si c'est le train
            {
                forceBreak = 3f * forceBreak;
            }
            axel.brakeTorque = 3f * forceBreak;
        }
    }

    private void WagonPower()
    {
        vehiculeList[5].transform.rotation = Quaternion.Slerp(vehiculeList[5].transform.rotation, GameManager.instance.destination.transform.rotation, Time.deltaTime * 2f);
        vehiculeList[5].transform.position = Vector3.Lerp(vehiculeList[5].transform.position, GameManager.instance.destination.transform.position+new Vector3(0f,0.25f,0f), Time.deltaTime * 5f);
    }

    private void BirdPower()
    {
        Vector3 offsetVehicule = GameManager.instance.destination.transform.position + ConvertOffsetPlayerPos(4, offsetDestinationPos[4]);
        vehiculeList[4].transform.position = offsetVehicule;
       // vehiculeList[4].transform.position = GameManager.instance.destination.transform.position + offsetDestinationPos[4];
        vehiculeList[4].transform.rotation = GameManager.instance.destination.transform.rotation;//pas de lerp ou slerp pour bien que le joueur suive l'oiseau malgrés la vitesse
        vehiculeList[0].transform.position = vehiculeList[4].transform.position;//on fait suivre le rover
        vehiculeList[0].transform.rotation = vehiculeList[4].transform.rotation;
    }

    private void OnEnable()
    {
        pareChocList = new List<GameObject>();
        axleInfos = new List<WheelCollider>();
        rouesList = new List<GameObject>();
        forceBreak = 500f;
        maxiBreakForce = 1500f;
        maxSteeringAngle = 70f;
        mathildeControler = GameManager.instance.genericPlayer.GetComponent<MathildeControler>();
        playerAnim = GameManager.instance.genericPlayer.GetComponent<Animator>();
        GameManager.instance.destination = vehiculeList[0];//Rover
        GameManager.NewNavigation += NavigatingListener;
        GameManager.NewPlayer += PlayerListener;//souscrit à l'évent system qui défini le bon joueur
        EventManagerPlayer.NewGear += Listener;//souscrit a l'évent system qui permet de déterminer grace à un string dans quel état est le moteur du vehicule
    }

    private void OnDesable()
    {
        GameManager.NewNavigation -= NavigatingListener;
        EventManagerPlayer.NewGear -= Listener;//de-inscription à l'envent system
        GameManager.NewPlayer -= PlayerListener;//souscrit à l'évent system qui défini le bon joueur
    }

    public void GoVehicule()
    {
        RBSwap(true);
        ChangeVehicule();//pour chaque saut on quitte le vehicule actuel pour revenir au rover
        rigidbodyVehicule = vehiculeList[indexVehicule].GetComponent<Rigidbody>();//choisi le bon véhicule et prends sont RB
    }

    private void Start()
    {
        delay = false;
        indexVehicule = 0;//Rover par défaut
        indexMat = 0;
        resetVehiculeRotation = false;
        oldVehicule = vehiculeList[0];//
        InvokeRepeating("CheckPareChoc", 1f, 0.1f);
    }

    private void Update()
    {
        if (navigating)
        {
            if (boiteAVitesse == Jump || boiteAVitesse == Transit || boiteAVitesse == Bird || boiteAVitesse == Wagon || boiteAVitesse == EnVoiture || boiteAVitesse == Train)
            {
                boiteAVitesse();//action on fonction de la vitesse enclenchée
            }
            if (Input.GetAxis("Mouse ScrollWheel") > 0) // back
            {
                maxMotorTorque += 300f; 
            }
            if (Input.GetAxis("Mouse ScrollWheel") < 0) // back
            {
                maxMotorTorque -= 300f;
            }
        }
    }

    private void FixedUpdate()
    {    //Moteur
        if (navigating)
        {
            vehiculeSpeed = rigidbodyVehicule.velocity.sqrMagnitude;//peut atteindre plus de 10000 en cas de chute. Peut être négatif aussi.
            localVelocityRB = rigidbodyVehicule.transform.InverseTransformDirection(rigidbodyVehicule.velocity);//z est positif si déplacement vers l'avant et négatif vers l'arrière. Y positif si montée négatif si descente, x positif is virage à droite et négatif si virage à gauche
            if (boiteAVitesse == MarcheAvant || boiteAVitesse == MarcheArriere || boiteAVitesse == Neutral)
            {
                boiteAVitesse();//action on fonction de la vitesse enclenchée
            }
        }
    }
}



