﻿using UnityEngine;
using System.Collections;

public class CustomCursor : MonoBehaviour {
	
	public Texture2D customCursorUp;//texture du pointeur 
	public Texture2D customCursorDown;//texture du pointeur quand on clic
	public Texture2D cursorTexture;//texture générique 
	public Vector2 hotspot;
	public CursorMode cursorMode;
	
	public void  Start (){
		cursorMode= CursorMode.Auto;
		hotspot = Vector2.zero;
		cursorTexture = customCursorUp;// default texture pointeur
	}
	
	private void  OnMouseEnter (){
		cursorTexture = customCursorDown;//change la texture pour le pointeur rouge
		Cursor.SetCursor(cursorTexture, hotspot, cursorMode);  
	}
	
	private void  OnMouseExit (){//si on passe sur collider avec la sourie on remet la texture jaune
		cursorTexture = customCursorDown;
		Cursor.SetCursor(cursorTexture, hotspot, cursorMode);  
	}	
	
	private void  OnDisable (){//retour au pointeur système
		Cursor.SetCursor(null, Vector2.zero, cursorMode);
	}
	
	
	
	
}