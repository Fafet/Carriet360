﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;

public class MyNameIs : MonoBehaviour, IPointerDownHandler, IPointerExitHandler, IPointerEnterHandler {
	
	//script sur les Ui Buttons afin de connaitre leur nom et définir les actions sur le script GUIAudio
	
	private AudioGUI audioGUI;
    private List<Vector3> defaultReverbPos;
	
	public void Awake(){
		audioGUI = GameObject.Find ("AudioContainer").GetComponent<AudioGUI> ();
	}

    public void OnPointerDown(PointerEventData eventData)
    {///on recupère l'event system pour avoir la main lors du click
		Button button = GetComponent<Button>();
        GameObject thisGO = button.gameObject;
        if (button.interactable)
        {
            if (this.gameObject.name.Contains("PlanLocation"))
            {
                for (int i = 0; i < audioGUI.planLocation.Count; i++)
                {
                    if (audioGUI.planLocation[i] == thisGO)
                    {
                        audioGUI.indexDestination = i;//defini l'index de la destination
                        GameManager.instance.destination = GameManager.instance.reverbZoneGO[i];
                        audioGUI.CloseInventory();
                        EventManagerPlayer.BooleanSetUp("Transit");//EVENT///////////////////déclenche l'évènement transit
                    }
                }
            }
           else
            {
                List<GameObject> playersList = GameManager.instance.playerList;
                for (int i = 0; i < audioGUI.avatarsList.Count; i++)
                {
                    GameManager.instance.reverbZoneGO[i].transform.position = defaultReverbPos[i];//remise en place éventuel de la reverbZone
                    if (audioGUI.avatarsList[i] == thisGO)
                    {
                        audioGUI.indexDestination = i;//defini l'index de la destination
                        GameManager.instance.reverbZoneGO[i].transform.position = playersList[i].transform.position + new Vector3(5f, 40f, 5f);
                        GameManager.instance.destination = GameManager.instance.reverbZoneGO[i];
                        audioGUI.CloseInventory();
                        EventManagerPlayer.BooleanSetUp("Transit");//EVENT///////////////////déclenche l'évènement transit
                    }
                }
            }
        }
    }

    public void OnPointerExit (PointerEventData eventData){//idem pour le mouseOver
		Button button = GetComponent<Button> ();
		GameObject thisGO = button.gameObject;
		if(button.interactable){
			for (int i=0; i< audioGUI.planLocation.Count;i++){
				if( audioGUI.planLocation[i] == thisGO){
					audioGUI.MouseOnPointerExit(button,i);
				}
			}
		}
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
        Button button = GetComponent<Button>();
        GameObject thisGO = button.gameObject;
        if (button.interactable)
        {
            for (int i = 0; i < audioGUI.planLocation.Count; i++)
            {
                if (audioGUI.planLocation[i] == thisGO)
                {
                    audioGUI.MouseOnPointerEnter(button, i);
                }
            }
        }
    }

    private void Start()
    {
        defaultReverbPos = new List<Vector3>();
        foreach(GameObject reverb in GameManager.instance.reverbZoneGO)
        {
            defaultReverbPos.Add(reverb.transform.position);
        }
    }
}
