﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NewGear : MonoBehaviour {//Script sur les sprites de vitesse soit MarcheAvant, MarcheArrirere, Neutral qui font afficher le changement de vitesse en déformant le sprite et en changeant son opacité
    public bool newGear;

    public void plusGearSprite()
    {
        StartCoroutine(GearScaleSprite());
    }

    private IEnumerator GearScaleSprite()
    {
        if (!newGear)
        {
            RectTransform spriteRecT = this.GetComponent<RectTransform>();
            float oldSizeX = spriteRecT.anchorMax.x;
            float oldSizeY = spriteRecT.anchorMax.y;
            Image thisImage = this.GetComponent<Image>();
            Text thisText = this.GetComponentInChildren<Text>();
            Color thisTextColor = thisText.color;
            float alpha = 1f;
            newGear = true;
            while (spriteRecT.anchorMax.x < 0.3f)
            {
                alpha -= Time.deltaTime *2f;
                Color col = new Color(1f, 1f, 1f, alpha);
                thisImage.color = col;
                thisText.color = new Color(thisTextColor.r, thisTextColor.g, thisTextColor.b, (1f-alpha));
                float offsetX = spriteRecT.anchorMax.x + Time.deltaTime;
                spriteRecT.anchorMax = new Vector2(offsetX, oldSizeY);
                yield return null;
            }
            while (spriteRecT.anchorMax.x > 0f)
            {
                alpha += Time.deltaTime;
                Color col = new Color(1f, 1f, 1f, alpha);
                thisImage.color = col;
                thisText.color = new Color(thisTextColor.r, thisTextColor.g, thisTextColor.b, (1f - alpha));
                float offsetX = spriteRecT.anchorMax.x - Time.deltaTime;
                spriteRecT.anchorMax = new Vector2(offsetX, oldSizeY);
                yield return null;
            }
            newGear = false;
            spriteRecT.anchorMax = new Vector2(oldSizeX, oldSizeY);
            alpha -= Time.deltaTime;
            Color col2 = new Color(1f, 1f, 1f, 1f);
            thisImage.color = col2;
            thisText.color = new Color(thisTextColor.r, thisTextColor.g, thisTextColor.b, 0f);
        }
    }
}
