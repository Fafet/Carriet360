﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OiseauxManager : MonoBehaviour
{

    private GameObject oiseauxRoot;
    public GameObject oiseauPrefab;
    public OiseauData datatPlayerIsOn;//le cheval sur lequel monte le joueur
    public OiseauData birdRide;//le cheval le plus proche du joueur
    private GameObject player;
    private GameObject birdMagnet;//empty avec sphereCollider qui se pose sur le bird en mode landing le plus proche du joeur et qui déclenche l'event le mode Bird (event plugNewGear)
    public GameObject hydro;
    public List<OiseauData> oiseauxData;
    public Vector3 defaultAltitude;
    public Vector3 birdMagnetPos;//l'endroit ou est positioné le birdRover correspond à l'oiseau le plus proche du joueur
    public bool birdOn;//les oiseaux sont'ils autorisés à voler ?
    private int totalOiseaux;//nombre d'oiseaux présents dans la scène

    public delegate void EventBirdMode(OiseauData e);
    public static event EventBirdMode NewBirdMode;

    private void Awake()
    {
        hydro = GameObject.Find("Hydro");
        oiseauxRoot = GameObject.Find("OiseauxRoot");
        birdMagnet = GameObject.Find("BirdMagnet");
    }

    private void Listener(string e)//reçoit de l'éventSystem le string qui définit l'état du moteur. Permet de choisir la bonne méthode pour le delegate boite à vitesse
    {
        if (e == "Neutral")
        {
            datatPlayerIsOn = null;//plus de player sur l'oiseau
        }
        if (e == "MarcheAvant")
        {
            datatPlayerIsOn = null;//plus de player sur l'oiseau
        }
        if (e == "MarcheArriere")
        {
            datatPlayerIsOn = null;//plus de player sur l'oiseau
        }
        if (e == "Jump")
        {
            datatPlayerIsOn = null;//plus de player sur l'oiseau
        }
        if (e == "Transit")
        {
            datatPlayerIsOn = null;//plus de player sur l'oiseau
        }
        if (e == "EnVoiture")
        {
            datatPlayerIsOn = null;//plus de player sur l'oiseau
        }
        if (e == "EnTrain")
        {
            datatPlayerIsOn = null;//plus de player sur l'oiseau
        }
        if (e == "Wagon")
        {
            datatPlayerIsOn = null;//plus de player sur l'oiseau
        }
    }

    private void BirdModeListener(OiseauData e)
    {
        datatPlayerIsOn = e;//defini quel est l'oiseau data de cet oiseau
    }

    private void PlayerListener(GameObject e)
    {
        player = e;
    }

    public void ChangeMode()//crée l'EVENT BirdModeListener
    {
        if (datatPlayerIsOn == null)//sur une frame. Fonction déclenchée par playerCOntroler suite à la mise ne place du player sur l'oiseau
        {
            NewBirdMode(oiseauxData[birdRide.indexOiseau]);//EVENT avec comme argument l'OiseauData de l'oiseau concerné. Player en place
        }
    }

    public List<GameObject> GetGoodHydroCells()
    {
        HydroManager hydroManager = hydro.GetComponent<HydroManager>();
        List<GameObject> hydroCellsElevationO = new List<GameObject>();//crée une liste qui liste les index de cellules d'hydro qui ont une élévation de zero
        for (int i = 0; i < hydroManager.cellsList.Count; i++)
        {
            if (hydroManager.elevationLevelList[i] == 0)
            {
                hydroCellsElevationO.Add(hydroManager.cellsList[i]);//ajoute cette cellule à la liste
            }
        }
        return hydroCellsElevationO;
    }

    public void SetUpOiseauxData()//sur une frame. SetUp général au demarrage
    {
        HydroManager hydroManager = hydro.GetComponent<HydroManager>();
        List<GameObject> hydroCellsConcern = GetGoodHydroCells();
        for (int i = 0; i < totalOiseaux; i++)
        {
            OiseauData oiseauData = new OiseauData();
            int randomStart = Random.Range(0, hydroCellsConcern.Count);//cellule de départ
            int randomEnd = Random.Range(0, hydroCellsConcern.Count);//cellule d'arrivée
            oiseauData.destination = hydroCellsConcern[randomEnd];//defini la destination de l'oiseau
            oiseauData.altitude = oiseauData.destination.transform.position + defaultAltitude;
           // Vector3 pos = hydroCellsConcern[randomStart].transform.position + defaultAltitude + RandomVector3() ;
            oiseauData.bird = (GameObject)Instantiate(oiseauPrefab,Vector3.zero, Quaternion.identity) as GameObject;//instantie un oiseau
            oiseauData.bird.gameObject.name = i.ToString()+"bird";
            oiseauData.bird.transform.SetParent(oiseauxRoot.transform, false);//parente l'oiseau à la racine
            oiseauData.indexOiseau = i;
            oiseauData.oiseauControler = oiseauData.bird.GetComponent<OiseauControler>();
            oiseauData.oiseauControler.indexBird = i;//affecte le bon numero d'index à cet oiseau pour l'utiliser dans les fonctions de boite à vitesse
            oiseauxData.Add(oiseauData);//garde une trace de ce nouvel oiseauData dans la liste totale des oiseaux    
            birdOn = true;//les oiseaux entrent en scene
        }
    }

    private void GetBirdRidable()//fonction qui permet de placer le trigger Vehicule sur l'oiseau qui s'est posé le plus près du joeur et déclencher lors du passage du joueur la chevauché de l'oiseau
    {
        List<OiseauData> birdLanded = new List<OiseauData>();//nouvelle liste avec seulement les oiseaux qui se sont posés
        foreach (OiseauData birdData in oiseauxData)
        {
            if (birdData.birdMode == birdData.bird.GetComponent<OiseauControler>().Landing)
            {
                birdLanded.Add(birdData);//crée la liste si un oiseau est en mode landing
            }
        }
        if (datatPlayerIsOn == null)//si null on peut déplacer le trigger. si le joueur monte sur un oiseau il ne faut pas faire suivre le bird_Rover car il va perturber le moteur physic
        {
            if (birdLanded.Count > 0)// si la liste n'est pas vide
            {
                List<OiseauData> birdLandedOK = birdLanded;//liste temporaire qui contient l'oiseau sur lequel le player est monté
                birdLanded = new List<OiseauData>();//nouvelle liste avec seulement les oiseaux qui se sont posés
                foreach (OiseauData piaf in birdLandedOK)
                {
                    if (piaf != datatPlayerIsOn)
                    {
                        birdLanded.Add(piaf);//cette liste ne contient que les oiseaux qui ont atterit sans celui sur lequel le joeur est monté
                    }
                }
            }
        }
        if (birdLanded.Count > 0)// si la liste n'est pas vide
        {
            int lowest = 1000;
            float distanceStart = 100000f;
            for (int i = 0; i < birdLanded.Count; i++)
            {
                float distance = Mathf.Abs(Vector3.Distance(birdLanded[i].bird.transform.position, player.transform.position));
                if (distance < distanceStart)
                {
                    lowest = birdLanded[i].indexOiseau;//prends l'oiseau posé le plus proche
                    distanceStart = distance;
                }
            }
            //Debug.Log(lowest + "  lowest   ");
            birdRide = oiseauxData[lowest];//l'oiseau suspetible de devenir un cheval pour le joeur
            GameManager.instance.playerList[8] = oiseauxData[lowest].bird;//mise à jour de l'oiseau le plus proche pour permettre un transit vers cet oiseau en passant par le sprite de l'oiseau dans l'inventaire
            birdMagnetPos = birdRide.bird.transform.position;
        }
    }

    private void FlyAgain()//fonction qui relance le vol des oiseaux
    {
        foreach(OiseauData birdDat in oiseauxData)
        {
            if (birdDat.birdMode == birdDat.oiseauControler.Landing)//Ne prends que les oiseaux qui se sont posés
            {
                if (birdDat != birdRide)//si ce n'est pas l'oiseau chevauché par le player ou si ce n'est pas l'oiseau le plus proche du joueur on relance la vol
                {
                    birdDat.oiseauControler.takeOffTimer = 0f;//timer à zero pour ne pas faire de raycast au décollage le temps que l'oiseau s'élève
                    birdDat.oiseauControler.PointCurve();
                    birdDat.birdMode = birdDat.oiseauControler.BirdFly;//change le mode des oiseaux dont c'est le cas pour le mode vol
                }
                if (birdDat == datatPlayerIsOn)//si l'oiseau avec le joueur sur le dos est posé on relance le vol
                {
                    birdDat.oiseauControler.takeOffTimer = 0f;//timer à zero pour ne pas faire de raycast au décollage le temps que l'oiseau s'élève
                    birdDat.oiseauControler.PointCurve();
                    birdDat.birdMode = birdDat.oiseauControler.BirdFly;//change le mode des oiseaux dont c'est le cas pour le mode vol
                }
            }
        }
    }

    public Vector3 RandomVector3()
    {
        float miniAltitude = -40f;
        if (datatPlayerIsOn != null)//On remonte l'altitude si le joueur est sur un oiseau
        {
            miniAltitude = 0f;
        }
        float randomX = Random.Range(-8f, 9f);
        float randomY = Random.Range(miniAltitude, 70f);
        float randomZ = Random.Range(-8f, 8f);
        return new Vector3(randomX, -randomY, randomZ);//permet de décaler la position de destination du centre de la cellule hydro sur x et z
    }

    public void Start()
    {
        birdOn = false;
        totalOiseaux = 20;//nombre d'instances d'oiseaux
        defaultAltitude = new Vector3(0f, 900f, 0f);//altitude par défaut
        oiseauxData = new List<OiseauData>();
        datatPlayerIsOn = null;
        SetUpOiseauxData();//mets en place le contexte général
        GameManager.instance.playerList.Add(oiseauxData[0].bird);//ajoute l'oiseau 0 à la liste des players de manière à pouvoir se rendre vers l'oiseau en mode transit
        InvokeRepeating("GetBirdRidable", 2f, 1f);
        InvokeRepeating("FlyAgain", 2f, 4f);//toute les 10 secondes relance les oiseaux qui se sont posés       
    }

    private void OnEnable()
    {
        GameManager.NewPlayer += PlayerListener;//souscrit à l'évent system qui défini le bon joueur  
        EventManagerPlayer.NewGear += Listener;
        NewBirdMode += BirdModeListener;
    }

    private void OnDesable()
    {
        GameManager.NewPlayer -= PlayerListener;//souscrit à l'évent system qui défini le bon joueur
        EventManagerPlayer.NewGear -= Listener;
        NewBirdMode -= BirdModeListener;
    }

    private void Update()
    {
        if (birdOn)
        {
            float distance = Vector3.Distance(player.transform.position , birdMagnet.transform.position);
            distance = Mathf.Clamp(distance,0f, 200f)/10f;//permet de faire monter la sphére qui indique où se trouve l'oiseau le plus proche
            Vector3 idealPos = birdMagnetPos + new Vector3(0f, distance, 0f);
            birdMagnet.transform.position = Vector3.Lerp(birdMagnet.transform.position, idealPos, Time.deltaTime * 3f);
        }
    }
}

public class OiseauData 
{
    public delegate void BirdMode();
    public BirdMode birdMode;
    public bool playerOnThatBird;//le joueur est-il sur cet oiseau ?
    public OiseauControler oiseauControler;
    public GameObject bird;//l'oiseau associé aux datas
    public GameObject destination;//la cellule hydro vers laquelle l'oiseau doit aller
    public int indexOiseau;
    public Vector3 altitude;
}
