﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextureManager : MonoBehaviour {
	
	//script sur le GO parent ObjectScene	

	private Texture[] textures;//liste de textures utilisables pour swap
    private Material oldMaterial;
    private GameObject zoneJuliette;
	private bool poissonRotateGo;

    private Dictionary<string, DicoTexture> racineGoDico;//le dico qui contient la liste des GO racine. Chaque dico contient le dico de la liste des mat des enfants de ce GO

    private void Awake()
    {
        zoneJuliette = GameObject.Find("ZoneJuliette");
    }

    private void SwitchMatSetup()
    {
        foreach(KeyValuePair<string, DicoTexture> dicList in racineGoDico)//regarde dans la liste d'instances de dico (un par objet racine)//dicList représente chaque GO root sous ZoneJuliette
        {
            foreach(KeyValuePair<string, Material> matDico in dicList.Value.oldDicoTextureClass)//regarde dans l'instance de classe DicoTexture le dictionnaire des texture//matDico représente les valeurs dans chaque dico oldDicoTestureClass
            {
                Material newMat = new Material(Shader.Find("Custom/StandardShaderTwikable"));
                //Get old mat setUp
                if (matDico.Value.shader == oldMaterial.shader)
                {
                    Texture _MainTex = matDico.Value.GetTexture("_MainTex");
                    Texture _BumpMap = matDico.Value.GetTexture("_BumpMap");
                    Texture _OcclusionMap = matDico.Value.GetTexture("_OcclusionMap");
                    Texture _DetailAlbedoMap = matDico.Value.GetTexture("_DetailAlbedoMap");
                    Texture _DetailNormalMap = matDico.Value.GetTexture("_DetailNormalMap");
                    Texture _ParallaxMap = matDico.Value.GetTexture("_ParallaxMap");
                    float _Glossiness = matDico.Value.GetFloat("_Glossiness");
                    float _Metallic = matDico.Value.GetFloat("_Metallic");
                    float _BumpScale = matDico.Value.GetFloat("_BumpScale");
                    float _OcclusionStrength = matDico.Value.GetFloat("_OcclusionStrength");
                    float _DetailNormalMapScale = matDico.Value.GetFloat("_DetailNormalMapScale");
                    float _Parallax = matDico.Value.GetFloat("_Parallax");
                    Color _Color = matDico.Value.GetColor("_Color");
                    Vector2 _MainTex_ST = matDico.Value.GetVector("_MainTex_ST");
                    Vector2 _BumpMap_ST = matDico.Value.GetVector("_BumpMap_ST");
                    Vector2 _OcclusionMap_ST = matDico.Value.GetVector("_OcclusionMap_ST");
                    Vector2 _DetailAlbedoMap_ST = matDico.Value.GetVector("_DetailAlbedoMap_ST");
                    Vector2 _DetailNormalMap_ST = matDico.Value.GetVector("_DetailNormalMap_ST");
                    Vector2 _ParallaxMap_ST = matDico.Value.GetVector("_ParallaxMap_ST");
                    //Set new material
                    newMat.SetTexture("_MainTex", _MainTex);
                    newMat.SetTexture("_MainTex2", _OcclusionMap);
                    newMat.SetTexture("_BumpMap", _BumpMap);
                    newMat.SetTexture("_OcclusionMap", _OcclusionMap);
                    newMat.SetTexture("_DetailAlbedoMap", _DetailAlbedoMap);
                    newMat.SetTexture("_DetailNormalMap", _DetailNormalMap);
                    newMat.SetTexture("_ParallaxMap", _ParallaxMap);
                    newMat.SetFloat("_Metallic", _Metallic);
                    newMat.SetFloat("_BumpScale", _BumpScale);
                    newMat.SetFloat("_OcclusionStrength", _OcclusionStrength);
                    newMat.SetFloat("_DetailNormalMapScale", _DetailNormalMapScale);
                    newMat.SetFloat("_Parallax", _Parallax);
                    newMat.SetFloat("_Blend", 0f);
                    newMat.SetColor("_Color", _Color);
                    newMat.SetVector("_MainTex_ST", _MainTex_ST);
                    newMat.SetVector("_BumpMap_ST", _BumpMap_ST);
                    newMat.SetVector("_OcclusionMap_ST", _OcclusionMap_ST);
                    newMat.SetVector("_DetailAlbedoMap_ST", _DetailAlbedoMap_ST);
                    newMat.SetVector("_DetailNormalMap_ST", _DetailNormalMap_ST);
                    newMat.SetVector("_ParallaxMap_ST", _ParallaxMap_ST);
                    dicList.Value.dicoTextureClass.Add(matDico.Key, newMat);//ajoute ce nouveau mat dans la liste avec le nom du GO concerné
                    GameObject tempGO;
                    if (dicList.Value.gameObjectDicoClass.TryGetValue(matDico.Key, out tempGO))
                    {
                        tempGO.GetComponent<MeshRenderer>().material = newMat;
                    }
                }
            }
            StartCoroutine(BlendTexture(dicList.Value.dicoTextureClass));
        }
    }

    public IEnumerator BlendTexture(Dictionary<string, Material> dicList)//fonction pour une seule branche de la racine. A faire pour chaque branche dont on veut changer le materiaux principal et baisser le Pump
    {
        float timer = 0f;
        while (timer < 1f)
        {
            timer += Time.deltaTime / 3f;
            timer = Mathf.Clamp(timer, 0f, 1f);
            foreach (KeyValuePair<string, Material> matDico in dicList)//regarde dans l'instance de classe DicoTexture le dictionnaire des texture//matDico représente les valeurs dans chaque dico oldDicoTestureClass
            {
                matDico.Value.SetFloat("_Blend", timer);
            }
            yield return null;
        }
    }

    private void OnEnable()
    {
        int increment = 0;
        oldMaterial = new Material(Shader.Find("Standard"));
        racineGoDico = new Dictionary<string, DicoTexture>();
        foreach (Transform objets in zoneJuliette.transform)//récupére tout les objets racines de ZoneJuliette
        {
            DicoTexture dicoTexture = new DicoTexture();//nouvelle instance de la classe ci-dessous DicoTexture
            dicoTexture.dicoTextureClass = new Dictionary<string, Material>();//initialise l'instance du nouveau dictionnaire
            dicoTexture.oldDicoTextureClass = new Dictionary<string, Material>();//initialise l'instance du nouveau dictionnaire qui va conserver les anciens matériaux
            dicoTexture.gameObjectDicoClass = new Dictionary<string, GameObject>();//initialise l'instance du nouveau dictionnaire
            foreach (Transform obj in objets.GetComponentsInChildren<Transform>())//prends tout les transform dans le transform (cherche dans les enfants d'enfants si nécésséssaire)
            {
                if(obj.gameObject.GetComponent<MeshRenderer>() != null)//si il y a bien un renderer sur cet objet
                {
                    Material matObjet = obj.gameObject.GetComponent<Renderer>().material;//va chercher le mat concerné
                    if(matObjet == null)
                    {
                        matObjet = new Material(Shader.Find("Custom/StandardShaderTwikable")); 
                    }
                    if (dicoTexture.oldDicoTextureClass.ContainsKey(obj.gameObject.name))//si un objet porte le même nom qu'un autre on le renome en ajoutant un chiffre incrementé à son nom
                    {
                        increment += 1;
                        obj.gameObject.name = obj.gameObject.name + increment;
                    }
                    dicoTexture.oldDicoTextureClass.Add(obj.gameObject.name, matObjet);//ajoute le material et le nom de l'objet au dico
                    dicoTexture.gameObjectDicoClass.Add(obj.gameObject.name, obj.gameObject);//joute le GO concerné au tableau des GO avec son nom afin de comparer les tableau sur le nom (Key)
                }
            }
            racineGoDico.Add(objets.gameObject.name, dicoTexture);//liste des GO racine avec nom et dico de l'ensembre des mat des enfants de cet objet racine
        }
    }

    private void Start()
    {
        poissonRotateGo = true;
        SwitchMatSetup();
    }

    [System.Serializable]
    public class DicoTexture//nouvelle classe de dico de texture
    {
        public Dictionary<string, Material> dicoTextureClass;//dictionnaire qui contient les matériaux avec le nom du GO concerné pour chaque objet racine de la hierarchie ObjectScene
        public Dictionary<string, Material> oldDicoTextureClass;//dictionnaire qui contient les anciens matériaux avec le nom du GO concerné pour chaque objet racine de la hierarchie ObjectScene
        public Dictionary<string, GameObject> gameObjectDicoClass;//liste avec nom des go concernés
    }
	/*

Shader "Myshaders/ChangeMaterial" {
    Properties {
        _Tint ("Tint Color", Color) = (.9, .9, .9, 1.0f)
        _TexMat1 ("Base (RGB)", 2D) = "white" {}
        _TexMat2 ("Base (RGB)", 2D) = "white" {}
        _Blend ("Blend", Range(0.0f,1.0f)) = 0.0f
    }
   
    Category {
        ZWrite On
        Alphatest Greater 0
        Tags {Queue=Transparent}
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask RGB
    SubShader {
        Pass {
           
            Material {
                Diffuse [_Tint]
                Ambient [_Tint]
            }
            Lighting On
           
            SetTexture [_TexMat1] { combine texture }
            SetTexture [_TexMat2] { constantColor (0,0,0,[_Blend]) combine texture lerp(constant) previous }
            SetTexture [_TexMat2] { combine previous +- primary, previous * primary }
        }
    }
    FallBack " Diffuse", 1
    */
}