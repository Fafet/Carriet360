﻿using UnityEngine;
using System.Collections;

public class OnCollisionSound : MonoBehaviour {

    AudioSourceEnvironnements audioSourceEnvironnements;

    private void OnEnable ()
    {
        audioSourceEnvironnements = GameObject.Find("AudioContainerEnvir").GetComponent<AudioSourceEnvironnements>();

    }

    // Update is called once per frame
    private void OnCollisionEnter(Collision other)//en cas de collision
    {
        if (other.gameObject.name.Contains("Toit"))
        {
            audioSourceEnvironnements.AddAudioMultipleManagement(AudioSourceEnvironnements.SoundCategorie.coups, 4);//joue l'audioSource en cas de collision. le nombre d'audioSource est géré par le second argument. Tout est géré sur le script audioSourceEnvironnement
        }
    }        
}
