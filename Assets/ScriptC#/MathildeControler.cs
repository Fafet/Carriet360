﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MathildeControler : MonoBehaviour {
    //Script attaché à Mathilde qui gére son animator contoler
    public List<float> animatorSpeedList;
    private VehiculeManager vehiculeManager;//le script VehiculeControler sur le rover
    public Animator animator;//L'animator de Mathilde
    public float normVehiculeSpeed;//vitesse de déplacement du véhicule (ngatif en marche arrière, positif en marche avant)
    public float animationSpeed;//permet de gérer marche avant et marche arrière en inversant la valeur
    public bool fall;//chûte
    public float compteur;//Depuis quand à commencé le state Idle afin de lancer twist
    public delegate void BoiteAVitesse();//surveille la position y de la sourie et enclenche soit la marche avant, le point mort ou la marche arrière
    public BoiteAVitesse boiteAVitesse;

    public void Awake()
    {
        vehiculeManager = GameObject.Find("VehiculesRoot").GetComponent<VehiculeManager>();
    }

    private void Listener(string e)//reçoit de l'éventSystem le string qui définit l'état du moteur. Permet de choisir la bonne méthode pour le delegate boite à vitesse
    {
        if (e == "Neutral")
        {
            animator.SetBool("marche", true);
            animator.SetBool("inWagon", false);
            boiteAVitesse = Neutral;
        }
        if (e == "MarcheAvant")
        {
            animator.SetBool("marche", true);
            boiteAVitesse = MarcheAvant;
            animator.SetBool("inWagon", false);
            animator.SetBool("twist", false);
        }
        if (e == "MarcheArriere")
        {
            animator.SetBool("marche", true);
            boiteAVitesse = MarcheArriere;
            animator.SetBool("inWagon", false);
            animator.SetBool("twist", false);
        }
        if (e == "Jump")
        {
            boiteAVitesse = Jump;
            animator.SetBool("skate", false);
            animator.SetBool("inWagon", false);
            animator.SetBool("drive", false);
            animator.SetBool("jump", true);
            animator.SetBool("land", false);//remise à zéro
            animator.SetBool("twist", false);
            animator.SetBool("bird", false);
            animator.SetBool("isGrounded", false);
        }
        if (e == "Transit")
        {
            boiteAVitesse = Jump;
            animator.SetBool("skate", false);
            animator.SetBool("inWagon", false);
            animator.SetBool("drive", false);
            animator.SetBool("jump", true);
            animator.SetBool("land", false);//remise à zéro
            animator.SetBool("twist", false);
            animator.SetBool("bird", false);
            animator.SetBool("isGrounded", false);
        }
        if (e == "Visite")
        {
            boiteAVitesse = Jump;//lance le raycast pour déterminer la distance au sol du joueur
            animator.SetBool("inWagon", false);
            animator.SetBool("drive", false);
            animator.SetBool("jump", true);
            animator.SetBool("land", false);//remise à zéro
            animator.SetBool("transit", true);
            animator.SetBool("twist", false);
            animator.SetBool("bird", false);
        }
        if (e == "EnVoiture")
        {
            animator.SetBool("marche", false);
            animator.SetBool("inWagon", false);
            animator.SetBool("drive", false);
            boiteAVitesse = EnVoiture;
            animator.SetBool("bird", false);
            animator.SetBool("transit", false);
            animator.SetBool("land", false);//remise à zéro
            animator.SetBool("twist", false);
        }

        if (e == "EnTrain")
        {
            animator.SetBool("marche", false);
            animator.SetBool("inWagon", false);
            animator.SetBool("drive", false);
            boiteAVitesse = EnTrain;
            animator.SetBool("bird", false);
            animator.SetBool("transit", false);
            animator.SetBool("land", false);//remise à zéro
            animator.SetBool("twist", false);
        }
        if (e == "Wagon")
        {
            animator.SetBool("marche", false);
            animator.SetBool("drive", false);
            animator.SetBool("inWagon", true);
            boiteAVitesse = Wagon;
            animator.SetBool("bird", false);
            animator.SetBool("transit", false);
            animator.SetBool("land", false);//remise à zéro
            animator.SetBool("twist", false);
        }
        if (e == "Bird")
        {
            animator.SetBool("marche", false);
            animator.SetBool("inWagon", false);
            animator.SetBool("drive", false);
            boiteAVitesse = Bird;
            animator.SetBool("transit", false);
            animator.SetBool("land", false);//remise à zéro
            animator.SetBool("twist", false);
            animator.SetBool("bird", true);
        }
    }

    public void Neutral()//Point mort
    {
        if (vehiculeManager.localVelocityRB.z > 0.01f)
        {
            animator.SetFloat("playerSpeed", normVehiculeSpeed);
        }
        if (vehiculeManager.localVelocityRB.z <= 0.01f)
        {
            animator.SetFloat("playerSpeed", -normVehiculeSpeed);
        }
        animator.SetFloat("xPosition", MouseManager.xMousePoint + 0.5f);
        compteur += Time.deltaTime;
        if(compteur > 3f)
        {
            if (animator.GetBool("twist") == false)
            {
                StartCoroutine(TwistEnd());
            }
        }
    }

    public void MarcheAvant()
    {
        if (vehiculeManager.localVelocityRB.z > 0.01f)
        {
            animator.SetFloat("playerSpeed", normVehiculeSpeed);
        }
        if (vehiculeManager.localVelocityRB.z <= 0.01f)
        {
            animator.SetFloat("playerSpeed", -normVehiculeSpeed);
        }
        animator.SetFloat("xPosition", MouseManager.xMousePoint + 0.5f);
    }

    public void MarcheArriere()
    {
        if (vehiculeManager.localVelocityRB.z < -0.1f)
        {
            animator.SetFloat("playerSpeed", -normVehiculeSpeed);
        }
        if (vehiculeManager.localVelocityRB.z >= -0.1f)
        {
            animator.SetFloat("playerSpeed", normVehiculeSpeed);
        }
    }

    public void Jump()
    {

    }

    public void EnVoiture()
    {
        animator.SetFloat("playerSpeed", 0f);
        animator.SetBool("drive", true);
        if (vehiculeManager.vehiculeList[vehiculeManager.indexVehicule].name.Contains("Skate"))
        {
            animator.SetBool("skate", true);
        }
        else animator.SetBool("skate", false);
    }

    public void EnTrain()
    {
        animator.SetFloat("playerSpeed", 0f);
    }

    public void Wagon()
    {
        animator.SetFloat("playerSpeed", 0f);
        animator.SetBool("drive", false);
    }

    public void Bird()
    {
        animator.SetBool("marche", false);
    }

    private IEnumerator TwistEnd()//fonction appelée depuis l'évent de l'animation twist de Mathilde
    {
        animator.SetBool("twist", true);
        AnimatorStateInfo currentState = animator.GetCurrentAnimatorStateInfo(0);
        float playbackTime = currentState.normalizedTime % 1;
        while (currentState.normalizedTime % 1 < 0.98f)
            {
            currentState = animator.GetCurrentAnimatorStateInfo(0);
            playbackTime = currentState.normalizedTime % 1;
                yield return null;
            }
        compteur = 0f;
        animator.SetBool("twist", false);
    }


    public void Fall(bool fell)
    {
        animator.SetBool("fall", fell);
    }

    public void OnEnable()
    {
        animator = GetComponent<Animator>();
        EventManagerPlayer.NewGear += Listener;//souscrit a l'évent system qui permet de déterminer grace à un string dans quel état est le moteur du vehicule
    }

    public void OnDesable()
    {
        EventManagerPlayer.NewGear -= Listener;//de-inscription à l'envent system
    }

    public void FixedUpdate()
    {
        if(boiteAVitesse != null)
        {
            normVehiculeSpeed = Mathf.Clamp(vehiculeManager.vehiculeSpeed, -3f, 5f);
            boiteAVitesse();//action on fonction de la vitesse enclenchée
        }
    }
}
