﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkyboxManager : MonoBehaviour {

	//script sur le GO GameControl

	public List<Material> skyboxList;
    public List<float> sunIntensityList;//intensité du soleil en fonction du ciel de la skybox
    public List<float> sunRotationList;//rotation du soleil en fonction du ciel de la skybox
    private GameObject lightMapping;//le soleil
	private Light sun;
	public Material skyboxCurrent;
    private float blendClamp;
    private float blendFactor;
	private float lightMappingDefaultIntensity;
    private float idealSunRotation;//change la rotation en x pour celle définie dans cette variable

    private bool nuageChangeIsRunning;//le processus du changement de skybox est-il à l'oeuvre ?

    private AudioSourceEnvironnements audioSourceEnvironnements;

    public void Awake(){
		lightMapping = GameObject.Find("LightMapping");
		sun = lightMapping.GetComponent<Light>();
		skyboxList[4].SetFloat("_Blend", 0f);
		skyboxList[4].SetFloat("_Fog", 0f);
        audioSourceEnvironnements = GameObject.Find("AudioContainerEnvir").GetComponent<AudioSourceEnvironnements>();
    }


    public void SetUpNuageChange(int ind, float speed, float fogPower,bool changeSoundContext)//fonction qui permet de changer la skybx progressivement et le sol au moment des sprites météo
    {
        blendClamp = skyboxCurrent.GetFloat("_Blend");
        RenderSettings.skybox = skyboxCurrent;//defini une skybox à deux materiaux pour faire un passage d'une skybox à l'autre. C'est la skybox à l'index 0 dans la liste des skybox skiboxList
        if (nuageChangeIsRunning)
        {
            StopAllCoroutines();
        }
        if (blendClamp < 0.5f)
        {
            StartCoroutine(BlendClampPlus(ind, speed));//blend de text primaire à secondaire
        }
        else {
            StartCoroutine(BlendClampMoins(ind, speed));//blend de text secondaire à primaire
        }
       StartCoroutine(SetFogIntensity(fogPower));//lance la fonction qui définie l'intensité du brouillard
       StartCoroutine(SetSunRotation(ind));//lance la fonction qui définie la rotation du soleil
       StartCoroutine(SetSunIntensity(ind, speed));//lance la fonction qui définie l'intensitée du soleil
        if (changeSoundContext)
        {
            AudioClip setThisClip = audioSourceEnvironnements.wheatherClipList[ind];
            AudioSource audioCurrent = audioSourceEnvironnements.AddAudio(setThisClip , true, false, 0f, 1, true);//ajoute l'audioSource sur 1 frame
            if(audioCurrent != null)
            {
                StartCoroutine(audioSourceEnvironnements.VolumePlus(audioCurrent, 0.13f, 0.13f, 0.1f, false));
            }
        }
    }

    public IEnumerator SetSunIntensity(int indexSky, float speedd)//fonction qui définie l'intensitée du soleil
    {
        float to = sunIntensityList[indexSky];
        float distance = 10f;
        while (distance > 0.1f)
        {
            distance = Mathf.Abs(sun.intensity - to);
            sun.intensity = Mathf.Lerp(sun.intensity, to, Time.deltaTime * speedd);
            yield return null;
        }
    }

    public IEnumerator SetSunRotation(int indexSky)//fonction qui définie la rotation du soleil
    {
        Vector3 to = new Vector3(sunRotationList[indexSky], sun.transform.localEulerAngles.y, sun.transform.localEulerAngles.z);
        float distance = 10.0f;
        while (distance > 2.0f)
        {
            distance = Vector3.Distance(sun.transform.localEulerAngles, to);
            sun.transform.localEulerAngles = Vector3.Lerp(sun.transform.localEulerAngles, to, Time.deltaTime);
            yield return null;
        }
    }

    public IEnumerator SetFogIntensity(float intense)//fonction qui définie l'intensité du brouillard
    {
        float fogIntensity = skyboxCurrent.GetFloat("_Fog"); //On récupére le fog sur la skybox qui permet le blend
        float min = new float();
        float max = new float();
        float timeToAdd = new float();
        if (fogIntensity < intense)//on compare l'intesité du fog actuel avec celui qu'on vise
        {
            timeToAdd = +Time.deltaTime;
            min = fogIntensity;
            max = intense;
        }
        else {
            timeToAdd = -Time.deltaTime;
            min = intense;//inverse swap
            max = fogIntensity;//inverse swap
        }
        while (fogIntensity != intense)
        {
            fogIntensity += timeToAdd; 
            fogIntensity = Mathf.Clamp(fogIntensity, min, max);
            skyboxCurrent.SetFloat("_Fog", fogIntensity);
            yield return null;
        }
    }

    public IEnumerator BlendClampPlus(int indexx, float speedd)//blend de text primaire à secondaire
    {
        nuageChangeIsRunning = true;
        Material newNuage = skyboxList[indexx];
        skyboxCurrent.SetTexture("_FrontTex2", GetSkyboxTexture(newNuage)[0]);
        skyboxCurrent.SetTexture("_BackTex2", GetSkyboxTexture(newNuage)[1]);
        skyboxCurrent.SetTexture("_LeftTex2", GetSkyboxTexture(newNuage)[2]);
        skyboxCurrent.SetTexture("_RightTex2", GetSkyboxTexture(newNuage)[3]);
        skyboxCurrent.SetTexture("_UpTex2", GetSkyboxTexture(newNuage)[4]);
        skyboxCurrent.SetTexture("_DownTex2", GetSkyboxTexture(newNuage)[5]);
        while (blendClamp < 1.0f)
        {
            blendClamp += Time.deltaTime * speedd;
            blendClamp = Mathf.Clamp(blendClamp, 0f, 1f);
            skyboxCurrent.SetFloat("_Blend", blendClamp);
            yield return null;
        }
        nuageChangeIsRunning = false;
    }

    public IEnumerator BlendClampMoins(int indexx, float speedd)//blend de text secondaire à primaire
    {
        nuageChangeIsRunning = true;
        Material newNuage = skyboxList[indexx];
        skyboxCurrent.SetTexture("_FrontTex", GetSkyboxTexture(newNuage)[0]);
        skyboxCurrent.SetTexture("_BackTex", GetSkyboxTexture(newNuage)[1]);
        skyboxCurrent.SetTexture("_LeftTex", GetSkyboxTexture(newNuage)[2]);
        skyboxCurrent.SetTexture("_RightTex", GetSkyboxTexture(newNuage)[3]);
        skyboxCurrent.SetTexture("_UpTex", GetSkyboxTexture(newNuage)[4]);
        skyboxCurrent.SetTexture("_DownTex", GetSkyboxTexture(newNuage)[5]);
        while (blendClamp > 0.0f)
        { 
            blendClamp -= Time.deltaTime * speedd;
            blendClamp = Mathf.Clamp(blendClamp, 0f, 1f);
            skyboxCurrent.SetFloat("_Blend", blendClamp);
            yield return null;
        }
        nuageChangeIsRunning = false;
    }

    public List<Texture> GetSkyboxTexture(Material skyboxConcern)//permet de prendre les texture de la skybox qui nous intéresse
    {
        List<Texture> skyTextures = new List<Texture>();
        for(int i = 0; i < 6; i++)
        {           
            skyTextures.Add(skyboxConcern.GetTexture("_FrontTex"));
            skyTextures.Add(skyboxConcern.GetTexture("_BackTex"));
            skyTextures.Add(skyboxConcern.GetTexture("_LeftTex"));
            skyTextures.Add(skyboxConcern.GetTexture("_RightTex"));
            skyTextures.Add(skyboxConcern.GetTexture("_UpTex"));
            skyTextures.Add(skyboxConcern.GetTexture("_DownTex"));
        }
        return skyTextures;

    }

    public void Start(){
        skyboxCurrent.SetFloat("_Blend",1.0f);
        nuageChangeIsRunning = false;
        idealSunRotation = 75f;
        blendFactor = 0f;
        blendClamp = 1.0f;
		lightMappingDefaultIntensity = sun.intensity;//valeur par défaut de l'intensité de la lumière
        //SetUpNuageChange(0, 1.5f,0.5f,72f);//fonction qui permet de changer la skybx progressivement et afficher la skybox par défaut de départ
    }
}
