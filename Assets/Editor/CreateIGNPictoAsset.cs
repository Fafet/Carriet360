﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System;

public class CreateIGNPictoAsset{

[MenuItem("Custom/ScriptableObject/Create PictoDictionary ")]
	public static void CreateMyAsset()
    {
		List<PictoDictionary> content = new List<PictoDictionary>();
		PictoDictionaryHolder  asset = new PictoDictionaryHolder (content);
		AssetDatabase.CreateAsset(asset, "Assets/Resources/PictoDictionaryDatabase.asset");
		AssetDatabase.SaveAssets();
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = asset;
	}
}



