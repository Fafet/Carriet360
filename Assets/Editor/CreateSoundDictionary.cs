﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System;


public class CreateCreateSoundDictionary { 

    [MenuItem("Custom/ScriptableObject/Create SoundDictionary ")]
    public static void CreateMyAsset()
    {
        SoundDictionary content = new SoundDictionary();
        SoundDictionaryHolder asset = new SoundDictionaryHolder(content);
        AssetDatabase.CreateAsset(asset, "Assets/Resources/SoundDictionaryDatabase.asset");
        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
}



