﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/StandartGlow" {

	Properties {
	 	_MainTex ("Base (RGB)", 2D) = "white" {}
		_GlowMap ("Mask texture", 2D) = "white" {}
	 	_Color ("Color", Color) = (1.0,1.0,1.0,1.0)
		_Intensity ("Intensité du glow",  float) = 1.0 //maxi 1, mini 
	}
	SubShader {
	   Tags { "RenderType"="Opaque" }
		//Cull Off
		Pass {
			CGPROGRAM
			#pragma vertex vert
			//#pragma fragment frag
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			sampler2D _GlowMap;
			float _Intensity;//
			float4 _Color;

			struct vertexInput {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};
			
			struct vertexOutput {
				float4 pos :SV_POSITION;
				float4 col :TEXCOORD0;
				float3 worldPos :TEXCOORD1;	
			};

				vertexOutput vert(vertexInput input){
					vertexOutput output;
					float4x4 worldObjectPos = unity_ObjectToWorld;
					output.worldPos = mul(worldObjectPos, input.vertex).xyz;
					output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
					output.col = ComputeScreenPos(output.pos);

					
				return output;		
				}
				
				half4 frag(vertexOutput input):COLOR{
					return input.col;
				}
			ENDCG
		} 
	}
}

