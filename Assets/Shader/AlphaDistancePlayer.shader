﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/AlphaDistancePlayer" {
	Properties{

		_MainTex("Texture", 2D) = "white" {}
		_Point("Player pos in world space", Vector) = (0., 0., 0., 1.0)

	}
		SubShader{
			Tags{ "RenderType" = "Opaque" "LightMode" = "ForwardBase" }
			//LOD 300

			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha

			Pass{
			CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"


		float3 _Point;
		sampler2D _MainTex;
		//float4 _MainTex_ST;

		struct appdata_t {
			float4 vertex : POSITION;
			float4 texcoord : TEXCOORD0;
		};

		struct v2f {
			float4 vertex : SV_POSITION;
			float4 texcoord : TEXCOORD0;
			float4 texcoord2 : TEXCOORD1;
		};
	//Vertex modifier function
		v2f vert(appdata_t v)
		{
			v2f o;
			o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
			//TRANSFORM_TEX(v.texcoord, _MainTex);
			o.texcoord = v.texcoord;
			o.texcoord2 = mul(unity_ObjectToWorld, v.vertex);
			return o;
		}


		fixed4 frag(v2f i) : Color
		{
			float distan =abs( distance(_WorldSpaceCameraPos, i.texcoord2)/5000);
		float4 col = tex2D(_MainTex,  i.texcoord.xy);
		float4 result = fixed4(col.r, col.g, col.b, distan);
			return result;
		}
			ENDCG
		}
		}
		FallBack "Diffuse"
}
