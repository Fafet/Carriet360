﻿Shader "Custom/DepthGrayscale" {

SubShader {
Tags { "RenderType"="Opaque" }
Pass{
CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

sampler2D _CameraDepthTexture;

struct v2f {
   float4 pos : SV_POSITION;
   float4 scrPos:TEXCOORD1;
};

//Our Vertex Shader
v2f vert (appdata_base v){
   v2f o;
   o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
   o.scrPos=ComputeScreenPos(o.pos);
  // o.scrPos.y = 1 - o.scrPos.y;
   return o;
}

//Our Fragment Shader
half4 frag (v2f i) : COLOR{
		
   //extract the value of depth for each screen position from _CameraDepthExture
   float depthValue = Linear01Depth (tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.scrPos)).r);

		//greyscale render of depth texture:
			half4 depth;			
			depth.r = depthValue;
			depth.g = depthValue;
			depth.b = depthValue;
			depth.a = 1;
			return depth;
		}
		ENDCG
	} 
	}
	FallBack "Diffuse"
}
