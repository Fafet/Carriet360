﻿Shader "Custom/TwoTexMask" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_MaskTex ("Mask texture", 2D) = "white" {}
		_maskBlend ("Mask blending", Float) = 0.5
		_TintColor ("TintColor", Color) = (0.0,0.0,0.0,0.0)

	}
    SubShader {
      Tags { "RenderType" = "Opaque" }
      CGPROGRAM
      #pragma surface surf Standard 
       #include "UnityCG.cginc"
    

      struct Input {
          float2 uv_MainTex;
          float2 uv_MaskTex;
          float4 screenPos;
      };
      
      sampler2D _MainTex;
      sampler2D _MaskTex;
      float4 _TintColor;
      
      void surf (Input IN, inout SurfaceOutputStandard o) {
          float4 t = tex2D (_MainTex, IN.uv_MainTex);//définition de la texture principale
          float2 screenUV = IN.uv_MaskTex;//définaition de l'espace UV de la seconde texture afin de la déformer
          screenUV += cos( _Time[1]*0.03);//calcul de la déformation
          float4 u = tex2D (_MaskTex, screenUV);//application de la déformation 
          float4 y =  u*(1-t.a)+(t * (t.a));// texture principale moins le canal alpha de cette texture * seconde texture
          y = y  ;//on ajoute au résultat la texture principale 
          o.Albedo = y*_TintColor;          
          //o.Albedo += u.rgb;
          o.Alpha = u.a;          
      }
      ENDCG
    }
    Fallback "Diffuse"

}
