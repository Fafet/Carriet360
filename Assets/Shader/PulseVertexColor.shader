﻿Shader "Custom/PulseVertexColor" {

		Properties{
			_MainTex("Albedo (RGB)", 2D) = "white" {}
			_Glossiness("Smoothness", Range(0,1)) = 0.5
			_Metallic("Metallic", Range(0,1)) = 0.0
		}
			SubShader{
			Tags{ "RenderType" = "Opaque" }

			Cull Off
			LOD 200

			CGPROGRAM
#pragma surface surf Standard vertex:vert
#include "UnityCG.cginc"

			// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0
				sampler2D _MainTex;
		half _Glossiness;
		half _Metallic;


		struct Input {
			float2 uv_MainTex;
			float3 worldPos;
			float4 _TintColor;
		};

		//Vertex modifier function
		void vert(inout appdata_full v) {
			v.vertex.xyz += v.normal* (cos(_Time[1])/8+0.12);
		}
		//surface
		void surf(Input IN, inout SurfaceOutputStandard o) {
			float distan = cos(_Time[1]);
			float4 b = float4((1-distan),0,(distan), 1);
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) ;
			o.Albedo = c.rgb;;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
		}
		ENDCG
		}
			Fallback "Diffuse"

	}

