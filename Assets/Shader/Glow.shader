﻿Shader "Custom/Glow" {
properties{
	 _MainTex ("Base (RGB)", 2D) = "white" {}
	 _Color ("Color tint glow", Color) = (1.0,0.0,0.0,1.0)
	 	_FadeDistance ("Fade Distance", float) = 30.0
		_Intensity ("Intensité du glow",  float) = 1.0 //maxi 1, mini 
		_DistanceFromCamera ("Distance de la Camera", float) = 1.0
}
   SubShader {
   	Tags { "RenderType"="Transparent" }
      Pass {
         CGPROGRAM

         #pragma vertex vert
         #pragma fragment frag

		sampler2D _MainTex;
		float4 _Color;
		
  		struct vertexInput{
  			float4 vertex:POSITION;
  		};
  		struct vertexOutput{
  			float4 pos : SV_POSITION;
   		};
  		
	  		vertexOutput vert(vertexInput input){///////////////////////
	  			vertexOutput output;	  			
	  			output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
	  			return output;
	  		}
   
			float4 frag(vertexOutput input):COLOR{///////////////////////////
			_Color.a += cos(_Time[1]*0.1);
			_Color = saturate(float4(_Color.r,_Color.g,_Color.b,_Color.a));//calcul de la déformation				
			return _Color;//
			}
         ENDCG
      }
   }
}