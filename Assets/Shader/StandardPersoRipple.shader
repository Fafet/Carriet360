﻿Shader "Custom/StandardPersoRipple" {

	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo", 2D) = "white" {}
		_Scale ("Scale", Range(0.5,500.0)) = 3.0
        _Speed ("Speed", Range(-50,50.0)) = 1.0
		_Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5

	}

	SubShader {	
		//  Deferred pass


			Tags {"RenderType"="Transparent" }
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 300
        Cull Off
        CGPROGRAM
        #pragma surface surf Lambert keepalpha 
        #include "UnityCG.cginc"
        
        half4 _Color;
        half _Scale;
        half _Speed;
        sampler2D _MainTex;
        
        struct Input {
            float2 uv_MainTex;
        };

        void surf (Input IN, inout SurfaceOutput o) {
            half2 uv = (IN.uv_MainTex - 0.5) * _Scale;
            half r = sqrt (uv.x*uv.x + uv.y*uv.y);
            half z = sin (r+_Time[1]*_Speed) / r;            
            half3 text = _Color.rgb * tex2D (_MainTex, IN.uv_MainTex+z).rgb;
            o.Albedo = text;
            o.Alpha = text.r > 0.4? 0.3 : 1.0;
            //o.Normal = (z, z, z);
           
        }
        ENDCG
    } 
	FallBack "VertexLit"
	

}
