﻿Shader "Custom/TwoTexBlend" {
		Properties{
			_MainTex("Albedo (RGB)", 2D) = "white" {}
			_MainTex2("Albedo2 (RGB)", 2D) = "white" {}			
			_Glossiness("Smoothness", Range(0,1)) = 0.5
			_Metallic("Metallic", Range(0,1)) = 0.0
			_Blend("Blend", Range(0.0,1.0)) = 0.5
		}
			SubShader{
			Tags{ "RenderType" = "Opaque" }
			LOD 200

			Cull Off
			CGPROGRAM
#pragma surface surf Standard fullforwardshadows
#pragma target 3.0

			sampler2D _MainTex;
			sampler2D _MainTex2;
			fixed _Blend;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;

		void surf(Input IN, inout SurfaceOutputStandard o) {
			float4 t = tex2D(_MainTex, IN.uv_MainTex);//définition de la texture principale
			float4 y = tex2D(_MainTex2, IN.uv_MainTex);//définition de la texture secondaire
			fixed4 tex = lerp(t, y, _Blend);
			o.Albedo = t;
			o.Smoothness = _Glossiness;
			o.Metallic = _Metallic;
		}
		ENDCG
		}
			FallBack "Diffuse"
	}

