﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Custom/FogVertex" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}

	CGINCLUDE
		//#pragma surface surf Standard fullforwardshadows

		#include "UnityCG.cginc"

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		float4 _Color;

		struct vertexInput{
			float4 vertex : POSITION;
			float4 texcoord : TEXCOORD0;
			float4 color : COLOR;
			float3 normal : NORMAL;
		};
		
		struct vertexOutput{
			float4 pos : SV_POSITION;
			float4 col : TEXCOORD0;
			float3 viewDir : TEXCOORD1;
			float3 norm : TEXCOORD2;
			float4 vertexCol : COLOR;
		};		
		half _Glossiness;
		half _Metallic;			
	ENDCG
	
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		Pass{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				
				vertexOutput vert (vertexInput input){
					vertexOutput output;//fonction vertexOutput avec comme nom de variable output
					output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
					output.col = input.texcoord;
					return output;
				}
				
				half4 frag(vertexOutput input):COLOR{
					return tex2D(_MainTex, input.col.xy);
								
				}				

			ENDCG
		}
		Pass{
		Blend SrcAlpha OneMinusSrcAlpha 

			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				
				vertexOutput vert (vertexInput input){
					vertexOutput output;//fonction vertexOutput avec comme nom de variable output
					output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
					output.norm = normalize(mul(float4(input.normal,0.0),unity_WorldToObject).xyz);//normales normalisées
					output.viewDir = normalize(_WorldSpaceCameraPos - mul(unity_ObjectToWorld, input.vertex ).xyz);
					float3 normDirection = output.norm;
					float3 viewDirection = output.viewDir;
					float dist = length(output.pos - _WorldSpaceCameraPos);
					float normDistance = 1/dist;
					output.col = input.texcoord;
					output.vertexCol = input.color;
					output.pos += (normDirection*output.vertexCol.r+normDistance,1.0);
					return output;
				}
				
				half4 frag(vertexOutput input):COLOR{
					float3 normDirection = normalize(input.norm);
					float3 viewDirection = normalize(input.viewDir);
					float dist = length(input.pos - _WorldSpaceCameraPos);
					float normDistance = 1/dist;
					float3 result = (_Color*(normDirection)*input.vertexCol.r+normDistance);
					
					return float4(result, input.vertexCol.r*normDistance);			
				}
				

			ENDCG
		}
	} 
	FallBack "Diffuse"
}
