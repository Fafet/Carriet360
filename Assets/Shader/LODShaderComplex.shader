﻿Shader "Custom/LODShaderComplex" {
Properties{
	_Color("Color", Color) = (1,1,1,1)
	_MainTex("Albedo (RGB)", 2D) = "white" {}
_Glossiness("Smoothness", Range(0,1)) = 0.5
_Metallic("Metallic", Range(0,1)) = 0.0
_BumpScale("Scale", Float) = 1.0
_BumpMap("Normal Map", 2D) = "bump" {}
_OcclusionStrength("Strength", Range(0.0, 1.0)) = 1.0
_OcclusionMap("Occlusion", 2D) = "white" {}
_DetailAlbedoMap("Detail Albedo x2", 2D) = "grey" {}
_DetailNormalMapScale("Scale", Float) = 1.0
_DetailNormalMap("Normal Map", 2D) = "bump" {}

[Enum(UV0, 0, UV1, 1)] _UVSec("UV Set for secondary textures", Float) = 0
}
SubShader{
	Tags{ "RenderType" = "Transparent" "Queue" = "Transparent" }
	LOD 200

	CGPROGRAM

#pragma multi_compile _ LOD_FADE_CROSSFADE

#pragma surface surf Standard
#pragma target 3.0


	sampler2D _MainTex;
sampler2D _BumpMap;
sampler2D _OcclusionMap;
sampler2D _DetailAlbedoMap;
sampler2D _DetailNormalMap;
struct Input {
	float2 uv_MainTex;
	float2 uv_BumpMap;
	float2 uv_OcclusionMap;
	float2 uv_DetailAlbedoMap;
	float2 uv_DetailNormalMap;
};

half _Glossiness;
half _Metallic;
half _BumpScale;
half _OcclusionStrength;
half _DetailNormalMapScale;
fixed4 _Color;

void surf(Input IN, inout SurfaceOutputStandard o) {
	// Albedo comes from a texture tinted by color
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
	o.Albedo = c.rgb;
	o.Albedo *= tex2D(_DetailAlbedoMap, IN.uv_DetailAlbedoMap).rgb * 2;
	o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
	o.Normal *= UnpackNormal(tex2D(_DetailNormalMap, IN.uv_DetailNormalMap));
	// Metallic and smoothness come from slider variables
	o.Metallic = _Metallic;
	o.Smoothness = _Glossiness;
	o.Occlusion = _OcclusionStrength;
#ifdef LOD_FADE_CROSSFADE
	o.Alpha = unity_LODFade.x;
#endif
}
ENDCG
}
FallBack "Diffuse"
}

