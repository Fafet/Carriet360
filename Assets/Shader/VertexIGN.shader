﻿Shader "Custom/VertexIGN" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		//_Alpha ("Alpha (A)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Amount ("Height Adjustment", float) = 1.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows  vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		//sampler2D _Alpha;

		struct Input {
			float2 uv_MainTex;
			//float2 uv_Alpha;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float _Amount;
		//Vertex modifier function
		void vert (inout appdata_full v) {
			v.vertex.z +=  cos( _Time[1]+v.color.r * _Amount);
			//v.vertex.z +=  cos( _Time[1]+v.color.b * _Amount);

		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			//fixed4 alph = tex2D (_Alpha, IN.uv_Alpha);
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;// - alph.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
