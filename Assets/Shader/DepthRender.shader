﻿Shader "Custom/DephRender" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
   SubShader {
   	Tags { "RenderType"="Opaque" }
      Pass {
         CGPROGRAM

         #pragma vertex vert
         #pragma fragment frag
         #include "UnityCG.cginc"
         
		sampler2D _MainTex;

  		struct vertexInput{
  			float4 vertex:POSITION;
  			float4 texcoord : TEXCOORD0;
  		};
  		struct vertexOutput{
  			float4 pos : SV_POSITION;
  			float4 col : TEXCOORD0;
   		};
  		
	  		vertexOutput vert(vertexInput input){///////////////////////
	  			vertexOutput output;
	  			
	  			output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
	  			
	  			UNITY_TRANSFER_DEPTH(output.col);
	  			return output;
	  		}
   
			float4 frag(vertexOutput input):COLOR{///////////////////////////
				UNITY_OUTPUT_DEPTH(input.col);
			}

         ENDCG
      }
   }
}