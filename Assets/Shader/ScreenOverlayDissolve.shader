﻿Shader "Custom/ScreenOverlayDissolve" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_StepX ("_StepX", float) = 0.0
		_StepY ("_StepY", float) = 0.0
		_X("X", float) =0.0
		_Y("Y", float) =0.0
		_Width("Width", float) = 128
		_Height("Height", float) = 128
		_Reverse("Reverse",int) = 1
	}
	SubShader {
	 Tags {"Queue"="AlphaTest"  "RenderType"="Cutout"}
		Pass {
		   ZWrite Off
    		//Blend SrcColor  DstColor 
    		//Blend SrcAlpha  OneMinusDstColor       
    		  Blend SrcAlpha OneMinusSrcAlpha 
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc" 
		uniform sampler2D _MainTex;
		uniform sampler2D _AlphaTex;
		uniform float _StepX;
		uniform float _StepY;
		uniform float4 _Color;
		uniform float _X;
		uniform float _Y;
		uniform float _Width;
		uniform float _Height;
		uniform int _Reverse;
		//uniform float4 _screenParams;
		
		struct vertexInput {
			float4 vertex : POSITION;
			float4 texcoord : TEXCOORD0;
		};
		struct vertexOutput{
			float4 pos : SV_POSITION;
			float4 tex : TEXCOORD0;
		};
		
		vertexOutput vert(vertexInput input){
			vertexOutput output;
			float2 rasterPosition = float2( _X + _ScreenParams.x / 2.0 
               + _Width*(input.vertex.x),
				_Y + _ScreenParams.y / 2.0 
               + _Height*(input.vertex.y));
	  		output.pos = float4(
               2.0 * rasterPosition.x / _ScreenParams.x - 1.0,
               2.0 * rasterPosition.y / _ScreenParams.y - 1.0,
               _ProjectionParams.y, // near plane is at -1.0 or at 0.0
               1.0);
               output.tex = float4(input.vertex.x + 0.5, 
               input.vertex.y + 0.5, 0.0, 0.0);
	  		return output;
		}

		float4 frag(vertexOutput input):COLOR{
			float xFactor = clamp(_Reverse,_StepX*2.0,1.0);
			float yFactor = clamp(_Reverse,_StepX*2.0,1.0);
			float4 couleur = tex2D(_MainTex,input.tex.xy);	
				if(input.tex.x>((xFactor-_StepX)-0.1)){
					_Color.a = _Color.a*step(input.tex.x,(xFactor-_StepX));
					_Color.a = _Color.a*step(input.tex.y,(yFactor-_StepY));
				}	
			float4 result = couleur*_Color.a;
			return result;
		}
		ENDCG
		}
	}
}
