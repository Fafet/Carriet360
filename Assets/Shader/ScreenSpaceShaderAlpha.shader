﻿Shader "Custom/ScreenSpaceShaderAlpha"
{

Properties
{
_MainTex("Texture", 2D) = "white" {}
_AlphaBlend("_AlphaBlend", Range(0.0,1.0)) = 1
}

SubShader{
	Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
	LOD 300
	Cull Off
	Blend One OneMinusSrcAlpha
	CGPROGRAM
#pragma surface surf Standard  alpha:blend


struct Input {
float2 uv_MainTex;
float4 screenPos;
};
sampler2D _MainTex;
float _AlphaBlend;

void surf(Input IN, inout SurfaceOutputStandard o) {
float2 screenUV = IN.screenPos.xy / IN.screenPos.w;
float distan = cos(_Time[1]);
screenUV *= float2(distan, distan);
float4 c = tex2D(_MainTex, screenUV);
o.Albedo = c.rgb ;
o.Alpha = _AlphaBlend;
}
ENDCG
}
Fallback "Diffuse"
}

