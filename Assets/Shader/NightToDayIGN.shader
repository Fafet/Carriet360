﻿Shader "Custom/NightToDayIGN" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_OccludeMap ("Mask texture", 2D) = "white" {}
		_Intensity("Intensité", float) = 1.0
	}
SubShader {
   		Tags { "RenderType"="Opaque" }
		LOD 300
		
	pass{//Pass CONTOUR

		CGPROGRAM

		#pragma vertex vert
		#pragma fragment frag
		
			sampler2D _MainTex;
			sampler2D _OccludeMap;
			float4 _Color;
			float _Intensity;

			struct vertexInput{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};
			
			struct vertexOutput{
				float4 pos : SV_POSITION;
				float4 textures : TEXCOORD0;
			};
			
				vertexOutput vert(vertexInput input){
					vertexOutput output;
					output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
					output.textures = input.texcoord;
					return output;
				}
				
			//glowing. Glowing color déterminé par le shader glowing avec le parametre _Color sur le script CameraManager (cameraFPS)
			half4 frag(vertexOutput input):COLOR{
				float4 t = tex2D(_MainTex, input.textures.xy);
				float4 p = tex2D(_OccludeMap, input.textures.xy);
				//p.a = 1 - (p.r*p.g*p.b);//alfa egale au blanc de l'image inversé
				//float4 lighter = max(t,p);
				_Intensity +=  cos( _Time[1]*0.1);
				float4 result = t*(1-p.a)+(p*_Intensity);
				return result;
				}
		ENDCG
	}
	}
}
