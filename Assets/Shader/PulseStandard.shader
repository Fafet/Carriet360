﻿Shader "Custom/PulseStandard" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_MaskTex("Mask texture", 2D) = "white" {}
		_Noise("Mask texture", 2D) = "white" {}
		_TintColor("TintColor", Color) = (0.0,0.0,0.0,0.0)
		_Point("Player pos in world space", Vector) = (0., 0., 0., 1.0)
		_PulseColor("Pulse factor by canal", Color) = (0.0,0.0,0.0,0.0) //maxi 1
		_BumpMap("Bumpmap", 2D) = "bump" {}
		_Speed("Vitesse Player", float) = 0.01
	}
		SubShader{
			Tags{ "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Lambert vertex:vert
#pragma target 3.0
#include "UnityCG.cginc"

	sampler2D _MainTex;
	sampler2D _MaskTex;
	float4 _TintColor;
	float4 _PulseColor;
	float3 _Point;
	sampler2D _BumpMap;
	sampler2D _Noise;
	float _Speed;


	struct Input {
		float2 uv_MainTex;
		float2 uv_MaskTex;
		float2 uv_BumpMap;
		float2 uv_Noise;
		float3 worldPos;
		float4 _PulseColor;
		
	};

	void vert(inout appdata_full v) 
	{
		//UNITY_INITIALIZE_OUTPUT(Input, o);
		//dist = mul(_Object2World, v.vertex));
		v.vertex.z += 0.5;
	}
	void surf(Input IN, inout SurfaceOutput o) {
		float4 t = tex2D(_MainTex, IN.uv_MainTex);//définition de la texture principale
		float2 screenUV = IN.uv_MaskTex;//définaition de l'espace UV de la seconde texture afin de la déformer
		float2 screenUV2 = IN.uv_Noise;
		float pulse = _PulseColor.r;
		screenUV += cos(_Time[1] * 0.03);//calcul de la déformation
		screenUV2 += _Speed;//calcul de la déformation
		float4 u = tex2D(_MaskTex, screenUV);//application de la déformation 
		float4 n = tex2D(_Noise, screenUV2);
		float4 y = u*(1 - t.a) + (t * (t.a));// texture principale moins le canal alpha de cette texture * seconde texture
		y = y;//on ajoute au résultat la texture principale 
		//float distan = distance(_WorldSpaceCameraPos, IN.worldPos);//depuis la camera
		float distan = abs(distance(_Point, IN.worldPos)/700);//distance maxi = 1
		//float3 wired = (clamp(_PulseColor.r, _TintColor.r, min(_PulseColor.r*(1 - distan), 1.0) - _TintColor.r), clamp(_PulseColor.g, _TintColor.g, min(_PulseColor.g*(1 - distan), 1.0) - _TintColor.g) , clamp(_PulseColor.b, _TintColor.b, min(_PulseColor.b*(1 - distan), 1.0) - _TintColor.b));
		
		float3 wired = _PulseColor*(1.0-distan);		
		float yFactor = step(distan, _PulseColor.r);// 0 si inférieur distance. else 1
		float sparkle = 1.0-(step(0.02*(_Speed*5.0), distan));
		//sparkle = exp(sparkle);
		//float wiredFactor = step(pulse, distan) * step(distan-1, pulse);
		float tintFactor = step(pulse, distan);
		o.Albedo = (y*yFactor) + ((_TintColor.rgb*1.5)*tintFactor) + (n.rgb *sparkle);
		o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
		//o.Metallic = _Metallic;
	}
	ENDCG
	}
		Fallback "Diffuse"

}
