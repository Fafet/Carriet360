﻿Shader "Custom/StandardShaderTwikable" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_Blend("Blend", Range(0.0,1.0)) = 0
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_MainTex2("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_BumpMap("Normal Map", 2D) = "bump" {}
		_BumpScale("Scale", Float) = 1.0
		_ParallaxMap("Height Map", 2D) = "black" {}
		_Parallax("Height Scale", Range(0.005, 0.08)) = 0.02
		_OcclusionStrength("Strength", Range(0.0, 1.0)) = 1.0
		_OcclusionMap("Occlusion", 2D) = "white" {}
		_DetailAlbedoMap("Detail Albedo x2", 2D) = "grey" {}
		_DetailNormalMapScale("Scale", Float) = 1.0
		_DetailNormalMap("Normal Map", 2D) = "bump" {}

	[Enum(UV0, 0, UV1, 1)] _UVSec("UV Set for secondary textures", Float) = 0
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

	sampler2D _WhiteTex;
	sampler2D _MainTex;
	sampler2D _MainTex2;
	sampler2D _BumpMap;
	sampler2D _OcclusionMap;
	sampler2D _DetailAlbedoMap;
	sampler2D _DetailNormalMap;
	sampler2D _ParallaxMap;
	struct Input {
		float2 uv_MainTex: TEXCOORD0;
		float2 uv_BumpMap;
		float2 uv_OcclusionMap : TEXCOORD1;
		float2 uv_DetailAlbedoMap;
		float2 uv_DetailNormalMap;
		float2 uv__ParallaxMap;
		float3 viewDir;
	};

	half _Glossiness;
	half _Metallic;
	half _BumpScale;
	half _OcclusionStrength;
	half _DetailNormalMapScale;
	half _Parallax;
	fixed4 _Color;
	fixed _Blend;

	void surf(Input IN, inout SurfaceOutputStandard o) {
		// Albedo comes from a texture tinted by color
		float4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		float4 d = tex2D(_MainTex2, IN.uv_MainTex) * _Color;
		half h = tex2D(_ParallaxMap, IN.uv_BumpMap).w;
		float2 offset = ParallaxOffset(h, _Parallax, IN.viewDir);
		fixed4 ao = tex2D(_OcclusionMap, IN.uv_MainTex).r;
		fixed4 w = (0, 0, 0, 1);
		ao.rgb = (ao.rgb+0.5)* _OcclusionStrength;	
		IN.uv_MainTex += offset;
		IN.uv_BumpMap += offset;
		fixed4 tex = lerp(c, d, _Blend);
		o.Albedo = lerp(tex.rgb, w , _Blend);
		o.Albedo *= lerp( tex2D(_DetailAlbedoMap, IN.uv_DetailAlbedoMap).rgb * 2, w, _Blend);
		o.Normal = lerp( UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap))*_BumpScale , w, _Blend);
		o.Normal *= lerp(UnpackNormal(tex2D(_DetailNormalMap, IN.uv_DetailNormalMap))*_DetailNormalMapScale, w, _Blend);
		// Metallic and smoothness come from slider variables
		o.Metallic = _Metallic;
		o.Smoothness = _Glossiness;
		o.Occlusion = ao;
#ifdef LOD_FADE_CROSSFADE
		o.Alpha = unity_LODFade.x;
#endif
	}
	ENDCG
	}
		FallBack "Diffuse"
}


