﻿Shader "Custom/OccludeOverlay" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_OccludeMap ("Mask texture", 2D) = "white" {}
		_Intensity("Intensité", float) = 1.0
	}
SubShader {
			Tags{ "LightMode" = "ForwardBase" "RenderType" = "Transparent" "ForceNoShadowCasting" = "true" "IgnoreProjector" = "true" }
		LOD 300
		
	pass{//Pass CONTOUR

		CGPROGRAM

		#pragma vertex vert
		#pragma fragment frag
		
			sampler2D _MainTex;
			sampler2D _OccludeMap;
			float4 _Color;
			float _Intensity;

			struct vertexInput{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};
			
			struct vertexOutput{
				float4 pos : SV_POSITION;
				float4 textures : TEXCOORD0;
			};
			
				vertexOutput vert(vertexInput input){
					vertexOutput output;
					output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
					output.textures = input.texcoord;
					return output;
				}
				//Permet de faire un contour glow autour d'un object
				half4 frag(vertexOutput input):COLOR{
				float4 t = tex2D(_MainTex, input.textures.xy);
				float4 p = tex2D(_OccludeMap, input.textures.xy);
				float4 mask = p - p.a;
				float alph = mask.r> 0.2 ? 0.0 : 0.5;				
				mask.a = alph;
				return t+(t*mask.a);
				}
		ENDCG
	}
		pass{//passe GLOW

		CGPROGRAM
		
		#pragma vertex vert
		#pragma fragment frag

			sampler2D _MainTex;
			sampler2D _OccludeMap;
			float _Intensity;
			
			struct vertexInput{
				float4 vertex :POSITION;
				float4 texcoord : TEXCOORD0;
			};
			
			struct vertexOutput{
				float4 pos : SV_POSITION;
				float4 textures : TEXCOORD0;
			};
			
				vertexOutput vert(vertexInput input){
					vertexOutput output;
					output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
					output.textures = input.texcoord;
					return output;
				}
			//glowing. Glowing color déterminé par le shader glowing avec le parametre _Color sur le script CameraManager (cameraFPS)
			half4 frag(vertexOutput input):COLOR{
				float4 t = tex2D(_MainTex, input.textures.xy);
				float4 p = tex2D(_OccludeMap, input.textures.xy);
				//p.a = 1 - (p.r*p.g*p.b);//alfa egale au blanc de l'image inversé
				//float4 lighter = max(t,p);
				float4 result = t*(1-p.a)+(p*_Intensity);
				return result;
			}
		ENDCG
	}	

}
}