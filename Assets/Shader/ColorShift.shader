﻿Shader "Custom/ColorShift" {//color shift and frac image (cut in slices)
	Properties{
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			Cull Off
			CGPROGRAM
			#pragma surface surf Standard fullforwardshadows
			#pragma target 3.0

			sampler2D _MainTex;

			struct Input {
				float2 uv_MainTex;
				float3 worldPos;

			};

			half _Glossiness;
			half _Metallic;

			void surf(Input IN, inout SurfaceOutputStandard o) {
				float2 screenUV = IN.uv_MainTex;				
				float y = cos(_Time[1] + 0.5)*0.06;
				screenUV.x += y;
				float4 t = tex2D(_MainTex, screenUV);//définition de la texture principale
				float u = clamp(t.r,y,1.0);
				float4 result = float4(t.r*u,t.g*(1 - u),t.b*u,1.0);
				clip(frac((IN.worldPos.y + IN.worldPos.z*0.1) *2) - 0.5);
				o.Albedo = t;
				o.Metallic = y*2.0;
				o.Smoothness = y;
			}
			ENDCG
		}
	FallBack "Diffuse"
}
