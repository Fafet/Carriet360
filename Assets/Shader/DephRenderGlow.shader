﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/DephRenderGlow" {
//shader qui analyse la Depth texure afin d'afficher du glow vue de loin. La distance du glow se regle avec le slider de fadeDistance
//couplé avec le shader Occluder/Overlay on peut superposer l'effet de glow à l'image final (OnRenderImage sur le script GlowManager)
	Properties {
	 	_FadeDistance ("Fade Distance", float) = 30.0
		_Color ("Color", Color) = (1.0,1.0,1.0,1.0)
		_Intensity ("Intensité du glow",  float) = 1.0 //maxi 1, mini 
		_DistanceFromCamera ("Distance de la Camera", float) = 1.0
	}
	SubShader {
		Tags{ "LightMode" = "ForwardBase" "RenderType" = "Transparent" "ForceNoShadowCasting" = "true" "IgnoreProjector" = "true"}
		ZTest Always Cull Off ZWrite Off Blend Off
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			sampler2D _CameraDepthTexture;
			float _FadeDistance;//30
			float _Intensity;//
			float4 _Color;

			struct vertexInput {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};
			
			struct vertexOutput {
				float4 pos :SV_POSITION;
				float4 col :TEXCOORD0;
				float3 worldPos :TEXCOORD1;	
			};

				vertexOutput vert(vertexInput input){
					vertexOutput output;
					float4x4 worldObjectPos = unity_ObjectToWorld;
					output.worldPos = mul(worldObjectPos, input.vertex).xyz;
					output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
					output.col = ComputeScreenPos(output.pos);

					
				return output;		
				}
				
				half4 frag(vertexOutput input):COLOR{					
					float depthVal = Linear01Depth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(input.col)).r);
					float zPos = input.col.z;					
					float dist = distance(input.worldPos, _WorldSpaceCameraPos);//la distance entre la camera et les vertex de l'objet
					float distLinear = dist/1000;
					float4 result2;
					float occlude = step(zPos,depthVal +_FadeDistance);//occlude est égale à 1 si depthVal est plus grand que zPos sinon il est égal à 0. (si 0 les valeurs du zBuffer ne sont pas changées)//point de passage entre glow et non glow
					float3 intense = clamp(float3(_Color.rgb*_Intensity-distLinear),0.0,1.0);					
					result2 = float4(depthVal*intense.rgb-occlude,1.0);
					result2.a = depthVal>0.001+occlude ? 0.1 : 0.0;//not used at the moment
					//result2.a += (distLinear);
					return result2;
				}
			ENDCG
		} 
	}
}
