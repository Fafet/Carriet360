﻿Shader "Custom/PulseNewtopies" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_TintColor("TintColor", Color) = (0.0,0.0,0.0,0.0)
		_PulseColor("Pulse factor by canal", Color) = (0.0,0.0,0.0,0.0) //maxi 1
	}
		SubShader{
			Tags{ "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Lambert vertex:vert
#pragma target 3.0
#include "UnityCG.cginc"

	sampler2D _MainTex;
	float4 _TintColor;
	float4 _PulseColor;


	struct Input {
		float2 uv_MainTex;
		float3 worldPos;
		float4 _PulseColor;
		
	};

	void vert(inout appdata_full v) 
	{

	}
	void surf(Input IN, inout SurfaceOutput o) {
		float pulse = _PulseColor.r;
		float distan = abs(distance(_WorldSpaceCameraPos, IN.worldPos) / 3000);//depuis la camera
		float4 t = tex2D(_MainTex, IN.uv_MainTex);//définition de la texture principale
		float yFactor = step(distan, pulse);// 0 si inférieur distance. else 1
		float tintFactor = step(pulse, distan);
		o.Albedo = (t*yFactor) + (_TintColor.rgb*tintFactor);
		//o.Metallic = _Metallic;

	}
	ENDCG
	}
		Fallback "Diffuse"

}
