﻿Shader "Custom/ScreenSpaceShader"
{

		Properties
		{
			_MainTex("Texture", 2D) = "white" {}
		_TintColor("TintColor", Color) = (1.0,1.0,1.0,1.0)
		}

			SubShader{
			Tags{"RenderType" = "Opaque" }
			LOD 300
			Cull Off
			CGPROGRAM
#pragma surface surf Standard


		struct Input {
			float2 uv_MainTex;
			float4 screenPos;
		};
		sampler2D _MainTex;
		float4 _TintColor;

		void surf(Input IN, inout SurfaceOutputStandard o) {
			float2 screenUV = IN.screenPos.xy / IN.screenPos.w;
			float distan = cos(_Time[1]);
			screenUV *= float2(distan, distan);
			float4 c = tex2D(_MainTex, screenUV);
			o.Albedo = c.rgb*_TintColor;
		}
		ENDCG
		}
			Fallback "Diffuse"
	}

