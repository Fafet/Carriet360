﻿Shader "Custom/VertexModifier" {
	Properties {

		_MainTex ("Texture", 2D) = "white" {}
		_Amount("Height Adjustment", float) = 1.0

	}
		SubShader{
			Tags{ "RenderType" = "Transparent" "Queue" = "Transparent" }
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
			LOD 200
			
				CGPROGRAM
	   #pragma surface surf Standard alpha:fade  vertex:vert
   #include "UnityCG.cginc"

		sampler2D _MainTex;
	    float _Amount;		
	  
		struct Input {
		float2 uv_MainTex;

		};				
		//Vertex modifier function
		void vert (inout appdata_full v) {
			v.vertex.z += cos(_Time[1] + v.color.r * _Amount);
		}
		//surface shader function
		void surf (Input IN,inout SurfaceOutputStandard o){
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			//fixed4 alph = tex2D (_Alpha, IN.uv_Alpha);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
