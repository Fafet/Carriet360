﻿Shader "Custom/PulseColor"{

	Properties{
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_TintColor("TintColor", Color) = (0.0,0.0,0.0,0.0)
		_PulseColor("Pulse factor by canal", Float) = 0.0 //maxi 1
		_MyVector("Some Vector", Vector) = (0,0,0,0)
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }

		Cull Off
		LOD 200

		CGPROGRAM
#pragma surface surf Standard 
#include "UnityCG.cginc"

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

	half _Glossiness;
	half _Metallic;
	float4 _TintColor;
	float4 _PulseColor;
	float4 _MyVector;


	struct Input {
		float3 worldPos;
		float4 _PulseColor;
		float4 _MyVector;

	};

	void surf(Input IN, inout SurfaceOutputStandard o) {
		float distan = min(abs(distance(_MyVector.rgb , IN.worldPos))/2,1);//depuis la camera
		float4 b = float4(0, (1-_PulseColor.r),0, 0);
		o.Albedo = (_PulseColor*distan)+ (b*distan);
		o.Metallic = _Metallic;
		o.Smoothness = _Glossiness;
	}
	ENDCG
	}
		Fallback "Diffuse"

}

