﻿Shader "Custom/ScreenToBlack" {
		Properties{
			_Color("Color", Color) = (0,0,0,1)
			_StepX("_StepX", float) = 0.0
			_Width("Width", float) = 128
			_Height("Height", float) = 128
		}
			SubShader{
			Tags{ "Queue" = "AlphaTest"  "RenderType" = "Cutout" }
			Pass{
			ZWrite Off
			//Blend SrcColor  DstColor 
			//Blend SrcAlpha  OneMinusDstColor       
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc" 
			uniform float _StepX;
		uniform float4 _Color;
		uniform float _Width;
		uniform float _Height;
		//uniform float4 _screenParams;

		struct vertexInput {
			float4 vertex : POSITION;
			float4 texcoord : TEXCOORD0;
		};
		struct vertexOutput {
			float4 pos : SV_POSITION;
			float4 tex : TEXCOORD0;
		};

		vertexOutput vert(vertexInput input) {
			vertexOutput output;
			float2 rasterPosition = float2(_ScreenParams.x / 2.0
				+ _Width*(input.vertex.x),
				_ScreenParams.y / 2.0
				+ _Height*(input.vertex.y));
			output.pos = float4(
				2.0 * rasterPosition.x / _ScreenParams.x - 1.0,
				2.0 * rasterPosition.y / _ScreenParams.y - 1.0,
				_ProjectionParams.y, // near plane is at -1.0 or at 0.0
				1.0);
			output.tex = float4(input.vertex.x + 0.5,
				input.vertex.y + 0.5, 0.0, 0.0);
			return output;
		}

		float4 frag(vertexOutput input) :COLOR{
			if (input.tex.y > ((1 - _StepX))) {
				_Color.a =  (1 - _StepX);
			}
		return _Color.a*_Color;
		}
			ENDCG
		}
		}
	}
