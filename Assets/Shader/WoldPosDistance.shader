﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Custom/WoldPosDistance" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_MaskTex("Mask texture", 2D) = "white" {}
		_Point("a point in world space", Vector) = (0., 0., 0., 1.0)
		_DistanceNear("threshold distance", Float) = 5.0
		_TintColor("TintColor", Color) = (0.0,0.0,0.0,0.0)
}
   SubShader {
	  Pass { 
			Tags{ "LightMode" = "ForwardBase" "RenderType" = "Opaque"}
			//ZTest Always Cull Off ZWrite On Blend Off
			//Blend SrcAlpha OneMinusSrcAlpha
         CGPROGRAM

         #pragma vertex vert  
         #pragma fragment frag 
 
		sampler2D _MaskTex;
		uniform float4 _MainTex_ST;
		float4 _Point;
		float _DistanceNear;
		float4 _TintColor;

         struct vertexInput {
            float4 vertex : POSITION;
			float4 texcoord : TEXCOORD0;
         };
         struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 position_in_world_space : TEXCOORD0;
			float2 tex1  : TEXCOORD1;
         };

         vertexOutput vert(vertexInput input) 
         {
            vertexOutput output;  
            output.pos =  mul(UNITY_MATRIX_MVP, input.vertex);
            output.position_in_world_space = 
               mul(unity_ObjectToWorld, input.vertex);
               // transformation of input.vertex from object 
               // coordinates to world coordinates;
			_MainTex_ST.xy += cos(_Time[1] * 0.01);//calcul de la déformation;
			_MainTex_ST.zw += cos(_Time[1] * 0.01);//calcul de la déformation
			output.tex1 = input.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;//définaition de l'espace UV de la seconde texture afin de la déformer
            return output;
         }
 
         float4 frag(vertexOutput input) : COLOR 
         {
             float dist = distance(input.position_in_world_space, _WorldSpaceCameraPos);
               // computes the distance between the fragment position and the camera
			 float4 screenUV = tex2D(_MaskTex, input.tex1.xy);//définaition de l'espace UV de la seconde texture afin de la déformer
            if (dist >50.0)
            {
               return _TintColor; 
                  // color near origin
            }
            else
            {
               return screenUV;
                  // color far from origin
            }
         }
 
         ENDCG  
      }
			Pass{
			 Tags{ "LightMode" = "ForwardBase" "RenderType" = "Transparent" }
			 //ZTest Always Cull Off ZWrite Off Blend Off
			 Blend SrcAlpha OneMinusSrcAlpha
			 CGPROGRAM

			#pragma vertex vert  
			#pragma fragment frag 
			#include "UnityCG.cginc"

		 sampler2D _MainTex;
		 sampler2D _MaskTex;
		 float4 _Point;
		 float _DistanceNear;
		 float4 _TintColor;
		 uniform float4 _LightColor0;

		 struct vertexInput {
			 float4 vertex : POSITION;
			 float4 texcoord : TEXCOORD0;
			 float3 normal : NORMAL;
		 };
		 struct vertexOutput {
			 float4 pos : SV_POSITION;
			 float4 position_in_world_space : TEXCOORD0;
			 float4 tex1  : TEXCOORD1;
			 float4 col : COLOR;
		 };

		 vertexOutput vert(vertexInput input)
		 {
			 vertexOutput output;

			 float4x4 modelMatrix = unity_ObjectToWorld;
			 float4x4 modelMatrixInverse = unity_WorldToObject;

			 float3 normalDirection = normalize(
				 mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
			 float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
			 output.tex1 = input.texcoord;
			 float3 diffuseReflection = _LightColor0.rgb * output.tex1
				 * max(0.0, dot(normalDirection, lightDirection)*2);

			 output.col = float4(diffuseReflection, 1.0);
			 output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
			 
			 output.position_in_world_space =
				 mul(unity_ObjectToWorld, input.vertex);
			 // transformation of input.vertex from object 
			 // coordinates to world coordinates;
			 return output;
		 }

		 float4 frag(vertexOutput input) : COLOR
		 {
		 float dist = distance(input.position_in_world_space, _WorldSpaceCameraPos);
		 // computes the distance between the fragment position and the camera
		 float4 t = tex2D(_MainTex, input.tex1.xy);//prends la couleur de la texture spécifiée pour ce fragment
		 if (dist >50.0)
		 {
			 return _TintColor;
			 // color near origin
		 }
		 else
		 {
			 return t;
			 // color far from origin
		 }
		 }

			 ENDCG
		 }
   }
   Fallback "Diffuse"
}