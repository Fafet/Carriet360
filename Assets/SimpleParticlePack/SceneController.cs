using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {
	private bool debugParticles = true;
	private GameObject effects;
	
	// Use this for initialization
	void Start () {
		StartCoroutine(ResetMachine());
		effects = GameObject.Find("Effects");
	}
	
	// Update is called once per frame
	void Update () {
		effects.SetActive(debugParticles);
	}
	
	void OnGUI () {
		if (GUILayout.Button("RESET"))
            SceneManager.LoadScene(0);
		debugParticles = GUILayout.Toggle(debugParticles, "Effects?");
	}
	
	IEnumerator ResetMachine () {
		yield return new WaitForSeconds(60);
        SceneManager.LoadScene(0);
    }
}
